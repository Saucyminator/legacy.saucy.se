<?php
session_start(); // Alltid överst på sidan

// Kolla om inloggad = sessionen satt
if (!isset($_SESSION['sess_user'])){
  header("Location: index.php");
  exit;
}

require("conn.php");	// Databasanslutningen
include("inc/functions.php");

$self = $_SERVER['PHP_SELF'];


/*************************************\
| ******** Tobias Bleckert ********** |
| ********* www.tb-one.se *********** |
| ****** Nyhets script v.2.1 ******** |
| ******** Modifierat script ******** |
| *Spara dessa rader för användning * |
\*************************************/

//Kolla resurser och ob_start()-funktionen för mer information
ob_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><?php require("header_title.html"); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/iframe.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/javascript.js"></script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3406958-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</head>
<body>


<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" valign="top" align="center">
<tr><td width="100%" height="100%" valign="top" align="left" class="admin_main_bg">

<?php

//Kod för att lägga till genom ett forumlär
if(isset($_GET['adddemo'])) {

?>
	<table width='100%' cellspacing='0' cellpadding='0' border='0' valign='top'>
	<tr><td width='100%' height='25' valign='center' align='center' class='historia_rubrik'>
		<span class='admin_gray'>[ </span><a href="admin_demos.php" title="Gå tillbaka" target="main" class='admin_orange'>Lägg till Demo</a><span class='admin_gray'> ]</span><br>
	</td></tr>
	<tr><td width='100%' valign='top' class='admin_bg'>

		<table width='100%' height='100%' cellspacing='0' cellpadding='0' border='0'>
		<tr><td valign='top'>

			<table width='100%' cellspacing='0' cellpadding='0' border='0'>
			<tr><td class='admin_nyheter_add_rubrik' valign='center' height='20'>
				<span class='admin_gray'>Motståndare:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
				<form action='admin_demos.php?adddemo&add' id='add' method='post' name='bb' style="margin-bottom:0;">
				<input name="opponent" type="text" style="width: 200;" autocomplete="on" class="shout_line" />
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Datum: ( <span class='admin_orange2'>Timme</span>:<span class='admin_orange2'>Minut</span> @ <span class='admin_orange2'>Dag</span>/<span class='admin_orange2'>Månad</span>-<span class='admin_orange2'>År</span> )</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="hour" class="login_line">
<option value="00" selected>00</option>
<option value="01">01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
</select>:<select name="minute" class="login_line">
<option value="00" selected>00</option>
<option value="05">05</option>
<option value="10">10</option>
<option value="15">15</option>
<option value="20">20</option>
<option value="25">25</option>
<option value="30">30</option>
<option value="35">35</option>
<option value="40">40</option>
<option value="45">45</option>
<option value="50">50</option>
<option value="55">55</option>
</select> @ <select name="day" class="login_line">
<option value="01" selected>01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
</select>/<select name="month" class="login_line">
<option value="01" selected>01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
</select>-<select name="year" class="login_line">
<option value="08" selected>2008</option>
</select>
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Spel:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="game" class="login_line">
<option value="1" selected>CS:S</option>
<option value="2">COD 4</option>
<option value="3">Q3</option>
<option value="4">WC3</option>
</select>
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Map:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="map" class="login_line">
<option selected>-- Map --</option>
<option value="de_aztec">de_cpl_strike</option>
<option value="de_cbble">de_cbble</option>
<option value="de_contra">de_contra</option>
<option value="de_cpl_fire">de_cpl_fire</option>
<option value="de_cpl_mill">de_cpl_mill</option>
<option value="de_cpl_strike">de_cpl_strike</option>
<option value="de_dust2">de_dust2</option>
<option value="de_dust">de_dust</option>
<option value="de_forge">de_forge</option>
<option value="de_inferno">de_inferno</option>
<option value="de_nuke">de_nuke</option>
<option value="de_phoenix">de_phoenix</option>
<option value="de_prodigy">de_prodigy</option>
<option value="de_russka">de_russka</option>
<option value="de_season">de_season</option>
<option value="de_train">de_train</option>
<option value="de_tuscan">de_tuscan</option>
</select>
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Server: ( ange URL till deras server om det finns, behöver inte till våran )</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="server" class="login_line">
<option value="1" selected>Våran</option>
<option value="2">Deras</option>
</select>
				<input name="server_url" type="text" style="width: 200;" autocomplete="off" class="shout_line" />
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Typ:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="demo" class="login_line">
<option value="1" selected>HLTV</option>
<option value="2">DEMO</option>
</select>
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Demo:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
				<input name="server_url" type="text" style="width: 250;" autocomplete="off" class="shout_line" />
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<script type="text/javascript" src="js/editor.js"></script>

					<a href="#" onclick="insert_text('^^', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/orc%5E%5E.gif" alt="^^" title="Happy Orc" height="22" hspace="2" vspace="2" width="23" border="0"></a>
					<a href="#" onclick="insert_text(':D', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_biggrin.gif" alt=":D" title="Very Happy" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_smile.gif" alt=":)" title="Smile" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':lol:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_lol.gif" alt=":lol:" title="Laughing" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':P', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_razz.gif" alt=":P" title="Razz" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':mrgreen:', true); return false;" style="line-height: 20px;">	<img src="forum/images/smilies/icon_mrgreen.gif" alt=":mrgreen:" title="Mr. Green" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(';)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_wink.gif" alt=";)" title="Wink" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':S', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_confused.gif" alt=":S" title="Confused" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':(', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_sad.gif" alt=":(" title="Sad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':cry:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_cry.gif" alt=":cry:" title="Neutral" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':|', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_neutral.gif" alt=":|" title="Neutral" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':$', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_redface.gif" alt=":$" title="Embarassed" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text('8)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_cool.gif" alt="8)" title="Cool" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':O', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_surprised.gif" alt=":O" title="Surprised" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':eek:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_eek.gif" alt=":eek:" title="Shocked" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':rolleyes:', true); return false;" style="line-height: 20px;"><img src="forum/images/smilies/icon_rolleyes.gif" alt=":rolleyes:" title="Rolling Eyes" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':mad:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_mad.gif" alt=":mad:" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':X', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_evil.gif" alt=":X" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':@', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_twisted.gif" alt=":@" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>

			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>

				<input class="btnbbcode" title="Fet text: [b]text[/b]" style="font-weight: bold;" type="button" name="bold"
				onclick="javascript:addtag('b');" value="b" />

				<input class="btnbbcode" title="Kursiv text: [i]text[/i]" style="font-style: italic;" type="button" name="italic"
				onclick="javascript:addtag('i');" value="i" />

				<input class="btnbbcode" title="Understruken text: [u]text[/u]" style="text-decoration: underline;" type="button" name="inserted"
				onclick="javascript:addtag('u');" value="u" />

				<input class="btnbbcode" title="!!OBS, NAMNET MÅSTE VARA MED!! Citera text: [quote=namnet]text[/quote]" type="button" name="blockquote"
				onclick="javascript:addtag('quote');" value="Quote" />

				<input class="btnbbcode" title="Infoga länk: [url=http://url]Länktext[/url]" style="text-decoration: underline;" type="button" name="link"
				onclick="javascript:addtag('url');" value="URL" />

				<input class="btnbbcode" title="!!ENDAST TILL FILER PÅ HEMSIDAN!! Infoga länk: [url2=http://url]Länktext[/url2]" style="text-decoration: underline;" type="button" name="link2"
				onclick="javascript:addtag('url2');" value="URL(intern)" />

				<input class="btnbbcode" title="Infoga bild: [img]http://bild_url.jpg[/img]" type="button" name="image"
				onclick="javascript:addtag('img');" value="Img" />

			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text' valign='center'>
				<span class='admin_gray'>Beskrivning:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
					<textarea name="text" title="Text" style="width: 100%; height: 75;" class="shout_line"></textarea>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_send' valign='center' align='center'>
				<input class="mainoption" type="submit" title="Lägg till Demon!" value="Lägg till Demon!">&nbsp;
				<input class="liteoption" type="reset" title="Rensa formuläret!" value="Rensa" onClick="return confirm('Är du säker på att du vill rensa?')"/>
				</form>
			</td></tr>
			</table>

		</td></tr>
		</table>

	</td></tr>
	</table>

<?php

$resultat = mysql_query("SELECT * FROM legacy_headz_players WHERE user='{$_SESSION['sess_user']}'") or die (mysql_error());
if($row = mysql_fetch_array($resultat)) {
$profile = $row['user'];
$profile_id = $row['profile_id'];

//Då ?addnews=$id&add står i adressfältet kommer följande kod att laddas
if(isset($_GET['add'])) {


//Lägger in dagens datum i variabeln $datum som ex 1/9-04
//$date = date('H:i @ d/m-y'); // 00:05 @ 05/03-07
/*
//SQL kod för att lägga in i databasen
mysql_query("INSERT INTO headz_demo (profile, profile_id, game, opponent, on, result_headz, result_opponent, date, league, league_url, map, team_headz_1, team_headz_2, team_headz_3, team_headz_4, team_headz_5, team_headz_6, team_opponent_1, team_opponent_2, team_opponent_3, team_opponent_4, team_opponent_5, team_opponent_6, server, server_url, demo, demo_id, text) VALUES (
			'$profile',
			'$profile_id',
			'".$_POST['game']."',
			'".$_POST['opponent']."',
			'".$_POST['on']."',
			'".$_POST['result_headz']."',
			'".$_POST['result_opponent']."',
			'".$_POST['date']."',
			'".$_POST['league']."',
			'".$_POST['league_url']."',
			'".$_POST['map']."',
			'".$_POST['team_headz_1']."',
			'".$_POST['team_headz_2']."',
			'".$_POST['team_headz_3']."',
			'".$_POST['team_headz_4']."',
			'".$_POST['team_headz_5']."',
			'".$_POST['team_headz_6']."',
			'".$_POST['team_opponent_1']."',
			'".$_POST['team_opponent_2']."',
			'".$_POST['team_opponent_3']."',
			'".$_POST['team_opponent_4']."',
			'".$_POST['team_opponent_5']."',
			'".$_POST['team_opponent_6']."',
			'".$_POST['server']."',
			'".$_POST['server_url']."',
			'".$_POST['demo']."',
			'".$_POST['demo_id']."',
			'".$_POST['text']."')") or exit(mysql_error());
*/
   //Skickar dig vidare till det inskrivna efter location:
   header('location: admin_demos.php');
  }
 }
}

//Kod för att ändra en nyhet med vald ID
if(isset($_GET['editdemo'])) {

//Denna kod hämtar ut informationen som vi ska ändra på
$qnyheter = mysql_query("SELECT * FROM legacy_headz_demos WHERE id=".$_GET['editdemo']) or exit(mysql_error());
if($snyheter = mysql_fetch_array($qnyheter)) {
$id = $snyheter['id'];
$match_id = $snyheter['match_id'];
$game = $snyheter['game'];
$profile = $snyheter['profile'];
$profile_id = $snyheter['profile_id'];
$uploaddate = $snyheter['uploaddate'];
$date = $snyheter['date'];
$size = $snyheter['size'];
$size_mb = $snyheter['size_mb'];
$opponent = $snyheter['opponent'];
$map = $snyheter['map'];
$server = $snyheter['server'];
$server_url = $snyheter['server_url'];
$type = $snyheter['type'];
$text = $snyheter['text'];

//Följande kod är ett forumlär med dess respektive information i sig
?>


	<table width='100%' cellspacing='0' cellpadding='0' border='0' valign='top'>
	<tr><td width='100%' height='25' valign='center' align='center' class='historia_rubrik'>
		<span class='admin_gray'>[ </span><a href="admin_demos.php" title="Gå tillbaka" target="main" class='admin_orange'>Ändra Demo</a><span class='admin_gray'> ]</span><br>
	</td></tr>
	<tr><td width='100%' valign='top' class='admin_bg'>

		<table width='100%' height='100%' cellspacing='0' cellpadding='0' border='0'>
		<tr><td valign='top'>

			<table width='100%' cellspacing='0' cellpadding='0' border='0'>
			<tr><td class='admin_nyheter_add_rubrik' valign='center' height='20'>
				<span class='admin_gray'>Motståndare:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
				<form action='admin_demos.php?adddemo&add' id='add' method='post' style="margin-bottom:0;">
				<input name="opponent" type="text" style="width: 200;" autocomplete="on"value='<?php echo $opponent; ?>' class="shout_line" />
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Datum: ( <span class='admin_orange2'>Timme</span>:<span class='admin_orange2'>Minut</span> @ <span class='admin_orange2'>Dag</span>/<span class='admin_orange2'>Månad</span>-<span class='admin_orange2'>År</span> )</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="hour" class="login_line">
<option value="00" selected>00</option>
<option value="01">01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
</select>:<select name="minute" class="login_line">
<option value="00" selected>00</option>
<option value="05">05</option>
<option value="10">10</option>
<option value="15">15</option>
<option value="20">20</option>
<option value="25">25</option>
<option value="30">30</option>
<option value="35">35</option>
<option value="40">40</option>
<option value="45">45</option>
<option value="50">50</option>
<option value="55">55</option>
</select> @ <select name="day" class="login_line">
<option value="01" selected>01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
</select>/<select name="month" class="login_line">
<option value="01" selected>01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
</select>-<select name="year" class="login_line">
<option value="08" selected>2008</option>
</select>
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Spel:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="game" class="login_line">
<option value="1" <?php if($game==1){ echo "selected"; } ?>>CS:S</option>
<option value="2" <?php if($game==2){ echo "selected"; } ?>>COD 4</option>
<option value="3" <?php if($game==3){ echo "selected"; } ?>>Q3</option>
<option value="4" <?php if($game==4){ echo "selected"; } ?>>WC3</option>
</select>
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Map:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="map" class="login_line">
<option value="de_aztec" <?php if($map==de_aztec){ echo "selected"; } ?>>de_aztec</option>
<option value="de_cbble" <?php if($map==de_cbble){ echo "selected"; } ?>>de_cbble</option>
<option value="de_contra" <?php if($map==de_contra){ echo "selected";	} ?>>de_contra</option>
<option value="de_cpl_fire" <?php if($map==de_cpl_fire){ echo "selected"; } ?>>de_cpl_fire</option>
<option value="de_cpl_mill" <?php if($map==de_cpl_mill){ echo "selected"; } ?>>de_cpl_mill</option>
<option value="de_cpl_strike" <?php if($map==de_cpl_strike){ echo "selected"; } ?>>de_cpl_strike</option>
<option value="de_dust2" <?php if($map==de_dust2){ echo "selected"; } ?>>de_dust2</option>
<option value="de_dust" <?php if($map==de_dust){ echo "selected"; } ?>>de_dust</option>
<option value="de_forge" <?php if($map==de_forge){ echo "selected"; } ?>>de_forge</option>
<option value="de_inferno" <?php if($map==de_inferno){ echo "selected"; } ?>>de_inferno</option>
<option value="de_nuke" <?php if($map==de_nuke){ echo "selected"; } ?>>de_nuke</option>
<option value="de_phoenix" <?php if($map==de_phoenix){ echo "selected"; } ?>>de_phoenix</option>
<option value="de_prodigy" <?php if($map==de_prodigy){ echo "selected"; } ?>>de_prodigy</option>
<option value="de_russka" <?php if($map==de_russka){ echo "selected";	} ?>>de_russka</option>
<option value="de_season" <?php if($map==de_season){ echo "selected";	} ?>>de_season</option>
<option value="de_train" <?php if($map==de_train){ echo "selected"; } ?>>de_train</option>
<option value="de_tuscan" <?php if($map==de_tuscan){ echo "selected";	} ?>>de_tuscan</option>
</select>
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Server: ( ange URL till deras server om det finns, behöver inte till våran )</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="server" class="login_line">
<option value="1" <?php if($server==1){ echo "selected"; } ?>>Våran</option>
<option value="2" <?php if($server==2){ echo "selected"; } ?>>Deras</option>
</select>
				<input name="server_url" type="text" style="width: 200;" autocomplete="off" value='<?php echo $server_url; ?>' class="shout_line" />
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Typ:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="demo" class="login_line">
<option value="1" <?php if($type==1){ echo "selected"; } ?>>HLTV</option>
<option value="2" <?php if($type==2){ echo "selected"; } ?>>DEMO</option>
</select>
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Demo:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
				<input name="demo" type="text" style="width: 250;" autocomplete="off" class="shout_line" />
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text' valign='center'>
				<span class='admin_gray'>Beskrivning:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
				<textarea name="text" title="Text" style="width: 100%; height: 75;" class="shout_line"><?php echo $text; ?></textarea>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_send' valign='center' align='center'>
				<input class="mainoption" type="submit" title="Ändra Demon!" value="Ändra Demon!" />
				</form>
			</td></tr>
			</table>

		</td></tr>
		</table>

	</td></tr>
	</table>

<?php

//Då ?editnews=$id&update står i adressfältet kommer följande kod att laddas
if(isset($_GET['update'])) {
/*
	//SQL kod för att uppdatera en vald nyhet
	mysql_query("UPDATE legacy_headz_demos SET
		profile = '$profile',
		profile_id = '$profile_id',
		opponent = '".$_POST['opponent']."',
		game = '".$_POST['game']."',
		map = '".$_POST['map']."',
		server = '".$_POST['server']."',
		server_url = '".$_POST['server_url']."',
		text = '".$_POST['text']."'
		WHERE id=".$_GET['editdemo']) or exit(mysql_error());
*/
	//Skickar dig vidare till det inskrivna efter location:
	header('location: admin_demos.php');
  }
 }
}

//Kod för att ta bort en nyhet
if(isset($_GET['deletedemo'])) {

//Först hämtar vi information om den nyheten vi vill ta bort, vilket vi gjorde med denna kod
$qdelete = mysql_query("SELECT * FROM legacy_headz_demos WHERE id=".$_GET['deletedemo']) or exit(mysql_error());
$sdelete = mysql_fetch_array($qdelete);
$game = $sdelete['game'];

//En bekräftningsfråga om du vill ta bort nyheten
echo"<table width='100%' cellspacing='0' cellpadding='0' border='0' valign='top'>
	<tr>
	<td width='100%' height='25' valign='center' align='center' class='historia_rubrik'>";
echo	"<span class='admin_gray'>[ ";
echo	"Är du säker på att du vill ta bort demon: </span><span class='admin_orange2'>";
echo	$sdelete['opponent'];
echo	"</span> ";

if($game == 1) {
echo	"<img src='images/icons/icon_css.gif' border='0' width='16' height='16' />";
} elseif($game == 2) {
echo	"<img src='images/icons/icon_cod4.gif' border='0' width='16' height='16' />";
} elseif($game == 3) {
echo	"<img src='images/icons/icon_q3.gif' border='0' width='16' height='16' />";
} elseif($game == 4) {
echo	"<img src='images/icons/icon_wc3.gif' border='0' width='16' height='16' />";
}

echo	" <span class='admin_gray'> ? ]<br /><br />";
echo	"<span class='admin_orange'><a href='admin_demos.php?deletedemo=";
echo	$sdelete['id'];
echo	"&delete'>Ja</a><span class='admin_gray'> | </span><span class='admin_orange'><a href='admin_demos.php' target='main'>Nej</a></span>";
echo"</td></tr>
	</table>";


//Då ?deletenews=$id&delete står i adressfältet kommer följande kod att laddas
if(isset($_GET['delete'])) {
/*
    mysql_query("DELETE FROM legacy_headz_demos WHERE id=".$_GET['deletedemo']) or exit(mysql_error());
#	mysql_query("DELETE FROM kommentarer WHERE nid=".$_GET['deletedemo']) or exit(mysql_error());
*/
  //Skickar dig vidare till det inskrivna efter location:
  header('location: admin_demos.php');
 }
}

//Om QUERY_STRING är tom laddas denna text, kolla under resurser och Server variablar för mer information
if(empty($_SERVER['QUERY_STRING'])) {

?>
	<table width='100%' cellspacing='0' cellpadding='0' border='0' valign='top'>
	<tr><td width='100%' height='25' valign='center' align='center' class='historia_rubrik'>
		<span class='medlemmar_text_citat'>[ </span><span class='medlemmar_text_citat_info'>Demos</span><span class='medlemmar_text_citat'> ]</span>
	</td></tr>
	<tr><td width='100%' valign='top' class='admin_bg'>

		<table width='100%' height='100%' cellspacing='0' cellpadding='0' border='0'>
		<tr><td valign='top'>

			<table width='100%' cellspacing='0' cellpadding='0' border='0'>
			<tr><td class='admin_nyheter_add' colspan='4' valign='center' align='center' height='20'>
<?php
echo "<span class='admin_gray'>[ </span>";
echo "<a href='admin_demos.php?adddemo' title='Lägg till en ny Demo' target='main' class='admin_orange'>Lägg till Demo</a>";
echo "<span class='admin_gray'> ]</span><br>";
?>
			</td></tr>

<?php


$color = 1;

//Här hämtar vi ut all information om alla nyheter och loopar ut dem, dvs skriver ut alla nyheter efter varandra
$qnyheter = mysql_query("SELECT * FROM legacy_headz_demos WHERE profile = '{$_SESSION['sess_user']}' OR '{$_SESSION['sess_user']}' = 'Sås' ORDER BY id DESC") or die (mysql_error());
while($snyheter = mysql_fetch_array($qnyheter)) {
$id = $snyheter['id'];
$match_id = $snyheter['match_id'];
$game = $snyheter['game'];
$profile = $snyheter['profile'];
$profile_id = $snyheter['profile_id'];
$uploaddate = $snyheter['uploaddate'];
$date = $snyheter['date'];
$size = $snyheter['size'];
$opponent = $snyheter['opponent'];
$team_headz_1 = $snyheter['team_headz_1'];
$map = $snyheter['map'];
$server = $snyheter['server'];
$server_url = $snyheter['server_url'];
$type = $snyheter['type'];
$demo = $snyheter['demo'];
$text = $snyheter['text'];
$version = $snyheter['version'];



if($color == 1){
echo		"<tr><td height='19' class='admin_nyheter' valign='center'>

				<table height='19' cellspacing='0' cellpadding='0' border='0'>
				<tr><td valign='center'>";

if($game == 1) {
echo				"<img src='images/icons/icon_css.gif' border='0' width='16' height='16' />";
} elseif($game == 2) {
echo				"<img src='images/icons/icon_cod4.gif' border='0' width='16' height='16' />";
} elseif($game == 3) {
echo				"<img src='images/icons/icon_q3.gif' border='0' width='16' height='16' />";
} elseif($game == 4) {
echo				"<img src='images/icons/icon_wc3.gif' border='0' width='16' height='16' />";
}
echo			"</td><td>
					<span class='admin_gray'>&nbsp;&nbsp;&nbsp;[</span>";

if($game == 1) {
echo					"<span class='text_medlemmar'><a href='demo_info.php?demo=$id' title='Ändra Demo #$id. Map: $map. Datum: $date.' target='main'>$opponent</a></span>";
} elseif($game == 2) {
echo					"<span class='text_medlemmar'><a href='demo_info.php?demo=$id' title='Ändra Demo #$id. Typ: $type. Datum: $date.' target='main'>$opponent</a></span>";
} elseif($game == 3) {
echo					"<span class='text_medlemmar'><a href='demo_info.php?demo=$id' title='Ändra Demo #$id. Typ: $type. Datum: $date.' target='main'>$opponent</a></span>";
} elseif($game == 4) {
echo					"<span class='text_medlemmar'><a href='demo_info.php?demo=$id' title='Ändra Demo #$id. Map: $map. Version: $version. Datum: $date.' target='main'>$team_headz_1 vs $opponent</a></span>";
}

echo				"<span class='admin_gray'>]</span>
				</td><td valign='center'>";

if($game == 1) {
	if($demo == 1) {
	echo			"<span class='matcher_text'>&nbsp;&nbsp;[ </span><span class='text_medlemmar_type'>HLTV</span><span class='matcher_text'> ]</span>";
	}
} elseif($game == 2) {
	if (!empty($type)) {
    echo			"<span class='matcher_text'>&nbsp;&nbsp;[ </span><span class='text_medlemmar_type'>$type</span><span class='matcher_text'> ]</span>";
	}
} elseif($game == 3) {
	if (!empty($type)) {
    echo			"<span class='matcher_text'>&nbsp;&nbsp;[ </span><span class='text_medlemmar_type'>$type</span><span class='matcher_text'> ]</span>";
	}
}

echo				"<span class='admin_gray'>&nbsp;&nbsp;&nbsp;&nbsp;[</span>";
echo				"<span class='admin_orange2'>$size</span>";
echo				"<span class='admin_gray'>] kb</span>
				</td></tr>
				</table>

			</td><td width='160' height='20' class='admin_nyheter_date' valign='center' align='center'>
				<span class='admin_gray'>[ </span>";
echo			"<span class='admin_tid'>$uploaddate</span>";
echo			"<span class='admin_gray'> ]</span><br>
			</td><td width='100' height='20' class='admin_nyheter_edit' valign='center' align='center'>";
echo			"<span class='admin_gray'>[ </span>";
echo			"<a href='medlemmar_info.php?medlem=$profile_id' class='admin_orange'>$profile</a>";
echo			"<span class='admin_gray'> ]</span><br>";
echo		"</td><td width='45' height='20' class='admin_nyheter_remove' valign='center' align='center'>
				<a href='admin_demos.php?deletedemo=$id' title='Ta bort Demo #$id'><img src='images/icons/icon_admin_remove.gif' border='0' width='18' height='16' alt=''></a><br>
			</td></tr>";

// Set $color==2, for switching to other color
$color = 2;

} else {

echo		"<tr><td height='19' class='admin_nyheter_dark' valign='center'>

				<table height='19' cellspacing='0' cellpadding='0' border='0'>
				<tr><td valign='center'>";

if($game == 1) {
echo				"<img src='images/icons/icon_css.gif' border='0' width='16' height='16' />";
} elseif($game == 2) {
echo				"<img src='images/icons/icon_cod4.gif' border='0' width='16' height='16' />";
} elseif($game == 3) {
echo				"<img src='images/icons/icon_q3.gif' border='0' width='16' height='16' />";
} elseif($game == 4) {
echo				"<img src='images/icons/icon_wc3.gif' border='0' width='16' height='16' />";
}

echo			"</td><td>";
echo				"<span class='admin_gray'>&nbsp;&nbsp;&nbsp;[</span>";

if($game == 1) {
echo					"<span class='text_medlemmar'><a href='demo_info.php?demo=$id' title='Ändra Demo #$id. Map: $map. Datum: $date.' target='main'>$opponent</a></span>";
} elseif($game == 2) {
echo					"<span class='text_medlemmar'><a href='demo_info.php?demo=$id' title='Ändra Demo #$id. Typ: $type. Datum: $date.' target='main'>$opponent</a></span>";
} elseif($game == 3) {
echo					"<span class='text_medlemmar'><a href='demo_info.php?demo=$id' title='Ändra Demo #$id. Typ: $type. Datum: $date.' target='main'>$opponent</a></span>";
} elseif($game == 4) {
echo					"<span class='text_medlemmar'><a href='demo_info.php?demo=$id' title='Ändra Demo #$id. Map: $map. Version: $version. Datum: $date.' target='main'>$team_headz_1 vs $opponent</a></span>";
}

echo				"<span class='admin_gray'>]</span>
				</td><td valign='center'>";

if($game == 1) {
	if($demo == 1) {
	echo			"<span class='matcher_text'>&nbsp;&nbsp;[ </span><span class='text_medlemmar_type'>HLTV</span><span class='matcher_text'> ]</span>";
	}
} elseif($game == 2) {
	if (!empty($type)) {
    echo			"<span class='matcher_text'>&nbsp;&nbsp;[ </span><span class='text_medlemmar_type'>$type</span><span class='matcher_text'> ]</span>";
	}
} elseif($game == 3) {
	if (!empty($type)) {
    echo			"<span class='matcher_text'>&nbsp;&nbsp;[ </span><span class='text_medlemmar_type'>$type</span><span class='matcher_text'> ]</span>";
	}
}

echo				"<span class='admin_gray'>&nbsp;&nbsp;&nbsp;&nbsp;[</span>";
echo				"<span class='admin_orange2'>$size</span>";
echo				"<span class='admin_gray'>] kb</span>
				</td></tr>
				</table>

			</td><td width='160' height='20' class='admin_nyheter_date_dark' valign='center' align='center'>
				<span class='admin_gray'>[ </span>";
echo			"<span class='admin_tid'>$uploaddate</span>";
echo			"<span class='admin_gray'> ]</span><br>
			</td><td width='100' height='20' class='admin_nyheter_edit_dark' valign='center' align='center'>";
echo			"<span class='admin_gray'><a href='medlemmar_info.php?medlem=$profile_id'>[ </span>";
echo			"<span class='admin_orange'>$profile</span>";
echo			"<span class='admin_gray'> ]</a></span><br>";
echo		"</td><td width='45' height='20' class='admin_nyheter_remove_dark' valign='center' align='center'>
				<a href='admin_demos.php?deletedemo=$id' title='Ta bort Demo #$id'><img src='images/icons/icon_admin_remove.gif' border='0' width='18' height='16' alt=''></a><br>
			</td></tr>";

// Set $color back to 1
$color = 1;

  }
 }
}
echo		"</table>";

?>
		</td></tr>
		</table>

	</td></tr>
	</table>

</td></tr>
</table>

</body>
</html>

<?php
//Kolla under resurser och ob_end_flush()-funktionen för mer information
ob_end_flush();
?>
