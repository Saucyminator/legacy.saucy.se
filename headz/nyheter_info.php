<?php

require("conn.php");	// Databasanslutningen
include("inc/functions.php");

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/iframe.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3406958-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>

</head>
<body>


<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
<tr><td width="100%" height="100%" valign="top" align="left" class="main_bg">

<?php

if (!isset($_GET['nyhet'])) {
  $nyhet = 0;
}
else
{
  $nyhet = intval ( $_GET['nyhet'] );
  /* Använd intval() för att undvika s.k. SQL INJECTIONS,
     dvs. att folk kan typ radera din databas... */
}

$sql = "SELECT * FROM legacy_headz_nyheter WHERE id = :id LIMIT 1";
$stmt = $conn->prepare($sql);
$stmt->bindParam(':id', $nyhet, PDO::PARAM_INT);
$stmt->execute();
if ($row = $stmt->fetch()) {
$id = $row['id'];
$title = $row['title'];
$profile = $row['profile'];
$profile_id = $row['profile_id'];
$profile_img = $row['profile_img'];
$date = $row['date'];
$text = $row['text'];


echo "<table width='100%' cellspacing='0' cellpadding='0' border='0'>
	<tr><td>

		<table width='100%' cellspacing='0' cellpadding='0' border='0'>
		<tr><td height='25' valign='middle' class='hem_rubrik'>";
echo 		"<span class='rubrik'>";
echo			bbkod($title);
echo 		"</span>";
echo	"</td><td width='134' height='25' valign='middle' align='right' class='hem_tid'>
			<span class='tid_info'>$date</span>
		</td></tr>
		<tr><td colspan='2'>

			<table width='100%' cellspacing='0' cellpadding='0' border='0'>
			<tr><td valign='top' class='hem_text_bg' colspan='2'>";
echo 			"<span class='hem_text'>";
echo				bbkod($text);
echo 			"</span>";
echo		"</td>
			<td width='92' align='center' valign='top' class='hem_bild_bg'>

				<table width='80' cellspacing='0' cellpadding='0' border='0'>
				<tr><td width='80' height='80' class='hem_bild' valign='middle' align='center'>
					<span class='text'><a href='medlemmar_info.php?medlem=$profile_id&css' title='Se Profil: $profile' target='main'>";

if ($profile_id == 1) {
echo					"<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($profile_id == 2) {
echo					"<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($profile_id == 3) {
echo					"<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($profile_id == 4) {
echo					"<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($profile_id == 5) {
echo					"<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($profile_id == 6) {
echo					"<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($profile_id == 7) {
echo					"<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($profile_id == 8) {
echo					"<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($profile_id == 9) {
echo					"<img src='images/profile/9.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($profile_id == 10) {
echo					"<img src='images/profile/10.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($profile_id == 11) {
echo					"<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($profile_id == 12) {
echo					"<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($profile_id == 13) {
echo					"<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($profile_id == 14) {
echo					"<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($profile_id == 15) {
echo					"<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($profile_id == 16) {
echo					"<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($profile_id == 17) {
echo					"<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} else {
echo					"<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
}

echo				"</a></span>
				</td></tr>
				</table>

			</td></tr>
			<tr><td valign='middle' class='nyheter_info_nyhetsid'>
				<span class='skriven'>Nyhets ID: </span>
					<span class='nyhetsid'>$id</span>
			</td><td valign='middle' align='right' class='nyheter_info_skriven'>
				<span class='skriven'>Skriven av:</span>
			</td><td width='92' valign='middle' align='center' class='hem_bild_profil'>
				<span class='profil_text'><a href='medlemmar_info.php?medlem=$profile_id&css' title='Se Profil: $profile' target='main'>$profile</a></span>
			</td></tr>
			</table>

		</td></tr>
		</table>

	</td></tr>
	</table>";
}
?>


</td></tr>
</table>


</body>
</html>
