<?php

require("conn.php");	// Databasanslutningen

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/iframe.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3406958-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>

</head>
<body>


<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
<tr><td width="100%" height="100%" valign="top" align="left" class="main_bg">

<?php

//Kod för att lägga till genom ett forumlär
if(isset($_GET['demos'])) {

echo"<table width='100%' cellspacing='0' cellpadding='0' border='0'>
	<tr><td height='25' valign='middle' align='center' class='demos_top_bg'>
		<span class='medlemmar_text_citat_info'>Demos</span>
	</td></tr>
	<tr><td valign='top' align='center' colspan='3'>

		<table width='100%' cellspacing='0' cellpadding='0' border='0' class='matcher_bg'>
		<tr><td valign='top' align='center'>";

// Define $color=1
$color = "1";

$sql = "SELECT * FROM legacy_headz_demos ORDER BY id DESC";
$stmt = $conn->prepare($sql);
$stmt->execute();
while ($row = $stmt->fetch()) {
$id = $row['id'];
$game = $row['game'];
$opponent = $row['opponent'];
$profile = $row['profile'];
$profile_id = $row['profile_id'];
$map = $row['map'];
$date = $row['date'];
$size = $row['size'];
$type = $row['type'];
$demo = $row['demo'];
$team_headz_1 = $row['team_headz_1'];
$version = $row['version'];

// If $color==1 table row color = DARK
if($color == 1){

echo		"<table width='100%' cellspacing='0' cellpadding='0' border='0'>
			<tr><td width='32' height='20' valign='middle' align='center' class='filer_demos_icon'>";

if($game == 1) {
echo			"<img src='images/icons/icon_css.gif' border='0' width='16' height='16' alt=''>";
} elseif($game == 2) {
echo			"<img src='images/icons/icon_cod4.gif' border='0' width='16' height='16' alt=''>";
} elseif($game == 3) {
echo			"<img src='images/icons/icon_q3.gif' border='0' width='16' height='16' alt=''>";
} elseif($game == 4) {
echo			"<img src='images/icons/icon_wc3.gif' border='0' width='16' height='16' alt=''>";
}

echo		"</td><td height='20' valign='middle' class='filer_demos_detaljer'>

				<table cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$profile_id&css' title='Se Profil: $profile' target='main'>$profile</a></span>";
echo				"<span class='medlemmar_text'>&nbsp;&nbsp;[</span>";

if($game == 1) {
echo					"<span class='text_medlemmar'><a href='demo_info.php?demo=$id' title='Se Demo #$id. Map: $map. Datum: $date.' target='main'>$opponent</a></span>";
} elseif($game == 2) {
echo					"<span class='text_medlemmar'><a href='demo_info.php?demo=$id' title='Se Demo #$id. Typ: $type. Datum: $date.' target='main'>$opponent</a></span>";
} elseif($game == 3) {
echo					"<span class='text_medlemmar'><a href='demo_info.php?demo=$id' title='Se Demo #$id. Typ: $type. Datum: $date.' target='main'>$opponent</a></span>";
} elseif($game == 4) {
echo					"<span class='text_medlemmar'><a href='demo_info.php?demo=$id' title='Se Demo #$id. Map: $map. Version: $version. Datum: $date.' target='main'>$team_headz_1 vs $opponent</a></span>";
}

echo				"<span class='medlemmar_text'>] </span>
				</td><td>";

if($game == 1) {
	if($demo == 1) {
	echo			"<span class='matcher_text'>&nbsp;&nbsp;[ </span><span class='text_medlemmar_type'>HLTV</span><span class='matcher_text'> ]</span>";
	}
} elseif($game == 2) {
	if (!empty($type)) {
    echo			"<span class='matcher_text'>&nbsp;&nbsp;[ </span><span class='text_medlemmar_type'>$type</span><span class='matcher_text'> ]</span>";
	}
} elseif($game == 3) {
	if (!empty($type)) {
    echo			"<span class='matcher_text'>&nbsp;&nbsp;[ </span><span class='text_medlemmar_type'>$type</span><span class='matcher_text'> ]</span>";
	}
} elseif($game == 4) {
	if (!empty($version)) {
    echo			"<span class='matcher_text'>&nbsp;&nbsp;[ </span><span class='text_medlemmar_type'>$version</span><span class='matcher_text'> ]</span>";
	}
}

echo			"</td><td width='6'></td><td>
					<a href='files/demos/$id.rar' title='Ladda ner Demo #$id'><img src='images/icons/icon_download.gif' border='0' width='14' height='15' alt=''></a>
				</td></tr>
				</table>

			</td><td width='110' height='20' valign='middle' align='right' class='filer_demos_tid'>
				<span class='tid_info'>$date</span>
			</td><td width='88' height='20' valign='middle' align='right' class='filer_demos_storlek'>
				<span class='medlemmar_text'>[</span><span class='medlemmar_text_info'>$size</span><span class='medlemmar_text'>]&nbsp;&nbsp;kb</span>
			</td><td width='20' height='20' valign='middle' align='center' class='filer_demos_icon2'>";

if($game == 1) {
echo			"<a href='demo_info.php?demo=$id' title='Se Demo #$id. Map: $map. Datum: $date.' target='main'>";
} elseif($game == 2) {
echo			"<a href='demo_info.php?demo=$id' title='Se Demo #$id. Typ: $type. Datum: $date.' target='main'>";
} elseif($game == 3) {
echo			"<a href='demo_info.php?demo=$id' title='Se Demo #$id. Typ: $type. Datum: $date.' target='main'>";
} elseif($game == 4) {
echo			"<a href='demo_info.php?demo=$id' title='Se Demo #$id. Map: $map. Version: $version. Datum: $date.' target='main'>";
}

echo			"<img src='images/icons/icon_folder.gif' border='0' width='14' height='15' alt=''></a>
			</td></tr>
			</table>";

$color = "2";	// Set $color==2, for switching to other color

} else {	// When $color not equal 1, use this table row color

echo		"<table width='100%' cellspacing='0' cellpadding='0' border='0'>
			<tr><td width='32' height='20' valign='middle' align='center' class='filer_demos_icon_dark'>";

if($game == 1) {
echo			"<img src='images/icons/icon_css.gif' border='0' width='16' height='16' alt=''>";
} elseif($game == 2) {
echo			"<img src='images/icons/icon_cod4.gif' border='0' width='16' height='16' alt=''>";
} elseif($game == 3) {
echo			"<img src='images/icons/icon_q3.gif' border='0' width='16' height='16' alt=''>";
} elseif($game == 4) {
echo			"<img src='images/icons/icon_wc3.gif' border='0' width='16' height='16' alt=''>";
}

echo		"</td><td height='20' valign='middle' class='filer_demos_detaljer_dark'>

				<table cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$profile_id&css' title='Se Profil: $profile' target='main'>$profile</a></span>";
echo				"<span class='medlemmar_text'>&nbsp;&nbsp;[</span>";

if($game == 1) {
echo					"<span class='text_medlemmar'><a href='demo_info.php?demo=$id' title='Se Demo #$id. Map: $map. Datum: $date.' target='main'>$opponent</a></span>";
} elseif($game == 2) {
echo					"<span class='text_medlemmar'><a href='demo_info.php?demo=$id' title='Se Demo #$id. Typ: $type. Datum: $date.' target='main'>$opponent</a></span>";
} elseif($game == 3) {
echo					"<span class='text_medlemmar'><a href='demo_info.php?demo=$id' title='Se Demo #$id. Typ: $type. Datum: $date.' target='main'>$opponent</a></span>";
} elseif($game == 4) {
echo					"<span class='text_medlemmar'><a href='demo_info.php?demo=$id' title='Se Demo #$id. Map: $map. Version: $version. Datum: $date.' target='main'>$team_headz_1 vs $opponent</a></span>";
}

echo				"<span class='medlemmar_text'>] </span>
				</td><td>";

if($game == 1) {
	if($demo == 1) {
	echo			"<span class='matcher_text'>&nbsp;&nbsp;[ </span><span class='text_medlemmar_type'>HLTV</span><span class='matcher_text'> ]</span>";
	}
} elseif($game == 2) {
	if (!empty($type)) {
    echo			"<span class='matcher_text'>&nbsp;&nbsp;[ </span><span class='text_medlemmar_type'>$type</span><span class='matcher_text'> ]</span>";
	}
} elseif($game == 3) {
	if (!empty($type)) {
    echo			"<span class='matcher_text'>&nbsp;&nbsp;[ </span><span class='text_medlemmar_type'>$type</span><span class='matcher_text'> ]</span>";
	}
} elseif($game == 4) {
	if (!empty($version)) {
    echo			"<span class='matcher_text'>&nbsp;&nbsp;[ </span><span class='text_medlemmar_type'>$version</span><span class='matcher_text'> ]</span>";
	}
}

echo			"</td><td width='6'></td><td>
					<a href='files/demos/$id.rar' title='Ladda ner Demo #$id'><img src='images/icons/icon_download.gif' border='0' width='14' height='15' alt=''></a>
				</td></tr>
				</table>

			</td><td width='110' height='20' valign='middle' align='right' class='filer_demos_tid_dark'>
				<span class='tid_info'>$date</span>
			</td><td width='88' height='20' valign='middle' align='right' class='filer_demos_storlek_dark'>
				<span class='medlemmar_text'>[</span><span class='medlemmar_text_info'>$size</span><span class='medlemmar_text'>]&nbsp;&nbsp;kb</span>
			</td><td width='20' height='20' valign='middle' align='center' class='filer_demos_icon2'>";

if($game == 1) {
echo			"<a href='demo_info.php?demo=$id' title='Se Demo #$id. Map: $map. Datum: $date.' target='main'>";
} elseif($game == 2) {
echo			"<a href='demo_info.php?demo=$id' title='Se Demo #$id. Typ: $type. Datum: $date.' target='main'>";
} elseif($game == 3) {
echo			"<a href='demo_info.php?demo=$id' title='Se Demo #$id. Typ: $type. Datum: $date.' target='main'>";
} elseif($game == 4) {
echo			"<a href='demo_info.php?demo=$id' title='Se Demo #$id. Map: $map. Version: $version. Datum: $date.' target='main'>";
}

echo			"<img src='images/icons/icon_folder.gif' border='0' width='14' height='15' alt=''></a>
			</td></tr>
			</table>";

// Set $color back to 1
$color = "1";

  }
 }

echo	"</td></tr>
		</table>

	</td></tr>
	</table>";

}


//Kod för att lägga till genom ett forumlär
if(isset($_GET['configs'])) {

echo"<table width='100%' cellspacing='0' cellpadding='0' border='0'>
	<tr><td height='25' valign='middle' align='center' class='demos_top_bg'>
		<span class='medlemmar_text_citat_info'>Configs</span>
	</td></tr>
	<tr><td valign='top' align='center' colspan='3'>

		<table width='100%' cellspacing='0' cellpadding='0' border='0' class='matcher_bg'>
		<tr><td valign='top' align='center'>";

// Define $color=1
$color = "1";

$sql = "SELECT * FROM legacy_headz_configs ORDER BY id DESC";
$stmt = $conn->prepare($sql);
$stmt->execute();
while ($row = $stmt->fetch()) {
$id = $row['id'];
$game = $row['game'];
$profile = $row['profile'];
$profile_id = $row['profile_id'];
$date = $row['date'];
$size = $row['size'];

// If $color==1 table row color = DARK
if($color == 1){
echo		"<table width='100%' cellspacing='0' cellpadding='0' border='0'>
			<tr><td width='32' height='20' valign='middle' align='center' class='filer_demos_icon'>";

if($game == 1) {
echo			"<img src='images/icons/icon_css.gif' border='0' width='16' height='16' alt=''>";
} elseif($game == 2) {
echo			"<img src='images/icons/icon_cod4.gif' border='0' width='16' height='16' alt=''>";
} elseif($game == 3) {
echo			"<img src='images/icons/icon_q3.gif' border='0' width='16' height='16' alt=''>";
} elseif($game == 4) {
echo			"<img src='images/icons/icon_wc3.gif' border='0' width='16' height='16' alt=''>";
}

echo		"</td><td height='20' valign='middle' class='filer_demos_detaljer'>

				<table cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$profile_id&css' title='Se Profil: $profile' target='main'>$profile</a></span><span class='medlemmar_text'>&nbsp;&nbsp;[</span><span class='text_medlemmar'><a href='config_info.php?config=$id' title='Se Config #$id' target='main'>Config</a></span><span class='medlemmar_text'>] </span>
				</td><td width='6'>
				</td><td>
					<a href='files/configs/$id.rar' title='Ladda ner Config #$id'><img src='images/icons/icon_download.gif' border='0' width='14' height='15' alt=''></a>
				</td></tr>
				</table>

			</td><td width='110' height='20' valign='middle' align='right' class='filer_demos_tid'>
				<span class='tid_info'>$date</span>
			</td><td width='88' height='20' valign='middle' align='right' class='filer_demos_storlek'>
				<span class='medlemmar_text'>[</span><span class='medlemmar_text_info'>$size</span><span class='medlemmar_text'>]&nbsp;&nbsp;kb</span>
			</td><td width='20' height='20' valign='middle' align='center' class='filer_demos_icon2'>
				<a href='config_info.php?config=$id' title='Se Config #$id' target='main'><img src='images/icons/icon_folder.gif' border='0' width='14' height='15' alt=''></a>
			</td></tr>
			</table>";

$color = "2";	// Set $color==2, for switching to other color

} else {	// When $color not equal 1, use this table row color

echo		"<table width='100%' cellspacing='0' cellpadding='0' border='0'>
			<tr><td width='32' height='20' valign='middle' align='center' class='filer_demos_icon_dark'>";

if($game == 1) {
echo			"<img src='images/icons/icon_css.gif' border='0' width='16' height='16' alt=''>";
} elseif($game == 2) {
echo			"<img src='images/icons/icon_cod4.gif' border='0' width='16' height='16' alt=''>";
} elseif($game == 3) {
echo			"<img src='images/icons/icon_q3.gif' border='0' width='16' height='16' alt=''>";
} elseif($game == 4) {
echo			"<img src='images/icons/icon_wc3.gif' border='0' width='16' height='16' alt=''>";
}

echo		"</td><td height='20' valign='middle' class='filer_demos_detaljer_dark'>

				<table cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$profile_id&css' title='Se Profil: $profile' target='main'>$profile</a></span><span class='medlemmar_text'>&nbsp;&nbsp;[</span><span class='text_medlemmar'><a href='config_info.php?config=$id' title='Se Config #$id' target='main'>Config</a></span><span class='medlemmar_text'>] </span>
				</td><td width='6'>
				</td><td>
					<a href='files/configs/$id.rar' title='Ladda ner Config #$id'><img src='images/icons/icon_download.gif' border='0' width='14' height='15' alt=''></a>
				</td></tr>
				</table>

			</td><td width='110' height='20' valign='middle' align='right' class='filer_demos_tid_dark'>
				<span class='tid_info'>$date</span>
			</td><td width='88' height='20' valign='middle' align='right' class='filer_demos_storlek_dark'>
				<span class='medlemmar_text'>[</span><span class='medlemmar_text_info'>$size</span><span class='medlemmar_text'>]&nbsp;&nbsp;kb</span>
			</td><td width='20' height='20' valign='middle' align='center' class='filer_demos_icon2_dark'>
				<a href='config_info.php?config=$id' title='Se Config #$id' target='main'><img src='images/icons/icon_folder.gif' border='0' width='14' height='15' alt=''></a>
			</td></tr>
			</table>";

// Set $color back to 1
$color = "1";

  }
 }

echo	"</td></tr>
		</table>

	</td></tr>
	</table>";

}

//Om QUERY_STRING är tom laddas denna text, kolla under resurser och Server variablar för mer information
if(empty($_SERVER['QUERY_STRING'])) {
?>

	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr><td width="20%">
	</td><td height="100" colspan="3" valign="middle" align="center">

		<table cellspacing="0" cellpadding="0" border="0">
		<tr><td width="300" height="40" valign="middle" align="center" class="logo_bg2">
			<img src="images/logo_filer.gif" width="117" height="38" border="0" vspace="0" alt="">
		</td></tr>
		</table>

	</td><td width="20%">
	</td></tr>
	<tr><td width="20%">
	</td><td>

		<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
		<tr><td valign="top" align="center">
			<a href='filer.php?demos' title='Se Demos!' target='main'><img src='images/demos.gif' border='0' width='150' height='150' alt=''></a><br>
		</td></tr>
		</table>

	</td><td width="10">
	</td><td>

		<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
		<tr><td valign="top" align="center">
			<a href='filer.php?configs' title='Se Configs!' target='main'><img src='images/cfgs.gif' border='0' width='150' height='150' alt=''></a><br>
		</td></tr>
		</table>

	</td><td width="20%">
	</td></tr>
	</table>

<?php
}
?>

</td></tr>
</table>


</body>
</html>
