﻿<?php

// Mysql config
$host     = 'localhost'; //mysql host
$database = 'database'; //databas namn
$username = 'username'; //database login
$password = 'password'; //databas lösenord

// Main connection
$conn = new PDO(
  "mysql:host=$host;dbname=$database",
  $username,
  $password,
  [
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::MYSQL_ATTR_FOUND_ROWS   => true,
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
  ]
);

// En funktion att användas när magic_quotes_gpc inte är satt. För att förhindra SQL-injections, eller i lidrigare fall MySQl-fel.
function db_escape ($post)
{
   if (is_string($post)) {
     if (get_magic_quotes_gpc()) {
        $post = stripslashes($post);
     }
     return $post;
   }

   foreach ($post as $key => $val) {
      $post[$key] = db_escape($val);
   }

   return $post;
}


/*
   Se till att det inte finns några dolda tecken, typ radbyte
   eller mellanslag, efter den avslutande PHP-taggen !!!
*/
