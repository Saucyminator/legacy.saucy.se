# HeadZ

## Changes to make the website display on a PHP v7 configuration
Some updates are made to make the website usable/visible on PHP version 7. I've tried to change as little as possible because I still want it to show how my code looked back then, and I don't want to spend time updating something that's (for me) completely useless:

/conn.php:
- Change mysql connection to PDO.
- Remove mysql_real_escape_string().

/filer.php, /hem.php, /matcher_info.php, /matcher.php, /medlemmar_info.php, /medlemmar.php, /nyheter_info.php, /nyheter.php:
- Change mysql functions to PDO.
