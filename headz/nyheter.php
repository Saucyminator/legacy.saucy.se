<?php

require("conn.php");	// Databasanslutningen

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/iframe.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3406958-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>

</head>
<body>


<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
<tr><td width="100%" height="100%" valign="top" align="left" class="main_bg">

	<table width='100%' cellspacing='0' cellpadding='0' border='0'>
	<tr><td valign='top'>

		<table width='100%' cellspacing='0' cellpadding='0' border='0'>
		<tr><td height='25' valign='middle' align='center' class='nyheter_announce'>
			<span class='rubrik_text'>.: Nyhets arkiv :.</span>
		</td></tr>
		</table>

	</td></tr>
	<tr><td valign='top'>

		<table width='100%' cellspacing='0' cellpadding='0' border='0' class='matcher_bg'>
		<tr><td height='20' valign='middle' align='center' class='matcher_opponent'>

<?php

// Nu bestämmer vi antal per sida och kollar vi upp totala antalet
$limit = 20; // Antal per sida

$sql = "SELECT count(*) as count FROM legacy_headz_nyheter";
$stmt = $conn->prepare($sql);
$stmt->execute();
$numrows = $stmt->fetchColumn();

// Sedan kollar vi om startvariabeln är satt
if (!isset($_GET['start']) || $_GET['start'] == "")
  $start = 0;
else
  $start = $_GET['start'];

// Då räknar vi ut hur många sidor det blev
$pages = intval($numrows/$limit);
if ($numrows%$limit)
  $pages++;

// Hämta länk till förstasidan och föregående sida
if ($start > 0) {
  $numlink = '<span class="matcher"><a href="?start=0">««</a> </span>';
  $numlink .= '<span class="matcher"><a href="?start='.($start - $limit).'">«</a> </span>';
} else {
  $numlink = '<span class="matcher">«« </span>';
  $numlink .= '<span class="matcher">« </span>';
}

// Hämta sidonummer
for ($i = 1; $i <= $pages; $i++) {
  $newoffset = $limit*($i-1);
  if ($start == $newoffset)
    $numlink .= '['.$i.'] ';
  else
    $numlink .= '<span class="matcher"><a href="?start='.$newoffset.'">'.$i.'</a> </span>';
}

// Hämta länk till nästa sida
if ($numrows > ($start + $limit))
  $numlink .= '<span class="matcher"><a href="?start='.($start + $limit).'">»</a> </span>';
else
  $numlink .= '<span class="matcher">» </span>';

// Hämta sista sidan
if ($start != $newoffset)
  $numlink .= '<span class="matcher"><a href="?start='.$newoffset.'">»»</a> </span>';
else
  $numlink .= '<span class="matcher">»»</span>';

// Skriv ut sidorna
echo $numlink;

?>

		</td></tr>
		</table>

	</td></tr>
	</table>

	<table width='100%' cellspacing='0' cellpadding='0' border='0'>
	<tr><td height='5'>
	</td></tr>
	</table>

<?php


$sql = "SELECT * FROM legacy_headz_nyheter ORDER BY id ASC LIMIT $start, $limit";
$stmt = $conn->prepare($sql);
$stmt->execute();
while ($row = $stmt->fetch()) {
$id = $row['id'];
$title = $row['title'];
$profile = $row['profile'];
$profile_id = $row['profile_id'];
$date = $row['date'];


//	$date = date("H:i @ d/m-y");	//	00:05 @ 05/03-07


echo"<table width='100%' cellspacing='0' cellpadding='0' border='0'>
	<tr><td valign='top'>

		<table width='100%' cellspacing='0' cellpadding='0' border='0'>
		<tr><td height='25' valign='middle' class='hem_rubrik'>
			<span class='rubrik_text'><a href='nyheter_info.php?nyhet=$id' title='Läs Nyhet #$id' target='main'>$title</a></span>
		</td><td width='150' height='25' valign='middle' align='right' class='nyheter_skriven'>
			<span class='skriven'>Skriven av: </span>
				<span class='profil_text'><a href='medlemmar_info.php?medlem=$profile_id&css' title='Se Profil: $profile' target='main'>$profile</a></span>
		</td><td width='120' height='25' valign='middle' align='center' class='nyheter_tid'>
			<span class='tid_info'>$date</span>
		</td></tr>
		</table>

	</td></tr>
	</table>

	<table width='100%' cellspacing='0' cellpadding='0' border='0'>
	<tr><td height='5'>
	</td></tr>
	</table>";

}

?>

</td></tr>
</table>


</body>
</html>
