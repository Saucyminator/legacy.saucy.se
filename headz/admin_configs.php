<?php
session_start(); // Alltid överst på sidan

// Kolla om inloggad = sessionen satt
if (!isset($_SESSION['sess_user'])){
  header("Location: index.php");
  exit;
}

require("conn.php");	// Databasanslutningen
include("inc/functions.php");

$self = $_SERVER['PHP_SELF'];


/*************************************\
| ******** Tobias Bleckert ********** |
| ********* www.tb-one.se *********** |
| ****** Nyhets script v.2.1 ******** |
| ******** Modifierat script ******** |
| *Spara dessa rader för användning * |
\*************************************/

//Kolla resurser och ob_start()-funktionen för mer information
ob_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><?php require("header_title.html"); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/iframe.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/javascript.js"></script>
<script language="JavaScript">
function checkFields() {
  missinginfo = "";
	if (document.bb.quantity.value == "") {
	missinginfo += "\n - Antal filer";
	}
	if(document.bb.file.value == "") {
	missinginfo += "\n - Filen";
	}
	if(document.bb.text.value == "") {
	missinginfo += "\n - Beskrivning";
	}
	if (missinginfo != "") {
	missinginfo ="_____________________________\n" +
	"Du har inte fyllt i fälten:\n" +
	missinginfo + "\n_____________________________" +
	"\nFyll i de angivna fälten innan du lägger in Nyheten!";
	alert(missinginfo);
	return false;
	}
	else return true;
}
</script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3406958-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</head>
<body>


<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" valign="top" align="center">
<tr><td width="100%" height="100%" valign="top" align="left" class="admin_main_bg">

<?php

//Kod för att lägga till genom ett forumlär
if(isset($_GET['addconfig'])) {

?>

	<table width='100%' cellspacing='0' cellpadding='0' border='0' valign='top'>
	<tr><td width='100%' height='25' valign='center' align='center' class='historia_rubrik'>
		<span class='admin_gray'>[ </span><a href="admin_configs.php" title="Gå tillbaka" target="main" class='admin_orange'>Lägg till Config</a><span class='admin_gray'> ]</span><br>
	</td></tr>
	<tr><td width='100%' valign='top' class='admin_bg'>

		<table width='100%' height='100%' cellspacing='0' cellpadding='0' border='0'>
		<tr><td valign='top'>

			<table width='100%' cellspacing='0' cellpadding='0' border='0'>
			<tr><td class='admin_nyheter_add_rubrik' valign='center' height='20'>
				<span class='admin_gray'>Spel:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
				<form action='admin_configs.php?addconfig&add' name='bb' method='post' onSubmit='return checkFields();' enctype='multipart/form-data' id='add' name='bb' method='post' style="margin-bottom:0;">
<select name="game" class="login_line">
<option value="1" selected>CS:S</option>
<option value="2">COD 4</option>
<option value="3">Q3</option>
</select>
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Antal filer: ( i rar/zip filen )</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
				<input name="quantity" type="text" value="1" style="width: 35;" autocomplete="off" class="shout_line">
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Config:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
<?php
/*
if (isset($_FILES['uplfile'])) {

	$resultat = mysql_query("SELECT * FROM legacy_headz_matcher") or die (mysql_error());
	$row = mysql_fetch_array($resultat);
		$id = $row['id'];


	// Mappen där filerna ska hamna
	$upload_dir = 'files/configs/';
	// De tillåtna filtyperna, separerade med komman, utan mellanrum
	$filetypes = 'zip,rar';
	// Den största tillåtna storleken (10 MB)
	$maxsize = (1024*1024*10);
	// Kontrollera att det angavs en fil
	if(empty($_FILES['uplfile']['name']))
		die('No file was selected.');

	// Kontrollera storleken
	if($_FILES['uplfile']['size'] > $maxsize)
		die('The file you selected to upload was too large. The maximum allowed filesize is currently: <b>10</b> MB.');

	// '.(string)($maxsize/1024).'


	// Kontrollera filtypen
	$types = explode(',', $filetypes);
	$file = explode('.', $_FILES['uplfile']['name']);
	if(!in_array(strtolower($file[1]), $types))
	die('This fileformat is not allowed. Only these are; .zip and .rar');

	if (strlen($file[0]) > 20)
	die('The filename is too long. Please shorten it to atleast 20 characters.');

	// Flytta filen rätt
	if (is_uploaded_file($_FILES['uplfile']['tmp_name']) && move_uploaded_file($_FILES['uplfile']['tmp_name'])) {
		echo "File successfully uploaded: <a href='$upload_dir$uploaded_file'>$uploaded_file</a>";

	// Uppladdningen lyckades.
	// Här kan man även lägga eventuell kod  för t.ex. databashantering.
	// Filnamnet ligger i $thefile.
	// Ytterliggare fält i formuläret får du som vanligt med $_POST


	} else {
		echo 'An error occured and the selected file could not be uploaded. Please send a mail to <a href="mailto:kakan@maxhuber.se">kakan@maxhuber.se</a> regarding this issue.';

	// Uppladdningen misslyckades.

   }
} else {

	<input type="file" name="uplfile" class="shout_line">

}
*/
?>
				<input name="file" type="file" id="file" style="width: 250;" class="shout_line">
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text' valign='center'>
				<script type="text/javascript" src="js/editor.js"></script>

					<a href="#" onclick="insert_text('^^', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/orc%5E%5E.gif" alt="^^" title="Happy Orc" height="22" hspace="2" vspace="2" width="23" border="0"></a>
					<a href="#" onclick="insert_text(':D', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_biggrin.gif" alt=":D" title="Very Happy" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_smile.gif" alt=":)" title="Smile" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':lol:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_lol.gif" alt=":lol:" title="Laughing" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':P', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_razz.gif" alt=":P" title="Razz" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':mrgreen:', true); return false;" style="line-height: 20px;">	<img src="forum/images/smilies/icon_mrgreen.gif" alt=":mrgreen:" title="Mr. Green" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(';)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_wink.gif" alt=";)" title="Wink" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':S', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_confused.gif" alt=":S" title="Confused" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':(', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_sad.gif" alt=":(" title="Sad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':cry:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_cry.gif" alt=":cry:" title="Neutral" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':|', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_neutral.gif" alt=":|" title="Neutral" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':$', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_redface.gif" alt=":$" title="Embarassed" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text('8)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_cool.gif" alt="8)" title="Cool" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':O', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_surprised.gif" alt=":O" title="Surprised" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':eek:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_eek.gif" alt=":eek:" title="Shocked" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':rolleyes:', true); return false;" style="line-height: 20px;"><img src="forum/images/smilies/icon_rolleyes.gif" alt=":rolleyes:" title="Rolling Eyes" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':mad:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_mad.gif" alt=":mad:" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':X', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_evil.gif" alt=":X" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':@', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_twisted.gif" alt=":@" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>

			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>

				<input class="btnbbcode" title="Fet text: [b]text[/b]" style="font-weight: bold;" type="button" name="bold"
				onclick="javascript:addtag('b');" value="b" />

				<input class="btnbbcode" title="Kursiv text: [i]text[/i]" style="font-style: italic;" type="button" name="italic"
				onclick="javascript:addtag('i');" value="i" />

				<input class="btnbbcode" title="Understruken text: [u]text[/u]" style="text-decoration: underline;" type="button" name="inserted"
				onclick="javascript:addtag('u');" value="u" />

				<input class="btnbbcode" title="!!OBS, NAMNET MÅSTE VARA MED!! Citera text: [quote=namnet]text[/quote]" type="button" name="blockquote"
				onclick="javascript:addtag('quote');" value="Quote" />

				<input class="btnbbcode" title="Infoga länk: [url=http://url]Länktext[/url]" style="text-decoration: underline;" type="button" name="link"
				onclick="javascript:addtag('url');" value="URL" />

				<input class="btnbbcode" title="!!ENDAST TILL FILER PÅ HEMSIDAN!! Infoga länk: [url2=http://url]Länktext[/url2]" style="text-decoration: underline;" type="button" name="link2"
				onclick="javascript:addtag('url2');" value="URL(intern)" />

				<input class="btnbbcode" title="Infoga bild: [img]http://bild_url.jpg[/img]" type="button" name="image"
				onclick="javascript:addtag('img');" value="Img" />

			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text' valign='center'>
				<span class='admin_gray'>Beskrivning:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
				<textarea name="text" title="Text" style="width: 100%; height: 75;" class="shout_line"></textarea>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_send' valign='center' align='center'>
				<input class="mainoption" type="submit" title="Lägg till Configen!" value="Lägg till Configen!">&nbsp;
				<input class="liteoption" type="reset" title="Rensa formuläret!" value="Rensa" onClick="return confirm('Är du säker på att du vill rensa?')">
				</form>
			</td></tr>
			</table>

		</td></tr>
		</table>

	</td></tr>
	</table>


<?php
//Då ?addnews=$id&add står i adressfältet kommer följande kod att laddas
if(isset($_GET['add'])) {

$resultat = mysql_query("SELECT * FROM legacy_headz_players WHERE user='{$_SESSION['sess_user']}'") or die (mysql_error());
if($row = mysql_fetch_array($resultat)) {
$profile = $row['user'];
$profile_id = $row['profile_id'];

//Lägger in dagens datum i variabeln $datum som ex 1/9-04
$date = date('H:i @ d/m-y'); // 00:05 @ 05/03-07
/*
//SQL kod för att lägga in i databasen
mysql_query("INSERT INTO legacy_headz_configs (profile, profile_id, date, size, game, quantity, text) VALUES (
			'$profile',
			'$profile_id',
			'$date',
			'$size',
			'".$_POST['game']."',
			'".$_POST['quantity']."',
			'".$_POST['text']."')") or exit(mysql_error());
*/
   //Skickar dig vidare till det inskrivna efter location:
   header('location: admin_configs.php');
  }
 }
}

//Kod för att ändra en nyhet med vald ID
if(isset($_GET['editconfig'])) {

//Denna kod hämtar ut informationen som vi ska ändra på
$qnyheter = mysql_query("SELECT * FROM legacy_headz_configs WHERE id=".$_GET['editconfig']) or exit(mysql_error());
if($snyheter = mysql_fetch_array($qnyheter)) {
$id = $snyheter['id'];
$game = $snyheter['game'];
$profile = $snyheter['profile'];
$profile_id = $snyheter['profile_id'];
$date = $snyheter['date'];
$size = $snyheter['size'];
$quantity = $snyheter['quantity'];
$downloads = $snyheter['downloads'];
$text = $snyheter['text'];

//Följande kod är ett forumlär med dess respektive information i sig

?>

	<table width='100%' cellspacing='0' cellpadding='0' border='0' valign='top'>
	<tr><td width='100%' height='25' valign='center' align='center' class='historia_rubrik'>
		<span class='admin_gray'>[ </span><a href="admin_configs.php" title="Gå tillbaka" target="main" class='admin_orange'>Ändra Config</a><span class='admin_gray'> ]</span><br>
	</td></tr>
	<tr><td width='100%' valign='top' class='admin_bg'>

		<table width='100%' height='100%' cellspacing='0' cellpadding='0' border='0'>
		<tr><td valign='top'>

			<table width='100%' cellspacing='0' cellpadding='0' border='0'>
			<tr><td class='admin_nyheter_add_rubrik' valign='center' height='20'>
				<span class='admin_gray'>Spel:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
				<form action='admin_configs.php?addconfig&add' id='add' method='post' style="margin-bottom:0;">
<select name="game" class="login_line">
<option value="1" <?php if($game==1){ echo "selected"; } ?>>CS:S</option>
<option value="2" <?php if($game==2){ echo "selected"; } ?>>COD 4</option>
<option value="3" <?php if($game==3){ echo "selected"; } ?>>Q3</option>
<option value="4" <?php if($game==4){ echo "selected"; } ?>>WC3</option>
</select>
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Antal filer: ( i zip filen )</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
				<input name="result_headz" type="text" style="width: 35;" autocomplete="off" value='<?php echo $quantity; ?>' class="shout_line" />
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Config:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
				<input name="demofile" type="text" style="width: 250;" autocomplete="off" class="shout_line" />
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text' valign='center'>
				<span class='admin_gray'>Beskrivning:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
				<textarea name="text" title="Text" autocomplete="off" rows="4" style="width: 100%;" class="shout_line"><?php echo $text; ?></textarea>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_send' valign='center' align='center'>
				<input class="mainoption" type="submit" title="Ändra Configen!" value="Ändra Configen!" />
				</form>
			</td></tr>
			</table>

		</td></tr>
		</table>

	</td></tr>
	</table>


<?php
//Då ?editnews=$id&update står i adressfältet kommer följande kod att laddas
if(isset($_GET['update'])) {
/*
	//SQL kod för att uppdatera en vald nyhet
	mysql_query("UPDATE headz_config SET
			'$profile',
			'$profile_id',
			'$date',
			'".$_POST['game']."',
			'".$_POST['size']."',
			'".$_POST['quantity']."',
			'".$_POST['text']."'
			WHERE id=".$_GET['editconfig']) or exit(mysql_error());
*/
	//Skickar dig vidare till det inskrivna efter location:
	header('location: admin_configs.php');
  }
 }
}

//Kod för att ta bort en nyhet
if(isset($_GET['deleteconfig'])) {

//Först hämtar vi information om den nyheten vi vill ta bort, vilket vi gjorde med denna kod
$qdelete = mysql_query("SELECT * FROM legacy_headz_configs WHERE id=".$_GET['deleteconfig']) or exit(mysql_error());
$sdelete = mysql_fetch_array($qdelete);
$game = $sdelete['game'];

//En bekräftningsfråga om du vill ta bort nyheten
echo"<table width='100%' cellspacing='0' cellpadding='0' border='0' valign='top'>
	<tr><td width='100%' height='25' valign='center' align='center' class='historia_rubrik'>
		<span class='admin_gray'>[ ";
echo	"Är du säker på att du vill ta bort configen: </span><span class='admin_orange2'>";
echo	$sdelete['profile'];
echo	"</span> ";

if($game == 1) {
echo	"<img src='images/icons/icon_css.gif' border='0' width='16' height='16' />";
} elseif($game == 2) {
echo	"<img src='images/icons/icon_cod4.gif' border='0' width='16' height='16' />";
} elseif($game == 3) {
echo	"<img src='images/icons/icon_q3.gif' border='0' width='16' height='16' />";
} elseif($game == 4) {
echo	"<img src='images/icons/icon_wc3.gif' border='0' width='16' height='16' />";
}

echo	" <span class='admin_gray'> ? ]<br /><br />";
echo	"<span class='admin_orange'><a href='admin_configs.php?deleteconfig=";
echo	$sdelete['id'];
echo	"&delete'>Ja</a><span class='admin_gray'> | </span><span class='admin_orange'><a href='admin_configs.php' target='main'>Nej</a></span>";
echo"</td></tr>
	</table>";


//Då ?deletenews=$id&delete står i adressfältet kommer följande kod att laddas
if(isset($_GET['delete'])) {
/*
    mysql_query("DELETE FROM legacy_headz_configs WHERE id=".$_GET['deleteconfig']) or exit(mysql_error());
#	mysql_query("DELETE FROM kommentarer WHERE nid=".$_GET['deleteconfig']) or exit(mysql_error());
*/
  //Skickar dig vidare till det inskrivna efter location:
  header('location: admin_configs.php');
 }
}

//Om QUERY_STRING är tom laddas denna text, kolla under resurser och Server variablar för mer information
if(empty($_SERVER['QUERY_STRING'])) {

?>

	<table width='100%' cellspacing='0' cellpadding='0' border='0' valign='top'>
	<tr><td width='100%' height='25' valign='center' align='center' class='historia_rubrik'>
		<span class='medlemmar_text_citat'>[ </span><span class='medlemmar_text_citat_info'>Configs</span><span class='medlemmar_text_citat'> ]</span>
	</td></tr>
	<tr><td width='100%' valign='top' class='admin_bg'>

		<table width='100%' height='100%' cellspacing='0' cellpadding='0' border='0'>
		<tr><td valign='top'>

			<table width='100%' cellspacing='0' cellpadding='0' border='0'>
			<tr><td class='admin_nyheter_add' colspan='4' valign='center' align='center' height='20'>

<?php

echo			"<span class='admin_gray'>[ </span>";
echo			"<a href='admin_configs.php?addconfig' title='Lägg till en ny Config' target='main' class='admin_orange'>Lägg till Config</a>";
echo			"<span class='admin_gray'> ]</span><br>";

?>

			</td></tr>

<?php

$color = 1;

//Här hämtar vi ut all information om alla nyheter och loopar ut dem, dvs skriver ut alla nyheter efter varandra
$qnyheter = mysql_query("SELECT * FROM legacy_headz_configs WHERE profile = '{$_SESSION['sess_user']}' OR '{$_SESSION['sess_user']}' = 'Sås' ORDER BY id DESC") or die (mysql_error());
while($snyheter = mysql_fetch_array($qnyheter)) {
$id = $snyheter['id'];
$game = $snyheter['game'];
$profile = $snyheter['profile'];
$profile_id = $snyheter['profile_id'];
$date = $snyheter['date'];
$size = $snyheter['size'];
$quantity = $snyheter['quantity'];
$downloads = $snyheter['downloads'];
$text = $snyheter['text'];


if($color == 1){

echo		"<tr><td height='19' class='admin_nyheter' valign='center'>

				<table height='19' cellspacing='0' cellpadding='0' border='0'>
				<tr><td valign='center'>";

if($game == 1) {
echo				"<img src='images/icons/icon_css.gif' border='0' width='16' height='16' />";
} elseif($game == 2) {
echo				"<img src='images/icons/icon_cod4.gif' border='0' width='16' height='16' />";
} elseif($game == 3) {
echo				"<img src='images/icons/icon_q3.gif' border='0' width='16' height='16' />";
} elseif($game == 4) {
echo				"<img src='images/icons/icon_wc3.gif' border='0' width='16' height='16' />";
}

echo			"</td><td>";
echo				"<span class='admin_gray'>&nbsp;&nbsp;&nbsp;[</span>";
echo				"<a href='admin_configs.php?editconfig=$id' title='Ändra Config #$id' class='admin_orange'>Config</a>";
echo				"<span class='admin_gray'>]</span>
				</td><td valign='center'>";
echo				"<span class='admin_gray'>&nbsp;&nbsp;[</span>";
echo				"<span class='admin_orange2'>$size</span>";
echo				"<span class='admin_gray'>] kb</span>
				</td></tr>
				</table>

			</td><td width='160' height='20' class='admin_nyheter_date' valign='center' align='center'>
				<span class='admin_gray'>[ </span>";
echo			"<span class='admin_tid'>$date</span>";
echo			"<span class='admin_gray'> ]</span>
			</td><td width='100' height='20' class='admin_nyheter_edit' valign='center' align='center'>";
echo			"<span class='admin_gray'>[ </span>";
echo			"<a href='medlemmar_info.php?medlem=$profile_id' title='Se Profil: $profile' class='admin_orange'>$profile</a>";
echo			"<span class='admin_gray'> ]</span><br>
			</td><td width='45' height='20' class='admin_nyheter_remove' valign='center' align='center'>
				<a href='admin_configs.php?deleteconfig=$id' title='Ta bort Config #<?php echo $id; ?>'><img src='images/icons/icon_admin_remove.gif' border='0' width='18' height='16' alt=''></a>
			</td></tr>";

// Set $color==2, for switching to other color
$color = 2;

} else {

echo		"<tr><td height='19' class='admin_nyheter_dark' valign='center'>

				<table height='19' cellspacing='0' cellpadding='0' border='0'>
				<tr><td valign='center'>";

if($game == 1) {
echo				"<img src='images/icons/icon_css.gif' border='0' width='16' height='16' />";
} elseif($game == 2) {
echo				"<img src='images/icons/icon_cod4.gif' border='0' width='16' height='16' />";
} elseif($game == 3) {
echo				"<img src='images/icons/icon_q3.gif' border='0' width='16' height='16' />";
} elseif($game == 4) {
echo				"<img src='images/icons/icon_wc3.gif' border='0' width='16' height='16' />";
}

echo			"</td><td>";
echo				"<span class='admin_gray'><a href='admin_configs.php?editconfig=$id' title='Ändra Config #$id'>&nbsp;&nbsp;&nbsp;[</span>";
echo				"<span class='admin_orange'>Config</span>";
echo				"<span class='admin_gray'>]</a></span></td>
				<td valign='center'>
					<span class='admin_gray'>&nbsp;&nbsp;[</span>";
echo				"<span class='admin_orange2'>$size</span>";
echo				"<span class='admin_gray'>] kb</span>
				</td></tr>
				</table>

			</td><td width='160' height='20' class='admin_nyheter_date_dark' valign='center' align='center'>
				<span class='admin_gray'>[ </span>";
echo			"<span class='admin_tid'>$date</span>";
echo			"<span class='admin_gray'> ]</span>
			</td><td width='100' height='20' class='admin_nyheter_edit_dark' valign='center' align='center'>";
echo			"<span class='admin_gray'><a href='medlemmar_info.php?medlem=$profile_id' title='Se Profil: $profile'>[ </span>";
echo			"<span class='admin_orange'>$profile</span>";
echo			"<span class='admin_gray'> ]</a></span>
			</td><td width='45' height='20' class='admin_nyheter_remove_dark' valign='center' align='center'>
				<a href='admin_configs.php?deleteconfig=$id' title='Ta bort Config #<?php echo $id; ?>'><img src='images/icons/icon_admin_remove.gif' border='0' width='18' height='16' alt=''></a>
			</td></tr>";

// Set $color back to 1
$color = 1;

  }
 }
}
echo		"</table>";

?>

		</td></tr>
		</table>

	</td></tr>
	</table>

</td></tr>
</table>


</body>
</html>

<?php
//Kolla under resurser och ob_end_flush()-funktionen för mer information
ob_end_flush();
?>
