<?php
session_start(); // Alltid överst på sidan

// Kolla om inloggad = sessionen satt
if (!isset($_SESSION['sess_user'])){
  header("Location: index.php");
  exit;
}

require("conn.php");	// Databasanslutningen
include("inc/functions.php");

$self = $_SERVER['PHP_SELF'];


/*************************************\
| ******** Tobias Bleckert ********** |
| ********* www.tb-one.se *********** |
| ****** Nyhets script v.2.1 ******** |
| ******** Modifierat script ******** |
| *Spara dessa rader för användning * |
\*************************************/

//Kolla resurser och ob_start()-funktionen för mer information
ob_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><?php require("header_title.html"); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/iframe.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/javascript.js"></script>
<script language="JavaScript">
function ShowMenu(num, menu, max)
{
                //num is selected value, menu is the name of the div, max is the number of divs
                for(i = 1; i <= max; i++){
                        //add number onto end of menu
                        var menu_div = menu + i;

                        //if current show
                        if(i == num) {
                                document.getElementById(menu_div).style.display = 'block';
                        } else {
                                //if not, hide
                                document.getElementById(menu_div).style.display = 'none';
                        }
                }



        }
</script>
<script language="JavaScript">
function setOptions(chosen) {
	var selbox = document.myform.opttwo;
	var selbox2 = document.myform.optthree;

selbox.options.length = 0;
selbox2.options.length = 0;
if (chosen == "0") {
selbox.options[selbox.options.length] = new Option('--',' ');
selbox2.options[selbox2.options.length] = new Option('--',' ');
}
if (chosen == "1") {
	selbox.options[selbox.options.length] = new Option('css - 1 - 1','css - 1 - value');
	selbox2.options[selbox2.options.length] = new Option('css - 2 - 1','css - 2 - value');
	}
if (chosen == "2") {
	selbox.options[selbox.options.length] = new Option('cod4 - 1 - 1','cod4 - 1 - value');
	selbox.options[selbox.options.length] = new Option('cod4 - 1 - 2','cod4 - 2 - value');
	selbox2.options[selbox2.options.length] = new Option('cod4 - 2 - 1','cod4 - 1 - value');
	selbox2.options[selbox2.options.length] = new Option('cod4 - 2 - 2','cod4 - 2 - value');
	}
if (chosen == "3") {
	selbox.options[selbox.options.length] = new Option('second choice - option one','twoone');
	selbox.options[selbox.options.length] = new Option('second choice - option two','twotwo');
	selbox2.options[selbox2.options.length] = new Option('second choice - option one A','twooneA');
	selbox2.options[selbox2.options.length] = new Option('second choice - option two A','twotwoA');
	selbox2.options[selbox2.options.length] = new Option('second choice - option three A','twothreeA');
	}
if (chosen == "4") {
	selbox.options[selbox.options.length] = new Option('third choice - option one','threeone');
	selbox.options[selbox.options.length] = new Option('third choice - option two','threetwo');
	selbox2.options[selbox2.options.length] = new Option('third choice - option one A','threeoneA');
	selbox2.options[selbox2.options.length] = new Option('third choice - option two A','threetwoA');
	}
}
</script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3406958-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</head>
<body>


<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" valign="top" align="center">
<tr><td width="100%" height="100%" valign="top" align="left" class="admin_main_bg">

<?php
//Kod för att lägga till genom ett forumlär
if(isset($_GET['addmatch'])) {


/*

function setOptions(chosen) {
var selbox = document.myform.opttwo;
var selbox2 = document.myform.optthree;

selbox.options.length = 0;
selbox2.options.length = 0;
if (chosen == " ") {
selbox.options[selbox.options.length] = new Option('Please select one of the options above first',' ');
selbox2.options[selbox2.options.length] = new Option('Please select one of the options above first',' ');
}
if (chosen == "1") {
selbox.options[selbox.options.length] = new Option('first choice - option one','oneone');
selbox.options[selbox.options.length] = new Option('first choice - option two','onetwo');
selbox2.options[selbox2.options.length] = new Option('first choice - option one A','oneoneA');
selbox2.options[selbox2.options.length] = new Option('first choice - option two A','onetwoA');
}
if (chosen == "2") {
selbox.options[selbox.options.length] = new Option('second choice - option one','twoone');
selbox.options[selbox.options.length] = new Option('second choice - option two','twotwo');
selbox2.options[selbox2.options.length] = new Option('second choice - option one A','twooneA');
selbox2.options[selbox2.options.length] = new Option('second choice - option two A','twotwoA');
selbox2.options[selbox2.options.length] = new Option('second choice - option three A','twothreeA');
}
if (chosen == "3") {
selbox.options[selbox.options.length] = new Option('third choice - option one','threeone');
selbox.options[selbox.options.length] = new Option('third choice - option two','threetwo');
selbox2.options[selbox2.options.length] = new Option('third choice - option one A','threeoneA');
}
}


<form name="myform"><div align="center">
<select name="optone" size="1"
onchange="setOptions(document.myform.optone.options[document.myform.optone.selectedIndex].value);">
<option value=" " selected="selected"> </option>
<option value="1">First Choice</option>
<option value="2">Second Choice</option>
<option value="3">Third Choice</option>
</select><br /> <br />
<select name="opttwo" size="1">
<option value=" " selected="selected">Please select one of the options above first</option>
</select><br />
<select name="optthree" size="1">
<option value=" " selected="selected">Please select one of the options above first</option>
</select> <input type="button" name="go" value="Value Selected"
onclick="alert(document.myform.opttwo.options[document.myform.opttwo.selectedIndex].value + ':' + document.myform.optthree.options[document.myform.optthree.selectedIndex].value);" />
</div></form>

*/

/*

		Spel:
			[X] (X = CS:S/COD4/Q3/WC3)

CSS		->
		Datum:
			[XX]:[XX] @ [XX]/[XX]-[XXXX] (Timme:Minut @ Dag/Månad-År, 00:00 @ 01/01-2008)
		Motståndare:
			[__]
		XonX:
			[XonX] (X = 1/2/3/4/5/6/7/8/9/10)
		HeadZ:
			[X][X][X][X][X][X][X][X][X][X] (X = #1/#2/#3/#4/#5/#6/#7/#8/#9/#10)		!!OM XonX = 3, GLÖM RESTEN AV HeadZ [X] > 3!!
		Motståndare:
			[_X_][_X_][_X_][_X_][_X_][_X_][_X_][_X_][_X_][_X_]
		Resultat:
			[__]:[__]
		Liga:
			[__] [_LigaURL_]
->			Map:
->				[--Map--]
		Server:
			[_Våran/Deras_][_ServerURL_] (1/2 + [__])		!!GLÖM SERVER URL OM "Våran" ÄR VALT!!
		Admin(s):
			[_InteTillgänglig/Vi/Dom] (0/1/2)
->			DEMO:
->				[_InteTillgänglig/HLTV] (0/1)
		Sammandrag:
			[____]

COD4	->
		Datum:
			[XX]:[XX] @ [XX]/[XX]-[XXXX] (Timme:Minut @ Dag/Månad-År, 00:00 @ 01/01-2008)
		Motståndare:
			[__]
		XonX:
			[XonX] (X = 1/2/3/4/5/6/7/8/9/10)
		HeadZ:
			[X][X][X][X][X][X][X][X][X][X] (X = #1/#2/#3/#4/#5/#6/#7/#8/#9/#10)		!!OM XonX = 3, GLÖM RESTEN AV HeadZ [X] > 3!!
		Motståndare:
			[_X_][_X_][_X_][_X_][_X_][_X_][_X_][_X_][_X_][_X_]
		Resultat:
			[__]:[__]
		Liga:
			[__] [_LigaURL_]
->			Typ:
->				[X] (X = DM/DOM/KOTH/SAB/SD/TDM)
->			Maps:
->				[--Map #X--][--Map #X--][--Map #X--][--Map #X--][--Map #X--][--Map #X--] (X = #1/#2/#3/#4/#5/#6)	!!OM MAP #1 == IFYLLD, VISA MAP #2, ETC
		Server:
			[_Våran/Deras_][_ServerURL_] (1/2 + [__])		!!GLÖM SERVER URL OM "Våran" ÄR VALT!!
		Admin(s):
			[_InteTillgänglig/Vi/Dom] (0/1/2)
->			DEMO:
->				[_InteTillgänglig/DEMO] (0/1)
		Sammandrag:
			[____]

Q3		->
		Datum:
			[XX]:[XX] @ [XX]/[XX]-[XXXX] (Timme:Minut @ Dag/Månad-År, 00:00 @ 01/01-2008)
		Motståndare:
			[__]
		XonX:
			[XonX] (X = 1/2/3/4/5/6/7/8/9/10)
		HeadZ:
			[X][X][X][X][X][X][X][X][X][X] (X = #1/#2/#3/#4/#5/#6/#7/#8/#9/#10)		!!OM XonX = 3, GLÖM RESTEN AV HeadZ [X] > 3!!
		Motståndare:
			[_X_][_X_][_X_][_X_][_X_][_X_][_X_][_X_][_X_][_X_]
		Resultat:
			[__]:[__]
		Liga:
			[__] [_LigaURL_]
->			Typ:
->				[X] (X = DM/CTF/TDM)						!!KOLLA UPP SPELSTILER!!
->			Maps:
->				[--Map #X--][--Map #X--][--Map #X--][--Map #X--][--Map #X--][--Map #X--] (X = #1/#2/#3/#4/#5/#6)	!!OM MAP #1 == IFYLLD, VISA MAP #2, ETC

		Server:
			[_Våran/Deras_][_ServerURL_] (1/2 + [__])		!!GLÖM SERVER URL OM "Våran" ÄR VALT!!
		Admin(s):
			[_InteTillgänglig/Vi/Dom] (0/1/2)

			DEMO:
				[_InteTillgänglig/DEMO] (0/1)

		Sammandrag:
			[____]

WC3		->
												Uppladdad av:
												Uppladdnings datum:
												Storlek:
												Antal nedladdningar:
		Datum:
			[XX]:[XX] @ [XX]/[XX]-[XXXX] (Timme:Minut @ Dag/Månad-År, 00:00 @ 01/01-2008)

->			XonX:
->				[XonX] (X = 1/2/3/4/5)
->			HeadZ:
->				[_X_][_X_][_X_][_X_][_X_]
->			Motståndare:
->				[_X_][_X_][_X_][_X_][_X_]
->			Map:
->				[--Map--]
		Resultat:
			[__]:[__]
		Liga:
			[__] [_LigaURL_]
->			Version:
->				[X] (X = 1.21 -> 1.20 -> etc)
->			Beskrivning:
->				[____]

*/

/*

<form name="test" action="" method="post">
Select how many flowers you would like:

<select id='numflowers'
onChange="ShowMenu(document.getElementById('numflowers').value,'divColor', 6);">
        <option value='0'>Number of Flowers</option>
        <option value='1'>1</option>
        <option value='2'>2</option>
        <option value='3'>3</option>
        <option value='4'>4</option>
        <option value='5'>5</option>
        <option value='5'>6</option>
</select>

<?php
if(document.test.numflowers.options.selectedIndex.value == 1){
echo	'<input name="opponent" type="text" style="width: 200;" autocomplete="on" class="shout_line">';
}
?>

<div id='divColor2' style="display: none;">
                Choose type of flower 2:
                <input type="radio" name="color2" value="red">Red</option>
                <input type="radio" name="color2" value="white">White</option>
                <input type="radio" name="color2" value="yellow">Yellow</option>
</div>

<input type="submit" value="Next Step">
</form>

*/

/*

<form name="myform">

<select name="game" class="login_line"
onchange="setOptions(document.myform.game.options[document.myform.game.selectedIndex].value);">
<option value="0" selected="selected">-- Spel --</option>
<option value="1">CS:S</option>
<option value="2">COD4</option>
<option value="3">Q3</option>
<option value="4">WC3</option>
</select>


<select name="opttwo" class="login_line">
<option value="0" selected="selected">--</option>
</select>

<select name="optthree" class="login_line">
<option value="0" selected="selected">--</option>
</select>

<input type="button" name="go" value="Value Selected"
onclick="alert(document.myform.opttwo.options[document.myform.opttwo.selectedIndex].value + ' : ' + document.myform.optthree.options[document.myform.optthree.selectedIndex].value);">
</form>

*/

?>

	<table width='100%' cellspacing='0' cellpadding='0' border='0' valign='top'>
	<tr><td width='100%' height='25' valign='center' align='center' class='historia_rubrik'>
		<span class='admin_gray'>[ </span><a href="admin_matcher.php" title="Gå tillbaka" target="main" class='admin_orange'>Lägg till Match</a><span class='admin_gray'> ]</span><br>
	</td></tr>
	<tr><td width='100%' valign='top' class='admin_bg'>

		<table width='100%' height='100%' cellspacing='0' cellpadding='0' border='0'>
		<tr><td valign='top'>

			<table width='100%' cellspacing='0' cellpadding='0' border='0'>
			<tr><td class='admin_nyheter_add_rubrik' valign='center' height='20'>
				<span class='admin_gray'>Spel:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
				<form action='admin_matcher.php?addmatch&add' id='add' name='bb' method='post' style="margin-bottom:0;">
<select name="game" class="login_line">
<option value="1" selected>CS:S</option>
<option value="2">COD4</option>
<option value="3">Q3</option>
<option value="4">WC3</option>
</select>
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Datum: ( <span class='admin_orange2'>Timme</span>:<span class='admin_orange2'>Minut</span> @ <span class='admin_orange2'>Dag</span>/<span class='admin_orange2'>Månad</span>-<span class='admin_orange2'>År</span> )</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="hour" class="login_line">
<option value="00" selected>00</option>
<option value="01">01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
</select>:<select name="minute" class="login_line">
<option value="00" selected>00</option>
<option value="05">05</option>
<option value="10">10</option>
<option value="15">15</option>
<option value="20">20</option>
<option value="25">25</option>
<option value="30">30</option>
<option value="35">35</option>
<option value="40">40</option>
<option value="45">45</option>
<option value="50">50</option>
<option value="55">55</option>
</select> @ <select name="day" class="login_line">
<option value="01" selected>01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
</select>/<select name="month" class="login_line">
<option value="01" selected>01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
</select>-<select name="year" class="login_line">
<option value="08" selected>2008</option>
</select>
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Motståndare:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
				<input name="opponent" type="text" style="width: 200;" autocomplete="on" class="shout_line">
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_orange2'>X</span><span class='admin_gray'>on</span><span class='admin_orange2'>X</span><span class='admin_gray'>:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="on" class="login_line">
<option value="1" selected>1on1</option>
<option value="2">2on2</option>
<option value="3">3on3</option>
<option value="4">4on4</option>
<option value="5">5on5</option>
<option value="6">6on6</option>
<option value="7">7on7</option>
<option value="8">8on8</option>
<option value="9">9on9</option>
<option value="10">10on10</option>
</select>
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>HeadZ medlemmar aktiva i matchen:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
<?php

$s = mysql_query("SELECT id, nick FROM legacy_headz_medlem ORDER BY id ASC") or die (mysql_error());

echo '<select name="team_headz_1" class="login_line">';
echo '<option value="0" selected>Medlem #1</option>';
while ($r = mysql_fetch_array($s)) {
echo '<option value="'.$r['id'].'">'.$r['nick'].'</option>"';
}
echo '</select> ';

$s = mysql_query("SELECT id, nick FROM legacy_headz_medlem ORDER BY id ASC") or die (mysql_error());
echo '<select name="team_headz_2" class="login_line">';
echo '<option value="0" selected>Medlem #2</option>';
while ($r = mysql_fetch_array($s)) {
echo '<option value="'.$r['id'].'">'.$r['nick'].'</option>"';
}
echo '</select> ';

$s = mysql_query("SELECT id, nick FROM legacy_headz_medlem ORDER BY id ASC") or die (mysql_error());
echo '<select name="team_headz_3" class="login_line">';
echo '<option value="0" selected>Medlem #3</option>';
while ($r = mysql_fetch_array($s)) {
echo '<option value="'.$r['id'].'">'.$r['nick'].'</option>"';
}
echo '</select> ';

$s = mysql_query("SELECT id, nick FROM legacy_headz_medlem ORDER BY id ASC") or die (mysql_error());
echo '<select name="team_headz_4" class="login_line">';
echo '<option value="0" selected>Medlem #4</option>';
while ($r = mysql_fetch_array($s)) {
echo '<option value="'.$r['id'].'">'.$r['nick'].'</option>"';
}
echo '</select> ';

$s = mysql_query("SELECT id, nick FROM legacy_headz_medlem ORDER BY id ASC") or die (mysql_error());
echo '<select name="team_headz_5" class="login_line">';
echo '<option value="0" selected>Medlem #5</option>';
while ($r = mysql_fetch_array($s)) {
echo '<option value="'.$r['id'].'">'.$r['nick'].'</option>"';
}
echo '</select> ';

$s = mysql_query("SELECT id, nick FROM legacy_headz_medlem ORDER BY id ASC") or die (mysql_error());
echo '<select name="team_headz_6" class="login_line">';
echo '<option value="0" selected>Medlem #6</option>';
while ($r = mysql_fetch_array($s)) {
echo '<option value="'.$r['id'].'">'.$r['nick'].'</option>"';
}
echo '</select> ';

$s = mysql_query("SELECT id, nick FROM legacy_headz_medlem ORDER BY id ASC") or die (mysql_error());
echo '<select name="team_headz_7" class="login_line">';
echo '<option value="0" selected>Medlem #7</option>';
while ($r = mysql_fetch_array($s)) {
echo '<option value="'.$r['id'].'">'.$r['nick'].'</option>"';
}
echo '</select> ';

$s = mysql_query("SELECT id, nick FROM legacy_headz_medlem ORDER BY id ASC") or die (mysql_error());
echo '<select name="team_headz_8" class="login_line">';
echo '<option value="0" selected>Medlem #8</option>';
while ($r = mysql_fetch_array($s)) {
echo '<option value="'.$r['id'].'">'.$r['nick'].'</option>"';
}
echo '</select> ';

$s = mysql_query("SELECT id, nick FROM legacy_headz_medlem ORDER BY id ASC") or die (mysql_error());
echo '<select name="team_headz_9" class="login_line">';
echo '<option value="0" selected>Medlem #9</option>';
while ($r = mysql_fetch_array($s)) {
echo '<option value="'.$r['id'].'">'.$r['nick'].'</option>"';
}
echo '</select> ';

$s = mysql_query("SELECT id, nick FROM legacy_headz_medlem ORDER BY id ASC") or die (mysql_error());
echo '<select name="team_headz_10" class="login_line">';
echo '<option value="0" selected>Medlem #10</option>';
while ($r = mysql_fetch_array($s)) {
echo '<option value="'.$r['id'].'">'.$r['nick'].'</option>"';
}
echo '</select> ';

?>
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Motståndare aktiva i matchen:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
				<input name="team_opponent_1" type="text" style="width: 93;" autocomplete="off" class="shout_line">
				<input name="team_opponent_2" type="text" style="width: 93;" autocomplete="off" class="shout_line">
				<input name="team_opponent_3" type="text" style="width: 93;" autocomplete="off" class="shout_line">
				<input name="team_opponent_4" type="text" style="width: 93;" autocomplete="off" class="shout_line">
				<input name="team_opponent_5" type="text" style="width: 93;" autocomplete="off" class="shout_line">
				<input name="team_opponent_6" type="text" style="width: 93;" autocomplete="off" class="shout_line">
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Resultat: ( </span><span class='admin_orange2'>HeadZ</span><span class='admin_gray'> : </span><span class='admin_orange2'>Motståndare</span><span class='admin_gray'> )</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
				<input name="result_headz" type="text" style="width: 35;" autocomplete="off" class="shout_line">:<input name="result_opponent" type="text" style="width: 35;" autocomplete="off" class="shout_line" />
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Liga: ( och URL om det finns )</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
				<input name="league" type="text" style="width: 125;" autocomplete="on" class="shout_line" /> <input name="league_url" type="text" style="width: 200;" autocomplete="off" class="shout_line" />
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Map:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="map" class="login_line">
<option selected>-- Map --</option>
<option value="de_aztec">de_cpl_strike</option>
<option value="de_cbble">de_cbble</option>
<option value="de_contra">de_contra</option>
<option value="de_cpl_fire">de_cpl_fire</option>
<option value="de_cpl_mill">de_cpl_mill</option>
<option value="de_cpl_strike">de_cpl_strike</option>
<option value="de_dust2">de_dust2</option>
<option value="de_dust">de_dust</option>
<option value="de_forge">de_forge</option>
<option value="de_inferno">de_inferno</option>
<option value="de_nuke">de_nuke</option>
<option value="de_phoenix">de_phoenix</option>
<option value="de_prodigy">de_prodigy</option>
<option value="de_russka">de_russka</option>
<option value="de_season">de_season</option>
<option value="de_train">de_train</option>
<option value="de_tuscan">de_tuscan</option>
</select>
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Server: ( ange URL till deras server om det finns, behöver inte till våran )</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="server" class="login_line">
<option value="1" selected>Våran</option>
<option value="2">Deras</option>
</select>
				<input name="server_url" type="text" style="width: 200;" autocomplete="off" class="shout_line" />
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Admin(s):</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="admins" class="login_line">
<option value="0" selected>Inte tillgänglig</option>
<option value="1">Vi</option>
<option value="2">Dom</option>
</select>
			</td></tr>
			<tr><td class='admin_nyheter_add_text' valign='center' height='20'>
				<span class='admin_gray'>Demo:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="demo" class="login_line">
<option value="0" selected>Inte tillgänglig</option>
<option value="1">HLTV</option>
</select>
				<input name="demofile" type="text" style="width: 200;" autocomplete="off" class="shout_line" />
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text' valign='center'>
				<script type="text/javascript" src="js/editor.js"></script>

					<a href="#" onclick="insert_text('^^', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/orc%5E%5E.gif" alt="^^" title="Happy Orc" height="22" hspace="2" vspace="2" width="23" border="0"></a>
					<a href="#" onclick="insert_text(':D', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_biggrin.gif" alt=":D" title="Very Happy" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_smile.gif" alt=":)" title="Smile" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':lol:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_lol.gif" alt=":lol:" title="Laughing" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':P', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_razz.gif" alt=":P" title="Razz" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':mrgreen:', true); return false;" style="line-height: 20px;">	<img src="forum/images/smilies/icon_mrgreen.gif" alt=":mrgreen:" title="Mr. Green" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(';)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_wink.gif" alt=";)" title="Wink" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':S', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_confused.gif" alt=":S" title="Confused" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':(', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_sad.gif" alt=":(" title="Sad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':cry:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_cry.gif" alt=":cry:" title="Neutral" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':|', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_neutral.gif" alt=":|" title="Neutral" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':$', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_redface.gif" alt=":$" title="Embarassed" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text('8)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_cool.gif" alt="8)" title="Cool" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':O', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_surprised.gif" alt=":O" title="Surprised" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':eek:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_eek.gif" alt=":eek:" title="Shocked" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':rolleyes:', true); return false;" style="line-height: 20px;"><img src="forum/images/smilies/icon_rolleyes.gif" alt=":rolleyes:" title="Rolling Eyes" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':mad:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_mad.gif" alt=":mad:" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':X', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_evil.gif" alt=":X" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':@', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_twisted.gif" alt=":@" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>

			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>

				<input class="btnbbcode" title="Fet text: [b]text[/b]" style="font-weight: bold;" type="button" name="bold"
				onclick="javascript:addtag('b');" value="b" />

				<input class="btnbbcode" title="Kursiv text: [i]text[/i]" style="font-style: italic;" type="button" name="italic"
				onclick="javascript:addtag('i');" value="i" />

				<input class="btnbbcode" title="Understruken text: [u]text[/u]" style="text-decoration: underline;" type="button" name="inserted"
				onclick="javascript:addtag('u');" value="u" />

				<input class="btnbbcode" title="!!OBS, NAMNET MÅSTE VARA MED!! Citera text: [quote=namnet]text[/quote]" type="button" name="blockquote"
				onclick="javascript:addtag('quote');" value="Quote" />

				<input class="btnbbcode" title="Infoga länk: [url=http://url]Länktext[/url]" style="text-decoration: underline;" type="button" name="link"
				onclick="javascript:addtag('url');" value="URL" />

				<input class="btnbbcode" title="!!ENDAST TILL FILER PÅ HEMSIDAN!! Infoga länk: [url2=http://url]Länktext[/url2]" style="text-decoration: underline;" type="button" name="link2"
				onclick="javascript:addtag('url2');" value="URL(intern)" />

				<input class="btnbbcode" title="Infoga bild: [img]http://bild_url.jpg[/img]" type="button" name="image"
				onclick="javascript:addtag('img');" value="Img" />

			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text' valign='center'>
				<span class='admin_gray'>Sammandrag:</span>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_text_text' valign='center'>
				<textarea name="text" title="Text" style="width: 100%; height: 75;" class="shout_line"></textarea>
			</td></tr>
			<tr><td height='20' class='admin_nyheter_add_send' valign='center' align='center'>
				<input class="mainoption" type="submit" title="Lägg till Matchen!" value="Lägg till Matchen!">&nbsp;
				<input class="liteoption" type="reset" title="Rensa formuläret!" value="Rensa" onClick="return confirm('Är du säker på att du vill rensa?')"/>
				</form>
			</td></tr>
			</table>

		</td></tr>
		</table>

	</td></tr>
	</table>

<?php


$resultat = mysql_query("SELECT * FROM legacy_headz_players WHERE user='{$_SESSION['sess_user']}'") or die (mysql_error());
if($row = mysql_fetch_array($resultat)) {
$profile = $row['user'];
$profile_id = $row['profile_id'];

//Då ?addnews=$id&add står i adressfältet kommer följande kod att laddas
if(isset($_GET['add'])) {

//Lägger in dagens datum i variabeln $datum som ex 1/9-04
// $date = date('H:i @ d/m-y'); // 00:05 @ 05/03-07
$date = $_POST['hour'].':'.$_POST['minute'].' @ '.$_POST['day'].'/'.$_POST['month'].'-'.$_POST['year'];
/*
//SQL kod för att lägga in i databasen
mysql_query("INSERT INTO legacy_headz_matcher ('profile', 'profile_id', 'game', 'opponent', 'on', 'result_headz', 'result_opponent', 'date', 'league', 'league_url', 'map', 'team_headz_1', 'team_headz_2', 'team_headz_3', 'team_headz_4', 'team_headz_5', 'team_headz_6', 'team_opponent_1', 'team_opponent_2', 'team_opponent_3', 'team_opponent_4', 'team_opponent_5', 'team_opponent_6', 'server', 'server_url', 'demo', 'demo_id', 'text') VALUES (
			'".$profile."',
			'".$profile_id."',
			'".$_POST['game']."',
			'".$_POST['opponent']."',
			'".$_POST['on']."',
			'".$_POST['result_headz']."',
			'".$_POST['result_opponent']."',
			'".$date."',
			'".$_POST['league']."',
			'".$_POST['league_url']."',
			'".$_POST['map']."',
			'".$_POST['team_headz_1']."',
			'".$_POST['team_headz_2']."',
			'".$_POST['team_headz_3']."',
			'".$_POST['team_headz_4']."',
			'".$_POST['team_headz_5']."',
			'".$_POST['team_headz_6']."',
			'".$_POST['team_opponent_1']."',
			'".$_POST['team_opponent_2']."',
			'".$_POST['team_opponent_3']."',
			'".$_POST['team_opponent_4']."',
			'".$_POST['team_opponent_5']."',
			'".$_POST['team_opponent_6']."',
			'".$_POST['server']."',
			'".$_POST['server_url']."',
			'".$_POST['demo']."',
			'".$_POST['demo']."',
			'".$_POST['text']."')") or exit(mysql_error());
*/
   //Skickar dig vidare till det inskrivna efter location:
   header('location: admin_matcher.php');
  }
 }
}

//Kod för att ändra en nyhet med vald ID
if(isset($_GET['editmatch'])) {

//Denna kod hämtar ut informationen som vi ska ändra på
$qnyheter = mysql_query("SELECT * FROM legacy_headz_matcher WHERE id=".$_GET['editmatch']) or exit(mysql_error());
if($snyheter = mysql_fetch_array($qnyheter)) {
$id = $snyheter['id'];
$profile = $snyheter['profile'];
$profile_id = $snyheter['profile_id'];
$opponent = $snyheter['opponent'];
$date = $snyheter['date'];
$game = $snyheter['game'];
$on = $snyheter['on'];
$league = $snyheter['league'];
$league_url = $snyheter['league_url'];
$map = $snyheter['map'];
$text = $snyheter['text'];
$result_headz = $snyheter['result_headz'];
$result_opponent = $snyheter['result_opponent'];
$team_headz_1 = $snyheter['team_headz_1'];
$team_headz_2 = $snyheter['team_headz_2'];
$team_headz_3 = $snyheter['team_headz_3'];
$team_headz_4 = $snyheter['team_headz_4'];
$team_headz_5 = $snyheter['team_headz_5'];
$team_headz_6 = $snyheter['team_headz_6'];
$team_headz_id_1 = $snyheter['team_headz_id_1'];
$team_headz_id_2 = $snyheter['team_headz_id_2'];
$team_headz_id_3 = $snyheter['team_headz_id_3'];
$team_headz_id_4 = $snyheter['team_headz_id_4'];
$team_headz_id_5 = $snyheter['team_headz_id_5'];
$team_headz_id_6 = $snyheter['team_headz_id_6'];
$team_opponent_1 = $snyheter['team_opponent_1'];
$team_opponent_2 = $snyheter['team_opponent_2'];
$team_opponent_3 = $snyheter['team_opponent_3'];
$team_opponent_4 = $snyheter['team_opponent_4'];
$team_opponent_5 = $snyheter['team_opponent_5'];
$team_opponent_6 = $snyheter['team_opponent_6'];
$server = $snyheter['server'];
$server_url = $snyheter['server_url'];
$demo = $snyheter['demo'];
$demo_id = $snyheter['demo_id'];

//Följande kod är ett forumlär med dess respektive information i sig
?>


			<table width='100%' cellspacing='0' cellpadding='0' border='0' valign='top'>
			<tr>
			<td width='100%' height='25' valign='center' align='center' class='historia_rubrik'>
				<span class='admin_gray'>[ </span><a href="admin_matcher.php" title="Gå tillbaka" target="main" class='admin_orange'>Ändra Match</a><span class='admin_gray'> ]</a></span><br>

			</td>
			</tr>
			<tr>
			<td width='100%' valign='top' class='admin_bg'>

				<table width='100%' height='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr>
				<td valign='top'>

					<table width='100%' cellspacing='0' cellpadding='0' border='0'>
					<tr>

					<td class='admin_nyheter_add_rubrik' valign='center' height='20'>
						<span class='admin_gray'>Motståndare:</span>
					</td>

					</tr><tr>

					<td height='20' class='admin_nyheter_add_text_text' valign='center'>
						<form action='admin_matcher.php?addmatch&add' id='add' method='post' style="margin-bottom:0;">
						<input name="opponent" type="text" style="width: 200;" autocomplete="on" value='<?php echo $opponent; ?>' class="shout_line" />
					</td>

					</tr><tr>

					<td class='admin_nyheter_add_text' valign='center' height='20'>
						<span class='admin_gray'>Datum: ( <span class='admin_orange2'>Timme</span>:<span class='admin_orange2'>Minut</span> @ <span class='admin_orange2'>Dag</span>/<span class='admin_orange2'>Månad</span>-<span class='admin_orange2'>År</span> )</span>
					</td>

					</tr><tr>

					<td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="hour" class="login_line">
<option value="00" selected>00</option>
<option value="01">01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
</select>:<select name="minute" class="login_line">
<option value="00" selected>00</option>
<option value="05">05</option>
<option value="10">10</option>
<option value="15">15</option>
<option value="20">20</option>
<option value="25">25</option>
<option value="30">30</option>
<option value="35">35</option>
<option value="40">40</option>
<option value="45">45</option>
<option value="50">50</option>
<option value="55">55</option>
</select> @ <select name="day" class="login_line">
<option value="01" selected>01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
</select>/<select name="month" class="login_line">
<option value="01" selected>01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
</select>-<select name="year" class="login_line">
<option value="08" selected>2008</option>
</select>
					</td>

					</tr><tr>

					<td class='admin_nyheter_add_text' valign='center' height='20'>
						<span class='admin_gray'>Spel:</span>
					</td>

					</tr><tr>

					<td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="game" class="login_line">
<option value="1" <?php if($game==1){ echo "selected"; } ?>>CS:S</option>
<option value="2" <?php if($game==2){ echo "selected"; } ?>>COD 4</option>
<option value="3" <?php if($game==3){ echo "selected"; } ?>>Q3</option>
<option value="4" <?php if($game==4){ echo "selected"; } ?>>WC3</option>
</select>
					</td>

					</tr><tr>

					<td class='admin_nyheter_add_text' valign='center' height='20'>
						<span class='admin_orange2'>X</span><span class='admin_gray'>on</span><span class='admin_orange2'>X</span><span class='admin_gray'>:</span>
					</td>

					</tr><tr>

					<td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="on" class="login_line">
<option value="1" <?php if($on==1){ echo "selected"; } ?>>1on1</option>
<option value="2" <?php if($on==2){ echo "selected"; } ?>>2on2</option>
<option value="3" <?php if($on==3){ echo "selected"; } ?>>3on3</option>
<option value="4" <?php if($on==4){ echo "selected"; } ?>>4on4</option>
<option value="5" <?php if($on==5){ echo "selected"; } ?>>5on5</option>
<option value="6" <?php if($on==6){ echo "selected"; } ?>>6on6</option>
</select>
					</td>

					</tr><tr>

					<td class='admin_nyheter_add_text' valign='center' height='20'>
						<span class='admin_gray'>Liga: ( och URL om det finns )</span>
					</td>

					</tr><tr>

					<td height='20' class='admin_nyheter_add_text_text' valign='center'>
						<input name="league" type="text" style="width: 125;" autocomplete="on" value='<?php echo $league; ?>' class="shout_line" /> <input name="league_url" type="text" style="width: 35%;" autocomplete="off" value='<?php echo $league_url; ?>' class="shout_line" />
					</td>

					</tr><tr>

					<td class='admin_nyheter_add_text' valign='center' height='20'>
						<span class='admin_gray'>Map:</span>
					</td>

					</tr><tr>

					<td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="map" class="login_line">
<option value="de_aztec" <?php if($map=="de_aztec"){ echo "selected"; } ?>>de_aztec</option>
<option value="de_cbble" <?php if($map=="de_cbble"){ echo "selected"; } ?>>de_cbble</option>
<option value="de_contra" <?php if($map=="de_contra"){ echo "selected";	} ?>>de_contra</option>
<option value="de_cpl_fire" <?php if($map=="de_cpl_fire"){ echo "selected"; } ?>>de_cpl_fire</option>
<option value="de_cpl_mill" <?php if($map=="de_cpl_mill"){ echo "selected"; } ?>>de_cpl_mill</option>
<option value="de_cpl_strike" <?php if($map=="de_cpl_strike"){ echo "selected"; } ?>>de_cpl_strike</option>
<option value="de_dust2" <?php if($map=="de_dust2"){ echo "selected"; } ?>>de_dust2</option>
<option value="de_dust" <?php if($map=="de_dust"){ echo "selected"; } ?>>de_dust</option>
<option value="de_forge" <?php if($map=="de_forge"){ echo "selected"; } ?>>de_forge</option>
<option value="de_inferno" <?php if($map=="de_inferno"){ echo "selected"; } ?>>de_inferno</option>
<option value="de_nuke" <?php if($map=="de_nuke"){ echo "selected"; } ?>>de_nuke</option>
<option value="de_phoenix" <?php if($map=="de_phoenix"){ echo "selected"; } ?>>de_phoenix</option>
<option value="de_prodigy" <?php if($map=="de_prodigy"){ echo "selected"; } ?>>de_prodigy</option>
<option value="de_russka" <?php if($map=="de_russka"){ echo "selected";	} ?>>de_russka</option>
<option value="de_season" <?php if($map=="de_season"){ echo "selected";	} ?>>de_season</option>
<option value="de_train" <?php if($map=="de_train"){ echo "selected"; } ?>>de_train</option>
<option value="de_tuscan" <?php if($map=="de_tuscan"){ echo "selected";	} ?>>de_tuscan</option>
</select>
					</td>

					</tr><tr>

					<td class='admin_nyheter_add_text' valign='center' height='20'>
						<span class='admin_gray'>Resultat: ( </span><span class='admin_orange2'>HeadZ</span><span class='admin_gray'> : </span><span class='admin_orange2'>Motståndare</span><span class='admin_gray'> )</span>
					</td>

					</tr><tr>

					<td height='20' class='admin_nyheter_add_text_text' valign='center'>
						<input name="result_headz" type="text" style="width: 35;" autocomplete="off" class="shout_line" value='<?php echo $result_headz; ?>' /> <input name="result_opponent" type="text" style="width: 35;" autocomplete="off" value='<?php echo $result_opponent; ?>' class="shout_line" />
					</td>

					</tr><tr>

					<td class='admin_nyheter_add_text' valign='center' height='20'>
						<span class='admin_gray'>HeadZ medlemmar aktiva i matchen:</span>
					</td>

					</tr><tr>

					<td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="team_headz_1" class="login_line">
<option value="0">Medlem #1</option>
<option value="1" <?php if($team_headz_1=="Tulley"){ echo "selected"; } ?>>Tulley</option>
<option value="2" <?php if($team_headz_1=="xep"){ echo "selected"; } ?>>xep</option>
<option value="3" <?php if($team_headz_1=="StarZinger"){ echo "selected"; } ?>>StarZinger</option>
<option value="4" <?php if($team_headz_1=="XFire"){ echo "selected"; } ?>>XFire</option>
<option value="5" <?php if($team_headz_1=="lillemil"){ echo "selected"; } ?>>lillemil</option>
<option value="6" <?php if($team_headz_1=="AhL"){ echo "selected"; } ?>>AhL</option>
<option value="7" <?php if($team_headz_1=="Jigz"){ echo "selected"; } ?>>Jigz</option>
<option value="8" <?php if($team_headz_1=="Iiggi"){ echo "selected"; } ?>>Iiggi</option>
<option value="9" <?php if($team_headz_1=="LittleFriend"){ echo "selected"; } ?>>LittleFriend</option>
<option value="10" <?php if($team_headz_1=="Sås"){ echo "selected"; } ?>>Sås</option>
</select>
<select name="team_headz_2" class="login_line">
<option value="0">Medlem #2</option>
<option value="1" <?php if($team_headz_2=="Tulley"){ echo "selected"; } ?>>Tulley</option>
<option value="2" <?php if($team_headz_2=="xep"){ echo "selected"; } ?>>xep</option>
<option value="3" <?php if($team_headz_2=="StarZinger"){ echo "selected"; } ?>>StarZinger</option>
<option value="4" <?php if($team_headz_2=="XFire"){ echo "selected"; } ?>>XFire</option>
<option value="5" <?php if($team_headz_2=="lillemil"){ echo "selected"; } ?>>lillemil</option>
<option value="6" <?php if($team_headz_2=="AhL"){ echo "selected"; } ?>>AhL</option>
<option value="7" <?php if($team_headz_2=="Jigz"){ echo "selected"; } ?>>Jigz</option>
<option value="8" <?php if($team_headz_2=="Iiggi"){ echo "selected"; } ?>>Iiggi</option>
<option value="9" <?php if($team_headz_2=="LittleFriend"){ echo "selected"; } ?>>LittleFriend</option>
<option value="10" <?php if($team_headz_2=="Sås"){ echo "selected"; } ?>>Sås</option>
</select>
<select name="team_headz_3" class="login_line">
<option value="0">Medlem #3</option>
<option value="1" <?php if($team_headz_3=="Tulley"){ echo "selected"; } ?>>Tulley</option>
<option value="2" <?php if($team_headz_3=="xep"){ echo "selected"; } ?>>xep</option>
<option value="3" <?php if($team_headz_3=="StarZinger"){ echo "selected"; } ?>>StarZinger</option>
<option value="4" <?php if($team_headz_3=="XFire"){ echo "selected"; } ?>>XFire</option>
<option value="5" <?php if($team_headz_3=="lillemil"){ echo "selected"; } ?>>lillemil</option>
<option value="6" <?php if($team_headz_3=="AhL"){ echo "selected"; } ?>>AhL</option>
<option value="7" <?php if($team_headz_3=="Jigz"){ echo "selected"; } ?>>Jigz</option>
<option value="8" <?php if($team_headz_3=="Iiggi"){ echo "selected"; } ?>>Iiggi</option>
<option value="9" <?php if($team_headz_3=="LittleFriend"){ echo "selected"; } ?>>LittleFriend</option>
<option value="10" <?php if($team_headz_3=="Sås"){ echo "selected"; } ?>>Sås</option>
</select>
<select name="team_headz_4" class="login_line">
<option value="0">Medlem #4</option>
<option value="1" <?php if($team_headz_4=="Tulley"){ echo "selected"; } ?>>Tulley</option>
<option value="2" <?php if($team_headz_4=="xep"){ echo "selected"; } ?>>xep</option>
<option value="3" <?php if($team_headz_4=="StarZinger"){ echo "selected"; } ?>>StarZinger</option>
<option value="4" <?php if($team_headz_4=="XFire"){ echo "selected"; } ?>>XFire</option>
<option value="5" <?php if($team_headz_4=="lillemil"){ echo "selected"; } ?>>lillemil</option>
<option value="6" <?php if($team_headz_4=="AhL"){ echo "selected"; } ?>>AhL</option>
<option value="7" <?php if($team_headz_4=="Jigz"){ echo "selected"; } ?>>Jigz</option>
<option value="8" <?php if($team_headz_4=="Iiggi"){ echo "selected"; } ?>>Iiggi</option>
<option value="9" <?php if($team_headz_4=="LittleFriend"){ echo "selected"; } ?>>LittleFriend</option>
<option value="10" <?php if($team_headz_4=="Sås"){ echo "selected"; } ?>>Sås</option>
</select>
<select name="team_headz_5" class="login_line">
<option value="0">Medlem #5</option>
<option value="1" <?php if($team_headz_5=="Tulley"){ echo "selected"; } ?>>Tulley</option>
<option value="2" <?php if($team_headz_5=="xep"){ echo "selected"; } ?>>xep</option>
<option value="3" <?php if($team_headz_5=="StarZinger"){ echo "selected"; } ?>>StarZinger</option>
<option value="4" <?php if($team_headz_5=="XFire"){ echo "selected"; } ?>>XFire</option>
<option value="5" <?php if($team_headz_5=="lillemil"){ echo "selected"; } ?>>lillemil</option>
<option value="6" <?php if($team_headz_5=="AhL"){ echo "selected"; } ?>>AhL</option>
<option value="7" <?php if($team_headz_5=="Jigz"){ echo "selected"; } ?>>Jigz</option>
<option value="8" <?php if($team_headz_5=="Iiggi"){ echo "selected"; } ?>>Iiggi</option>
<option value="9" <?php if($team_headz_5=="LittleFriend"){ echo "selected"; } ?>>LittleFriend</option>
<option value="10" <?php if($team_headz_5=="Sås"){ echo "selected"; } ?>>Sås</option>
</select>
<select name="team_headz_6" class="login_line">
<option value="0">Medlem #6</option>
<option value="1" <?php if($team_headz_6=="Tulley"){ echo "selected"; } ?>>Tulley</option>
<option value="2" <?php if($team_headz_6=="xep"){ echo "selected"; } ?>>xep</option>
<option value="3" <?php if($team_headz_6=="StarZinger"){ echo "selected"; } ?>>StarZinger</option>
<option value="4" <?php if($team_headz_6=="XFire"){ echo "selected"; } ?>>XFire</option>
<option value="5" <?php if($team_headz_6=="lillemil"){ echo "selected"; } ?>>lillemil</option>
<option value="6" <?php if($team_headz_6=="AhL"){ echo "selected"; } ?>>AhL</option>
<option value="7" <?php if($team_headz_6=="Jigz"){ echo "selected"; } ?>>Jigz</option>
<option value="8" <?php if($team_headz_6=="Iiggi"){ echo "selected"; } ?>>Iiggi</option>
<option value="9" <?php if($team_headz_6=="LittleFriend"){ echo "selected"; } ?>>LittleFriend</option>
<option value="10" <?php if($team_headz_6=="Sås"){ echo "selected"; } ?>>Sås</option>
</select>
					</td>

					</tr><tr>

					<td class='admin_nyheter_add_text' valign='center' height='20'>
						<span class='admin_gray'>Motståndare aktiva i matchen:</span>
					</td>

					</tr><tr>

					<td height='20' class='admin_nyheter_add_text_text' valign='center'>
						<input name="team_opponent_1" type="text" style="width: 93;" autocomplete="off" value='<?php echo $team_opponent_1; ?>' class="shout_line" />
						<input name="team_opponent_2" type="text" style="width: 93;" autocomplete="off" value='<?php echo $team_opponent_2; ?>' class="shout_line" />
						<input name="team_opponent_3" type="text" style="width: 93;" autocomplete="off" value='<?php echo $team_opponent_3; ?>' class="shout_line" />
						<input name="team_opponent_4" type="text" style="width: 93;" autocomplete="off" value='<?php echo $team_opponent_4; ?>' class="shout_line" />
						<input name="team_opponent_5" type="text" style="width: 93;" autocomplete="off" value='<?php echo $team_opponent_5; ?>' class="shout_line" />
						<input name="team_opponent_6" type="text" style="width: 93;" autocomplete="off" value='<?php echo $team_opponent_6; ?>' class="shout_line" />
					</td>

					</tr><tr>

					<td class='admin_nyheter_add_text' valign='center' height='20'>
						<span class='admin_gray'>Server: ( ange URL till deras server om det finns, behöver inte till våran )</span>
					</td>

					</tr><tr>

					<td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="server" class="login_line">
<option value="1" <?php if($server==1){ echo "selected"; } ?>>Våran</option>
<option value="2" <?php if($server==2){ echo "selected"; } ?>>Deras</option>
</select>
<input name="server_url" type="text" style="width: 30%;" autocomplete="off" value='<?php echo $server_url; ?>' class="shout_line" />
					</td>

					</tr><tr>

					<td class='admin_nyheter_add_text' valign='center' height='20'>
						<span class='admin_gray'>Demo:</span>
					</td>

					</tr><tr>

					<td height='20' class='admin_nyheter_add_text_text' valign='center'>
<select name="demo" class="login_line">
<option value="0" <?php if($demo==0){ echo "selected"; } ?>>Inte tillgänglig</option>
<option value="1" <?php if($demo==1){ echo "selected"; } ?>>HLTV</option>
</select>
<input name="demofile" type="text" style="width: 200;" autocomplete="off" class="shout_line" />
					</td>

					</tr><tr>

					<td height='20' class='admin_nyheter_add_text' valign='center'>
						<span class='admin_gray'>Sammandrag:</span>
					</td>

					</tr><tr>

					<td height='20' class='admin_nyheter_add_text_text' valign='center'>
						<textarea name="text" title="Text" autocomplete="off" rows="4" style="width: 100%;" class="shout_line"><?php echo $text; ?></textarea>
					</td>

					</tr><tr>

					<td height='20' class='admin_nyheter_add_send' valign='center' align='center'>
						<input class="mainoption" type="submit" title="Ändra Matchen!" value="Ändra Matchen!" />
						</form>
					</td>

					</tr>
					</table>


<?php
//Då ?editnews=$id&update står i adressfältet kommer följande kod att laddas
if(isset($_GET['update'])) {
/*
//SQL kod för att lägga in i databasen
mysql_query("UPDATE legacy_headz_matcher SET
			'$profile',
			'$profile_id',
			'".$_POST['game']."',
			'".$_POST['opponent']."',
			'".$_POST['on']."',
			'".$_POST['result_headz']."',
			'".$_POST['result_opponent']."',
			'".$_POST['date']."',
			'".$_POST['league']."',
			'".$_POST['league_url']."',
			'".$_POST['map']."',
			'".$_POST['team_headz_1']."',
			'".$_POST['team_headz_2']."',
			'".$_POST['team_headz_3']."',
			'".$_POST['team_headz_4']."',
			'".$_POST['team_headz_5']."',
			'".$_POST['team_headz_6']."',
			'".$_POST['team_opponent_1']."',
			'".$_POST['team_opponent_2']."',
			'".$_POST['team_opponent_3']."',
			'".$_POST['team_opponent_4']."',
			'".$_POST['team_opponent_5']."',
			'".$_POST['team_opponent_6']."',
			'".$_POST['server']."',
			'".$_POST['server_url']."',
			'".$_POST['demo']."',
			'".$_POST['demo_id']."',
			'".$_POST['text']."'
			WHERE id=".$_GET['editmatch']) or exit(mysql_error());
*/
	//Skickar dig vidare till det inskrivna efter location:
	header('location: admin_matcher.php');
  }
 }
}

//Kod för att ta bort en nyhet
if(isset($_GET['deletematch'])) {

//Först hämtar vi information om den nyheten vi vill ta bort, vilket vi gjorde med denna kod
$qdelete = mysql_query("SELECT * FROM legacy_headz_matcher WHERE id=".$_GET['deletematch']) or exit(mysql_error());
$sdelete = mysql_fetch_array($qdelete);
$game = $sdelete['game'];

//En bekräftningsfråga om du vill ta bort nyheten
echo		"<table width='100%' cellspacing='0' cellpadding='0' border='0' valign='top'>
			<tr>
			<td width='100%' height='25' valign='center' align='center' class='historia_rubrik'>";
echo"<span class='admin_gray'>[ ";
echo"Är du säker på att du vill ta bort matchen: </span><span class='admin_orange2'>";
echo $sdelete['opponent'];
echo "</span> ";
if($game == 1) {
	echo "<img src='images/icons/icon_css.gif' border='0' width='16' height='16' />";
} elseif($game == 2) {
	echo "<img src='images/icons/icon_cod4.gif' border='0' width='16' height='16' />";
} elseif($game == 3) {
	echo "<img src='images/icons/icon_q3.gif' border='0' width='16' height='16' />";
} elseif($game == 4) {
	echo "<img src='images/icons/icon_wc3.gif' border='0' width='16' height='16' />";
}
echo " <span class='admin_gray'> ? ]<br /><br />";
echo "<span class='admin_orange'><a href='admin_matcher.php?deletematch=";
echo $sdelete['id'];
echo "&delete'>Ja</a><span class='admin_gray'> | </span><span class='admin_orange'><a href='admin_matcher.php' target='main'>Nej</a></span>";
echo		"</td>
			</tr>
			</table>";

//Då ?deletenews=$id&delete står i adressfältet kommer följande kod att laddas
if(isset($_GET['delete'])) {
/*
    mysql_query("DELETE FROM legacy_headz_matcher WHERE id=".$_GET['deletematch']) or exit(mysql_error());
#	mysql_query("DELETE FROM kommentarer WHERE nid=".$_GET['deletematch']) or exit(mysql_error());
*/
  //Skickar dig vidare till det inskrivna efter location:
  header('location: admin_matcher.php');
 }
}

//Om QUERY_STRING är tom laddas denna text, kolla under resurser och Server variablar för mer information
if(empty($_SERVER['QUERY_STRING'])) {
?>
			<table width='100%' cellspacing='0' cellpadding='0' border='0' valign='top'>
			<tr>
			<td width='100%' height='25' valign='center' align='center' class='historia_rubrik'>
				<span class='medlemmar_text_citat'>[ </span><span class='medlemmar_text_citat_info'>Matcher</span><span class='medlemmar_text_citat'> ]</span>
			</td>
			</tr>
			<tr>
			<td width='100%' valign='top' class='admin_bg'>

				<table width='100%' height='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr>
				<td valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' border='0'>
					<tr>
					<td class='admin_nyheter_add' colspan='4' valign='center' align='center' height='20'>
<?php
echo "<span class='admin_gray'>[ </span>";
echo "<a href='admin_matcher.php?addmatch' title='Lägg till en ny Match' target='main' class='admin_orange'>Lägg till Match</a>";
echo "<span class='admin_gray'> ]</span><br>";
?>
					</td>
					</tr>

<?php


$color = 1;

//Här hämtar vi ut all information om alla nyheter och loopar ut dem, dvs skriver ut alla nyheter efter varandra
$qnyheter = mysql_query("SELECT * FROM legacy_headz_matcher WHERE profile = '{$_SESSION['sess_user']}' OR '{$_SESSION['sess_user']}' = 'Sås' ORDER BY id DESC") or die (mysql_error());
while($snyheter = mysql_fetch_array($qnyheter)) {
$id = $snyheter['id'];
$game = $snyheter['game'];
$opponent = $snyheter['opponent'];
$on = $snyheter['on'];
$date = $snyheter['date'];
$profile = $snyheter['profile'];
$profile_id = $snyheter['profile_id'];




if($color == 1){
echo				"<tr>
					<td height='19' class='admin_nyheter' valign='center'>
						<table height='19' cellspacing='0' cellpadding='0' border='0'>
						<tr>
						<td valign='center'>";
if($game == 1) {
	echo "<img src='images/icons/icon_css.gif' border='0' width='16' height='16' />";
} elseif($game == 2) {
	echo "<img src='images/icons/icon_cod4.gif' border='0' width='16' height='16' />";
} elseif($game == 3) {
	echo "<img src='images/icons/icon_q3.gif' border='0' width='16' height='16' />";
} elseif($game == 4) {
	echo "<img src='images/icons/icon_wc3.gif' border='0' width='16' height='16' />";
}
echo					"</td><td>";
echo					"<span class='admin_gray'>&nbsp;&nbsp;&nbsp;[ </span>";
echo					"<a href='admin_matcher.php?editmatch=$id' title='Ändra Match #$id' class='admin_orange'>$opponent</a>";
echo					"<span class='admin_gray'> ]</span></td>
						</tr>
						</table>
					</td>
					<td width='160' height='20' class='admin_nyheter_date' valign='center' align='center'>";
echo					"<span class='admin_gray'>[ </span>";
echo					"<span class='admin_tid'>$date</span>";
echo					"<span class='admin_gray'> ]</span><br>";
echo				"</td>
					<td width='100' height='20' class='admin_nyheter_edit' valign='center' align='center'>";
echo					"<span class='admin_gray'>[ </span>";
echo					"<a href='medlemmar_info.php?medlem=$profile_id' class='admin_orange'>$profile</a>";
echo					"<span class='admin_gray'> ]</span><br>";
echo				"</td>
					<td width='45' height='20' class='admin_nyheter_remove' valign='center' align='center'>
						<a href='admin_matcher.php?deletematch=$id' title='Ta bort Match #$id'><img src='images/icons/icon_admin_remove.gif' border='0' width='18' height='16' vspace='0' /></a><br>
					</td>
					</tr>";
// Set $color==2, for switching to other color
$color = 2;
} else {
echo				"<tr>
					<td height='19' class='admin_nyheter_dark' valign='center'>
						<table height='19' cellspacing='0' cellpadding='0' border='0'>
						<tr>
						<td valign='center'>";
if($game == 1) {
	echo "<img src='images/icons/icon_css.gif' border='0' width='16' height='16' />";
} elseif($game == 2) {
	echo "<img src='images/icons/icon_cod4.gif' border='0' width='16' height='16' />";
} elseif($game == 3) {
	echo "<img src='images/icons/icon_q3.gif' border='0' width='16' height='16' />";
} elseif($game == 4) {
	echo "<img src='images/icons/icon_wc3.gif' border='0' width='16' height='16' />";
}
echo					"</td><td>";
echo					"<span class='admin_gray'>&nbsp;&nbsp;&nbsp;[ </span>";
echo					"<a href='admin_matcher.php?editmatch=$id' title='Ändra Match #$id' class='admin_orange'>$opponent</a>";
echo					"<span class='admin_gray'> ]</span></td>
						</tr>
						</table>
					</td>
					<td width='160' height='20' class='admin_nyheter_date_dark' valign='center' align='center'>";
echo					"<span class='admin_gray'>[ </span>";
echo					"<span class='admin_tid'>$date</span>";
echo					"<span class='admin_gray'> ]</span><br>";
echo				"</td>
					<td width='100' height='20' class='admin_nyheter_edit_dark' valign='center' align='center'>";
echo					"<span class='admin_gray'>[ </span>";
echo					"<a href='medlemmar_info.php?medlem=$profile_id' class='admin_orange'>$profile</a>";
echo					"<span class='admin_gray'> ]</span><br>";
echo				"</td>
					<td width='45' height='20' class='admin_nyheter_remove_dark' valign='center' align='center'>
						<a href='admin_matcher.php?deletematch=$id' title='Ta bort Match #$id'><img src='images/icons/icon_admin_remove.gif' border='0' width='18' height='16' vspace='0' /></a><br>
					</td>
					</tr>";

// Set $color back to 1
$color = 1;

  }
 }
}
echo "</table>\n";
?>

</body>
</html>

<?php
//Kolla under resurser och ob_end_flush()-funktionen för mer information
ob_end_flush();
?>
