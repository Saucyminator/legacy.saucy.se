<?php

function bbkod($text) {

//Funktionen str_replace() drar igenom den valda texten och ersätter med respektive kommando. Sedan funktionen nl2br() lägger automatiskt in <br> vid varje enter slag. Och sist, trim funktionen tar bort onödiga mellanslag samt onödiga enter slag i slutet av texten.

	    $text = htmlentities($text);
	    $text = stripslashes($text);

        $text = str_replace("<", "&lt;", $text);
        $text = str_replace(">", "&gt", $text);
        $text = str_replace('"', "&quot;", $text);

        $text = str_replace("[b]", "<strong>", $text);
        $text = str_replace("[/b]", "</strong>", $text);
        $text = str_replace("[i]", "<em>", $text);
        $text = str_replace("[/i]", "</em>", $text);
        $text = str_replace("[u]", "<ins>", $text);
        $text = str_replace("[/u]", "</ins>", $text);
        $text = str_replace("[s]", "<del>", $text);
        $text = str_replace("[/s]", "</del>", $text);
        $text = preg_replace("#\[url=(.*?)\](.*?)\[/url\]#is", "<span class='text'><a href=\"$1\" target='_blank'>$2</a></span>", $text);
        $text = preg_replace("#\[url2=(.*?)\](.*?)\[/url2\]#is", "<span class='text'><a href=\"$1\" target='main'>$2</a></span>", $text);
        $text = str_replace('[mail=', '<a href="mailto:', $text);
        $text = str_replace('[/mail]', '</a>', $text);
        $text = str_replace('[img]', '<img src="', $text);
        $text = str_replace('[/img]', '">', $text);
        $text = preg_replace("#\[quote=(.*?)\](.*?)\[/quote\]#is", "<div class='quotewrapper'><div class='quotetitle'>$1</div><div class='quotecontent'>$2</div></div>", $text);
        $text = str_replace('[br]', '<br>', $text);
        $text = str_replace('[width]', '" width="', $text);
        $text = str_replace('[height]', '" heigth="', $text);
        $text = str_replace('[size=', '<font size="', $text);
        $text = str_replace('[/size]', '</font>', $text);

        $text = str_replace("[B]", "<strong>", $text);
        $text = str_replace("[/B]", "</strong>", $text);
        $text = str_replace("[I]", "<em>", $text);
        $text = str_replace("[/I]", "</em>", $text);
        $text = str_replace("[U]", "<ins>", $text);
        $text = str_replace("[/U]", "</ins>", $text);
        $text = str_replace("[S]", "<del>", $text);
        $text = str_replace("[/S]", "</del>", $text);
        $text = str_replace('[URL=', '<a href="', $text);
        $text = str_replace('[/URL]', '</a>', $text);
        $text = str_replace('[IMG]', '<img src="', $text);
        $text = str_replace('[/IMG]', '">', $text);
        $text = str_replace('[QUOTE]', '<blockquote>', $text);
        $text = str_replace('[/QUOTE]', '</blockquote>', $text);
        $text = str_replace('[BR]', '<br>', $text);
        $text = str_replace('[WIDTH]', '" width="', $text);
        $text = str_replace('[HEIGTH]', '" heigth="', $text);
        $text = str_replace('[SIZE=', '<font size="', $text);
        $text = str_replace('[/SIZE]', '</font>', $text);

		$text = str_replace("/^^/",'<img src="./forum/images/smilies/orc^^.gif" width="15" height="15" alt="^^" />', $text);
		$text = str_replace("/:|/",'<img src="./forum/images/smilies/icon_neutral.gif" width="15" height="15" alt=":|" />', $text);
		$text = str_replace("/:-|/",'<img src="./forum/images/smilies/icon_neutral.gif" width="15" height="15" alt=":-|" />', $text);
		$text = str_replace("/=|/",'<img src="./forum/images/smilies/icon_neutral.gif" width="15" height="15" alt="=|" />', $text);
		$text = str_replace("/:O/",'<img src="./forum/images/smilies/icon_surprised.gif" width="15" height="15" alt=":O" />', $text);
		$text = str_replace("/:-O/",'<img src="./forum/images/smilies/icon_surprised.gif" width="15" height="15" alt=":-O" />', $text);
		$text = str_replace("/=O/",'<img src="./forum/images/smilies/icon_surprised.gif" width="15" height="15" alt="=O" />', $text);
		$text = str_replace("/:S/",'<img src="./forum/images/smilies/icon_confused.gif" width="15" height="15" alt=":S" />', $text);
		$text = str_replace("/:-S/",'<img src="./forum/images/smilies/icon_confused.gif" width="15" height="15" alt=":-S" />', $text);
		$text = str_replace("/=S/",'<img src="./forum/images/smilies/icon_confused.gif" width="15" height="15" alt="=S" />', $text);
		$text = str_replace("/:)/",'<img src="./forum/images/smilies/icon_smile.gif" width="15" height="15" alt=":)" />', $text);
		$text = str_replace("/:-)/",'<img src="./forum/images/smilies/icon_smile.gif" width="15" height="15" alt=":-)" />', $text);
		$text = str_replace("/=)/",'<img src="./forum/images/smilies/icon_smile.gif" width="15" height="15" alt="=)" />', $text);
		$text = str_replace("/:(/",'<img src="./forum/images/smilies/icon_sad.gif" width="15" height="15" alt=":(" />', $text);
		$text = str_replace("/:-(/",'<img src="./forum/images/smilies/icon_sad.gif" width="15" height="15" alt=":-(" />', $text);
		$text = str_replace("/=(/",'<img src="./forum/images/smilies/icon_sad.gif" width="15" height="15" alt="=(" />', $text);
		$text = str_replace("/:D/",'<img src="./forum/images/smilies/icon_biggrin.gif" width="15" height="15" alt=":D" />', $text);
		$text = str_replace("/:-D/",'<img src="./forum/images/smilies/icon_biggrin.gif" width="15" height="15" alt=":-D" />', $text);
		$text = str_replace("/=D/",'<img src="./forum/images/smilies/icon_biggrin.gif" width="15" height="15" alt="=D" />', $text);
		$text = str_replace("/;)/",'<img src="./forum/images/smilies/icon_wink.gif" width="15" height="15" alt=";)" />', $text);
		$text = str_replace("/;-)/",'<img src="./forum/images/smilies/icon_wink.gif" width="15" height="15" alt=";-)" />', $text);
		$text = str_replace("/:X/",'<img src="./forum/images/smilies/icon_evil.gif" width="15" height="15" alt=":X" />', $text);
		$text = str_replace("/:-X/",'<img src="./forum/images/smilies/icon_evil.gif" width="15" height="15" alt=":-X" />', $text);
		$text = str_replace("/:@/",'<img src="./forum/images/smilies/icon_twisted.gif" width="15" height="15" alt=":@" />', $text);
		$text = str_replace("/:-@/",'<img src="./forum/images/smilies/icon_twisted.gif" width="15" height="15" alt=":-@" />', $text);
		$text = str_replace("/:rolleyes:/",'<img src="./forum/images/smilies/icon_rolleyes.gif" width="15" height="15" alt=":rolleyes:" />', $text);
		$text = str_replace("/:P/",'<img src="./forum/images/smilies/icon_razz.gif" width="15" height="15" alt=":-P" />', $text);
		$text = str_replace("/:-P/",'<img src="./forum/images/smilies/icon_razz.gif" width="15" height="15" alt=":P" />', $text);
		$text = str_replace("/=P/",'<img src="./forum/images/smilies/icon_razz.gif" width="15" height="15" alt="=P" />', $text);

	    $smilies = array('^^',
		 ':|', ':-|', '=|',
		 ':O', ':-O', '=O',
		 ':S', ':-S', '=S',
		 ':)', ':-)', '=)',
		 ':(', ':-(', '=(',
		 ':D', ':-D', '=D',
		 ':P', ':-P', '=P',
		 ';)', ';-)',
		 ':X', ':-X',
		 ':@', ':-@',
		 ':$',
		 '8)',
		 ':cry:',
		 ':mad:',
		 ':lol:',
		 ':eek:',
		 ':mrgreen:',
		 ':rolleyes:');

	    $img = array('orc^^.gif',
		 'icon_neutral.gif', 'icon_neutral.gif', 'icon_neutral.gif',
		 'icon_surprised.gif', 'icon_surprised.gif', 'icon_surprised.gif',
		 'icon_confused.gif', 'icon_confused.gif', 'icon_confused.gif',
		 'icon_smile.gif', 'icon_smile.gif', 'icon_smile.gif',
		 'icon_sad.gif', 'icon_sad.gif', 'icon_sad.gif',
		 'icon_biggrin.gif', 'icon_biggrin.gif', 'icon_biggrin.gif',
		 'icon_razz.gif', 'icon_razz.gif', 'icon_razz.gif',
		 'icon_wink.gif', 'icon_wink.gif',
		 'icon_evil.gif', 'icon_evil.gif',
		 'icon_twisted.gif', 'icon_twisted.gif',
		 'icon_redface.gif',
		 'icon_cool.gif',
		 'icon_cry.gif',
		 'icon_mad.gif',
		 'icon_lol.gif',
		 'icon_eek.gif',
		 'icon_mrgreen.gif',
		 'icon_rolleyes.gif');

	    $text = ' '.$text.' ';

	    $num_smilies = count($smilies);
	    for ($i = 0; $i < $num_smilies; $i++)
	        $text = preg_replace("#(?<=.\W|\W.|^\W)".preg_quote($smilies[$i], '#')."(?=.\W|\W.|\W$)#m",
	                             '$1<img src="./forum/images/smilies/'.$img[$i].'">$2', $text);


        $text = nl2br($text);
        $text = trim($text);

        //Returnerar den genomgågna texten
        return $text;
}

?>
