<?php

require("conn.php");	// Databasanslutningen

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/iframe.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3406958-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>

</head>
<body>


<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
<tr><td width="100%" height="100%" valign="top" align="left" class="main_bg">

<?php

//Kod för att lägga till genom ett forumlär
if(isset($_GET['css'])) {

echo"<table width='100%' cellspacing='0' cellpadding='0' border='0'>
	<tr><td width='25' height='25' valign='middle' align='center' class='matcher_icon_left'>
		<img src='images/icons/icon_css.gif' border='0' width='16' height='16' alt=''>
	</td><td height='25' valign='middle' align='center' class='matcher_icon_bg'>
		<span class='medlemmar_text_citat_info'>Counter-Strike: Source</span>
	</td><td width='25' height='25' valign='middle' align='center' class='matcher_icon_right'>
		<img src='images/icons/icon_css.gif' border='0' width='16' height='16' alt=''>
	</td></tr>
	<tr><td valign='top' align='center' colspan='3'>

		<table width='100%' cellspacing='0' cellpadding='0' border='0' class='matcher_bg'>";


// Define $color=1
$color = "1";


$sql = "SELECT * FROM legacy_headz_matcher ORDER BY id DESC";
$stmt = $conn->prepare($sql);
$stmt->execute();
while ($row = $stmt->fetch()) {
$id = $row['id'];
$game = $row['game'];
$on = $row['on'];
$date = $row['date'];
$opponent = $row['opponent'];
$result_headz = $row['result_headz'];
$result_opponent = $row['result_opponent'];
$map = $row['map'];

if($game == 1){

// If $color==1 table row color = DARK
if($color == 1){
echo	"<tr><td height='20' valign='middle' class='matcher_opponent_dark'>
			<span class='matcher'><a href='matcher_info.php?match=$id' title='Map: $map. Datum: $date.' target='main'>$opponent</a></span>
		</td><td width='100' height='20' valign='middle' align='right' class='matcher_result_dark'>";

if($result_headz > $result_opponent) {
echo		"<span class='matcher_won'>Vann</span>";
} elseif($result_headz == $result_opponent) {
echo		"<span class='matcher_equal'>Lika</span>";
} else {
echo		"<span class='matcher_lost'>Förlorade</span>";
}

echo	"</td><td width='20' height='20' valign='middle' align='center' class='matcher_icon_dark'>
			<a href='matcher_info.php?match=$id' title='Se Match #$id' target='main'><img src='images/icons/icon_folder.gif' border='0' width='14' height='15' alt=''></a>
		</td></tr>";

$color = "2";	// Set $color==2, for switching to other color

} else {	// When $color not equal 1, use this table row color

echo	"<tr><td height='20' valign='middle' class='matcher_opponent'>
			<span class='matcher'><a href='matcher_info.php?match=$id' title='Map: $map. Datum: $date.' target='main'>$opponent</a></span>
		</td><td width='100' height='20' valign='middle' align='right' class='matcher_result'>";

if($result_headz > $result_opponent) {
echo		"<span class='matcher_won'>Vann</span>";
} elseif($result_headz == $result_opponent) {
echo		"<span class='matcher_equal'>Lika</span>";
} else {
echo		"<span class='matcher_lost'>Förlorade</span>";
}

echo	"</td><td width='20' height='20' valign='middle' align='center' class='matcher_icon'>
			<a href='matcher_info.php?match=$id' title='Se Match #$id' target='main'><img src='images/icons/icon_folder.gif' border='0' width='14' height='15' alt=''></a>
		</td></tr>";

// Set $color back to 1
$color = "1";

  }
 }
}

echo	"</table>

	</td></tr>
	</table>";

}

//Kod för att lägga till genom ett forumlär
if(isset($_GET['cod4'])) {

echo"<table width='100%' cellspacing='0' cellpadding='0' border='0'>
	<tr><td width='25' height='25' valign='middle' align='center' class='matcher_icon_left'>
		<img src='images/icons/icon_cod4.gif' border='0' width='16' height='16' alt=''>
	</td><td height='25' valign='middle' align='center' class='matcher_icon_bg'>
		<span class='medlemmar_text_citat_info'>Call of Duty 4: Modern Warfare</span>
	</td><td width='25' height='25' valign='middle' align='center' class='matcher_icon_right'>
		<img src='images/icons/icon_cod4.gif' border='0' width='16' height='16' alt=''>
	</td></tr>
	<tr><td valign='top' align='center' colspan='3'>

		<table width='100%' cellspacing='0' cellpadding='0' border='0' class='matcher_bg'>";


// Define $color=1
$color = "1";


$sql = "SELECT * FROM legacy_headz_matcher ORDER BY id DESC";
$stmt = $conn->prepare($sql);
$stmt->execute();
while ($row = $stmt->fetch()) {
$id = $row['id'];
$game = $row['game'];
$on = $row['on'];
$date = $row['date'];
$opponent = $row['opponent'];
$result_headz = $row['result_headz'];
$result_opponent = $row['result_opponent'];
$type = $row['type'];
$map_1 = $row['map_1'];

if($game == 2){

// If $color==1 table row color = DARK
if($color == 1){
echo	"<tr><td height='20' valign='middle' class='matcher_opponent_dark'>
			<span class='matcher'><a href='matcher_info.php?match=$id' title='Typ: $type. Datum: $date.' target='main'>$opponent</a></span>
		</td><td width='100' height='20' valign='middle' align='right' class='matcher_result_dark'>";

if($result_headz > $result_opponent) {
echo		"<span class='matcher_won'>Vann</span>";
} elseif($result_headz == $result_opponent) {
echo		"<span class='matcher_equal'>Lika</span>";
} else {
echo		"<span class='matcher_lost'>Förlorade</span>";
}

echo	"</td><td width='20' height='20' valign='middle' align='center' class='matcher_icon_dark'>
			<a href='matcher_info.php?match=$id' title='Se Match #$id' target='main'><img src='images/icons/icon_folder.gif' border='0' width='14' height='15' alt=''></a>
		</td></tr>";

$color = "2";	// Set $color==2, for switching to other color

} else {	// When $color not equal 1, use this table row color

echo	"<tr><td height='20' valign='middle' class='matcher_opponent'>
			<span class='matcher'><a href='matcher_info.php?match=$id' title='Typ: $type. Datum: $date.' target='main'>$opponent</a></span>
		</td><td width='100' height='20' valign='middle' align='right' class='matcher_result'>";

if($result_headz > $result_opponent) {
echo		"<span class='matcher_won'>Vann</span>";
} elseif($result_headz == $result_opponent) {
echo		"<span class='matcher_equal'>Lika</span>";
} else {
echo		"<span class='matcher_lost'>Förlorade</span>";
}

echo	"</td><td width='20' height='20' valign='middle' align='center' class='matcher_icon'>
			<a href='matcher_info.php?match=$id' title='Se Match #$id' target='main'><img src='images/icons/icon_folder.gif' border='0' width='14' height='15' alt=''></a>
		</td></tr>";

// Set $color back to 1
$color = "1";

  }
 }
}

echo	"</table>

	</td></tr>
	</table>";

}


//Kod för att lägga till genom ett forumlär
if(isset($_GET['q3'])) {

echo"<table width='100%' cellspacing='0' cellpadding='0' border='0'>
	<tr><td width='25' height='25' valign='middle' align='center' class='matcher_icon_left'>
		<img src='images/icons/icon_q3.gif' border='0' width='16' height='16' alt=''>
	</td><td height='25' valign='middle' align='center' class='matcher_icon_bg'>
		<span class='medlemmar_text_citat_info'>Quake III Arena</span>
	</td><td width='25' height='25' valign='middle' align='center' class='matcher_icon_right'>
		<img src='images/icons/icon_q3.gif' border='0' width='16' height='16' alt=''>
	</td></tr>
	<tr><td valign='top' align='center' colspan='3'>

		<table width='100%' cellspacing='0' cellpadding='0' border='0' class='matcher_bg'>";


// Define $color=1
$color = "1";


$sql = "SELECT * FROM legacy_headz_matcher ORDER BY id DESC";
$stmt = $conn->prepare($sql);
$stmt->execute();
while ($row = $stmt->fetch()) {
$id = $row['id'];
$game = $row['game'];
$on = $row['on'];
$date = $row['date'];
$opponent = $row['opponent'];
$result_headz = $row['result_headz'];
$result_opponent = $row['result_opponent'];
$type = $row['type'];

if($game == 3){

// If $color==1 table row color = DARK
if($color == 1){
echo	"<tr><td height='20' valign='middle' class='matcher_opponent_dark'>
			<span class='matcher'><a href='matcher_info.php?match=$id' title='Typ: $type. Datum: $date.' target='main'>$opponent</a></span>
		</td><td width='100' height='20' valign='middle' align='right' class='matcher_result_dark'>";

if($result_headz > $result_opponent) {
echo		"<span class='matcher_won'>Vann</span>";
} elseif($result_headz == $result_opponent) {
echo		"<span class='matcher_equal'>Lika</span>";
} else {
echo		"<span class='matcher_lost'>Förlorade</span>";
}

echo	"</td><td width='20' height='20' valign='middle' align='center' class='matcher_icon_dark'>
			<a href='matcher_info.php?match=$id' title='Se Match #$id' target='main'><img src='images/icons/icon_folder.gif' border='0' width='14' height='15' alt=''></a>
		</td></tr>";

$color = "2";	// Set $color==2, for switching to other color

} else {	// When $color not equal 1, use this table row color

echo	"<tr><td height='20' valign='middle' class='matcher_opponent'>
			<span class='matcher'><a href='matcher_info.php?match=$id' title='Typ: $type. Datum: $date.' target='main'>$opponent</a></span>
		</td><td width='100' height='20' valign='middle' align='right' class='matcher_result'>";

if($result_headz > $result_opponent) {
echo		"<span class='matcher_won'>Vann</span>";
} elseif($result_headz == $result_opponent) {
echo		"<span class='matcher_equal'>Lika</span>";
} else {
echo		"<span class='matcher_lost'>Förlorade</span>";
}

echo	"</td><td width='20' height='20' valign='middle' align='center' class='matcher_icon'>
			<a href='matcher_info.php?match=$id' title='Se Match #$id' target='main'><img src='images/icons/icon_folder.gif' border='0' width='14' height='15' alt=''></a>
		</td></tr>";

// Set $color back to 1
$color = "1";

  }
 }
}

echo	"</table>

	</td></tr>
	</table>";

}


//Kod för att lägga till genom ett forumlär
if(isset($_GET['wc3'])) {

echo"<table width='100%' cellspacing='0' cellpadding='0' border='0'>
	<tr><td width='25' height='25' valign='middle' align='center' class='matcher_icon_left'>
		<img src='images/icons/icon_wc3.gif' border='0' width='16' height='16' alt=''>
	</td><td height='25' valign='middle' align='center' class='matcher_icon_bg'>
		<span class='medlemmar_text_citat_info'>Warcraft III: The Frozen Throne</span>
	</td><td width='25' height='25' valign='middle' align='center' class='matcher_icon_right'>
		<img src='images/icons/icon_wc3.gif' border='0' width='16' height='16' alt=''>
	</td></tr>
	<tr><td valign='top' align='center' colspan='3'>

		<table width='100%' cellspacing='0' cellpadding='0' border='0' class='matcher_bg'>";


// Define $color=1
$color = "1";


$sql = "SELECT * FROM legacy_headz_demos ORDER BY id DESC";
$stmt = $conn->prepare($sql);
$stmt->execute();
while ($row = $stmt->fetch()) {
$id = $row['id'];
$game = $row['game'];
$profile = $row['profile'];
$profile_id = $row['profile_id'];
$date = $row['date'];
$team_headz_1 = $row['team_headz_1'];
$opponent = $row['opponent'];
$result_headz = $row['result_headz'];
$result_opponent = $row['result_opponent'];
$map = $row['map'];
$version = $row['version'];

if($game == 4){

// If $color==1 table row color = DARK
if($color==1){
echo	"<tr><td height='20' valign='middle' class='matcher_opponent_dark'>
			<span class='matcher'><a href='demo_info.php?demo=$id' title='Map: $map. Version: $version. Datum: $date.' target='main'>$team_headz_1 vs $opponent</a></span>
		</td><td width='100' height='20' valign='middle' align='right' class='matcher_result_dark'>";

if($result_headz > $result_opponent) {
echo		"<span class='matcher_won'>Vann</span>";
} elseif($result_headz == $result_opponent) {
echo		"<span class='matcher_equal'>Lika</span>";
} else {
echo		"<span class='matcher_lost'>Förlorade</span>";
}

echo	"</td><td width='20' height='20' valign='middle' align='center' class='matcher_icon_dark'>
			<a href='matcher_info.php?match=$id' title='Se Match #$id' target='main'><img src='images/icons/icon_folder.gif' border='0' width='14' height='15' alt=''></a>
		</td></tr>";

$color = "2";	// Set $color==2, for switching to other color

} else {	// When $color not equal 1, use this table row color

echo	"<tr><td height='20' valign='middle' class='matcher_opponent'>
			<span class='matcher'><a href='matcher_info.php?match=$id' title='Map: $map. Version: $version. Datum: $date.' target='main'>$opponent</a></span>
		</td><td width='100' height='20' valign='middle' align='right' class='matcher_result'>";

if($result_headz > $result_opponent) {
echo		"<span class='matcher_won'>Vann</span>";
} elseif($result_headz == $result_opponent) {
echo		"<span class='matcher_equal'>Lika</span>";
} else {
echo		"<span class='matcher_lost'>Förlorade</span>";
}

echo	"</td><td width='20' height='20' valign='middle' align='center' class='matcher_icon'>
			<a href='matcher_info.php?match=$id' title='Se Match #$id' target='main'><img src='images/icons/icon_folder.gif' border='0' width='14' height='15' alt=''></a>
		</td></tr>";

// Set $color back to 1
$color = "1";

  }
 }
}

echo	"</table>

	</td></tr>
	</table>";

}


//Om QUERY_STRING är tom laddas denna text, kolla under resurser och Server variablar för mer information
if(empty($_SERVER['QUERY_STRING'])) {
?>

	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr><td width="20%">
	</td><td height="100" colspan="3" valign="middle" align="center">

		<table cellspacing="0" cellpadding="0" border="0">
		<tr><td width="300" height="40" valign="middle" align="center" class="logo_bg2">
			<img src="images/logo_matcher.gif" width="191" height="38" border="0" vspace="0" alt="">
		</td></tr>
		</table>

	</td><td width="20%">
	</td></tr>
	<tr><td width="20%">
	</td><td>

		<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
		<tr><td valign="top" align="center">
			<a href='matcher.php?css' title='Se CS:S Matcherna!' target='main'><img src='images/logos_css.gif' border='0' width='150' height='150' alt=''></a>
		</td></tr>
		</table>

	</td><td width="10">
	</td><td>

		<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
		<tr><td valign="top" align="center">
			<a href='matcher.php?cod4' title='Se COD4 Matcherna!' target='main'><img src='images/logos_cod4.gif' border='0' width='150' height='150' alt=''></a>
		</td></tr>
		</table>

	</td><td width="20%">
	</td></tr>
	<tr><td height="50" colspan="5">
	</td></tr>
	<tr><td width="20%">
	</td><td>

		<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
		<tr><td valign="top" align="center">
			<a href='matcher.php?q3' title='Se Q3 Matcherna!' target='main'><img src='images/logos_q3.gif' border='0' width='150' height='150' alt=''></a>
		</td></tr>
		</table>

	</td><td>
	</td><td>

		<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
		<tr><td valign="top" align="center">
			<a href='matcher.php?wc3' title='Se WC3 Matcherna!' target='main'><img src='images/logos_wc3.gif' border='0' width='150' height='150' alt=''></a>
		</td></tr>
		</table>

	</td><td width="20%">
	</td></tr>
	</table>

<?php
}
?>

</td></tr>
</table>


</body>
</html>
