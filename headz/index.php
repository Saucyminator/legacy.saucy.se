<?php
session_start(); // Alltid överst på sidan

require("conn.php");	// Databasanslutningen

// Inloggning
if (isset($_POST['submit'])){

	$_POST = db_escape($_POST);

	$sql = "SELECT id FROM legacy_headz_players
			WHERE user='{$_POST['user']}'
			AND pass='{$_POST['passwd']}'";
	$result = mysql_query($sql);

	// Hittades inte användarnamn och lösenord
	// skicka till formulär med felmeddelande
	if (mysql_num_rows($result) == 0){
		header("Location: index.php?badlogin=");
		exit;
	}

	// Sätt sessionen med unikt index
	$_SESSION['sess_id'] = mysql_result($result, 0, 'id');
	$_SESSION['sess_user'] = $_POST['user'];
	header("Location: admin.php");
	exit;
}

//
if (isset($_SESSION['sess_user'])){

	//SQL kod för att uppdatera en vald nyhet
	mysql_query("UPDATE legacy_headz_medlem SET
		online = '1'
		WHERE nick='{$_SESSION['sess_user']}'") or die (mysql_error());
}

// Utloggning
if (isset($_GET['logout'])){

	//SQL kod för att uppdatera en vald nyhet
	mysql_query("UPDATE legacy_headz_medlem SET
		online = '0'
		WHERE nick='{$_SESSION['sess_user']}'") or die (mysql_error());

  session_unset();
  session_destroy();
  header("Location: index.php");
  exit;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><?php include("header_title.html"); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="keywords" content="Klan, Clan, HeadZ, Klan HeadZ, Clan HeadZ, css, cs:s, counterstrike source, counter-strike: source, cod4, cod 4, call of duty 4, call of duty 4: modern warfare, q3, quake 3, quake iii arena, wc3, war3, warcraft iii: the frozen throne">
<meta name="description" content="Clan HeadZ - A Counter-Strike: Source, Call of Duty 4: Modern Warfare, Quake III Arena and Warcraft III: The Frozen Throne clan!">
<meta name="author" content="Patrik Holmberg">
<meta name="copyright" content="Copyright © HeadZ 2007-2009">
<link href="css/body.css" rel="stylesheet" type="text/css">
<link rel="icon" href="images/icons/favicon.ico">

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18538968-1']);
  _gaq.push(['_setDomainName', '.saucy.se']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>
<body>


<!-- COPYRIGHT © HeadZ 2007-2009 -->

<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
<tr><td height="100%" valign="top">

	<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
	<tr><td width="100%" height="5"></td></tr>
	<tr><td width="100%" height="140" align="center" valign="top" class="logo_bg">

		<table width="550" height="100%" cellspacing="0" cellpadding="0" border="0">
	  	<tr><td width="550" height="140" class="logo"></td></tr>
	  	</table>

	</td></tr>
	<tr><td height="5"></td></tr>
	<tr><td width="100%" height="100%" valign="top">

		<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
		<tr><td width="114" height="100%" valign="top">

			<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
			<tr><td width="100%" height="105" valign="top" align="center" class="login_top">

<?php

// Om inte inloggad visa formulär, annars logga ut-länk
if (!isset($_SESSION['sess_user'])){

  // Visa felmeddelande vid felaktig inloggning
  if (isset($_GET['badlogin'])){
    echo "<span class='login_text'>Fel användarnamn eller lösenord!<br></span>\n";
  }

?>

				<?php /* INLOGGNINGS RUTA */ ?>
				<table width="114" cellspacing="0" cellpadding="0" border="0">
				<tr><td width="114" height="3">
				</td></tr>
				<tr><td height="20" class="login_text" align="center">
					Användarnamn:<br>
				</td></tr>
				<tr><td align="center">
					<form action="index.php" method="post" style="margin-bottom:0;">
						<input type="text" name="user" size="19" class="login_line"><br>
				</td></tr>
				<tr><td height="20" class="login_text" align="center">
					Lösenord:<br>
				</td></tr>
				<tr><td align="center">
					<input type="password" name="passwd" size="19" class="login_line"><br>
				</td></tr>
				<tr><td align="center" height="28">
						<input type="submit" name="submit" value="Logga in" class="mainoption">
					</form>
				</td></tr>
				</table>

			</td></tr>
			<tr><td width="100%" height="38" valign="top" class="login_bottom">

				<table width="114" cellspacing="0" cellpadding="0" border="0">
				<tr><td>
					<span class="menu"><a href="registrera.html" target="main">Registrera dig</a></span><br>
				</td></tr>
				<tr><td height="3">
				</td></tr>
				<tr><td>
					<span class="menu"><a href="glomt_losen.html" target="main">Glömt lösen?</a></span><br>
				</td></tr>
				</table>
				<?php /* INLOGGNINGS RUTA */ ?>

<?php

} else {


$resultat = mysql_query("SELECT * FROM legacy_headz_players WHERE user='{$_SESSION['sess_user']}'") or die (mysql_error());
if($row = mysql_fetch_array($resultat)) {
$user = $row['user'];

echo			"<table width='120' cellspacing='0' cellpadding='0' border='0'>
				<tr><td height='3'></td></tr>
				<tr><td class='admin_profile' align='center' height='20'>";
echo				"<span class='admin_gray'><a href=\"admin_profil.php\" target='main'>[ </span>";
echo					"<span class='admin_orange'>$user</span>";
echo				"<span class='admin_gray'> ]</a></span><br>";
echo			"</td></tr><tr><td align='center' height='20'>";
echo				"<span class='admin_gray'><a href=\"admin_nyheter.php\" target='main'>[ </span>";
echo					"<span class='admin_orange'>Nyheter</span>";
echo				"<span class='admin_gray'> ]</a></span><br>";
echo			"</td></tr><tr><td align='center' height='20'>";
echo				"<del><span class='admin_gray'><a href=\"admin_matcher.php\" title='!!FUNKAR INTE ÄNNU!!' target='main'>[ </span>";
echo					"<span class='admin_orange'>Matcher</span>";
echo				"<span class='admin_gray'> ]</a></span></del><br>";
echo			"</td></tr><tr><td align='center' height='20'>";
echo				"<del><span class='admin_gray'><a href=\"admin_configs.php\" title='!!FUNKAR INTE ÄNNU!!' target='main'>[ </span>";
echo					"<span class='admin_orange'>Configs</span>";
echo				"<span class='admin_gray'> ]</a></span></del><br>";
echo			"</td></tr><tr><td align='center' height='20'>";
echo				"<del><span class='admin_gray'><a href=\"admin_demos.php\" title='!!FUNKAR INTE ÄNNU!!' target='main'>[ </span>";
echo					"<span class='admin_orange'>Demos</span>";
echo				"<span class='admin_gray'> ]</a></span></del><br>";
echo			"</td></tr>
				</table>";

}


echo	"</td></tr>
		<tr><td width='100%' height='37' valign='top' class='login_bottom2'>

			<table width='100%' cellspacing='0' cellpadding='0' border='0'>
			<tr><td height='37' valign='center' align='center'>
				<form action='index.php?logout=\' method='post' style='margin-bottom:0;'>
				<input type='submit' name='logout' value='Logga Ut' class='mainoption'><br>
				</form>
			</td></tr>
			</table>";
}
?>

			</td></tr>
			<tr><td width="100%" height="5"></td></tr>
			<tr><td width="100%" height="100%" valign="top" class="bg">

<?php include("menu.html"); ?>

				<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr><td height="15"></td></tr>
				</table>
<?php

// Om inte inloggad visa formulär, annars logga ut-länk
if (!isset($_SESSION['sess_user'])){

include("banners.html");
?>

					<table width="100%" cellspacing="0" cellpadding="0" border="0">
					<tr><td align="center">
						<a href="http://www.firefox2.com/se/" title="Skaffa FireFox2!" target="_blank"><img src="images/getfirefox.png" border="0" width="80" height="15" vspace="0" alt=""></a>
					</td></tr>
					<tr><td align="center">
						<span class="copy-right"><a href="http://www.jalba.se/headz/" title="HeadZ" target="_blank">© 2007-2009 HeadZ</a></span>
					</td></tr>
					<tr><td align="center">

<?php
###############################################
#    en enkel räknare för antal besökare.     #
###############################################

  $counter_file = "logs/counter.txt";
  if(!($fp = fopen($counter_file, "r"))) die ("Kan inte öppna $counter_file.");
  $counter = (int) fread ($fp, 20);
  fclose($fp);

  $counter++;  // räknar upp ett för varje besök

  echo "<span class='admin_gray'>Antal Besök: </span><span class='admin_orange2'> $counter</span><br>";

  $fp = fopen($counter_file, "w");
  fwrite($fp, $counter);
  fclose($fp);





$rip = $_SERVER['REMOTE_ADDR'];
$sd  = time();
$count = 1;
$maxu = 1;

$file1 = "logs/ip.txt";
$lines = file($file1);
$line2 = "";
foreach ($lines as $line_num => $line)
   {
      if($line_num == 0)
      {
         $maxu = $line;
      }
      else
      {
         //echo $line."<br>";
         $fp = strpos($line,'****');
         $nam = substr($line,0,$fp);
         $sp = strpos($line,'++++');
         $val = substr($line,$fp+4,$sp-($fp+4));
         $diff = $sd-$val;

         if($diff < 150 && $nam != $rip)
            {
             $count = $count+1;
             $line2 = $line2.$line;
             //echo $line2;
            }
      }
   }
$my = $rip."****".$sd."++++\n";
if($count > $maxu)
   $maxu = $count;

echo "<span class='admin_gray'>Aktiva Besökare: </span><span class='admin_orange2'>$count</span>";

$open1 = fopen($file1, "w");
fwrite($open1,"$maxu\n");
fwrite($open1,"$line2");
fwrite($open1,"$my");
fclose($open1);

?>

					</td></tr>
					</table>

<?php
} else {
?>
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr><td colspan="3" width="100%" height="31" valign="middle" align="center" class="banner_bg">
				<span class="admin_gray">Medlemmar online:</span>
			</td></tr>
			<tr><td height="5" colspan="3"></td>
			</tr>


<?php

$sql = "SELECT * FROM legacy_headz_medlem WHERE online='1' ORDER BY id ASC";
$result = mysql_query($sql);

for($i = 0; $row = mysql_fetch_array($result); $i++) {
echo	"<tr><td colspan='3' width='100%' valign='middle' align='center' class='banner_bg'>
			<span class='admin_orange'><a href='medlemmar_info.php?medlem={$row['id']}' target='main'>{$row['nick']}</a></span>
		</td></tr><tr><td height='5' colspan='5'></td></tr>";
}

?>

			</table>

<?php
}
?>
				</td></tr>
				</table>

			</td><td width="5" height="100%">

				<table width="5" height="100%" cellspacing="0" cellpadding="0" border="0">
				<tr><td width="5" height="100%"></td></tr>
				</table>

			</td><td width="100%" height="100%" valign="top">

				<table width="100%" height="100%" cellspacing="0" cellpadding="5" border="0">
				<tr><td width="100%" height="100%" valign="top" class="bg">
					<iframe src="hem.php" name="main" width="100%" height="100%" frameborder="0"></iframe>
				</td></tr>
				</table>

			</td><td width="5" height="100%">

				<table width="5" cellspacing="0" cellpadding="0" border="0">
				<tr><td width="5" height="100%"></td></tr>
				</table>

			</td><td width="202" height="100%" valign="top">

				<table width="202" height="100%" cellspacing="0" cellpadding="5" border="0">
				<tr><td width="202" height="100%" valign="top" align="center" class="bg">

					<table height="100%" cellspacing="0" cellpadding="0" border="0">
					<tr><td height="100%" valign="top">
						<!-- <noscript>Enable Javascript to get full functionality of this <a href="http://www.freeshoutbox.net/">shoutbox</a><br></noscript><iframe src="http://saus.freeshoutbox.net/" width="260" height="100%" frameborder="0"></iframe> -->
<?php

#						<iframe src="shoutbox.php" name="shoutbox" width="100%" height="100%" scrolling="no" frameborder="0" allowtransparency="true"></iframe>

?>
					</td></tr>
					</table>
				</td></tr>
				</table>

		</td></tr>
		</table>

	</td></tr>
	</table>

</td></tr>
</table>

<!-- COPYRIGHT © HeadZ 2007-2009 -->


</body>
</html>
