<?php

require("conn.php");	// Databasanslutningen

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/iframe.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3406958-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>

</head>
<body>


<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
<tr><td width="100%" height="100%" valign="top" align="left" class="main_bg">

<?php

if (!isset($_GET['match'])) {
  $match = 0;
}
else
{
  $match = intval ( $_GET['match'] );
  /* Använd intval() för att undvika s.k. SQL INJECTIONS,
     dvs. att folk kan typ radera din databas... */
}

$sql = "SELECT * FROM legacy_headz_matcher WHERE id = $match LIMIT 1";
$stmt = $conn->prepare($sql);
$stmt->execute();
if ($row = $stmt->fetch()) {
$id = $row['id'];
$game = $row['game'];
$opponent = $row['opponent'];
$on = $row['on'];
$date = $row['date'];
$league = $row['league'];
$league_url = $row['league_url'];
$map = $row['map'];
$server = $row['server'];
$server_url = $row['server_url'];
$text = $row['text'];
$result_headz = $row['result_headz'];
$result_opponent = $row['result_opponent'];
$team_headz_1 = $row['team_headz_1'];
$team_headz_2 = $row['team_headz_2'];
$team_headz_3 = $row['team_headz_3'];
$team_headz_4 = $row['team_headz_4'];
$team_headz_5 = $row['team_headz_5'];
$team_headz_6 = $row['team_headz_6'];
$team_headz_id_1 = $row['team_headz_id_1'];
$team_headz_id_2 = $row['team_headz_id_2'];
$team_headz_id_3 = $row['team_headz_id_3'];
$team_headz_id_4 = $row['team_headz_id_4'];
$team_headz_id_5 = $row['team_headz_id_5'];
$team_headz_id_6 = $row['team_headz_id_6'];
$team_opponent_1 = $row['team_opponent_1'];
$team_opponent_2 = $row['team_opponent_2'];
$team_opponent_3 = $row['team_opponent_3'];
$team_opponent_4 = $row['team_opponent_4'];
$team_opponent_5 = $row['team_opponent_5'];
$team_opponent_6 = $row['team_opponent_6'];
$demo = $row['demo'];
$demo_id = $row['demo_id'];
$type = $row['type'];
$admins = $row['admins'];
$map_1 = $row['map_1'];
$map_2 = $row['map_2'];
$map_3 = $row['map_3'];
$map_4 = $row['map_4'];
$map_5 = $row['map_5'];
$map_6 = $row['map_6'];


if($game == 1) {

echo"<table width='100%' cellspacing='0' cellpadding='0' border='0'>
	<tr><td width='25' height='25' valign='middle' align='center' class='matcher_icon_left'>
		<img src='images/icons/icon_css.gif' border='0' width='16' height='16' alt=''>
	</td><td height='25' valign='middle' align='center' class='matcher_icon_bg'>
		<span class='medlemmar_text_citat_info'>HeadZ</span><span class='medlemmar_text_citat'> vs </span><span class='medlemmar_text_citat_info'>$opponent</span><span class='medlemmar_text_citat'>&nbsp;&nbsp;&nbsp;[ </span><span class='medlemmar_text_citat_info'>$on</span><span class='medlemmar_text_citat'>on</span><span class='medlemmar_text_citat_info'>$on</span><span class='medlemmar_text_citat'> ]</span>
	</td><td width='25' height='25' valign='middle' align='center' class='matcher_icon_right'>
		<img src='images/icons/icon_css.gif' border='0' width='16' height='16' alt=''>
	</td></tr>
	<tr><td valign='top' align='center' colspan='3'>

		<table width='100%' cellpadding='0' cellspacing='0' border='0'>
		<tr><td height='19' class='matcher_info_bg_left'>
			<span class='matcher_text'>Datum: </span>
				<span class='tid_info'>$date</span>
		</td><td class='matcher_info_bg_middle'>
			<span class='matcher_text'>Match ID: </span>
				<span class='matcher_text_info'>$id</span>
		</td><td width='10' class='matcher_info_bg_right'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left_dark'>
			<span class='matcher_text_info'>X</span><span class='matcher_text'>on</span><span class='matcher_text_info'>X</span><span class='matcher_text'>:&nbsp;&nbsp;&nbsp;</span>";
echo			"<span class='matcher_text'>[ </span>";
echo			"<span class='matcher_text_info'>$on</span>";
echo			"<span class='matcher_text'>on</span>";
echo			"<span class='matcher_text_info'>$on</span>";
echo			"<span class='matcher_text'> ]</span>";
echo	"</td><td width='241' height='180' rowspan='9' valign='middle' align='center' class='matcher_info_bg_bild'>";

if ($map == "de_aztec") {
    echo "<img src='images/maps/de_aztec.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map == "de_cbble") {
    echo "<img src='images/maps/de_cbble.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map == "de_cpl_fire") {
    echo "<img src='images/maps/de_cpl_fire.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map == "de_cpl_mill") {
    echo "<img src='images/maps/de_cpl_mill.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map == "de_cpl_strike") {
    echo "<img src='images/maps/de_cpl_strike.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map == "de_dust2") {
    echo "<img src='images/maps/de_dust2.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map == "de_dust") {
    echo "<img src='images/maps/de_dust.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map == "de_forge") {
    echo "<img src='images/maps/de_forge.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map == "de_inferno") {
    echo "<img src='images/maps/de_inferno.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map == "de_nuke") {
    echo "<img src='images/maps/de_nuke.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map == "de_prodigy") {
    echo "<img src='images/maps/de_prodigy.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map == "de_russka") {
    echo "<img src='images/maps/de_russka.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map == "de_train") {
    echo "<img src='images/maps/de_train.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map == "de_tuscan") {
    echo "<img src='images/maps/de_tuscan.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map == "de_contra") {
    echo "<img src='images/maps/de_contra.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map == "de_phoenix") {
    echo "<img src='images/maps/de_phoenix.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map == "de_season") {
    echo "<img src='images/maps/de_season.gif' border='0' width='241' height='178' alt=''>";
} else {
    echo "<img src='images/maps/na.gif' border='0' width='241' height='178' alt=''>";
}

echo	"</td><td width='10' class='matcher_info_bg_right_dark'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left'>
			<span class='matcher_text'>HeadZ: </span>";

if(!empty($team_headz_1)) {
echo			"<span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$team_headz_id_1&css' title='Se Profil: $team_headz_1' target='main'>$team_headz_1</a></span>";
}
if(!empty($team_headz_2)) {
echo			"<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$team_headz_id_2&css' title='Se Profil: $team_headz_2' target='main'>$team_headz_2</a></span>";
}
if(!empty($team_headz_3)) {
echo			"<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$team_headz_id_3&css' title='Se Profil: $team_headz_3' target='main'>$team_headz_3</a></span>";
}
if(!empty($team_headz_4)) {
echo			"<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$team_headz_id_4&css' title='Se Profil: $team_headz_4' target='main'>$team_headz_4</a></span>";
}
if(!empty($team_headz_5)) {
echo			"<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$team_headz_id_5&css' title='Se Profil: $team_headz_5' target='main'>$team_headz_5</a></span>";
}
if(!empty($team_headz_6)) {
echo			"<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$team_headz_id_6&css' title='Se Profil: $team_headz_6' target='main'>$team_headz_6</a></span>";
}

echo	"</td><td width='10' class='matcher_info_bg_right'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left_dark'>
			<span class='matcher_text'>$opponent: </span>";

if(!empty($team_opponent_1)) {
echo			"<span class='matcher_text_info'>$team_opponent_1</span>";
}
if(!empty($team_opponent_2)) {
echo			"<span class='matcher_text'>, </span><span class='matcher_text_info'>$team_opponent_2</span>";
}
if(!empty($team_opponent_3)) {
echo			"<span class='matcher_text'>, </span><span class='matcher_text_info'>$team_opponent_3</span>";
}
if(!empty($team_opponent_4)) {
echo			"<span class='matcher_text'>, </span><span class='matcher_text_info'>$team_opponent_4</span>";
}
if(!empty($team_opponent_5)) {
echo			"<span class='matcher_text'>, </span><span class='matcher_text_info'>$team_opponent_5</span>";
}
if(!empty($team_opponent_6)) {
echo			"<span class='matcher_text'>, </span><span class='matcher_text_info'>$team_opponent_6</span>";
}

echo	"</td><td width='10' class='matcher_info_bg_right_dark'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left'>
			<span class='matcher_text'>Resultat:&nbsp;&nbsp;&nbsp;</span>";

if($result_headz > $result_opponent) {
echo			"<span class='matcher_text'> [ </span>";
echo			"<span class='matcher_won'>$result_headz</span>";
echo			"<span class='matcher_text'>:</span>";
echo			"<span class='matcher_lost'>$result_opponent</span>";
echo			"<span class='matcher_text'> ]</span>";
} elseif($result_headz == $result_opponent) {
echo			"<span class='matcher_text'>[ </span>";
echo			"<span class='matcher_equal'>$result_headz</span>";
echo			"<span class='matcher_text'>:</span>";
echo			"<span class='matcher_equal'>$result_opponent</span>";
echo			"<span class='matcher_text'> ]</span>";
} else {
echo			"<span class='matcher_text'>[ </span>";
echo			"<span class='matcher_lost'>$result_headz</span>";
echo			"<span class='matcher_text'>:</span>";
echo			"<span class='matcher_won'>$result_opponent</span>";
echo			"<span class='matcher_text'> ]</span>";
}

echo	"</td><td width='10' class='matcher_info_bg_right'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left_dark'>
			<span class='matcher_text'>Liga: </span>
				<span class='matcher_text_info'>$league</span>";

if(!empty($league_url)) {
echo				"<span class='matcher_text'>&nbsp;&nbsp;&nbsp;[</span><span class='text_medlemmar'><a href='$league_url' title='$league_url' target='_blank'>Matchlänk</a></span><span class='matcher_text'>]</span>";
}

echo	"</td><td width='10' class='matcher_info_bg_right_dark'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left'>
			<span class='matcher_text'>Map: </span>
				<span class='matcher_text_info'>$map</span>
		</td><td width='10' class='matcher_info_bg_right'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left_dark'>
			<span class='matcher_text'>Server:&nbsp;&nbsp;&nbsp;</span>";

if($server == 1) {
echo			"<span class='matcher_text'>[ </span><span class='text_medlemmar'><a href=\"http://www.jalba.se/headz/forum/viewtopic.php?f=6&t=10\" title='Våran server info på vårat forum.' target='_blank'>Våran</a></span><span class='matcher_text'> ]</span>";
} elseif($server == 2) {
	if(!empty($server_url)){
	echo			"<span class='matcher_text'>[ </span><span class='text_medlemmar'><a href='$server_url' title='$server_url' target='_blank'>Deras</a></span><span class='matcher_text'> ]</span>";
	} else {
	echo			"<span class='matcher_text'>[ Deras ]</span>";
	}
}

echo	"</td><td width='10' class='matcher_info_bg_right_dark'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left'>
			<span class='matcher_text'>Admin(s): </span>";

if($admins == 1) {
echo			"<span class='matcher_text_info'>Vi</span>";
} elseif($admins == 2) {
echo			"<span class='matcher_text_info'>Dom</span>";
} elseif($admins == 0) {
echo			"<span class='matcher_text'>Inte tillgänglig</span>";
}

echo	"</td><td width='10' class='matcher_info_bg_right'>
			&nbsp;
		</td></tr>
		<tr><td height='20' class='matcher_info_bg_left_dark'>
			<span class='matcher_text'>DEMO:&nbsp;&nbsp;&nbsp;</span>";

if($demo == 1) {
echo			"<span class='matcher_text'>[ </span><span class='text_medlemmar'><a href='demo_info.php?demo=$demo_id' title='Se Demo #$demo_id' target='main'>HLTV</a></span><span class='matcher_text'> ]</span>";
} elseif($demo == 0) {
echo			"<span class='matcher_text'>[ Inte tillgänglig ]</span>";
}

echo	"<td width='10' class='matcher_info_bg_right'>
			&nbsp;
		</td></tr>
		<tr><td colspan='2' class='matcher_info_bg_left_bottom2' valign='top'>
			<span class='matcher_text'>Sammandrag: </span>
				<span class='matcher_text_info'>$text</span>
		</td><td width='10' class='matcher_info_bg_right_bottom'>
			&nbsp;
		</td></tr>
		</table>

		<!-- KOMMENTARER FUNKTION KOMMER SNART -->

	</td></tr>
	</table>";


} elseif($game == 2) {

echo"<table width='100%' cellspacing='0' cellpadding='0' border='0'>
	<tr><td width='25' height='25' valign='middle' align='center' class='matcher_icon_left'>
		<img src='images/icons/icon_cod4.gif' border='0' width='16' height='16' alt=''>
	</td><td height='25' valign='middle' align='center' class='matcher_icon_bg'>
		<span class='medlemmar_text_citat_info'>HeadZ</span><span class='medlemmar_text_citat'> vs </span><span class='medlemmar_text_citat_info'>$opponent</span><span class='medlemmar_text_citat'>&nbsp;&nbsp;&nbsp;[ </span><span class='medlemmar_text_citat_info'>$on</span><span class='medlemmar_text_citat'>on</span><span class='medlemmar_text_citat_info'>$on</span><span class='medlemmar_text_citat'> ]</span>
	</td><td width='25' height='25' valign='middle' align='center' class='matcher_icon_right'>
		<img src='images/icons/icon_cod4.gif' border='0' width='16' height='16' alt=''>
	</td></tr>
	<tr><td valign='top' align='center' colspan='3'>

		<table width='100%' cellpadding='0' cellspacing='0' border='0'>
		<tr><td height='19' class='matcher_info_bg_left'>
			<span class='matcher_text'>Datum: </span>
				<span class='tid_info'>$date</span>
		</td><td class='matcher_info_bg_middle'>
			<span class='matcher_text'>Match ID: </span>
				<span class='matcher_text_info'>$id</span>
		</td><td width='10' class='matcher_info_bg_right'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left_dark'>
			<span class='matcher_text_info'>X</span><span class='matcher_text'>on</span><span class='matcher_text_info'>X</span><span class='matcher_text'>:&nbsp;&nbsp;&nbsp;</span>";
echo			"<span class='matcher_text'>[ </span>";
echo			"<span class='matcher_text_info'>$on</span>";
echo			"<span class='matcher_text'>on</span>";
echo			"<span class='matcher_text_info'>$on</span>";
echo			"<span class='matcher_text'> ]</span>";
echo	"</td><td width='241' height='180' rowspan='9' valign='middle' align='center' class='matcher_info_bg_bild'>";

if ($map_1 == "mp_convoy") {
    echo "<img src='images/maps/mp_convoy.png' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_backlot") {
    echo "<img src='images/maps/mp_backlot.png' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_bloc") {
    echo "<img src='images/maps/mp_bloc.png' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_bog") {
    echo "<img src='images/maps/mp_bog.png' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_countdown") {
    echo "<img src='images/maps/mp_countdown.png' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_crash") {
    echo "<img src='images/maps/mp_crash.png' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_crash_snow") {
    echo "<img src='images/maps/mp_crash_snow.png' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_crossfire") {
    echo "<img src='images/maps/mp_crossfire.png' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_citystreets") {
    echo "<img src='images/maps/mp_citystreets.png' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_farm") {
    echo "<img src='images/maps/mp_farm.png' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_overgrown") {
    echo "<img src='images/maps/mp_overgrown.png' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_pipeline") {
    echo "<img src='images/maps/mp_pipeline.png' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_shipment") {
    echo "<img src='images/maps/mp_shipment.png' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_showdown") {
    echo "<img src='images/maps/mp_showdown.png' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_strike") {
    echo "<img src='images/maps/mp_strike.png' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_vacant") {
    echo "<img src='images/maps/mp_vacant.png' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_cargoship") {
    echo "<img src='images/maps/mp_cargoship.png' border='0' width='241' height='178' alt=''>";
} else {
    echo "<img src='images/maps/na.gif' border='0' width='241' height='178' alt=''>";
}

echo	"</td><td width='10' class='matcher_info_bg_right_dark'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left'>
			<span class='matcher_text'>HeadZ: </span>";

if(!empty($team_headz_1)) {
echo			"<span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$team_headz_id_1&css' title='Se Profil: $team_headz_1' target='main'>$team_headz_1</a></span>";
}
if(!empty($team_headz_2)) {
echo			"<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$team_headz_id_2&css' title='Se Profil: $team_headz_2' target='main'>$team_headz_2</a></span>";
}
if(!empty($team_headz_3)) {
echo			"<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$team_headz_id_3&css' title='Se Profil: $team_headz_3' target='main'>$team_headz_3</a></span>";
}
if(!empty($team_headz_4)) {
echo			"<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$team_headz_id_4&css' title='Se Profil: $team_headz_4' target='main'>$team_headz_4</a></span>";
}
if(!empty($team_headz_5)) {
echo			"<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$team_headz_id_5&css' title='Se Profil: $team_headz_5' target='main'>$team_headz_5</a></span>";
}
if(!empty($team_headz_6)) {
echo			"<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$team_headz_id_6&css' title='Se Profil: $team_headz_6' target='main'>$team_headz_6</a></span>";
}

echo	"</td><td width='10' class='matcher_info_bg_right'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left_dark'>
			<span class='matcher_text'>$opponent: </span>";

if(!empty($team_opponent_1)) {
echo			"<span class='matcher_text_info'>$team_opponent_1</span>";
}
if(!empty($team_opponent_2)) {
echo			"<span class='matcher_text'>, </span><span class='matcher_text_info'>$team_opponent_2</span>";
}
if(!empty($team_opponent_3)) {
echo			"<span class='matcher_text'>, </span><span class='matcher_text_info'>$team_opponent_3</span>";
}
if(!empty($team_opponent_4)) {
echo			"<span class='matcher_text'>, </span><span class='matcher_text_info'>$team_opponent_4</span>";
}
if(!empty($team_opponent_5)) {
echo			"<span class='matcher_text'>, </span><span class='matcher_text_info'>$team_opponent_5</span>";
}
if(!empty($team_opponent_6)) {
echo			"<span class='matcher_text'>, </span><span class='matcher_text_info'>$team_opponent_6</span>";
}

echo	"</td><td width='10' class='matcher_info_bg_right_dark'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left'>
			<span class='matcher_text'>Resultat:&nbsp;&nbsp;&nbsp;</span>";

if($result_headz > $result_opponent) {
echo					"<span class='matcher_text'> [ </span>";
echo					"<span class='matcher_won'>$result_headz</span>";
echo					"<span class='matcher_text'>:</span>";
echo					"<span class='matcher_lost'>$result_opponent</span>";
echo					"<span class='matcher_text'> ]</span>";
} elseif($result_headz == $result_opponent) {
echo					"<span class='matcher_text'>[ </span>";
echo					"<span class='matcher_equal'>$result_headz</span>";
echo					"<span class='matcher_text'>:</span>";
echo					"<span class='matcher_equal'>$result_opponent</span>";
echo					"<span class='matcher_text'> ]</span>";
} else {
echo					"<span class='matcher_text'>[ </span>";
echo					"<span class='matcher_lost'>$result_headz</span>";
echo					"<span class='matcher_text'>:</span>";
echo					"<span class='matcher_won'>$result_opponent</span>";
echo					"<span class='matcher_text'> ]</span>";
}

echo	"</td><td width='10' class='matcher_info_bg_right'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left_dark'>
			<span class='matcher_text'>Liga: </span>
				<span class='matcher_text_info'>$league</span>";

if(!empty($league_url)) {
echo				"<span class='matcher_text'>&nbsp;&nbsp;&nbsp;[</span><span class='text_medlemmar'><a href='$league_url' title='$league_url' target='_blank'>Matchlänk</a></span><span class='matcher_text'>]</span>";
}

echo	"</td><td width='10' class='matcher_info_bg_right_dark'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left'>
			<span class='matcher_text'>Typ: </span>";

if ($type == "DM") {
echo			"<span class='matcher_text_info'>Deathmatch </span><span class='matcher_text'>(</span><span class='matcher_text_info'>$type</span><span class='matcher_text'>)</span>";
} elseif ($type == "DOM") {
echo			"<span class='matcher_text_info'>Domination </span><span class='matcher_text'>(</span><span class='matcher_text_info'>$type</span><span class='matcher_text'>)</span>";
} elseif ($type == "KOTH") {
echo			"<span class='matcher_text_info'>Headquaters </span><span class='matcher_text'>(</span><span class='matcher_text_info'>$type</span><span class='matcher_text'>)</span>";
} elseif ($type == "SAB") {
echo			"<span class='matcher_text_info'>Sabotage </span><span class='matcher_text'>(</span><span class='matcher_text_info'>$type</span><span class='matcher_text'>)</span>";
} elseif ($type == "SD") {
echo			"<span class='matcher_text_info'>Search and Destroy </span><span class='matcher_text'>(</span><span class='matcher_text_info'>$type</span><span class='matcher_text'>)</span>";
} elseif ($type == "TDM") {
echo			"<span class='matcher_text_info'>Team Deathmatch </span><span class='matcher_text'>(</span><span class='matcher_text_info'>$type</span><span class='matcher_text'>)</span>";
} else {
echo			"<span class='matcher_text'>Inte tillgänglig</span>";
}

echo	"</td><td width='10' class='matcher_info_bg_right'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left_dark'>
			<span class='matcher_text'>Maps: </span>";

if(!empty($map_1)) {
	if ($map_1 == "mp_convoy") {
	    echo "<span class='text_medlemmar'><a href='images/maps/mp_convoy.png' title='Se map: $map_1'>Ambush</a></span>";
	} elseif ($map_1 == "mp_backlot") {
	    echo "<span class='text_medlemmar'><a href='images/maps/mp_backlot.png' title='Se map: $map_1'>Backlot</a></span>";
	} elseif ($map_1 == "mp_bloc") {
	    echo "<span class='text_medlemmar'><a href='images/maps/mp_bloc.png' title='Se map: $map_1'>Bloc</a></span>";
	} elseif ($map_1 == "mp_bog") {
	    echo "<span class='text_medlemmar'><a href='images/maps/mp_bog.png' title='Se map: $map_1'>Bog</a></span>";
	} elseif ($map_1 == "mp_countdown") {
	    echo "<span class='text_medlemmar'><a href='images/maps/mp_countdown.png' title='Se map: $map_1'>Countdown</a></span>";
	} elseif ($map_1 == "mp_crash") {
	    echo "<span class='text_medlemmar'><a href='images/maps/mp_crash.png' title='Se map: $map_1'>Crash</a></span>";
	} elseif ($map_1 == "mp_crash_snow") {
	    echo "<span class='text_medlemmar'><a href='images/maps/mp_crash_snow.png' title='Se map: $map_1'>Crash Winter</a></span>";
	} elseif ($map_1 == "mp_crossfire") {
	    echo "<span class='text_medlemmar'><a href='images/maps/mp_crossfire.png' title='Se map: $map_1'>Crossfire</a></span>";
	} elseif ($map_1 == "mp_citystreets") {
	    echo "<span class='text_medlemmar'><a href='images/maps/mp_citystreets.png' title='Se map: $map_1'>District</a></span>";
	} elseif ($map_1 == "mp_farm") {
	    echo "<span class='text_medlemmar'><a href='images/maps/mp_farm.png' title='Se map: $map_1'>Downpour</a></span>";
	} elseif ($map_1 == "mp_overgrown") {
	    echo "<span class='text_medlemmar'><a href='images/maps/mp_overgrown.png' title='Se map: $map_1'>Overgrown</a></span>";
	} elseif ($map_1 == "mp_pipeline") {
	    echo "<span class='text_medlemmar'><a href='images/maps/mp_pipeline.png' title='Se map: $map_1'>Pipeline</a></span>";
	} elseif ($map_1 == "mp_shipment") {
	    echo "<span class='text_medlemmar'><a href='images/maps/mp_shipment.png' title='Se map: $map_1'>Shipment</a></span>";
	} elseif ($map_1 == "mp_showdown") {
	    echo "<span class='text_medlemmar'><a href='images/maps/mp_showdown.png' title='Se map: $map_1'>Showdown</a></span>";
	} elseif ($map_1 == "mp_strike") {
	    echo "<span class='text_medlemmar'><a href='images/maps/mp_strike.png' title='Se map: $map_1'>Strike</a></span>";
	} elseif ($map_1 == "mp_vacant") {
	    echo "<span class='text_medlemmar'><a href='images/maps/mp_vacant.png' title='Se map: $map_1'>Vacant</a></span>";
	} elseif ($map_1 == "mp_cargoship") {
	    echo "<span class='text_medlemmar'><a href='images/maps/mp_cargoship.png' title='Se map: $map_1'>Wet Work</a></span>";
	}
} else {
echo			"<span class='matcher_text'>Inte tillgänglig</span>";
}

if(!empty($map_2)) {
	if ($map_2 == "mp_convoy") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_convoy.png' title='Se map: $map_2'>Ambush</a></span>";
	} elseif ($map_2 == "mp_backlot") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_backlot.png' title='Se map: $map_2'>Backlot</a></span>";
	} elseif ($map_2 == "mp_bloc") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_bloc.png' title='Se map: $map_2'>Bloc</a></span>";
	} elseif ($map_2 == "mp_bog") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_bog.png' title='Se map: $map_2'>Bog</a></span>";
	} elseif ($map_2 == "mp_countdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_countdown.png' title='Se map: $map_2'>Countdown</a></span>";
	} elseif ($map_2 == "mp_crash") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_crash.png' title='Se map: $map_2'>Crash</a></span>";
	} elseif ($map_2 == "mp_crash_snow") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_crash_snow.png' title='Se map: $map_2'>Crash Winter</a></span>";
	} elseif ($map_2 == "mp_crossfire") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_crossfire.png' title='Se map: $map_2'>Crossfire</a></span>";
	} elseif ($map_2 == "mp_citystreets") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_citystreets.png' title='Se map: $map_2'>District</a></span>";
	} elseif ($map_2 == "mp_farm") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_farm.png' title='Se map: $map_2'>Downpour</a></span>";
	} elseif ($map_2 == "mp_overgrown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_overgrown.png' title='Se map: $map_2'>Overgrown</a></span>";
	} elseif ($map_2 == "mp_pipeline") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_pipeline.png' title='Se map: $map_2'>Pipeline</a></span>";
	} elseif ($map_2 == "mp_shipment") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_shipment.png' title='Se map: $map_2'>Shipment</a></span>";
	} elseif ($map_2 == "mp_showdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_showdown.png' title='Se map: $map_2'>Showdown</a></span>";
	} elseif ($map_2 == "mp_strike") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_strike.png' title='Se map: $map_2'>Strike</a></span>";
	} elseif ($map_2 == "mp_vacant") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_vacant.png' title='Se map: $map_2'>Vacant</a></span>";
	} elseif ($map_2 == "mp_cargoship") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_cargoship.png' title='Se map: $map_2'>Wet Work</a></span>";
	}
}

if(!empty($map_3)) {
	if ($map_3 == "mp_convoy") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_convoy.png' title='Se map: $map_3'>Ambush</a></span>";
	} elseif ($map_3 == "mp_backlot") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_backlot.png' title='Se map: $map_3'>Backlot</a></span>";
	} elseif ($map_3 == "mp_bloc") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_bloc.png' title='Se map: $map_3'>Bloc</a></span>";
	} elseif ($map_3 == "mp_bog") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_bog.png' title='Se map: $map_3'>Bog</a></span>";
	} elseif ($map_3 == "mp_countdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_countdown.png' title='Se map: $map_3'>Countdown</a></span>";
	} elseif ($map_3 == "mp_crash") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_crash.png' title='Se map: $map_3'>Crash</a></span>";
	} elseif ($map_3 == "mp_crash_snow") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_crash_snow.png' title='Se map: $map_3'>Crash Winter</a></span>";
	} elseif ($map_3 == "mp_crossfire") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_crossfire.png' title='Se map: $map_3'>Crossfire</a></span>";
	} elseif ($map_3 == "mp_citystreets") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_citystreets.png' title='Se map: $map_3'>District</a></span>";
	} elseif ($map_3 == "mp_farm") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_farm.png' title='Se map: $map_3'>Downpour</a></span>";
	} elseif ($map_3 == "mp_overgrown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_overgrown.png' title='Se map: $map_3'>Overgrown</a></span>";
	} elseif ($map_3 == "mp_pipeline") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_pipeline.png' title='Se map: $map_3'>Pipeline</a></span>";
	} elseif ($map_3 == "mp_shipment") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_shipment.png' title='Se map: $map_3'>Shipment</a></span>";
	} elseif ($map_3 == "mp_showdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_showdown.png' title='Se map: $map_3'>Showdown</a></span>";
	} elseif ($map_3 == "mp_strike") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_strike.png' title='Se map: $map_3'>Strike</a></span>";
	} elseif ($map_3 == "mp_vacant") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_vacant.png' title='Se map: $map_3'>Vacant</a></span>";
	} elseif ($map_3 == "mp_cargoship") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_cargoship.png' title='Se map: $map_3'>Wet Work</a></span>";
	}
}

if(!empty($map_4)) {
	if ($map_4 == "mp_convoy") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_convoy.png' title='Se map: $map_4'>Ambush</a></span>";
	} elseif ($map_4 == "mp_backlot") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_backlot.png' title='Se map: $map_4'>Backlot</a></span>";
	} elseif ($map_4 == "mp_bloc") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_bloc.png' title='Se map: $map_4'>Bloc</a></span>";
	} elseif ($map_4 == "mp_bog") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_bog.png' title='Se map: $map_4'>Bog</a></span>";
	} elseif ($map_4 == "mp_countdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_countdown.png' title='Se map: $map_4'>Countdown</a></span>";
	} elseif ($map_4 == "mp_crash") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_crash.png' title='Se map: $map_4'>Crash</a></span>";
	} elseif ($map_4 == "mp_crash_snow") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_crash_snow.png' title='Se map: $map_4'>Crash Winter</a></span>";
	} elseif ($map_4 == "mp_crossfire") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_crossfire.png' title='Se map: $map_4'>Crossfire</a></span>";
	} elseif ($map_4 == "mp_citystreets") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_citystreets.png' title='Se map: $map_4'>District</a></span>";
	} elseif ($map_4 == "mp_farm") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_farm.png' title='Se map: $map_4'>Downpour</a></span>";
	} elseif ($map_4 == "mp_overgrown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_overgrown.png' title='Se map: $map_4'>Overgrown</a></span>";
	} elseif ($map_4 == "mp_pipeline") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_pipeline.png' title='Se map: $map_4'>Pipeline</a></span>";
	} elseif ($map_4 == "mp_shipment") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_shipment.png' title='Se map: $map_4'>Shipment</a></span>";
	} elseif ($map_4 == "mp_showdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_showdown.png' title='Se map: $map_4'>Showdown</a></span>";
	} elseif ($map_4 == "mp_strike") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_strike.png' title='Se map: $map_4'>Strike</a></span>";
	} elseif ($map_4 == "mp_vacant") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_vacant.png' title='Se map: $map_4'>Vacant</a></span>";
	} elseif ($map_4 == "mp_cargoship") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_cargoship.png' title='Se map: $map_4'>Wet Work</a></span>";
	}
}

if(!empty($map_5)) {
	if ($map_5 == "mp_convoy") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_convoy.png' title='Se map: $map_5'>Ambush</a></span>";
	} elseif ($map_5 == "mp_backlot") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_backlot.png' title='Se map: $map_5'>Backlot</a></span>";
	} elseif ($map_5 == "mp_bloc") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_bloc.png' title='Se map: $map_5'>Bloc</a></span>";
	} elseif ($map_5 == "mp_bog") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_bog.png' title='Se map: $map_5'>Bog</a></span>";
	} elseif ($map_5 == "mp_countdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_countdown.png' title='Se map: $map_5'>Countdown</a></span>";
	} elseif ($map_5 == "mp_crash") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_crash.png' title='Se map: $map_5'>Crash</a></span>";
	} elseif ($map_5 == "mp_crash_snow") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_crash_snow.png' title='Se map: $map_5'>Crash Winter</a></span>";
	} elseif ($map_5 == "mp_crossfire") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_crossfire.png' title='Se map: $map_5'>Crossfire</a></span>";
	} elseif ($map_5 == "mp_citystreets") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_citystreets.png' title='Se map: $map_5'>District</a></span>";
	} elseif ($map_5 == "mp_farm") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_farm.png' title='Se map: $map_5'>Downpour</a></span>";
	} elseif ($map_5 == "mp_overgrown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_overgrown.png' title='Se map: $map_5'>Overgrown</a></span>";
	} elseif ($map_5 == "mp_pipeline") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_pipeline.png' title='Se map: $map_5'>Pipeline</a></span>";
	} elseif ($map_5 == "mp_shipment") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_shipment.png' title='Se map: $map_5'>Shipment</a></span>";
	} elseif ($map_5 == "mp_showdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_showdown.png' title='Se map: $map_5'>Showdown</a></span>";
	} elseif ($map_5 == "mp_strike") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_strike.png' title='Se map: $map_5'>Strike</a></span>";
	} elseif ($map_5 == "mp_vacant") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_vacant.png' title='Se map: $map_5'>Vacant</a></span>";
	} elseif ($map_5 == "mp_cargoship") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_cargoship.png' title='Se map: $map_5'>Wet Work</a></span>";
	}
}

if(!empty($map_6)) {
	if ($map_6 == "mp_convoy") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_convoy.png' title='Se map: $map_6'>Ambush</a></span>";
	} elseif ($map_6 == "mp_backlot") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_backlot.png' title='Se map: $map_6'>Backlot</a></span>";
	} elseif ($map_6 == "mp_bloc") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_bloc.png' title='Se map: $map_6'>Bloc</a></span>";
	} elseif ($map_6 == "mp_bog") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_bog.png' title='Se map: $map_6'>Bog</a></span>";
	} elseif ($map_6 == "mp_countdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_countdown.png' title='Se map: $map_6'>Countdown</a></span>";
	} elseif ($map_6 == "mp_crash") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_crash.png' title='Se map: $map_6'>Crash</a></span>";
	} elseif ($map_6 == "mp_crash_snow") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_crash_snow.png' title='Se map: $map_6'>Crash Winter</a></span>";
	} elseif ($map_6 == "mp_crossfire") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_crossfire.png' title='Se map: $map_6'>Crossfire</a></span>";
	} elseif ($map_6 == "mp_citystreets") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_citystreets.png' title='Se map: $map_6'>District</a></span>";
	} elseif ($map_6 == "mp_farm") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_farm.png' title='Se map: $map_6'>Downpour</a></span>";
	} elseif ($map_6 == "mp_overgrown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_overgrown.png' title='Se map: $map_6'>Overgrown</a></span>";
	} elseif ($map_6 == "mp_pipeline") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_pipeline.png' title='Se map: $map_6'>Pipeline</a></span>";
	} elseif ($map_6 == "mp_shipment") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_shipment.png' title='Se map: $map_6'>Shipment</a></span>";
	} elseif ($map_6 == "mp_showdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_showdown.png' title='Se map: $map_6'>Showdown</a></span>";
	} elseif ($map_6 == "mp_strike") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_strike.png' title='Se map: $map_6'>Strike</a></span>";
	} elseif ($map_6 == "mp_vacant") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_vacant.png' title='Se map: $map_6'>Vacant</a></span>";
	} elseif ($map_6 == "mp_cargoship") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/mp_cargoship.png' title='Se map: $map_6'>Wet Work</a></span>";
	}
}

echo	"</td><td width='10' class='matcher_info_bg_right_dark'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left'>
			<span class='matcher_text'>Server:&nbsp;&nbsp;&nbsp;</span>";

if($server == 1) {
echo			"<span class='matcher_text'>[ </span><span class='text_medlemmar'><a href=\"http://www.jalba.se/headz/forum/viewtopic.php?f=5&t=42\" title='Våran server info på vårat forum.' target='_blank'>Våran</a></span><span class='matcher_text'> ]</span>";
} elseif($server == 2) {
	if(!empty($server_url)){
	echo			"<span class='matcher_text'>[ </span><span class='text_medlemmar'><a href='$server_url' title='$server_url' target='_blank'>Deras</a></span><span class='matcher_text'> ]</span>";
	} else {
	echo			"<span class='matcher_text'>[ Deras ]</span>";
	}
}

echo	"</td><td width='10' class='matcher_info_bg_right'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left_dark'>
			<span class='matcher_text'>Admin(s): </span>";

if($admins == 1) {
echo			"<span class='matcher_text_info'>Vi</span>";
} elseif($admins == 2) {
echo			"<span class='matcher_text_info'>Dom</span>";
} elseif($admins == 0) {
echo			"<span class='matcher_text'>Inte tillgänglig</span>";
}

echo	"</td><td width='10' class='matcher_info_bg_right_dark'>
			&nbsp;
		</td></tr>
		<tr><td height='20' class='matcher_info_bg_left'>
			<span class='matcher_text'>DEMO:&nbsp;&nbsp;&nbsp;</span>";

if($demo == 1) {
echo			"<span class='matcher_text'>[ </span><span class='text_medlemmar'><a href='demo_info.php?demo=$demo_id' title='Se Demo #$demo_id' target='main'>DEMO</a></span><span class='matcher_text'> ]</span>";
} elseif($demo == 0) {
echo			"<span class='matcher_text'>[ Inte tillgänglig ]</span>";
}

echo	"</td><td class='matcher_info_bg_middle'>
			&nbsp;
		</td><td width='10' class='matcher_info_bg_right'>
			&nbsp;
		</td></tr>
		<tr><td colspan='2' class='matcher_info_bg_left_dark_bottom2' valign='top'>
			<span class='matcher_text'>Sammandrag: </span>
				<span class='matcher_text_info'>$text</span>
		</td><td width='10' class='matcher_info_bg_right_dark_bottom'>
			&nbsp;
		</td></tr>
		</table>

		<!-- KOMMENTARER FUNKTION KOMMER SNART -->

	</td></tr>
	</table>";


} elseif($game == 3) {

echo"<table width='100%' cellspacing='0' cellpadding='0' border='0'>
	<tr><td width='25' height='25' valign='middle' align='center' class='matcher_icon_left'>
		<img src='images/icons/icon_q3.gif' border='0' width='16' height='16' alt=''>
	</td><td height='25' valign='middle' align='center' class='matcher_icon_bg'>
		<span class='medlemmar_text_citat_info'>HeadZ</span><span class='medlemmar_text_citat'> vs </span><span class='medlemmar_text_citat_info'>$opponent</span><span class='medlemmar_text_citat'>&nbsp;&nbsp;&nbsp;[ </span><span class='medlemmar_text_citat_info'>$on</span><span class='medlemmar_text_citat'>on</span><span class='medlemmar_text_citat_info'>$on</span><span class='medlemmar_text_citat'> ]</span>
	</td><td width='25' height='25' valign='middle' align='center' class='matcher_icon_right'>
		<img src='images/icons/icon_q3.gif' border='0' width='16' height='16' alt=''>
	</td></tr>
	<tr><td valign='top' align='center' colspan='3'>

		<table width='100%' cellpadding='0' cellspacing='0' border='0'>
		<tr><td height='19' class='matcher_info_bg_left'>
			<span class='matcher_text'>Datum: </span>
				<span class='tid_info'>$date</span>
		</td><td class='matcher_info_bg_middle'>
			<span class='matcher_text'>Match ID: </span>
				<span class='matcher_text_info'>$id</span>
		</td><td width='10' class='matcher_info_bg_right'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left_dark'>
			<span class='matcher_text_info'>X</span><span class='matcher_text'>on</span><span class='matcher_text_info'>X</span><span class='matcher_text'>:&nbsp;&nbsp;&nbsp;</span>";
echo			"<span class='matcher_text'>[ </span>";
echo			"<span class='matcher_text_info'>$on</span>";
echo			"<span class='matcher_text'>on</span>";
echo			"<span class='matcher_text_info'>$on</span>";
echo			"<span class='matcher_text'> ]</span>";
echo	"</td><td width='241' height='180' rowspan='9' valign='middle' align='center' class='matcher_info_bg_bild'>";

if ($map_1 == "mp_convoy") {
    echo "<img src='images/maps/mp_convoy.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_backlot") {
    echo "<img src='images/maps/mp_backlot.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_bloc") {
    echo "<img src='images/maps/mp_bloc.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_bog") {
    echo "<img src='images/maps/mp_bog.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_countdown") {
    echo "<img src='images/maps/mp_countdown.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_crash") {
    echo "<img src='images/maps/mp_crash.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_crash_snow") {
    echo "<img src='images/maps/mp_crash_snow.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_crossfire") {
    echo "<img src='images/maps/mp_crossfire.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_citystreets") {
    echo "<img src='images/maps/mp_citystreets.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_farm") {
    echo "<img src='images/maps/mp_farm.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_overgrown") {
    echo "<img src='images/maps/mp_overgrown.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_pipeline") {
    echo "<img src='images/maps/mp_pipeline.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_shipment") {
    echo "<img src='images/maps/mp_shipment.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_showdown") {
    echo "<img src='images/maps/mp_showdown.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_strike") {
    echo "<img src='images/maps/mp_strike.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_vacant") {
    echo "<img src='images/maps/mp_vacant.gif' border='0' width='241' height='178' alt=''>";
} elseif ($map_1 == "mp_cargoship") {
    echo "<img src='images/maps/mp_cargoship.gif' border='0' width='241' height='178' alt=''>";
} else {
    echo "<img src='images/maps/na.gif' border='0' width='241' height='178' alt=''>";
}

echo	"</td><td width='10' class='matcher_info_bg_right_dark'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left'>
			<span class='matcher_text'>HeadZ: </span>";

if(!empty($team_headz_1)) {
echo			"<span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$team_headz_id_1&css' title='Se Profil: $team_headz_1' target='main'>$team_headz_1</a></span>";
}
if(!empty($team_headz_2)) {
echo			"<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$team_headz_id_2&css' title='Se Profil: $team_headz_2' target='main'>$team_headz_2</a></span>";
}
if(!empty($team_headz_3)) {
echo			"<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$team_headz_id_3&css' title='Se Profil: $team_headz_3' target='main'>$team_headz_3</a></span>";
}
if(!empty($team_headz_4)) {
echo			"<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$team_headz_id_4&css' title='Se Profil: $team_headz_4' target='main'>$team_headz_4</a></span>";
}
if(!empty($team_headz_5)) {
echo			"<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$team_headz_id_5&css' title='Se Profil: $team_headz_5' target='main'>$team_headz_5</a></span>";
}
if(!empty($team_headz_6)) {
echo			"<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='medlemmar_info.php?medlem=$team_headz_id_6&css' title='Se Profil: $team_headz_6' target='main'>$team_headz_6</a></span>";
}

echo	"</td><td width='10' class='matcher_info_bg_right'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left_dark'>
			<span class='matcher_text'>$opponent: </span>";

if(!empty($team_opponent_1)) {
echo			"<span class='matcher_text_info'>$team_opponent_1</span>";
}
if(!empty($team_opponent_2)) {
echo			"<span class='matcher_text'>, </span><span class='matcher_text_info'>$team_opponent_2</span>";
}
if(!empty($team_opponent_3)) {
echo			"<span class='matcher_text'>, </span><span class='matcher_text_info'>$team_opponent_3</span>";
}
if(!empty($team_opponent_4)) {
echo			"<span class='matcher_text'>, </span><span class='matcher_text_info'>$team_opponent_4</span>";
}
if(!empty($team_opponent_5)) {
echo			"<span class='matcher_text'>, </span><span class='matcher_text_info'>$team_opponent_5</span>";
}
if(!empty($team_opponent_6)) {
echo			"<span class='matcher_text'>, </span><span class='matcher_text_info'>$team_opponent_6</span>";
}

echo	"</td><td width='10' class='matcher_info_bg_right_dark'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left'>
			<span class='matcher_text'>Resultat:&nbsp;&nbsp;&nbsp;</span>";

if($result_headz > $result_opponent) {
echo					"<span class='matcher_text'> [ </span>";
echo					"<span class='matcher_won'>$result_headz</span>";
echo					"<span class='matcher_text'>:</span>";
echo					"<span class='matcher_lost'>$result_opponent</span>";
echo					"<span class='matcher_text'> ]</span>";
} elseif($result_headz == $result_opponent) {
echo					"<span class='matcher_text'>[ </span>";
echo					"<span class='matcher_equal'>$result_headz</span>";
echo					"<span class='matcher_text'>:</span>";
echo					"<span class='matcher_equal'>$result_opponent</span>";
echo					"<span class='matcher_text'> ]</span>";
} else {
echo					"<span class='matcher_text'>[ </span>";
echo					"<span class='matcher_lost'>$result_headz</span>";
echo					"<span class='matcher_text'>:</span>";
echo					"<span class='matcher_won'>$result_opponent</span>";
echo					"<span class='matcher_text'> ]</span>";
}

echo	"</td><td width='10' class='matcher_info_bg_right'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left_dark'>
			<span class='matcher_text'>Liga: </span>
				<span class='matcher_text_info'>$league</span>";

if(!empty($league_url)) {
echo				"<span class='matcher_text'>&nbsp;&nbsp;&nbsp;[</span><span class='text_medlemmar'><a href='$league_url' title='$league_url' target='_blank'>Matchlänk</a></span><span class='matcher_text'>]</span>";
}

echo	"</td><td width='10' class='matcher_info_bg_right_dark'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left'>
			<span class='matcher_text'>Typ: </span>";

if ($type == "DM") {
echo			"<span class='matcher_text_info'>Deathmatch </span><span class='matcher_text'>(</span><span class='matcher_text_info'>$type</span><span class='matcher_text'>)</span>";
} elseif ($type == "CTF") {
echo			"<span class='matcher_text_info'>Capture the Flag </span><span class='matcher_text'>(</span><span class='matcher_text_info'>$type</span><span class='matcher_text'>)</span>";
} elseif ($type == "TDM") {
echo			"<span class='matcher_text_info'>Team Deathmatch </span><span class='matcher_text'>(</span><span class='matcher_text_info'>$type</span><span class='matcher_text'>)</span>";
} else {
echo			"<span class='matcher_text'>Inte tillgänglig</span>";
}

echo	"</td><td width='10' class='matcher_info_bg_right'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left_dark'>
			<span class='matcher_text'>Maps: </span>";

if(!empty($map_1)) {
	if ($map_1 == "q3dm6") {
	    echo "<span class='matcher_text_info'>q3dm6</span>"; // ÄNDRA SENARE!!
	} elseif ($map_1 == "mp_backlot") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_backlot.jpg'>Backlot</a></span>";
	} elseif ($map_1 == "mp_bloc") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_bloc.jpg'>Bloc</a></span>";
	} elseif ($map_1 == "mp_bog") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_bog.jpg'>Bog</a></span>";
	} elseif ($map_1 == "mp_countdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_countdown.jpg'>Countdown</a></span>";
	} elseif ($map_1 == "mp_crash") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_crash.jpg'>Crash</a></span>";
	} elseif ($map_1 == "mp_crash_snow") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_crash_snow.jpg'>Crash Winter</a></span>";
	} elseif ($map_1 == "mp_crossfire") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_crossfire.jpg'>Crossfire</a></span>";
	} elseif ($map_1 == "mp_citystreets") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_citystreets.jpg'>District</a></span>";
	} elseif ($map_1 == "mp_farm") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_farm.jpg'>Downpour</a></span>";
	} elseif ($map_1 == "mp_overgrown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_overgrown.jpg'>Overgrown</a></span>";
	} elseif ($map_1 == "mp_pipeline") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_pipeline.jpg'>Pipeline</a></span>";
	} elseif ($map_1 == "mp_shipment") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_shipment.jpg'>Shipment</a></span>";
	} elseif ($map_1 == "mp_showdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_showdown.jpg'>Showdown</a></span>";
	} elseif ($map_1 == "mp_strike") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_strike.jpg'>Strike</a></span>";
	} elseif ($map_1 == "mp_vacant") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_vacant.jpg'>Vacant</a></span>";
	} elseif ($map_1 == "mp_cargoship") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_cargoship.jpg'>Wet Work</a></span>";
	}
} else {
echo	"<span class='matcher_text'>Inte tillgänglig</span>";
}

if(!empty($map_2)) {
	if ($map_2 == "mp_convoy") {
	    echo "<span class='text_medlemmar'><a href='images/maps/big/mp_convoy.jpg'>Ambush</a></span>";
	} elseif ($map_2 == "mp_backlot") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_backlot.jpg'>Backlot</a></span>";
	} elseif ($map_2 == "mp_bloc") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_bloc.jpg'>Bloc</a></span>";
	} elseif ($map_2 == "mp_bog") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_bog.jpg'>Bog</a></span>";
	} elseif ($map_2 == "mp_countdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_countdown.jpg'>Countdown</a></span>";
	} elseif ($map_2 == "mp_crash") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_crash.jpg'>Crash</a></span>";
	} elseif ($map_2 == "mp_crash_snow") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_crash_snow.jpg'>Crash Winter</a></span>";
	} elseif ($map_2 == "mp_crossfire") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_crossfire.jpg'>Crossfire</a></span>";
	} elseif ($map_2 == "mp_citystreets") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_citystreets.jpg'>District</a></span>";
	} elseif ($map_2 == "mp_farm") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_farm.jpg'>Downpour</a></span>";
	} elseif ($map_2 == "mp_overgrown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_overgrown.jpg'>Overgrown</a></span>";
	} elseif ($map_2 == "mp_pipeline") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_pipeline.jpg'>Pipeline</a></span>";
	} elseif ($map_2 == "mp_shipment") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_shipment.jpg'>Shipment</a></span>";
	} elseif ($map_2 == "mp_showdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_showdown.jpg'>Showdown</a></span>";
	} elseif ($map_2 == "mp_strike") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_strike.jpg'>Strike</a></span>";
	} elseif ($map_2 == "mp_vacant") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_vacant.jpg'>Vacant</a></span>";
	} elseif ($map_2 == "mp_cargoship") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_cargoship.jpg'>Wet Work</a></span>";
	}
}

if(!empty($map_3)) {
	if ($map_3 == "mp_convoy") {
	    echo "<span class='text_medlemmar'><a href='images/maps/big/mp_convoy.jpg'>Ambush</a></span>";
	} elseif ($map_3 == "mp_backlot") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_backlot.jpg'>Backlot</a></span>";
	} elseif ($map_3 == "mp_bloc") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_bloc.jpg'>Bloc</a></span>";
	} elseif ($map_3 == "mp_bog") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_bog.jpg'>Bog</a></span>";
	} elseif ($map_3 == "mp_countdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_countdown.jpg'>Countdown</a></span>";
	} elseif ($map_3 == "mp_crash") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_crash.jpg'>Crash</a></span>";
	} elseif ($map_3 == "mp_crash_snow") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_crash_snow.jpg'>Crash Winter</a></span>";
	} elseif ($map_3 == "mp_crossfire") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_crossfire.jpg'>Crossfire</a></span>";
	} elseif ($map_3 == "mp_citystreets") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_citystreets.jpg'>District</a></span>";
	} elseif ($map_3 == "mp_farm") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_farm.jpg'>Downpour</a></span>";
	} elseif ($map_3 == "mp_overgrown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_overgrown.jpg'>Overgrown</a></span>";
	} elseif ($map_3 == "mp_pipeline") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_pipeline.jpg'>Pipeline</a></span>";
	} elseif ($map_3 == "mp_shipment") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_shipment.jpg'>Shipment</a></span>";
	} elseif ($map_3 == "mp_showdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_showdown.jpg'>Showdown</a></span>";
	} elseif ($map_3 == "mp_strike") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_strike.jpg'>Strike</a></span>";
	} elseif ($map_3 == "mp_vacant") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_vacant.jpg'>Vacant</a></span>";
	} elseif ($map_3 == "mp_cargoship") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_cargoship.jpg'>Wet Work</a></span>";
	}
}

if(!empty($map_4)) {
	if ($map_4 == "mp_convoy") {
	    echo "<span class='text_medlemmar'><a href='images/maps/big/mp_convoy.jpg'>Ambush</a></span>";
	} elseif ($map_4 == "mp_backlot") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_backlot.jpg'>Backlot</a></span>";
	} elseif ($map_4 == "mp_bloc") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_bloc.jpg'>Bloc</a></span>";
	} elseif ($map_4 == "mp_bog") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_bog.jpg'>Bog</a></span>";
	} elseif ($map_4 == "mp_countdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_countdown.jpg'>Countdown</a></span>";
	} elseif ($map_4 == "mp_crash") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_crash.jpg'>Crash</a></span>";
	} elseif ($map_4 == "mp_crash_snow") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_crash_snow.jpg'>Crash Winter</a></span>";
	} elseif ($map_4 == "mp_crossfire") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_crossfire.jpg'>Crossfire</a></span>";
	} elseif ($map_4 == "mp_citystreets") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_citystreets.jpg'>District</a></span>";
	} elseif ($map_4 == "mp_farm") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_farm.jpg'>Downpour</a></span>";
	} elseif ($map_4 == "mp_overgrown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_overgrown.jpg'>Overgrown</a></span>";
	} elseif ($map_4 == "mp_pipeline") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_pipeline.jpg'>Pipeline</a></span>";
	} elseif ($map_4 == "mp_shipment") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_shipment.jpg'>Shipment</a></span>";
	} elseif ($map_4 == "mp_showdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_showdown.jpg'>Showdown</a></span>";
	} elseif ($map_4 == "mp_strike") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_strike.jpg'>Strike</a></span>";
	} elseif ($map_4 == "mp_vacant") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_vacant.jpg'>Vacant</a></span>";
	} elseif ($map_4 == "mp_cargoship") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_cargoship.jpg'>Wet Work</a></span>";
	}
}

if(!empty($map_5)) {
	if ($map_5 == "mp_convoy") {
	    echo "<span class='text_medlemmar'><a href='images/maps/big/mp_convoy.jpg'>Ambush</a></span>";
	} elseif ($map_5 == "mp_backlot") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_backlot.jpg'>Backlot</a></span>";
	} elseif ($map_5 == "mp_bloc") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_bloc.jpg'>Bloc</a></span>";
	} elseif ($map_5 == "mp_bog") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_bog.jpg'>Bog</a></span>";
	} elseif ($map_5 == "mp_countdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_countdown.jpg'>Countdown</a></span>";
	} elseif ($map_5 == "mp_crash") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_crash.jpg'>Crash</a></span>";
	} elseif ($map_5 == "mp_crash_snow") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_crash_snow.jpg'>Crash Winter</a></span>";
	} elseif ($map_5 == "mp_crossfire") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_crossfire.jpg'>Crossfire</a></span>";
	} elseif ($map_5 == "mp_citystreets") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_citystreets.jpg'>District</a></span>";
	} elseif ($map_5 == "mp_farm") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_farm.jpg'>Downpour</a></span>";
	} elseif ($map_5 == "mp_overgrown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_overgrown.jpg'>Overgrown</a></span>";
	} elseif ($map_5 == "mp_pipeline") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_pipeline.jpg'>Pipeline</a></span>";
	} elseif ($map_5 == "mp_shipment") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_shipment.jpg'>Shipment</a></span>";
	} elseif ($map_5 == "mp_showdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_showdown.jpg'>Showdown</a></span>";
	} elseif ($map_5 == "mp_strike") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_strike.jpg'>Strike</a></span>";
	} elseif ($map_5 == "mp_vacant") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_vacant.jpg'>Vacant</a></span>";
	} elseif ($map_5 == "mp_cargoship") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_cargoship.jpg'>Wet Work</a></span>";
	}
}

if(!empty($map_6)) {
	if ($map_6 == "mp_convoy") {
	    echo "<span class='text_medlemmar'><a href='images/maps/big/mp_convoy.jpg'>Ambush</a></span>";
	} elseif ($map_6 == "mp_backlot") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_backlot.jpg'>Backlot</a></span>";
	} elseif ($map_6 == "mp_bloc") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_bloc.jpg'>Bloc</a></span>";
	} elseif ($map_6 == "mp_bog") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_bog.jpg'>Bog</a></span>";
	} elseif ($map_6 == "mp_countdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_countdown.jpg'>Countdown</a></span>";
	} elseif ($map_6 == "mp_crash") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_crash.jpg'>Crash</a></span>";
	} elseif ($map_6 == "mp_crash_snow") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_crash_snow.jpg'>Crash Winter</a></span>";
	} elseif ($map_6 == "mp_crossfire") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_crossfire.jpg'>Crossfire</a></span>";
	} elseif ($map_6 == "mp_citystreets") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_citystreets.jpg'>District</a></span>";
	} elseif ($map_6 == "mp_farm") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_farm.jpg'>Downpour</a></span>";
	} elseif ($map_6 == "mp_overgrown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_overgrown.jpg'>Overgrown</a></span>";
	} elseif ($map_6 == "mp_pipeline") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_pipeline.jpg'>Pipeline</a></span>";
	} elseif ($map_6 == "mp_shipment") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_shipment.jpg'>Shipment</a></span>";
	} elseif ($map_6 == "mp_showdown") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_showdown.jpg'>Showdown</a></span>";
	} elseif ($map_6 == "mp_strike") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_strike.jpg'>Strike</a></span>";
	} elseif ($map_6 == "mp_vacant") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_vacant.jpg'>Vacant</a></span>";
	} elseif ($map_6 == "mp_cargoship") {
	    echo "<span class='matcher_text'>, </span><span class='text_medlemmar'><a href='images/maps/big/mp_cargoship.jpg'>Wet Work</a></span>";
	}
}

echo	"</td><td width='10' class='matcher_info_bg_right_dark'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left'>
			<span class='matcher_text'>Server:&nbsp;&nbsp;&nbsp;</span>";

if($server == 1) {
echo			"<span class='matcher_text'>[ </span><span class='text_medlemmar'><a href=\"http://www.jalba.se/headz/forum/viewtopic.php?f=13&t=30\" title='Våran server info på vårat forum.' target='_blank'>Våran</a></span><span class='matcher_text'> ]</span>";
} elseif($server == 2) {
	if(!empty($server_url)){
	echo			"<span class='matcher_text'>[ </span><span class='text_medlemmar'><a href='$server_url' title='$server_url' target='_blank'>Deras</a></span><span class='matcher_text'> ]</span>";
	} else {
	echo			"<span class='matcher_text'>[ Deras ]</span>";
	}
}

echo	"</td><td width='10' class='matcher_info_bg_right'>
			&nbsp;
		</td></tr>
		<tr><td class='matcher_info_bg_left_dark'>
			<span class='matcher_text'>Admin(s): </span>";

if($admins == 1) {
echo			"<span class='matcher_text_info'>Vi</span>";
} elseif($admins == 2) {
echo			"<span class='matcher_text_info'>Dom</span>";
} elseif($admins == 0) {
echo			"<span class='matcher_text'>Inte tillgänglig</span>";
}

echo	"</td><td width='10' class='matcher_info_bg_right_dark'>
			&nbsp;
		</td></tr>
		<tr><td height='20' class='matcher_info_bg_left'>
			<span class='matcher_text'>DEMO:&nbsp;&nbsp;&nbsp;</span>";

if($demo == 1) {
echo			"<span class='matcher_text'>[ </span><span class='text_medlemmar'><a href='demo_info.php?demo=$demo_id' title='Se Demo #$demo_id' target='main'>DEMO</a></span><span class='matcher_text'> ]</span>";
} elseif($demo == 0) {
echo			"<span class='matcher_text'>[ Inte tillgänglig ]</span>";
}

echo	"</td><td class='matcher_info_bg_middle'>
			&nbsp;
		</td><td width='10' class='matcher_info_bg_right'>
			&nbsp;
		</td></tr>
		<tr><td colspan='2' class='matcher_info_bg_left_dark_bottom2' valign='top'>
			<span class='matcher_text'>Sammandrag: </span>
				<span class='matcher_text_info'>$text</span>
		</td><td width='10' class='matcher_info_bg_right_dark_bottom'>
			&nbsp;
		</td></tr>
		</table>

		<!-- KOMMENTARER FUNKTION KOMMER SNART -->

	</td></tr>
	</table>";

 }
}

?>

</td></tr>
</table>


</body>
</html>
