-- phpMyAdmin SQL Dump
-- version 4.3.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 26, 2014 at 07:56 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.2

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `database_name`
--
CREATE DATABASE IF NOT EXISTS `database_name` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `database_name`;

-- --------------------------------------------------------

--
-- Table structure for table `legacy_headz_configs`
--

CREATE TABLE IF NOT EXISTS `legacy_headz_configs` (
  `id` int(11) NOT NULL,
  `game` int(11) NOT NULL,
  `profile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_id` int(11) NOT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `downloads` int(11) NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_headz_configs`
--

INSERT INTO `legacy_headz_configs` (`id`, `game`, `profile`, `profile_id`, `date`, `size`, `quantity`, `downloads`, `text`) VALUES
(1, 1, 'admin', 1, '00:00 @ 01/02-14', 1, 0, 0, 'Text');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_headz_demos`
--

CREATE TABLE IF NOT EXISTS `legacy_headz_demos` (
  `id` int(11) NOT NULL,
  `match_id` int(11) NOT NULL,
  `game` int(11) NOT NULL,
  `profile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_id` int(11) NOT NULL,
  `uploaddate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `opponent` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_headz_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_headz_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_headz_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_headz_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_headz_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_headz_6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_headz_id_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_headz_id_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_headz_id_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_headz_id_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_headz_id_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_headz_id_6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_opponent_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_opponent_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_opponent_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_opponent_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_opponent_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_opponent_6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `server` int(11) NOT NULL,
  `server_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `map` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `demo` int(11) NOT NULL,
  `on` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `downloads` int(11) NOT NULL,
  `map_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `map_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `map_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `map_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `map_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `map_6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `result_headz` int(11) NOT NULL,
  `result_opponent` int(11) NOT NULL,
  `league` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `league_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_headz_demos`
--

INSERT INTO `legacy_headz_demos` (`id`, `match_id`, `game`, `profile`, `profile_id`, `uploaddate`, `date`, `size`, `opponent`, `team_headz_1`, `team_headz_2`, `team_headz_3`, `team_headz_4`, `team_headz_5`, `team_headz_6`, `team_headz_id_1`, `team_headz_id_2`, `team_headz_id_3`, `team_headz_id_4`, `team_headz_id_5`, `team_headz_id_6`, `team_opponent_1`, `team_opponent_2`, `team_opponent_3`, `team_opponent_4`, `team_opponent_5`, `team_opponent_6`, `server`, `server_url`, `map`, `type`, `demo`, `on`, `downloads`, `map_1`, `map_2`, `map_3`, `map_4`, `map_5`, `map_6`, `result_headz`, `result_opponent`, `league`, `league_url`, `version`, `text`) VALUES
(1, 1, 1, 'admin', 1, '00:00 @ 01/02-14', '00:00 @ 01/02-14', 1, 'Opponent', 'Profile 1', 'Profile 2', 'Profile 3', 'Profile 4', 'Profile 5', 'Profile 6', '1', '2', '3', '4', '5', '6', 'Opponent 1', 'Opponent 2', 'Opponent 3', 'Opponent 4', 'Opponent 5', 'Opponent 6', 1, 'Server url', 'de_aztec', 'Type', 1, '6', 0, 'Map 1', 'Map 2', 'Map 3', 'Map 4', 'Map 5', 'Map 6', 12, 0, 'League', 'League url', 'Version', 'Text');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_headz_matcher`
--

CREATE TABLE IF NOT EXISTS `legacy_headz_matcher` (
  `id` int(11) NOT NULL,
  `profile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_id` int(11) NOT NULL,
  `game` int(11) NOT NULL,
  `opponent` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `on` int(11) NOT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `league` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `league_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `map` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `server` int(11) NOT NULL,
  `server_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `result_headz` int(11) NOT NULL,
  `result_opponent` int(11) NOT NULL,
  `team_headz_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_headz_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_headz_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_headz_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_headz_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_headz_6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_headz_id_1` int(11) NOT NULL,
  `team_headz_id_2` int(11) NOT NULL,
  `team_headz_id_3` int(11) NOT NULL,
  `team_headz_id_4` int(11) NOT NULL,
  `team_headz_id_5` int(11) NOT NULL,
  `team_headz_id_6` int(11) NOT NULL,
  `team_opponent_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_opponent_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_opponent_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_opponent_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_opponent_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_opponent_6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `demo` int(11) NOT NULL,
  `demo_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admins` int(11) NOT NULL,
  `map_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `map_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `map_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `map_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `map_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `map_6` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_headz_matcher`
--

INSERT INTO `legacy_headz_matcher` (`id`, `profile`, `profile_id`, `game`, `opponent`, `on`, `date`, `league`, `league_url`, `map`, `server`, `server_url`, `text`, `result_headz`, `result_opponent`, `team_headz_1`, `team_headz_2`, `team_headz_3`, `team_headz_4`, `team_headz_5`, `team_headz_6`, `team_headz_id_1`, `team_headz_id_2`, `team_headz_id_3`, `team_headz_id_4`, `team_headz_id_5`, `team_headz_id_6`, `team_opponent_1`, `team_opponent_2`, `team_opponent_3`, `team_opponent_4`, `team_opponent_5`, `team_opponent_6`, `demo`, `demo_id`, `type`, `admins`, `map_1`, `map_2`, `map_3`, `map_4`, `map_5`, `map_6`) VALUES
(1, 'admin', 1, 1, 'Opponent', 6, '00:00 @ 01/02-14', 'League', 'League url', 'de_aztec', 1, 'Server url', 'Text', 0, 12, 'Profile 1', 'Profile 2', 'Profile 3', 'Profile 4', 'Profile 5', 'Profile 6', 1, 2, 3, 4, 5, 6, 'Opponent 1', 'Opponent 2', 'Opponent 3', 'Opponent 4', 'Opponent 5', 'Opponent 6', 1, 1, '', 1, '', '', '', '', '', ''),
(2, 'admin', 1, 2, 'Opponent', 6, '00:00 @ 01/02-14', 'League', 'League url', 'mp_backlot', 1, 'Server url', 'Text', 12, 12, 'Profile 1', 'Profile 2', 'Profile 3', 'Profile 4', 'Profile 5', 'Profile 6', 1, 2, 3, 4, 5, 6, 'Opponent 1', 'Opponent 2', 'Opponent 3', 'Opponent 4', 'Opponent 5', 'Opponent 6', 0, 0, 'TDM', 1, 'mp_convoy', 'mp_backlot', 'mp_bloc', 'mp_bog', 'mp_countdown', 'mp_crash'),
(3, 'admin', 1, 3, 'Opponent', 6, '00:00 @ 01/02-14', 'League', 'League url', '', 1, 'Server url', 'Text', 12, 0, 'Profile 1', 'Profile 2', 'Profile 3', 'Profile 4', 'Profile 5', 'Profile 6', 1, 2, 3, 4, 5, 6, 'Opponent 1', 'Opponent 2', 'Opponent 3', 'Opponent 4', 'Opponent 5', 'Opponent 6', 0, 0, 'CTF', 1, 'q3dm6', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_headz_medlem`
--

CREATE TABLE IF NOT EXISTS `legacy_headz_medlem` (
  `id` int(11) NOT NULL,
  `nick` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_aka` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_last` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `online` tinyint(4) NOT NULL DEFAULT '0',
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `age` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quote` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cpu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `graphiccard` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `headset` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mouse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mousepad` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `memory` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keyboard` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `motherboard` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `screen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `homepage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `favourite_movie` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `favourite_game` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `favourite_music` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `favourite_drink` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `css_resolution` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `css_sensitivity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `css_steamid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `css_favourite_gun` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `css_favourite_pistol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `css_favourite_map` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `css_favourite_side` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `css_favourite_player` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cod4_resolution` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cod4_sensitivity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cod4_favourite_gun` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cod4_favourite_gun_attachment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cod4_favourite_pistol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cod4_favourite_pistol_attachment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cod4_favourite_map` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cod4_favourite_perk_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cod4_favourite_perk_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cod4_favourite_perk_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `q3_resolution` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `q3_sensitivity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `q3_favourite_map` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `q3_favourite_player` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wc3_resolution` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wc3_favourite_race` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wc3_favourite_hero` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wc3_favourite_map` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wc3_favourite_player` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_headz_medlem`
--

INSERT INTO `legacy_headz_medlem` (`id`, `nick`, `name`, `name_aka`, `name_last`, `online`, `city`, `age`, `gender`, `quote`, `cpu`, `graphiccard`, `headset`, `mouse`, `mousepad`, `memory`, `keyboard`, `motherboard`, `screen`, `homepage`, `text`, `favourite_movie`, `favourite_game`, `favourite_music`, `favourite_drink`, `css_resolution`, `css_sensitivity`, `css_steamid`, `css_favourite_gun`, `css_favourite_pistol`, `css_favourite_map`, `css_favourite_side`, `css_favourite_player`, `cod4_resolution`, `cod4_sensitivity`, `cod4_favourite_gun`, `cod4_favourite_gun_attachment`, `cod4_favourite_pistol`, `cod4_favourite_pistol_attachment`, `cod4_favourite_map`, `cod4_favourite_perk_1`, `cod4_favourite_perk_2`, `cod4_favourite_perk_3`, `q3_resolution`, `q3_sensitivity`, `q3_favourite_map`, `q3_favourite_player`, `wc3_resolution`, `wc3_favourite_race`, `wc3_favourite_hero`, `wc3_favourite_map`, `wc3_favourite_player`) VALUES
(1, 'admin', 'Name', 'Name aka', 'Name last', 0, 'City', 'Age', 'Gender', 'Quote', 'Cpu', 'Graphic card', 'Headset', 'Mouse', 'Mousepad', 'Memory', 'Keyboard', 'Motherboard', 'Screen', 'http://homepage', 'Text', 'Favorite movie', 'Favorite game', 'Favorite music', 'Favorite drink', '-', 'CS:S sensitivity', 'CS:S steam_id', '-', '-', 'de_aztec', '-', 'CS:S favorite player', '-', 'COD4 sensitivity', '-', '-', '-', '-', 'mp_backlot', '-', '-', '-', '-', 'Q3 sensitivity', 'Q3 favorite map', 'Q3 favorite player', '-', '-', '-', 'WC3 favorite map', 'WC3 favorite player');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_headz_nyheter`
--

CREATE TABLE IF NOT EXISTS `legacy_headz_nyheter` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_id` int(11) NOT NULL,
  `profile_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_headz_nyheter`
--

INSERT INTO `legacy_headz_nyheter` (`id`, `title`, `profile`, `profile_id`, `profile_img`, `date`, `text`) VALUES
(1, 'Title', 'admin', 1, NULL, '00:00 @ 01/02-14', 'Text :D ^^');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_headz_players`
--

CREATE TABLE IF NOT EXISTS `legacy_headz_players` (
  `id` int(11) NOT NULL,
  `user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `online` tinyint(4) NOT NULL DEFAULT '0',
  `profile_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_headz_players`
--

INSERT INTO `legacy_headz_players` (`id`, `user`, `pass`, `online`, `profile_id`) VALUES
(1, 'admin', 'admin', 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `legacy_headz_configs`
--
ALTER TABLE `legacy_headz_configs`
ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_headz_demos`
--
ALTER TABLE `legacy_headz_demos`
ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_headz_matcher`
--
ALTER TABLE `legacy_headz_matcher`
ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_headz_medlem`
--
ALTER TABLE `legacy_headz_medlem`
ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_headz_nyheter`
--
ALTER TABLE `legacy_headz_nyheter`
ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_headz_players`
--
ALTER TABLE `legacy_headz_players`
ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `legacy_headz_configs`
--
ALTER TABLE `legacy_headz_configs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `legacy_headz_demos`
--
ALTER TABLE `legacy_headz_demos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `legacy_headz_matcher`
--
ALTER TABLE `legacy_headz_matcher`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `legacy_headz_medlem`
--
ALTER TABLE `legacy_headz_medlem`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `legacy_headz_nyheter`
--
ALTER TABLE `legacy_headz_nyheter`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `legacy_headz_players`
--
ALTER TABLE `legacy_headz_players`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
