<?php

require("conn.php");	// Databasanslutningen
include("inc/functions.php");

$self = $_SERVER['PHP_SELF'];

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/iframe.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3406958-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>

</head>
<body>


<table width="100%" height='100%' cellspacing="0" cellpadding="0" border="0" align="center">
<tr><td width="100%" valign="top" align="left" class="main_bg">

<?php
if (!isset($_GET['medlem'])) {
  $medlem = 0;
}
else
{
  $medlem = intval ( $_GET['medlem'] );
  /* Använd intval() för att undvika s.k. SQL INJECTIONS,
     dvs. att folk kan typ radera din databas... */
}

$sql = "SELECT * FROM legacy_headz_medlem WHERE id = :id LIMIT 1";
$stmt = $conn->prepare($sql);
$stmt->bindParam(':id', $medlem, PDO::PARAM_INT);
$stmt->execute();
if ($row = $stmt->fetch()) {
$id = $row['id'];
$nick = $row['nick'];
$name = $row['name'];
$name_aka = $row['name_aka'];
$name_last = $row['name_last'];
$city = $row['city'];
$age = $row['age'];
$gender = $row['gender'];
$quote = $row['quote'];
$cpu = $row['cpu'];
$graphiccard = $row['graphiccard'];
$headset = $row['headset'];
$mouse = $row['mouse'];
$mousepad = $row['mousepad'];
$memory = $row['memory'];
$keyboard = $row['keyboard'];
$motherboard = $row['motherboard'];
$screen = $row['screen'];
$homepage = $row['homepage'];
$text = $row['text'];
$favourite_movie = $row['favourite_movie'];
$favourite_game = $row['favourite_game'];
$favourite_music = $row['favourite_music'];
$favourite_drink = $row['favourite_drink'];

$css_resolution = 		$row['css_resolution'];
$css_sensitivity = 		$row['css_sensitivity'];
$css_steamid = 			$row['css_steamid'];
$css_favourite_gun = 	$row['css_favourite_gun'];
$css_favourite_pistol = $row['css_favourite_pistol'];
$css_favourite_map = 	$row['css_favourite_map'];
$css_favourite_side = 	$row['css_favourite_side'];
$css_favourite_player = $row['css_favourite_player'];

$cod4_resolution = 		$row['cod4_resolution'];
$cod4_sensitivity = 	$row['cod4_sensitivity'];
$cod4_favourite_gun = 	$row['cod4_favourite_gun'];
$cod4_favourite_gun_attachment = 	$row['cod4_favourite_gun_attachment'];
$cod4_favourite_pistol = $row['cod4_favourite_pistol'];
$cod4_favourite_pistol_attachment = $row['cod4_favourite_pistol_attachment'];
$cod4_favourite_map = 	$row['cod4_favourite_map'];
$cod4_favourite_perk_1 = $row['cod4_favourite_perk_1'];
$cod4_favourite_perk_2 = $row['cod4_favourite_perk_2'];
$cod4_favourite_perk_3 = $row['cod4_favourite_perk_3'];

$q3_resolution = 		$row['q3_resolution'];
$q3_sensitivity = 		$row['q3_sensitivity'];
$q3_favourite_map = 	$row['q3_favourite_map'];
$q3_favourite_player = 	$row['q3_favourite_player'];

$wc3_resolution = 		$row['wc3_resolution'];
$wc3_favourite_race = 	$row['wc3_favourite_race'];
$wc3_favourite_hero = 	$row['wc3_favourite_hero'];
$wc3_favourite_map = 	$row['wc3_favourite_map'];
$wc3_favourite_player = $row['wc3_favourite_player'];


echo"<table width='100%' height='100%' cellspacing='0' cellpadding='0' border='0'>
	<tr><td valign='top'>

		<table width='100%' height='100%' cellspacing='0' cellpadding='0' border='0'>
		<tr><td width='100%' height='25' valign='middle' align='center' class='historia_rubrik'>
			<span class='medlemmar_text_citat'>Citat: ''</span><span class='medlemmar_text_citat_info'>$quote</span><span class='medlemmar_text_citat'>''</span>
		</td></tr>
		<tr><td width='100%' height='100%' valign='top' class='medlemmar_bg'>

			<table width='100%' height='100%' cellspacing='0' cellpadding='0' border='0' class='medlemmar_bg2'>
			<tr><td valign='top'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				</td><td width='25%' height='20' class='medlemmar_games' valign='middle' align='center'>
					<span class='text_medlemmar_info'><a href='medlemmar_info.php?medlem=$id&css' target='main'>CS: Source</a></span>
				</td><td width='25%' height='20' class='medlemmar_games' valign='middle' align='center'>
					<span class='text_medlemmar_info'><a href='medlemmar_info.php?medlem=$id&cod4' target='main'>Call of Duty 4</a></span>
				</td><td width='25%' height='20' class='medlemmar_games' valign='middle' align='center'>
					<span class='text_medlemmar_info'><a href='medlemmar_info.php?medlem=$id&q3' target='main'>Quake III</a></span>
				</td><td width='25%' height='20' class='medlemmar_games' valign='middle' align='center'>
					<span class='text_medlemmar_info'><a href='medlemmar_info.php?medlem=$id&wc3' target='main'>Warcraft III</a></span>
				</td></tr>
				</table>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td width='80' height='80' rowspan='4' class='medlemmar_text_bild' align='center'>";

if ($id == 1) {
    echo "<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 2) {
    echo "<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 3) {
    echo "<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 4) {
    echo "<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 5) {
    echo "<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 6) {
    echo "<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 7) {
    echo "<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 8) {
    echo "<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 9) {
    echo "<img src='images/profile/9.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 10) {
    echo "<img src='images/profile/10.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 11) {
    echo "<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 12) {
    echo "<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 13) {
    echo "<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 14) {
    echo "<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 15) {
    echo "<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 16) {
    echo "<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 17) {
    echo "<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
} else {
    echo "<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
}

echo			"</td><td width='45%' height='20' class='medlemmar_text_left_top'>
					<span class='medlemmar_text'>Nick: </span>
						<span class='medlemmar_text_info'>$nick</span>
				</td><td class='medlemmar_text_right'>
					<span class='medlemmar_text'>CPU: </span>
						<span class='medlemmar_text_info'>$cpu</span>
				</td></tr>
				<tr><td height='20' class='medlemmar_text_left_top_dark'>
					<span class='medlemmar_text'>Namn: </span>
						<span class='medlemmar_text_info'>$name</span>";
if(!empty($name_aka)) {
echo						"<span class='matcher_text'> ''</span><span class='medlemmar_text_info'>$name_aka</span><span class='matcher_text'>''</span>";
}
echo					"<span class='medlemmar_text_info'> $name_last</span>
				</td><td class='medlemmar_text_right_dark'>
					<span class='medlemmar_text'>Grafikkort: </span>
						<span class='medlemmar_text_info'>$graphiccard</span>
				</td></tr>
				<tr><td height='20' class='medlemmar_text_left_top'>
					<span class='medlemmar_text'>Stad: </span>
						<span class='medlemmar_text_info'>$city</span>
				</td><td class='medlemmar_text_right'>
					<span class='medlemmar_text'>Headset: </span>
						<span class='medlemmar_text_info'>$headset</span>
				</td></tr>
				<tr><td height='19' class='medlemmar_text_left_top_dark'>
					<span class='medlemmar_text'>Ålder: </span>
						<span class='medlemmar_text_info'>$age</span>
				</td><td class='medlemmar_text_right_dark'>
					<span class='medlemmar_text'>Mus: </span>
						<span class='medlemmar_text_info'>$mouse</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg'>";

if(!empty($homepage)) {
echo				"<span class='text_medlemmar_info'><a href=\"$homepage\" target='_blank'>HEMSIDA</a></span>";
} else {
echo				"<span class='text_medlemmar'>HEMSIDA</span>";
}

echo			"</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Kön: </span>
						<span class='medlemmar_text_info'>$gender</span>
				</td><td class='medlemmar_text_right' height='20'>
					<span class='medlemmar_text'>Musmatta: </span>
						<span class='medlemmar_text_info'>$mousepad</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_dark'>
					<span class='text_medlemmar'>PM</span>
				</td>
				<td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>Favorit drink: </span>
						<span class='medlemmar_text_info'>$favourite_drink</span>
				</td><td class='medlemmar_text_right_dark' height='20'>
					<span class='medlemmar_text'>Minne: </span>
						<span class='medlemmar_text_info'>$memory</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg'>
					<span class='text_medlemmar'>CFGs</span>
				</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Favorit musik: </span>
						<span class='medlemmar_text_info'>$favourite_music</span>
				</td><td class='medlemmar_text_right' height='20'>
					<span class='medlemmar_text'>Tangentbord: </span>
						<span class='medlemmar_text_info'>$keyboard</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_dark'>
					<span class='text_medlemmar'>DEMOs</span>
				</td><td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>Favorit film(er): </span>
						<span class='medlemmar_text_info'>$favourite_movie</span>
				</td><td class='medlemmar_text_right_dark' height='20'>
					<span class='medlemmar_text'>Moderkort: </span>
						<span class='medlemmar_text_info'>$motherboard</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg'>
					<span class='text_medlemmar'>MATCHER</span>
				</td><td height='20' class='medlemmar_text_left_middle'>
					<span class='medlemmar_text'>Favorit spel: </span>
						<span class='medlemmar_text_info'>$favourite_game</span>
				</td><td class='medlemmar_text_right_middle' height='20'>
					<span class='medlemmar_text'>Skärm: </span>
						<span class='medlemmar_text_info'>$screen</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_dark'>
					<span class='text_medlemmar'>NYHETER</span>";

if(isset($_GET['css'])) {
echo			"</td><td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>Upplösning: </span>
						<span class='medlemmar_text_info'>$css_resolution</span>
				</td><td class='medlemmar_text_right_dark' height='20'>
					<span class='medlemmar_text'>Favorit gevär: </span>
						<span class='medlemmar_text_info'>$css_favourite_gun</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Sensitivity: </span>
						<span class='medlemmar_text_info'>$css_sensitivity</span>
				</td><td class='medlemmar_text_right' height='20'>
					<span class='medlemmar_text'>Favorit pistol: </span>
						<span class='medlemmar_text_info'>$css_favourite_pistol</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_dark'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>SteamID: </span>
						<span class='medlemmar_text_info'>$css_steamid</span>
				</td><td class='medlemmar_text_right_dark' height='20'>
					<span class='medlemmar_text'>Favorit sida: </span>
						<span class='medlemmar_text_info'>$css_favourite_side</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_bottom'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Favorit map: </span>";

if ($css_favourite_map == "-") {
    echo "<span class='medlemmar_text_info'>-</span>";
} elseif ($css_favourite_map == "de_aztec") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_aztec.gif' title='Se map: $css_favourite_map'>de_aztec</a></span>";
} elseif ($css_favourite_map == "de_cbble") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_cbble.gif' title='Se map: $css_favourite_map'>de_cbble</a></span>";
} elseif ($css_favourite_map == "de_cpl_fire") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_cpl_fire.gif' title='Se map: $css_favourite_map'>de_cpl_fire</a></span>";
} elseif ($css_favourite_map == "de_cpl_mill") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_cpl_mill.gif' title='Se map: $css_favourite_map'>de_cpl_mill</a></span>";
} elseif ($css_favourite_map == "de_cpl_strike") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_cpl_strike.gif' title='Se map: $css_favourite_map'>de_cpl_strike</a></span>";
} elseif ($css_favourite_map == "de_dust2") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_dust2.gif' title='Se map: $css_favourite_map'>de_dust2</a></span>";
} elseif ($css_favourite_map == "de_dust") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_dust.gif' title='Se map: $css_favourite_map'>de_dust</a></span>";
} elseif ($css_favourite_map == "de_forge") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_forge.gif' title='Se map: $css_favourite_map'>de_forge</a></span>";
} elseif ($css_favourite_map == "de_inferno") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_inferno.gif' title='Se map: $css_favourite_map'>de_inferno</a></span>";
} elseif ($css_favourite_map == "de_nuke") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_nuke.gif' title='Se map: $css_favourite_map'>de_nuke</a></span>";
} elseif ($css_favourite_map == "de_prodigy") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_prodigy.gif' title='Se map: $css_favourite_map'>de_prodigy</a></span>";
} elseif ($css_favourite_map == "de_russka") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_russka.gif' title='Se map: $css_favourite_map'>de_russka</a></span>";
} elseif ($css_favourite_map == "de_train") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_train.gif' title='Se map: $css_favourite_map'>de_train</a></span>";
} elseif ($css_favourite_map == "de_tuscan") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_tuscan.gif' title='Se map: $css_favourite_map'>de_tuscan</a></span>";
} elseif ($css_favourite_map == "de_contra") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_contra.gif' title='Se map: $css_favourite_map'>de_contra</a></span>";
} elseif ($css_favourite_map == "de_phoenix") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_phoenix.gif' title='Se map: $css_favourite_map'>de_phoenix</a></span>";
} elseif ($css_favourite_map == "de_season") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_season.gif' title='Se map: $css_favourite_map'>de_season</a></span>";
}

echo			"</td><td class='medlemmar_text_right' height='20'>
					<span class='medlemmar_text'>Favorit spelare: </span>
						<span class='medlemmar_text_info'>$css_favourite_player</span>
				</td></tr>";
}

if(isset($_GET['cod4'])) {
echo			"</td><td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>Upplösning: </span>
						<span class='medlemmar_text_info'>$cod4_resolution</span>
				</td><td class='medlemmar_text_right_dark' height='20'>
					<span class='medlemmar_text'>Favorit map: </span>";

if ($cod4_favourite_map == "-") {
    echo "<span class='medlemmar_text_info'>-</span>";
} elseif ($cod4_favourite_map == "mp_convoy") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_convoy.png' title='Se map: $cod4_favourite_map'>Ambush</a></span>";
} elseif ($cod4_favourite_map == "mp_backlot") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_backlot.png' title='Se map: $cod4_favourite_map'>Backlot</a></span>";
} elseif ($cod4_favourite_map == "mp_bloc") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_bloc.png' title='Se map: $cod4_favourite_map'>Bloc</a></span>";
} elseif ($cod4_favourite_map == "mp_bog") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_bog.png' title='Se map: $cod4_favourite_map'>Bog</a></span>";
} elseif ($cod4_favourite_map == "mp_countdown") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_countdown.png' title='Se map: $cod4_favourite_map'>Countdown</a></span>";
} elseif ($cod4_favourite_map == "mp_crash") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_crash.png' title='Se map: $cod4_favourite_map'>Crash</a></span>";
} elseif ($cod4_favourite_map == "mp_crash_snow") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_crash_snow.png' title='Se map: $cod4_favourite_map'>Crash Winter</a></span>";
} elseif ($cod4_favourite_map == "mp_crossfire") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_crossfire.png' title='Se map: $cod4_favourite_map'>Crossfire</a></span>";
} elseif ($cod4_favourite_map == "mp_citystreets") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_citystreets.png' title='Se map: $cod4_favourite_map'>District</a></span>";
} elseif ($cod4_favourite_map == "mp_farm") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_farm.png' title='Se map: $cod4_favourite_map'>Downpour</a></span>";
} elseif ($cod4_favourite_map == "mp_overgrown") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_overgrown.png' title='Se map: $cod4_favourite_map'>Overgrown</a></span>";
} elseif ($cod4_favourite_map == "mp_pipeline") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_pipeline.png' title='Se map: $cod4_favourite_map'>Pipeline</a></span>";
} elseif ($cod4_favourite_map == "mp_shipment") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_shipment.png' title='Se map: $cod4_favourite_map'>Shipment</a></span>";
} elseif ($cod4_favourite_map == "mp_showdown") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_showdown.png' title='Se map: $cod4_favourite_map'>Showdown</a></span>";
} elseif ($cod4_favourite_map == "mp_strike") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_strike.png' title='Se map: $cod4_favourite_map'>Strike</a></span>";
} elseif ($cod4_favourite_map == "mp_vacant") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_vacant.png' title='Se map: $cod4_favourite_map'>Vacant</a></span>";
} elseif ($cod4_favourite_map == "mp_cargoship") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_cargoship.png' title='Se map: $cod4_favourite_map'>Wet Work</a></span>";
}

echo			"</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Sensitivity: </span>
						<span class='medlemmar_text_info'>$cod4_sensitivity</span>
				</td><td class='medlemmar_text_right' height='20'>
					<span class='medlemmar_text'>Favorit perk #1 (Blå): </span>
						<span class='medlemmar_text_info'>$cod4_favourite_perk_1</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_dark'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>Favorit gevär: </span>
						<span class='medlemmar_text_info'>$cod4_favourite_gun</span>";

if($cod4_favourite_gun_attachment == "-") {
} elseif ($cod4_favourite_gun_attachment == "Grenade Launcher") {
echo					"<span class='medlemmar_text_info'> med $cod4_favourite_gun_attachment</span>";
} elseif ($cod4_favourite_gun_attachment == "Silencer") {
echo					"<span class='medlemmar_text_info'> med $cod4_favourite_gun_attachment</span>";
} elseif ($cod4_favourite_gun_attachment == "Red Dot Sight") {
echo					"<span class='medlemmar_text_info'> med $cod4_favourite_gun_attachment</span>";
} elseif ($cod4_favourite_gun_attachment == "ACOG Scope") {
echo					"<span class='medlemmar_text_info'> med $cod4_favourite_gun_attachment</span>";
} elseif ($cod4_favourite_gun_attachment == "Silencer2") {
echo					"<span class='medlemmar_text_info'> med Silencer</span>";
} elseif ($cod4_favourite_gun_attachment == "Red Dot Sight2") {
echo					"<span class='medlemmar_text_info'> med Red Dot Sight</span>";
} elseif ($cod4_favourite_gun_attachment == "ACOG Scope2") {
echo					"<span class='medlemmar_text_info'> med ACOG Scope</span>";
} elseif ($cod4_favourite_gun_attachment == "Red Dot Sight3") {
echo					"<span class='medlemmar_text_info'> med Red Dot Sight</span>";
} elseif ($cod4_favourite_gun_attachment == "Grip") {
echo					"<span class='medlemmar_text_info'> med Grip</span>";
} elseif ($cod4_favourite_gun_attachment == "ACOG Scope3") {
echo					"<span class='medlemmar_text_info'> med ACOG Scope</span>";
} elseif ($cod4_favourite_gun_attachment == "Red Dot Sight4") {
echo					"<span class='medlemmar_text_info'> med Red Dot Sight</span>";
} elseif ($cod4_favourite_gun_attachment == "Grip2") {
echo					"<span class='medlemmar_text_info'> med Grip</span>";
} elseif ($cod4_favourite_gun_attachment == "ACOG Scope4") {
echo					"<span class='medlemmar_text_info'> med ACOG Scope</span>";
}

echo			"</td><td class='medlemmar_text_right_dark' height='20'>
					<span class='medlemmar_text'>Favorit perk #2 (Röd): </span>
						<span class='medlemmar_text_info'>$cod4_favourite_perk_2</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_bottom'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Favorit pistol: </span>
						<span class='medlemmar_text_info'>$cod4_favourite_pistol </span>";

if($cod4_favourite_pistol_attachment == "-") {
} elseif ($cod4_favourite_pistol_attachment == "Silencer" && $cod4_favourite_pistol != "Desert Eagle" && $cod4_favourite_pistol != "Gold Desert Eagle") {
echo					"<span class='medlemmar_text_info'>med $cod4_favourite_pistol_attachment</span>";
}

echo			"</td><td class='medlemmar_text_right' height='20'>
					<span class='medlemmar_text'>Favorit perk #3 (Gul): </span>
						<span class='medlemmar_text_info'>$cod4_favourite_perk_3</span>
				</td></tr>";

}

if(isset($_GET['q3'])) {
echo			"</td><td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>Upplösning: </span>
						<span class='medlemmar_text_info'>$q3_resolution</span>
				</td><td class='medlemmar_text_right_dark' height='20'>
					&nbsp;
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Sensitivity: </span>
						<span class='medlemmar_text_info'>$q3_sensitivity</span>
				</td><td class='medlemmar_text_right' height='20'>
					&nbsp;
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_dark'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>Favorit map: </span>
						<span class='medlemmar_text_info'>$q3_favourite_map</span>
				</td><td class='medlemmar_text_right_dark' height='20'>
					&nbsp;
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_bottom'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Favorit spelare: </span>
						<span class='medlemmar_text_info'>$q3_favourite_player</span>
				</td><td class='medlemmar_text_right' height='20'>
					&nbsp;
				</td></tr>";
}

if(isset($_GET['wc3'])) {
echo			"</td><td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>Upplösning: </span>
						<span class='medlemmar_text_info'>$wc3_resolution</span>
				</td><td class='medlemmar_text_right_dark' height='20'>
					<span class='medlemmar_text'>Favorit spelare: </span>
						<span class='medlemmar_text_info'>$wc3_favourite_player</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Favorit ras: </span>
						<span class='medlemmar_text_info'>$wc3_favourite_race</span>
				</td><td class='medlemmar_text_right' height='20'>
					&nbsp;
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_dark'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>Favorit hero: </span>
						<span class='medlemmar_text_info'>$wc3_favourite_hero</span>
				</td><td class='medlemmar_text_right_dark' height='20'>
					&nbsp;
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_bottom'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Favorit map: </span>
						<span class='medlemmar_text_info'>$wc3_favourite_map</span>
				</td><td class='medlemmar_text_right' height='20'>
					&nbsp;
				</td></tr>";
}

echo			"<tr><td class='medlemmar_text_historia_bg' colspan='3' valign='middle' height='20'>
					<span class='medlemmar_text'>Historia/bakgrund:</span>
				</td></tr>
				<tr><td class='medlemmar_text_historia_text_bg' colspan='3' valign='top'>";
echo 				"<span class='hem_text'>";
echo					bbkod($text);
echo 				"</span>";
echo			"</td></tr>
				</table>

			</td></tr>
			</table>

		</td></tr>
		</table>

	</td></tr>
	</table>";

}

?>

</td></tr>
</table>


</body>
</html>
