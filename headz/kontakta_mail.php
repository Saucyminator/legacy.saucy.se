<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/iframe.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3406958-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>

</head>
<body>


<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
<tr><td width="100%" height="100%" valign="top" align="left" class="main_bg">

	<table width="100%" cellspacing="0" cellpadding="0" border="0" class="kontakta_error">
	<tr><td valign="top">

<?php

// anger en variabel som kan lagra de eventuella felaktigheterna
$errors = array();

// kontrollera om ett Namn angivits
if (!$_POST["name"])
$errors[] = "<span class='medlemmar_text'>&raquo; </span><span class='medlemmar_text_info'>Ditt namn</span>";

// kontrollera om ett Ämne angivits
if (!$_POST["subject"])
$errors[] = "<span class='medlemmar_text'>&raquo; </span><span class='medlemmar_text_info'>Ärende i ämnesraden</span>";

// kontrollera om en Epostadress angivits
$emailcheck = $_POST["email"];
if(!preg_match("/^[a-z0-9\å\ä\ö._-]+@[a-z0-9\å\ä\ö.-]+\.[a-z]{2,6}$/i", $emailcheck))
$errors[] = "<span class='medlemmar_text'>&raquo; </span><span class='medlemmar_text_info'>Din epostadress</span>";

// kontrollera om ett Meddelande angivits
if (!$_POST["message"])
$errors[] = "<span class='medlemmar_text'>&raquo; </span><span class='medlemmar_text_info'>Inget meddelande har skrivits!</span>";

//kontrollera om scriptet anropas från ditt formulär
if ($_SERVER['HTTP_REFERER']!= "http://www.jalba.se/headz/kontakta.php")
$errors[] = "<span class='medlemmar_text'>&raquo; </span><span class='medlemmar_text_info'>Din användning av vårt Formmail är inte tillåten!</span>";

// om felaktig information finns visas detta meddelande
if(count($errors)>0) {
echo "<span class='medlemmar_text'><b>Följande information måste anges innan du kan skicka formuläret:</b><br><br></span>";
foreach($errors as $fel)
echo "<span class='medlemmar_text'>$fel <br></span>";
echo "<span class='medlemmar_text'><br>Ange den information som saknas och skicka formuläret igen. Tack! </span><br>";
echo "<span class='text_medlemmar'><a href='kontakta.php' title='Gå tillbaka' target='main'>klicka här för att komma tillbaka till formuläret</a></span>";
} else {
// formuläret är korrekt ifyllt och informationen bearbetas
$to = "email@removed.com";
$from = $_POST["email"];
$subject = $_POST["subject"];
$name = $_POST["name"];
$message = $_POST["message"];

	if(mail($to, $subject, $message ,"From: $name <$from>")) {
		echo nl2br("<span class='medlemmar_text'><font size='2'>Ditt meddelande har skickats!</font></span>
		<span class='medlemmar_text'><b>Ämne:</b> </span><span class='medlemmar_text_info'>$subject</span>
		<span class='medlemmar_text'><b>Meddelande:</b></span>
		<span class='medlemmar_text_info'>$message</span>
		");
	} else {
		echo "<span class='medlemmar_text'>Det gick inte att skicka ditt meddelande</span>";
	}
}

?>

	</td></tr>
	</table>

</td></tr>
</table>


</body>
</html>
