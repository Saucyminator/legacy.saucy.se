<?php
session_start(); // Alltid överst på sidan

// Kolla om inloggad = sessionen satt
if (!isset($_SESSION['sess_user'])){
  header("Location: index.php");
  exit;
}

require("conn.php");	// Databasanslutningen
include("inc/functions.php");

$self = $_SERVER['PHP_SELF'];


/*************************************\
| ******** Tobias Bleckert ********** |
| ********* www.tb-one.se *********** |
| ****** Nyhets script v.2.1 ******** |
| ******** Modifierat script ******** |
| *Spara dessa rader för användning * |
\*************************************/

//Kolla resurser och ob_start()-funktionen för mer information
ob_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><?php require("header_title.html"); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/iframe.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/javascript.js"></script>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3406958-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>

</head>
<body>


<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" valign="top" align="center">
<tr><td width="100%" height="100%" valign="top" align="left" class="admin_main_bg">

<?php

//Kod för att lägga till genom ett forumlär
if(isset($_GET['viewprofile'])) {

$resultat = mysql_query("SELECT * FROM legacy_headz_medlem WHERE nick='{$_SESSION['sess_user']}'") or die (mysql_error());
if($row = mysql_fetch_array($resultat)) {
$id = $row['id'];
$nick = $row['nick'];
$name = $row['name'];
$name_aka = $row['name_aka'];
$name_last = $row['name_last'];
$city = $row['city'];
$age = $row['age'];
$gender = $row['gender'];
$quote = $row['quote'];
$cpu = $row['cpu'];
$graphiccard = $row['graphiccard'];
$headset = $row['headset'];
$mouse = $row['mouse'];
$mousepad = $row['mousepad'];
$memory = $row['memory'];
$keyboard = $row['keyboard'];
$motherboard = $row['motherboard'];
$screen = $row['screen'];
$homepage = $row['homepage'];
$text = $row['text'];
$favourite_movie = $row['favourite_movie'];
$favourite_game = $row['favourite_game'];
$favourite_music = $row['favourite_music'];
$favourite_drink = $row['favourite_drink'];

$css_resolution = 		$row['css_resolution'];
$css_sensitivity = 		$row['css_sensitivity'];
$css_steamid = 			$row['css_steamid'];
$css_favourite_gun = 	$row['css_favourite_gun'];
$css_favourite_pistol = $row['css_favourite_pistol'];
$css_favourite_map = 	$row['css_favourite_map'];
$css_favourite_side = 	$row['css_favourite_side'];
$css_favourite_player = $row['css_favourite_player'];

$cod4_resolution = 		$row['cod4_resolution'];
$cod4_sensitivity = 	$row['cod4_sensitivity'];
$cod4_favourite_gun = 	$row['cod4_favourite_gun'];
$cod4_favourite_gun_attachment = $row['cod4_favourite_gun_attachment'];
$cod4_favourite_pistol =$row['cod4_favourite_pistol'];
$cod4_favourite_pistol_attachment = $row['cod4_favourite_pistol_attachment'];
$cod4_favourite_map = 	$row['cod4_favourite_map'];
$cod4_favourite_perk_1 = $row['cod4_favourite_perk_1'];
$cod4_favourite_perk_2 = $row['cod4_favourite_perk_2'];
$cod4_favourite_perk_3 = $row['cod4_favourite_perk_3'];

$q3_resolution = 		$row['q3_resolution'];
$q3_sensitivity = 		$row['q3_sensitivity'];
$q3_favourite_map = 	$row['q3_favourite_map'];
$q3_favourite_player = $row['q3_favourite_player'];

$wc3_resolution = 		$row['wc3_resolution'];
$wc3_favourite_race = 	$row['wc3_favourite_race'];
$wc3_favourite_hero = 	$row['wc3_favourite_hero'];
$wc3_favourite_map = 	$row['wc3_favourite_map'];
$wc3_favourite_player = $row['wc3_favourite_player'];

echo"<table width='100%' height='100%' cellspacing='0' cellpadding='0' border='0'>
	<tr><td valign='top'>

		<table width='100%' height='100%' cellspacing='0' cellpadding='0' border='0'>
		<tr><td width='100%' height='25' valign='middle' align='center' class='historia_rubrik'>
			<span class='medlemmar_text_citat'>Citat: ''</span><span class='profil_text'><a href='admin_profil.php' title='Gå tillbaka' target='main'>$quote</a></span><span class='medlemmar_text_citat'>''</span>		</td></tr>
		<tr><td width='100%' height='100%' valign='top' class='medlemmar_bg'>

			<table width='100%' height='100%' cellspacing='0' cellpadding='0' border='0' class='medlemmar_bg2'>
			<tr><td valign='top'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				</td><td width='25%' height='20' class='medlemmar_games' valign='middle' align='center'>
					<span class='text_medlemmar_info'><a href='admin_profil.php?viewprofile&css' target='main'>CS: Source</a></span>
				</td><td width='25%' height='20' class='medlemmar_games' valign='middle' align='center'>
					<span class='text_medlemmar_info'><a href='admin_profil.php?viewprofile&cod4' target='main'>Call of Duty 4</a></span>
				</td><td width='25%' height='20' class='medlemmar_games' valign='middle' align='center'>
					<span class='text_medlemmar_info'><a href='admin_profil.php?viewprofile&q3' target='main'>Quake III</a></span>
				</td><td width='25%' height='20' class='medlemmar_games' valign='middle' align='center'>
					<span class='text_medlemmar_info'><a href='admin_profil.php?viewprofile&wc3' target='main'>Warcraft III</a></span>
				</td></tr>
				</table>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td width='80' height='80' rowspan='4' class='medlemmar_text_bild' align='center'>";

if ($id == 1) {
    echo "<img src='images/profile/1.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 2) {
    echo "<img src='images/profile/2.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 3) {
    echo "<img src='images/profile/3.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 4) {
    echo "<img src='images/profile/4.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 5) {
    echo "<img src='images/profile/5.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 6) {
    echo "<img src='images/profile/6.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 7) {
    echo "<img src='images/profile/7.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 8) {
    echo "<img src='images/profile/8.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 9) {
    echo "<img src='images/profile/9.jpg' border='0' width='80' height='80' alt=''>";
} elseif ($id == 10) {
    echo "<img src='images/profile/10.jpg' border='0' width='80' height='80' alt=''>";
} else {
    echo "<img src='images/profile/na.jpg' border='0' width='80' height='80' alt=''>";
}

echo			"</td><td width='45%' height='20' class='medlemmar_text_left_top'>
					<span class='medlemmar_text'>Nick: </span>
						<span class='medlemmar_text_info'>$nick</span>
				</td><td class='medlemmar_text_right'>
					<span class='medlemmar_text'>CPU: </span>
						<span class='medlemmar_text_info'>$cpu</span>
				</td></tr>
				<tr><td height='20' class='medlemmar_text_left_top_dark'>
					<span class='medlemmar_text'>Namn: </span>
						<span class='medlemmar_text_info'>$name</span>";
if(!empty($name_aka)) {
echo						"<span class='matcher_text'> ''</span><span class='medlemmar_text_info'>$name_aka</span><span class='matcher_text'>''</span>";
}
echo					"<span class='medlemmar_text_info'> $name_last</span>
				</td><td class='medlemmar_text_right_dark'>
					<span class='medlemmar_text'>Grafikkort: </span>
						<span class='medlemmar_text_info'>$graphiccard</span>
				</td></tr>
				<tr><td height='20' class='medlemmar_text_left_top'>
					<span class='medlemmar_text'>Stad: </span>
						<span class='medlemmar_text_info'>$city</span>
				</td><td class='medlemmar_text_right'>
					<span class='medlemmar_text'>Headset: </span>
						<span class='medlemmar_text_info'>$headset</span>
				</td></tr>
				<tr><td height='19' class='medlemmar_text_left_top_dark'>
					<span class='medlemmar_text'>Ålder: </span>
						<span class='medlemmar_text_info'>$age</span>
				</td><td class='medlemmar_text_right_dark'>
					<span class='medlemmar_text'>Mus: </span>
						<span class='medlemmar_text_info'>$mouse</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg'>";

if(!empty($homepage)) {
echo				"<span class='text_medlemmar_info'><a href=\"$homepage\" target='_blank'>HEMSIDA</a></span>";
} else {
echo				"<span class='text_medlemmar'>HEMSIDA</span>";
}

echo			"</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Kön: </span>
						<span class='medlemmar_text_info'>$gender</span>
				</td><td class='medlemmar_text_right' height='20'>
					<span class='medlemmar_text'>Musmatta: </span>
						<span class='medlemmar_text_info'>$mousepad</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_dark'>
					<span class='text_medlemmar'>PM</span>
				</td>
				<td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>Favorit drink: </span>
						<span class='medlemmar_text_info'>$favourite_drink</span>
				</td><td class='medlemmar_text_right_dark' height='20'>
					<span class='medlemmar_text'>Minne: </span>
						<span class='medlemmar_text_info'>$memory</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg'>
					<span class='text_medlemmar'>CFGs</span>
				</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Favorit musik: </span>
						<span class='medlemmar_text_info'>$favourite_music</span>
				</td><td class='medlemmar_text_right' height='20'>
					<span class='medlemmar_text'>Tangentbord: </span>
						<span class='medlemmar_text_info'>$keyboard</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_dark'>
					<span class='text_medlemmar'>DEMOs</span>
				</td><td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>Favorit film(er): </span>
						<span class='medlemmar_text_info'>$favourite_movie</span>
				</td><td class='medlemmar_text_right_dark' height='20'>
					<span class='medlemmar_text'>Moderkort: </span>
						<span class='medlemmar_text_info'>$motherboard</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg'>
					<span class='text_medlemmar'>MATCHER</span>
				</td><td height='20' class='medlemmar_text_left_middle'>
					<span class='medlemmar_text'>Favorit spel: </span>
						<span class='medlemmar_text_info'>$favourite_game</span>
				</td><td class='medlemmar_text_right_middle' height='20'>
					<span class='medlemmar_text'>Skärm: </span>
						<span class='medlemmar_text_info'>$screen</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_dark'>
					<span class='text_medlemmar'>NYHETER</span>";

if(isset($_GET['css'])) {
echo			"</td><td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>Upplösning: </span>
						<span class='medlemmar_text_info'>$css_resolution</span>
				</td><td class='medlemmar_text_right_dark' height='20'>
					<span class='medlemmar_text'>Favorit gevär: </span>
						<span class='medlemmar_text_info'>$css_favourite_gun</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Sensitivity: </span>
						<span class='medlemmar_text_info'>$css_sensitivity</span>
				</td><td class='medlemmar_text_right' height='20'>
					<span class='medlemmar_text'>Favorit pistol: </span>
						<span class='medlemmar_text_info'>$css_favourite_pistol</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_dark'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>SteamID: </span>
						<span class='medlemmar_text_info'>$css_steamid</span>
				</td><td class='medlemmar_text_right_dark' height='20'>
					<span class='medlemmar_text'>Favorit sida: </span>
						<span class='medlemmar_text_info'>$css_favourite_side</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_bottom'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Favorit map: </span>";

if ($css_favourite_map == "-") {
    echo "<span class='medlemmar_text_info'>-</span>";
} elseif ($css_favourite_map == "de_aztec") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_aztec.gif' title='Se map: $css_favourite_map'>de_aztec</a></span>";
} elseif ($css_favourite_map == "de_cbble") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_cbble.gif' title='Se map: $css_favourite_map'>de_cbble</a></span>";
} elseif ($css_favourite_map == "de_cpl_fire") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_cpl_fire.gif' title='Se map: $css_favourite_map'>de_cpl_fire</a></span>";
} elseif ($css_favourite_map == "de_cpl_mill") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_cpl_mill.gif' title='Se map: $css_favourite_map'>de_cpl_mill</a></span>";
} elseif ($css_favourite_map == "de_cpl_strike") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_cpl_strike.gif' title='Se map: $css_favourite_map'>de_cpl_strike</a></span>";
} elseif ($css_favourite_map == "de_dust2") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_dust2.gif' title='Se map: $css_favourite_map'>de_dust2</a></span>";
} elseif ($css_favourite_map == "de_dust") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_dust.gif' title='Se map: $css_favourite_map'>de_dust</a></span>";
} elseif ($css_favourite_map == "de_forge") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_forge.gif' title='Se map: $css_favourite_map'>de_forge</a></span>";
} elseif ($css_favourite_map == "de_inferno") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_inferno.gif' title='Se map: $css_favourite_map'>de_inferno</a></span>";
} elseif ($css_favourite_map == "de_nuke") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_nuke.gif' title='Se map: $css_favourite_map'>de_nuke</a></span>";
} elseif ($css_favourite_map == "de_prodigy") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_prodigy.gif' title='Se map: $css_favourite_map'>de_prodigy</a></span>";
} elseif ($css_favourite_map == "de_russka") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_russka.gif' title='Se map: $css_favourite_map'>de_russka</a></span>";
} elseif ($css_favourite_map == "de_train") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_train.gif' title='Se map: $css_favourite_map'>de_train</a></span>";
} elseif ($css_favourite_map == "de_tuscan") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_tuscan.gif' title='Se map: $css_favourite_map'>de_tuscan</a></span>";
} elseif ($css_favourite_map == "de_contra") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_contra.gif' title='Se map: $css_favourite_map'>de_contra</a></span>";
} elseif ($css_favourite_map == "de_phoenix") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_phoenix.gif' title='Se map: $css_favourite_map'>de_phoenix</a></span>";
} elseif ($css_favourite_map == "de_season") {
    echo "<span class='text_medlemmar'><a href='images/maps/de_season.gif' title='Se map: $css_favourite_map'>de_season</a></span>";
}

echo			"</td><td class='medlemmar_text_right' height='20'>
					<span class='medlemmar_text'>Favorit spelare: </span>
						<span class='medlemmar_text_info'>$css_favourite_player</span>
				</td></tr>";
}

if(isset($_GET['cod4'])) {
echo			"</td><td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>Upplösning: </span>
						<span class='medlemmar_text_info'>$cod4_resolution</span>
				</td><td class='medlemmar_text_right_dark' height='20'>
					<span class='medlemmar_text'>Favorit map: </span>";

if ($cod4_favourite_map == "-") {
    echo "<span class='medlemmar_text_info'>-</span>";
} elseif ($cod4_favourite_map == "mp_convoy") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_convoy.png' title='Se map: $cod4_favourite_map'>Ambush</a></span>";
} elseif ($cod4_favourite_map == "mp_backlot") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_backlot.png' title='Se map: $cod4_favourite_map'>Backlot</a></span>";
} elseif ($cod4_favourite_map == "mp_bloc") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_bloc.png' title='Se map: $cod4_favourite_map'>Bloc</a></span>";
} elseif ($cod4_favourite_map == "mp_bog") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_bog.png' title='Se map: $cod4_favourite_map'>Bog</a></span>";
} elseif ($cod4_favourite_map == "mp_countdown") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_countdown.png' title='Se map: $cod4_favourite_map'>Countdown</a></span>";
} elseif ($cod4_favourite_map == "mp_crash") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_crash.png' title='Se map: $cod4_favourite_map'>Crash</a></span>";
} elseif ($cod4_favourite_map == "mp_crash_snow") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_crash_snow.png' title='Se map: $cod4_favourite_map'>Crash Winter</a></span>";
} elseif ($cod4_favourite_map == "mp_crossfire") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_crossfire.png' title='Se map: $cod4_favourite_map'>Crossfire</a></span>";
} elseif ($cod4_favourite_map == "mp_citystreets") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_citystreets.png' title='Se map: $cod4_favourite_map'>District</a></span>";
} elseif ($cod4_favourite_map == "mp_farm") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_farm.png' title='Se map: $cod4_favourite_map'>Downpour</a></span>";
} elseif ($cod4_favourite_map == "mp_overgrown") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_overgrown.png' title='Se map: $cod4_favourite_map'>Overgrown</a></span>";
} elseif ($cod4_favourite_map == "mp_pipeline") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_pipeline.png' title='Se map: $cod4_favourite_map'>Pipeline</a></span>";
} elseif ($cod4_favourite_map == "mp_shipment") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_shipment.png' title='Se map: $cod4_favourite_map'>Shipment</a></span>";
} elseif ($cod4_favourite_map == "mp_showdown") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_showdown.png' title='Se map: $cod4_favourite_map'>Showdown</a></span>";
} elseif ($cod4_favourite_map == "mp_strike") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_strike.png' title='Se map: $cod4_favourite_map'>Strike</a></span>";
} elseif ($cod4_favourite_map == "mp_vacant") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_vacant.png' title='Se map: $cod4_favourite_map'>Vacant</a></span>";
} elseif ($cod4_favourite_map == "mp_cargoship") {
    echo "<span class='text_medlemmar'><a href='images/maps/mp_cargoship.png' title='Se map: $cod4_favourite_map'>Wet Work</a></span>";
}

echo			"</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Sensitivity: </span>
						<span class='medlemmar_text_info'>$cod4_sensitivity</span>
				</td><td class='medlemmar_text_right' height='20'>
					<span class='medlemmar_text'>Favorit perk #1 (Blå): </span>
						<span class='medlemmar_text_info'>$cod4_favourite_perk_1</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_dark'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>Favorit gevär: </span>
						<span class='medlemmar_text_info'>$cod4_favourite_gun</span>";

if($cod4_favourite_gun_attachment == "-") {
} elseif ($cod4_favourite_gun_attachment == "Grenade Launcher") {
echo					"<span class='medlemmar_text_info'> med $cod4_favourite_gun_attachment</span>";
} elseif ($cod4_favourite_gun_attachment == "Silencer") {
echo					"<span class='medlemmar_text_info'> med $cod4_favourite_gun_attachment</span>";
} elseif ($cod4_favourite_gun_attachment == "Red Dot Sight") {
echo					"<span class='medlemmar_text_info'> med $cod4_favourite_gun_attachment</span>";
} elseif ($cod4_favourite_gun_attachment == "ACOG Scope") {
echo					"<span class='medlemmar_text_info'> med $cod4_favourite_gun_attachment</span>";
} elseif ($cod4_favourite_gun_attachment == "Silencer2") {
echo					"<span class='medlemmar_text_info'> med Silencer</span>";
} elseif ($cod4_favourite_gun_attachment == "Red Dot Sight2") {
echo					"<span class='medlemmar_text_info'> med Red Dot Sight</span>";
} elseif ($cod4_favourite_gun_attachment == "ACOG Scope2") {
echo					"<span class='medlemmar_text_info'> med ACOG Scope</span>";
} elseif ($cod4_favourite_gun_attachment == "Red Dot Sight3") {
echo					"<span class='medlemmar_text_info'> med Red Dot Sight</span>";
} elseif ($cod4_favourite_gun_attachment == "Grip") {
echo					"<span class='medlemmar_text_info'> med Grip</span>";
} elseif ($cod4_favourite_gun_attachment == "ACOG Scope3") {
echo					"<span class='medlemmar_text_info'> med ACOG Scope</span>";
} elseif ($cod4_favourite_gun_attachment == "Red Dot Sight4") {
echo					"<span class='medlemmar_text_info'> med Red Dot Sight</span>";
} elseif ($cod4_favourite_gun_attachment == "Grip2") {
echo					"<span class='medlemmar_text_info'> med Grip</span>";
} elseif ($cod4_favourite_gun_attachment == "ACOG Scope4") {
echo					"<span class='medlemmar_text_info'> med ACOG Scope</span>";
}

echo			"</td><td class='medlemmar_text_right_dark' height='20'>
					<span class='medlemmar_text'>Favorit perk #2 (Röd): </span>
						<span class='medlemmar_text_info'>$cod4_favourite_perk_2</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_bottom'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Favorit pistol: </span>
						<span class='medlemmar_text_info'>$cod4_favourite_pistol </span>";

if($cod4_favourite_pistol_attachment == "-") {
} elseif ($cod4_favourite_pistol_attachment == "Silencer" && $cod4_favourite_pistol != "Desert Eagle" && $cod4_favourite_pistol != "Gold Desert Eagle") {
echo					"<span class='medlemmar_text_info'>med $cod4_favourite_pistol_attachment</span>";
}

echo			"</td><td class='medlemmar_text_right' height='20'>
					<span class='medlemmar_text'>Favorit perk #3 (Gul): </span>
						<span class='medlemmar_text_info'>$cod4_favourite_perk_3</span>
				</td></tr>";

}

if(isset($_GET['q3'])) {
echo			"</td><td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>Upplösning: </span>
						<span class='medlemmar_text_info'>$q3_resolution</span>
				</td><td class='medlemmar_text_right_dark' height='20'>
					&nbsp;
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Sensitivity: </span>
						<span class='medlemmar_text_info'>$q3_sensitivity</span>
				</td><td class='medlemmar_text_right' height='20'>
					&nbsp;
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_dark'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>Favorit map: </span>
						<span class='medlemmar_text_info'>$q3_favourite_map</span>
				</td><td class='medlemmar_text_right_dark' height='20'>
					&nbsp;
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_bottom'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Favorit spelare: </span>
						<span class='medlemmar_text_info'>$q3_favourite_player</span>
				</td><td class='medlemmar_text_right' height='20'>
					&nbsp;
				</td></tr>";
}

if(isset($_GET['wc3'])) {
echo			"</td><td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>Upplösning: </span>
						<span class='medlemmar_text_info'>$wc3_resolution</span>
				</td><td class='medlemmar_text_right_dark' height='20'>
					<span class='medlemmar_text'>Favorit spelare: </span>
						<span class='medlemmar_text_info'>$wc3_favourite_player</span>
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Favorit ras: </span>
						<span class='medlemmar_text_info'>$wc3_favourite_race</span>
				</td><td class='medlemmar_text_right' height='20'>
					&nbsp;
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_dark'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left_dark'>
					<span class='medlemmar_text'>Favorit hero: </span>
						<span class='medlemmar_text_info'>$wc3_favourite_hero</span>
				</td><td class='medlemmar_text_right_dark' height='20'>
					&nbsp;
				</td></tr>
				<tr><td height='20' align='center' class='medlemmar_text_info_bg_bottom'>
					<span class='text_medlemmar'>&nbsp;</span>
				</td><td height='20' class='medlemmar_text_left'>
					<span class='medlemmar_text'>Favorit map: </span>
						<span class='medlemmar_text_info'>$wc3_favourite_map</span>
				</td><td class='medlemmar_text_right' height='20'>
					&nbsp;
				</td></tr>";
}

echo			"<tr><td class='medlemmar_text_historia_bg' colspan='3' valign='middle' height='20'>
					<span class='medlemmar_text'>Historia/bakgrund:</span>
				</td></tr>
				<tr><td class='medlemmar_text_historia_text_bg' colspan='3' valign='top'>";
echo 				"<span class='hem_text'>";
echo					bbkod($text);
echo 				"</span>";
echo			"</td></tr>
				</table>

			</td></tr>
			</table>

		</td></tr>
		</table>

	</td></tr>
	</table>";

 }
}


//Kod för att ändra en nyhet med vald ID
if(isset($_GET['editprofile'])) {

//Denna kod hämtar ut informationen som vi ska ändra på
$qnyheter = mysql_query("SELECT * FROM legacy_headz_medlem WHERE nick='{$_SESSION['sess_user']}'") or exit(mysql_error());
if($row = mysql_fetch_array($qnyheter)) {
$id = $row['id'];
$nick = $row['nick'];
$name = $row['name'];
$name_aka = $row['name_aka'];
$name_last = $row['name_last'];
$city = $row['city'];
$age = $row['age'];
$gender = $row['gender'];
$quote = $row['quote'];
$cpu = $row['cpu'];
$graphiccard = $row['graphiccard'];
$headset = $row['headset'];
$mouse = $row['mouse'];
$mousepad = $row['mousepad'];
$memory = $row['memory'];
$keyboard = $row['keyboard'];
$motherboard = $row['motherboard'];
$screen = $row['screen'];
$homepage = $row['homepage'];
$text = $row['text'];
$favourite_movie = $row['favourite_movie'];
$favourite_game = $row['favourite_game'];
$favourite_music = $row['favourite_music'];
$favourite_drink = $row['favourite_drink'];

$css_resolution = 		$row['css_resolution'];
$css_sensitivity = 		$row['css_sensitivity'];
$css_steamid = 			$row['css_steamid'];
$css_favourite_gun = 	$row['css_favourite_gun'];
$css_favourite_pistol = $row['css_favourite_pistol'];
$css_favourite_map = 	$row['css_favourite_map'];
$css_favourite_side = 	$row['css_favourite_side'];
$css_favourite_player = $row['css_favourite_player'];

$cod4_resolution = 		$row['cod4_resolution'];
$cod4_sensitivity = 	$row['cod4_sensitivity'];
$cod4_favourite_gun = 	$row['cod4_favourite_gun'];
$cod4_favourite_gun_attachment = 	$row['cod4_favourite_gun_attachment'];
$cod4_favourite_pistol =$row['cod4_favourite_pistol'];
$cod4_favourite_pistol_attachment =$row['cod4_favourite_pistol_attachment'];
$cod4_favourite_map =	$row['cod4_favourite_map'];
$cod4_favourite_perk_1 =$row['cod4_favourite_perk_1'];
$cod4_favourite_perk_2 =$row['cod4_favourite_perk_2'];
$cod4_favourite_perk_3 =$row['cod4_favourite_perk_3'];

$q3_resolution = 		$row['q3_resolution'];
$q3_sensitivity = 		$row['q3_sensitivity'];
$q3_favourite_map = 	$row['q3_favourite_map'];
$q3_favourite_player =	$row['q3_favourite_player'];

$wc3_resolution = 		$row['wc3_resolution'];
$wc3_favourite_race = 	$row['wc3_favourite_race'];
$wc3_favourite_hero = 	$row['wc3_favourite_hero'];
$wc3_favourite_map = 	$row['wc3_favourite_map'];
$wc3_favourite_player = 	$row['wc3_favourite_player'];

//Följande kod är ett forumlär med dess respektive information i sig
?>


	<table width='100%' cellspacing='0' cellpadding='0' border='0' valign='top'>
	<tr><td width='100%' height='25' valign='middle' align='center' class='historia_rubrik'>
		<span class='admin_gray'>[ </span><a href="admin_profil.php" title="Gå tillbaka" target="main" class='admin_orange'>Ändra Profil</a><span class='admin_gray'> ]</span><br>
	</td></tr>
	<tr><td width='100%' valign='top' class='admin_bg'>

		<table width='100%' height='100%' cellspacing='0' cellpadding='0' border='0'>
		<tr><td valign='top'>

			<table width='100%' cellspacing='0' cellpadding='0' border='0'>
			<tr><td width='50%' height='20' class='admin_profile_edit_left_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td align='center'>
					<form action='admin_profil.php?editprofile=<?php echo $id; ?>&update' id='edit' method='post' name='bb' style="margin-bottom:0;">
					<span class='admin_gray'>.: Allmän Infomation :.</span>
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td align='center'>
					<span class='admin_gray'>.: Counter-Srike: Source :.</span>
				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Förnamn:</span>
				</td><td align='right'>
					<input name="name" type="text" value='<?php echo $name; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Upplösning:</span>
				</td><td align='right'>

<select name="css_resolution" class="login_line">
<option value="-" <?php if($css_resolution=="-"){ echo "selected"; } ?>>-- Vet ej --</option>
<option value="640x480" <?php if($css_resolution=="640x480"){ echo "selected"; } ?>>640x480</option>
<option value="720x576" <?php if($css_resolution=="720x480"){ echo "selected"; } ?>>720x576</option>
<option value="800x600" <?php if($css_resolution=="800x600"){ echo "selected"; } ?>>800x600</option>
<option value="1024x768" <?php if($css_resolution=="1024x768"){ echo "selected"; } ?>>1024x768</option>
<option value="1152x864" <?php if($css_resolution=="1152x864"){ echo "selected"; } ?>>1152x864</option>
<option value="1280x960" <?php if($css_resolution=="1280x960"){ echo "selected"; } ?>>1280x960</option>
<option value="1280x1024 (LCD)" <?php if($css_resolution=="1280x1024 (LCD)"){ echo "selected"; } ?>>1280x1024 (LCD)</option>
<option value="1360x1024" <?php if($css_resolution=="1360x1024"){ echo "selected"; } ?>>1360x1024</option>
<option value="1400x1050" <?php if($css_resolution=="1400x1050"){ echo "selected"; } ?>>1400x1050</option>
<option value="1600x1200" <?php if($css_resolution=="1600x1200"){ echo "selected"; } ?>>1600x1200</option>
</select>

				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Efternamn:</span>
				</td><td align='right'>
					<input name="name_last" type="text" value='<?php echo $name_last; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Sensitivity:</span>
				</td><td align='right'>
					<input name="css_sensitivity" type="text" value='<?php echo $css_sensitivity; ?>' style="width: 50;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Alias:</span>
				</td><td align='right'>
					<input name="name_aka" title="Vad du brukar kallas IRL" type="text" value='<?php echo $name_aka; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>SteamID:</span>
				</td><td align='right'>
					<input name="css_steamid" type="text" value='<?php echo $css_steamid; ?>' style="width: 100;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Stad:</span>
				</td><td align='right'>
					<input name="city" type="text" value='<?php echo $city; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit map:</span>
				</td><td align='right'>

<select name="css_favourite_map" class="login_line">
<option value="-" <?php if($cod4_favourite_map=="-"){ echo "selected"; } ?>>-- Vet ej --</option>
<option value="de_aztec" <?php if($css_favourite_map=="de_aztec"){ echo "selected"; } ?>>de_aztec</option>
<option value="de_cbble" <?php if($css_favourite_map=="de_cbble"){ echo "selected"; } ?>>de_cbble</option>
<option value="de_contra" <?php if($css_favourite_map=="de_contra"){ echo "selected";	} ?>>de_contra</option>
<option value="de_cpl_fire" <?php if($css_favourite_map=="de_cpl_fire"){ echo "selected"; } ?>>de_cpl_fire</option>
<option value="de_cpl_mill" <?php if($css_favourite_map=="de_cpl_mill"){ echo "selected"; } ?>>de_cpl_mill</option>
<option value="de_cpl_strike" <?php if($css_favourite_map=="de_cpl_strike"){ echo "selected"; } ?>>de_cpl_strike</option>
<option value="de_dust2" <?php if($css_favourite_map=="de_dust2"){ echo "selected"; } ?>>de_dust2</option>
<option value="de_dust" <?php if($css_favourite_map=="de_dust"){ echo "selected"; } ?>>de_dust</option>
<option value="de_forge" <?php if($css_favourite_map=="de_forge"){ echo "selected"; } ?>>de_forge</option>
<option value="de_inferno" <?php if($css_favourite_map=="de_inferno"){ echo "selected"; } ?>>de_inferno</option>
<option value="de_nuke" <?php if($css_favourite_map=="de_nuke"){ echo "selected"; } ?>>de_nuke</option>
<option value="de_phoenix" <?php if($css_favourite_map=="de_phoenix"){ echo "selected"; } ?>>de_phoenix</option>
<option value="de_prodigy" <?php if($css_favourite_map=="de_prodigy"){ echo "selected"; } ?>>de_prodigy</option>
<option value="de_russka" <?php if($css_favourite_map=="de_russka"){ echo "selected";	} ?>>de_russka</option>
<option value="de_season" <?php if($css_favourite_map=="de_season"){ echo "selected";	} ?>>de_season</option>
<option value="de_train" <?php if($css_favourite_map=="de_train"){ echo "selected"; } ?>>de_train</option>
<option value="de_tuscan" <?php if($css_favourite_map=="de_tuscan"){ echo "selected";	} ?>>de_tuscan</option>
</select>

				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Ålder:</span>
				</td><td align='right'>
					<input name="age" type="text" value='<?php echo $age; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit gevär:</span>
				</td><td align='right'>

<select name="css_favourite_gun" class="login_line">
<option value="-" <?php if($css_favourite_gun=="-"){ echo "selected"; } ?>>-- Vet ej --</option>

<option value="-">.: Rifles :.</option>
<option value="Famas" <?php if($css_favourite_gun=="Famas"){ echo "selected"; } ?>>Famas</option>
<option value="Defender" <?php if($css_favourite_gun=="Defender"){ echo "selected"; } ?>>Defender</option>
<option value="Colt" <?php if($css_favourite_gun=="Colt"){ echo "selected"; } ?>>Colt</option>
<option value="AK-47" <?php if($css_favourite_gun=="AK-47"){ echo "selected"; } ?>>AK-47</option>
<option value="Scout" <?php if($css_favourite_gun=="Scout"){ echo "selected"; } ?>>Scout</option>
<option value="Bullpup" <?php if($css_favourite_gun=="Bullpup"){ echo "selected"; } ?>>Bullpup</option>
<option value="Kreig 550 Commando" <?php if($css_favourite_gun=="Kreig 550 Commando"){ echo "selected"; } ?>>Kreig 550 Commando</option>
<option value="Kreig 552" <?php if($css_favourite_gun=="Kreig 552"){ echo "selected"; } ?>>Kreig 552</option>
<option value="D3/AU-1" <?php if($css_favourite_gun=="D3/AU-1"){ echo "selected"; } ?>>D3/AU-1</option>
<option value="AWP" <?php if($css_favourite_gun=="AWP"){ echo "selected"; } ?>>AWP</option>

<option value="-"></option>
<option value="-">.: Smg :.</option>
<option value="TMP" <?php if($css_favourite_gun==""){ echo "selected"; } ?>>TMP</option>
<option value="Mac-10" <?php if($css_favourite_gun=="Mac-10"){ echo "selected"; } ?>>Mac-10</option>
<option value="MP5" <?php if($css_favourite_gun=="MP5"){ echo "selected"; } ?>>MP5</option>
<option value="UMP" <?php if($css_favourite_gun=="UMP"){ echo "selected"; } ?>>UMP</option>
<option value="P90" <?php if($css_favourite_gun=="P90"){ echo "selected"; } ?>>P90</option>

<option value="-"></option>
<option value="-">.: Shotguns :.</option>
<option value="Leone 12 Gauge Super" <?php if($css_favourite_gun=="Leone 12 Gauge Super"){ echo "selected"; } ?>>Leone 12 Gauge Super</option>
<option value="Leone YG1265 Auto Shotgun" <?php if($css_favourite_gun=="Leone YG1265 Auto Shotgun"){ echo "selected"; } ?>>Leone YG1265 Auto Shotgun</option>

<option value="-"></option>
<option value="-">.: Machine Guns :.</option>
<option value="M249" <?php if($css_favourite_gun=="M249"){ echo "selected"; } ?>>M249</option>

</select>

				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Kön:</span>
				</td><td align='right'>
					<input name="gender" type="text" value='<?php echo $gender; ?>' style="width: 200;" autocomplete="of" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit pistol:</span>
				</td><td align='right'>

<select name="css_favourite_pistol" class="login_line">
<option value="-" <?php if($css_favourite_pistol=="-"){ echo "selected"; } ?>>-- Vet ej --</option>

<option value="Glock" <?php if($css_favourite_pistol=="Glock"){ echo "selected"; } ?>>Glock</option>
<option value="USP" <?php if($css_favourite_pistol=="USP"){ echo "selected"; } ?>>USP</option>
<option value="Compact" <?php if($css_favourite_pistol=="Compact"){ echo "selected"; } ?>>Compact</option>
<option value="Desert Eagle" <?php if($css_favourite_pistol=="Desert Eagle"){ echo "selected"; } ?>>Desert Eagle</option>
<option value="Dual Elites" <?php if($css_favourite_pistol=="Dual Elites"){ echo "selected"; } ?>>Dual Elites</option>
<option value="Five-Seven" <?php if($css_favourite_pistol=="Five-Seven"){ echo "selected"; } ?>>Five-Seven</option>

</select>

				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Citat:</span>
				</td><td align='right'>
					<input name="quote" type="text" value='<?php echo $quote; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit spelare:</span>
				</td><td align='right'>
					<input name="css_favourite_player" type="text" value='<?php echo $css_favourite_player; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit film(er):</span>
				</td><td align='right'>
					<input name="favourite_movie" type="text" value='<?php echo $favourite_movie; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit sida:</span>
				</td><td align='right'>

<select name="css_favourite_side" class="login_line">
<option value="-" <?php if($css_favourite_side=="-"){ echo "selected"; } ?>>-- Vet ej --</option>
<option value="CT" <?php if($css_favourite_side=="CT"){ echo "selected"; } ?>>CT</option>
<option value="T" <?php if($css_favourite_side=="T"){ echo "selected"; } ?>>T</option>
</select>

				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit spel:</span>
				</td><td align='right'>
					<input name="favourite_game" type="text" value='<?php echo $favourite_game; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td align='center'>
					<span class='admin_gray'>.: Call of Duty 4: Modern Warfare :.</span>
				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit musik:</span>
				</td><td align='right'>
					<input name="favourite_music" type="text" value='<?php echo $favourite_music; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Upplösning:</span>
				</td><td align='right'>

<select name="cod4_resolution" class="login_line">
<option value="-" <?php if($cod4_resolution=="-"){ echo "selected"; } ?>>-- Vet ej --</option>
<option value="640x480" <?php if($cod4_resolution=="640x480"){ echo "selected"; } ?>>640x480</option>
<option value="720x480" <?php if($cod4_resolution=="720x480"){ echo "selected"; } ?>>720x480</option>
<option value="720x576" <?php if($cod4_resolution=="720x576"){ echo "selected"; } ?>>720x576</option>
<option value="800x480" <?php if($cod4_resolution=="800x480"){ echo "selected"; } ?>>800x480</option>
<option value="800x600" <?php if($cod4_resolution=="800x600"){ echo "selected"; } ?>>800x600</option>
<option value="848x480" <?php if($cod4_resolution=="848x480"){ echo "selected"; } ?>>848x480</option>
<option value="1024x768" <?php if($cod4_resolution=="1024x768"){ echo "selected"; } ?>>1024x768</option>
<option value="1152x864" <?php if($cod4_resolution=="1152x864"){ echo "selected"; } ?>>1152x864</option>
<option value="1280x720" <?php if($cod4_resolution=="1280x720"){ echo "selected"; } ?>>1280x720</option>
<option value="1280x768" <?php if($cod4_resolution=="1280x768"){ echo "selected"; } ?>>1280x768</option>
<option value="1280x800" <?php if($cod4_resolution=="1280x800"){ echo "selected"; } ?>>1280x800</option>
<option value="1280x960" <?php if($cod4_resolution=="1280x960"){ echo "selected"; } ?>>1280x960</option>
<option value="1280x1024" <?php if($cod4_resolution=="1280x1024"){ echo "selected"; } ?>>1280x1024</option>
<option value="1360x768" <?php if($cod4_resolution=="1360x768"){ echo "selected"; } ?>>1360x768</option>
<option value="1360x1024" <?php if($cod4_resolution=="1360x1024"){ echo "selected"; } ?>>1360x1024</option>
<option value="1400x1050" <?php if($cod4_resolution=="1400x1050"){ echo "selected"; } ?>>1400x1050</option>
<option value="1440x900" <?php if($cod4_resolution=="1440x900"){ echo "selected"; } ?>>1440x900</option>
<option value="1600x1200" <?php if($cod4_resolution=="1600x1200"){ echo "selected"; } ?>>1600x1200</option>
</select>

				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit drink:</span>
				</td><td align='right'>
					<input name="favourite_drink" type="text" value='<?php echo $favourite_drink; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Sensitivity:</span>
				</td><td align='right'>
					<input name="cod4_sensitivity" type="text" value='<?php echo $cod4_sensitivity; ?>' style="width: 50;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Hemsida:</span>
				</td><td align='right'>
					<input name="homepage" title="Måste börja på http://" type="text" value='<?php echo $homepage; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit gevär:</span>
				</td><td align='right'>

<select name="cod4_favourite_gun" class="login_line">
<option value="-" <?php if($cod4_favourite_gun=="-"){ echo "selected"; } ?>>-- Vet ej --</option>

<option value="-">.: Assault Rifles :.</option>
<option value="M16A4" <?php if($cod4_favourite_gun=="M16A4"){ echo "selected"; } ?>>M16A4</option>
<option value="AK-47" <?php if($cod4_favourite_gun=="AK-47"){ echo "selected"; } ?>>AK-47</option>
<option value="M4 Carbine" <?php if($cod4_favourite_gun=="M4 Carbine"){ echo "selected"; } ?>>M4 Carbine</option>
<option value="G3" <?php if($cod4_favourite_gun=="G3"){ echo "selected"; } ?>>G3</option>
<option value="G36C" <?php if($cod4_favourite_gun=="G36C"){ echo "selected"; } ?>>G36C</option>
<option value="M14" <?php if($cod4_favourite_gun=="M14"){ echo "selected"; } ?>>M14</option>
<option value="MP44" <?php if($cod4_favourite_gun=="MP44"){ echo "selected"; } ?>>MP44</option>

<option value="-"></option>
<option value="-">.: Sub Machine Guns :.</option>
<option value="MP5" <?php if($cod4_favourite_gun=="MP5"){ echo "selected"; } ?>>MP5</option>
<option value="Skorpion" <?php if($cod4_favourite_gun=="Skorpion"){ echo "selected"; } ?>>Skorpion</option>
<option value="Mini-Uzi" <?php if($cod4_favourite_gun=="Mini-Uzi"){ echo "selected"; } ?>>Mini-Uzi</option>
<option value="AK-74u" <?php if($cod4_favourite_gun=="AK-74u"){ echo "selected"; } ?>>AK-74u</option>
<option value="P90" <?php if($cod4_favourite_gun=="P90"){ echo "selected"; } ?>>P90</option>

<option value="-"></option>
<option value="-">.: Light Machine Guns :.</option>
<option value="M249 SAW" <?php if($cod4_favourite_gun=="M249 SAW"){ echo "selected"; } ?>>M249 SAW</option>
<option value="RPD" <?php if($cod4_favourite_gun=="RPD"){ echo "selected"; } ?>>RPD</option>
<option value="M60E4" <?php if($cod4_favourite_gun=="M60E4"){ echo "selected"; } ?>>M60E4</option>

<option value="-"></option>
<option value="-">.: Shotguns :.</option>
<option value="W1200" <?php if($cod4_favourite_gun=="W1200"){ echo "selected"; } ?>>W1200</option>
<option value="M1014" <?php if($cod4_favourite_gun=="M1014"){ echo "selected"; } ?>>M1014</option>

<option value="-"></option>
<option value="-">.: Sniper Rifles :.</option>
<option value="M40A3" <?php if($cod4_favourite_gun=="M40A3"){ echo "selected"; } ?>>M40A3</option>
<option value="M21" <?php if($cod4_favourite_gun=="M21"){ echo "selected"; } ?>>M21</option>
<option value="Dragunov" <?php if($cod4_favourite_gun=="Dragunov"){ echo "selected"; } ?>>Dragunov</option>
<option value="R700" <?php if($cod4_favourite_gun=="R700"){ echo "selected"; } ?>>R700</option>
<option value="Barret .50cal" <?php if($cod4_favourite_gun=="Barret .50cal"){ echo "selected"; } ?>>Barret .50cal</option>
</select>

				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td align='center'>
					<span class='admin_gray'>.: Bild :.</span>
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit gevär attachment:</span>
				</td><td align='right'>

<select name="cod4_favourite_gun_attachment" class="login_line">
<option value="-" <?php if($cod4_favourite_gun_attachment=="-"){ echo "selected"; } ?>>No Attachment</option>

<option value="-"></option>
<option value="-">.: Assault Rifles :.</option>
<option value="Grenade Launcher" <?php if($cod4_favourite_gun_attachment=="Grenade Launcher"){ echo "selected"; } ?>>Grenade Launcher</option>
<option value="Silencer" <?php if($cod4_favourite_gun_attachment=="Silencer"){ echo "selected"; } ?>>Silencer</option>
<option value="Red Dot Sight" <?php if($cod4_favourite_gun_attachment=="Red Dot Sight"){ echo "selected"; } ?>>Red Dot Sight</option>
<option value="ACOG Scope" <?php if($cod4_favourite_gun_attachment=="ACOG Scope"){ echo "selected"; } ?>>ACOG Scope</option>

<option value="-"></option>
<option value="-">.: Sub Machine Guns :.</option>
<option value="Silencer2" <?php if($cod4_favourite_gun_attachment=="Silencer2"){ echo "selected"; } ?>>Silencer</option>
<option value="Red Dot Sight2" <?php if($cod4_favourite_gun_attachment=="Red Dot Sight2"){ echo "selected"; } ?>>Red Dot Sight</option>
<option value="ACOG Scope2" <?php if($cod4_favourite_gun_attachment=="ACOG Scope2"){ echo "selected"; } ?>>ACOG Scope</option>

<option value="-"></option>
<option value="-">.: Light Machine Guns :.</option>
<option value="Red Dot Sight3" <?php if($cod4_favourite_gun_attachment=="Red Dot Sight3"){ echo "selected"; } ?>>Red Dot Sight</option>
<option value="Grip" <?php if($cod4_favourite_gun_attachment=="Grip"){ echo "selected"; } ?>>Grip</option>
<option value="ACOG Scope3" <?php if($cod4_favourite_gun_attachment=="ACOG Scope3"){ echo "selected"; } ?>>ACOG Scope</option>

<option value="-"></option>
<option value="-">.: Shotguns :.</option>
<option value="Red Dot Sight4" <?php if($cod4_favourite_gun_attachment=="Red Dot Sight4"){ echo "selected"; } ?>>Red Dot Sight</option>
<option value="Grip2" <?php if($cod4_favourite_gun_attachment=="Grip2"){ echo "selected"; } ?>>Grip</option>

<option value="-"></option>
<option value="-">.: Sniper Rifles :.</option>
<option value="ACOG Scope4" <?php if($cod4_favourite_gun_attachment=="ACOG Scope4"){ echo "selected"; } ?>>ACOG Scope</option>
</select>

				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<del><span class='admin_orange2'>Ändra </span><span class='admin_gray'>Bild:</span></del>
				</td><td align='right'>
					<input name="picture" title="!!FUNKAR INTE ÄNNU!!" type="text" value='' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit pistol:</span>
				</td>
				<td align='right'>

<select name="cod4_favourite_pistol" class="login_line">
<option value="-" <?php if($cod4_favourite_pistol=="-"){ echo "selected"; } ?>>-- Vet ej --</option>

<option value="M9" <?php if($cod4_favourite_pistol=="M9"){ echo "selected"; } ?>>M9</option>
<option value="USP .45" <?php if($cod4_favourite_pistol=="USP .45"){ echo "selected"; } ?>>USP .45</option>
<option value="M1911 .45" <?php if($cod4_favourite_pistol=="M1911 .45"){ echo "selected"; } ?>>M1911 .45</option>
<option value="Desert Eagle" <?php if($cod4_favourite_pistol=="Desert Eagle"){ echo "selected"; } ?>>Desert Eagle</option>
<option value="Gold Desert Eagle" <?php if($cod4_favourite_pistol=="Gold Desert Eagle"){ echo "selected"; } ?>>Gold Desert Eagle</option>
</select>

				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td align='center'>
					<span class='admin_gray'>.: Datoregenskaper :.</span>
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit pistol attachment:</span>
				</td>
				<td align='right'>

<select name="cod4_favourite_pistol_attachment" class="login_line">
<option value="-" <?php if($cod4_favourite_pistol_attachment=="-"){ echo "selected"; } ?>>No Attachment</option>

<option value="Silencer" <?php if($cod4_favourite_pistol_attachment=="Silencer"){ echo "selected"; } ?>>Silencer</option>
</select>

				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Cpu:</span>
				</td><td align='right'>
					<input name="cpu" type="text" value='<?php echo $cpu; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit map:</span>
				</td><td align='right'>

<select name="cod4_favourite_map" class="login_line">
<option value="-" <?php if($cod4_favourite_map=="-"){ echo "selected"; } ?>>-- Vet ej --</option>
<option value="mp_convoy" <?php if($cod4_favourite_map=="mp_convoy"){ echo "selected"; } ?>>Ambush</option>
<option value="mp_backlot" <?php if($cod4_favourite_map=="mp_backlot"){ echo "selected"; } ?>>Backlot</option>
<option value="mp_bloc" <?php if($cod4_favourite_map=="mp_bloc"){ echo "selected"; } ?>>Bloc</option>
<option value="mp_bog" <?php if($cod4_favourite_map=="mp_bog"){ echo "selected"; } ?>>Bog</option>
<option value="mp_countdown" <?php if($cod4_favourite_map=="mp_countdown"){ echo "selected"; } ?>>Countdown</option>
<option value="mp_crash" <?php if($cod4_favourite_map=="mp_crash"){ echo "selected"; } ?>>Crash</option>
<option value="mp_crash_snow" <?php if($cod4_favourite_map=="mp_crash_snow"){ echo "selected"; } ?>>Crash Winter</option>
<option value="mp_crossfire" <?php if($cod4_favourite_map=="mp_crossfire"){ echo "selected"; } ?>>Crossfire</option>
<option value="mp_citystreets" <?php if($cod4_favourite_map=="mp_citystreets"){ echo "selected"; } ?>>District</option>
<option value="mp_farm" <?php if($cod4_favourite_map=="mp_farm"){ echo "selected"; } ?>>Downpour</option>
<option value="mp_overgrown" <?php if($cod4_favourite_map=="mp_overgrown"){ echo "selected"; } ?>>Overgrown</option>
<option value="mp_pipeline" <?php if($cod4_favourite_map=="mp_pipeline"){ echo "selected"; } ?>>Pipeline</option>
<option value="mp_shipment" <?php if($cod4_favourite_map=="mp_shipment"){ echo "selected"; } ?>>Shipment</option>
<option value="mp_showdown" <?php if($cod4_favourite_map=="mp_showdown"){ echo "selected"; } ?>>Showdown</option>
<option value="mp_strike" <?php if($cod4_favourite_map=="mp_strike"){ echo "selected"; } ?>>Strike</option>
<option value="mp_vacant" <?php if($cod4_favourite_map=="mp_vacant"){ echo "selected"; } ?>>Vacant</option>
<option value="mp_cargoship" <?php if($cod4_favourite_map=="mp_cargoship"){ echo "selected"; } ?>>Wet Work</option>
</select>

				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Grafikkort:</span>
				</td><td align='right'>
					<input name="graphiccard" type="text" value='<?php echo $graphiccard; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit perk #1 (Blå):</span>
				</td><td align='right'>

<select name="cod4_favourite_perk_1" class="login_line">
<option value="-" <?php if($cod4_favourite_perk_1=="-"){ echo "selected"; } ?>>-- Vet ej --</option>
<option value="C4" <?php if($cod4_favourite_perk_1=="C4"){ echo "selected"; } ?>>C4</option>
<option value="Special Grenades" <?php if($cod4_favourite_perk_1=="Special Grenades"){ echo "selected"; } ?>>Special Grenades</option>
<option value="RPG" <?php if($cod4_favourite_perk_1=="RPG"){ echo "selected"; } ?>>RPG-7</option>
<option value="Claymore" <?php if($cod4_favourite_perk_1=="Claymore"){ echo "selected"; } ?>>Claymore</option>
<option value="Frag" <?php if($cod4_favourite_perk_1=="Frag"){ echo "selected"; } ?>>Frag</option>
<option value="Bandolier" <?php if($cod4_favourite_perk_1=="Bandolier"){ echo "selected"; } ?>>Bandolier</option>
<option value="Bomb Squad" <?php if($cod4_favourite_perk_1=="Bomb Squad"){ echo "selected"; } ?>>Bomb Squad</option>
</select>

				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Headset:</span>
				</td><td align='right'>
					<input name="headset" type="text" value='<?php echo $headset; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit perk #2 (Röd):</span>
				</td><td align='right'>

<select name="cod4_favourite_perk_2" class="login_line">
<option value="-" <?php if($cod4_favourite_perk_2=="-"){ echo "selected"; } ?>>-- Vet ej --</option>
<option value="Stopping Power" <?php if($cod4_favourite_perk_2=="Stopping Power"){ echo "selected"; } ?>>Stopping Power</option>
<option value="Juggernaut" <?php if($cod4_favourite_perk_2=="juggernaut"){ echo "selected"; } ?>>Juggernaut</option>
<option value="Sleight Of Hand" <?php if($cod4_favourite_perk_2=="Sleight Of Hand"){ echo "selected"; } ?>>Sleight of Hand</option>
<option value="Double Tap" <?php if($cod4_favourite_perk_2=="Double Tap"){ echo "selected"; } ?>>Double Tap</option>
<option value="Overkill" <?php if($cod4_favourite_perk_2=="Overkill"){ echo "selected"; } ?>>Overkill</option>
<option value="UAV Jammer" <?php if($cod4_favourite_perk_2=="UAV Jammer"){ echo "selected"; } ?>>UAV Jammer</option>
<option value="Sonic Boom" <?php if($cod4_favourite_perk_2=="Sonic Boom"){ echo "selected"; } ?>>Sonic Boom</option>
</select>

				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Mus:</span>
				</td><td align='right'>
					<input name="mouse" type="text" value='<?php echo $mouse; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit perk #3 (Gul):</span>
				</td><td align='right'>

<select name="cod4_favourite_perk_3" class="login_line">
<option value="-" <?php if($cod4_favourite_perk_3=="-"){ echo "selected"; } ?>>-- Vet ej --</option>
<option value="Extreme Conditioning" <?php if($cod4_favourite_perk_3=="Extreme Conditioning"){ echo "selected"; } ?>>Extreme Conditioning</option>
<option value="Steady Aim" <?php if($cod4_favourite_perk_3=="Steady Aim"){ echo "selected"; } ?>>Steady Aim</option>
<option value="Last Stand" <?php if($cod4_favourite_perk_3=="Last Stand"){ echo "selected"; } ?>>Last Stand</option>
<option value="Martyrdom" <?php if($cod4_favourite_perk_3=="Martyrdom"){ echo "selected"; } ?>>Martyrdom</option>
<option value="Deep Impact" <?php if($cod4_favourite_perk_3=="Deep Impact"){ echo "selected"; } ?>>Deep Impact</option>
<option value="Iron Lungs" <?php if($cod4_favourite_perk_3=="Iron Lungs"){ echo "selected"; } ?>>Iron Lungs</option>
<option value="Dead Silence" <?php if($cod4_favourite_perk_3=="Dead Silence"){ echo "selected"; } ?>>Dead Silence</option>
<option value="Eavesdrop" <?php if($cod4_favourite_perk_3=="Eavesdrop"){ echo "selected"; } ?>>Eavesdrop</option>
</select>

				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Musmatta:</span>
				</td><td align='right'>
					<input name="mousepad" type="text" value='<?php echo $mousepad; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td align='center'>
					<span class='admin_gray'>.: Quake III Arena :.</span>
				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Minne:</span>
				</td><td align='right'>
					<input name="memory" type="text" value='<?php echo $memory; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Upplösning:</span>
				</td><td align='right'>

<select name="q3_resolution" class="login_line">
<option value="-" <?php if($q3_resolution=="-"){ echo "selected"; } ?>>-- Vet ej --</option>
<option value="320x240" <?php if($q3_resolution=="320x240"){ echo "selected"; } ?>>320x240</option>
<option value="400x300" <?php if($q3_resolution=="400x300"){ echo "selected"; } ?>>400x300</option>
<option value="512x384" <?php if($q3_resolution=="512x384"){ echo "selected"; } ?>>512x384</option>
<option value="640x480" <?php if($q3_resolution=="640x480"){ echo "selected"; } ?>>640x480</option>
<option value="800x600" <?php if($q3_resolution=="800x600"){ echo "selected"; } ?>>800x600</option>
<option value="856x480 (Wide Screen)" <?php if($q3_resolution=="856x480 (Wide Screen)"){ echo "selected"; } ?>>856x480 (Wide Screen)</option>
<option value="960x720" <?php if($q3_resolution=="960x720"){ echo "selected"; } ?>>960x720</option>
<option value="1024x768" <?php if($q3_resolution=="1024x768"){ echo "selected"; } ?>>1024x768</option>
<option value="1152x864" <?php if($q3_resolution=="1152x864"){ echo "selected"; } ?>>1152x864</option>
<option value="1600x1200" <?php if($q3_resolution=="1600x1200"){ echo "selected"; } ?>>1600x1200</option>
<option value="2048x1536" <?php if($q3_resolution=="2048x1536"){ echo "selected"; } ?>>2048x1536</option>
</select>

				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Tangentbord:</span>
				</td><td align='right'>
					<input name="keyboard" type="text" value='<?php echo $keyboard; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Sensitivity:</span>
				</td><td align='right'>
					<input name="q3_sensitivity" type="text" value='<?php echo $q3_sensitivity; ?>' style="width: 50;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Moderkort:</span>
				</td><td align='right'>
					<input name="motherboard" type="text" value='<?php echo $motherboard; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit map:</span>
				</td><td align='right'>
					<input name="q3_favourite_map" type="text" value='<?php echo $q3_favourite_map; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Skärm:</span>
				</td><td align='right'>
					<input name="screen" type="text" value='<?php echo $screen; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit spelare:</span>
				</td><td align='right'>
					<input name="q3_favourite_player" type="text" value='<?php echo $q3_favourite_player; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					&nbsp;
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td align='center'>
					<span class='admin_gray'>.: Warcraft III: The Frozen Throne :.</span>
				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					&nbsp;
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Upplösning:</span>
				</td><td align='right'>

<select name="wc3_resolution" class="login_line">
<option value="-" <?php if($wc3_resolution=="-"){ echo "selected"; } ?>>-- Vet ej --</option>
<option value="640x480x32" <?php if($wc3_resolution=="640x480x32"){ echo "selected"; } ?>>640x480x32</option>
<option value="720x576x32" <?php if($wc3_resolution=="720x576x32"){ echo "selected"; } ?>>720x576x32</option>
<option value="800x600x32" <?php if($wc3_resolution=="800x600x32"){ echo "selected"; } ?>>800x600x32</option>
<option value="1024x768x32" <?php if($wc3_resolution=="1024x768x32"){ echo "selected"; } ?>>1024x768x32</option>
<option value="1152x864x32" <?php if($wc3_resolution=="1152x864x32"){ echo "selected"; } ?>>1152x864x32</option>
<option value="1280x960x32" <?php if($wc3_resolution=="1280x960x32"){ echo "selected"; } ?>>1280x960x32</option>
<option value="1280x1024x32" <?php if($wc3_resolution=="1280x1024x32"){ echo "selected"; } ?>>1280x1024x32</option>
<option value="1400x1050x32" <?php if($wc3_resolution=="1400x1050x32"){ echo "selected"; } ?>>1400x1050x32</option>
<option value="1600x1200x32" <?php if($wc3_resolution=="1600x1200x32"){ echo "selected"; } ?>>1600x1200x32</option>
<option value="640x480x16" <?php if($wc3_resolution=="640x480x16"){ echo "selected"; } ?>>640x480x16</option>
<option value="720x576x16" <?php if($wc3_resolution=="720x576x16"){ echo "selected"; } ?>>720x576x16</option>
<option value="800x600x16" <?php if($wc3_resolution=="800x600x16"){ echo "selected"; } ?>>800x600x16</option>
<option value="1024x768x16" <?php if($wc3_resolution=="1024x768x16"){ echo "selected"; } ?>>1024x768x16</option>
<option value="1152x864x16" <?php if($wc3_resolution=="1152x864x16"){ echo "selected"; } ?>>1152x864x16</option>
<option value="1280x960x16" <?php if($wc3_resolution=="1280x960x16"){ echo "selected"; } ?>>1280x960x16</option>
<option value="1280x1024x16" <?php if($wc3_resolution=="1280x1024x16"){ echo "selected"; } ?>>1280x1024x16</option>
<option value="1400x1050x16" <?php if($wc3_resolution=="1400x1050x16"){ echo "selected"; } ?>>1400x1050x16</option>
<option value="1600x1200x16" <?php if($wc3_resolution=="1600x1200x16"){ echo "selected"; } ?>>1600x1200x16</option>
</select>


				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					&nbsp;
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit ras:</span>
				</td><td align='right'>

<select name="wc3_favourite_race" class="login_line">
<option value="-" <?php if($wc3_favourite_race=="-"){ echo "selected"; } ?>>-- Vet ej --</option>

<option value="Human" <?php if($wc3_favourite_race=="Human"){ echo "selected"; } ?>>Human</option>
<option value="Orc" <?php if($wc3_favourite_race=="Orc"){ echo "selected"; } ?>>Orc</option>
<option value="Undead" <?php if($wc3_favourite_race=="Undead"){ echo "selected"; } ?>>Undead</option>
<option value="Night Elf" <?php if($wc3_favourite_race=="Night Elf"){ echo "selected"; } ?>>Night Elf</option>
</select>

				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					&nbsp;
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit hero:</span>
				</td><td align='right'>

<select name="wc3_favourite_hero" class="login_line">
<option value="-" <?php if($wc3_favourite_hero=="-"){ echo "selected"; } ?>>-- Vet ej --</option>

<option value="-">.: Human Hjältar :.</option>
<option value="Paladin" <?php if($wc3_favourite_hero=="Paladin"){ echo "selected"; } ?>>Paladin</option>
<option value="Archmage" <?php if($wc3_favourite_hero=="Archmage"){ echo "selected"; } ?>>Archmage</option>
<option value="Mountain King" <?php if($wc3_favourite_hero=="Mountain King"){ echo "selected"; } ?>>Mountain King</option>
<option value="Blood Mage" <?php if($wc3_favourite_hero=="Blood Mage"){ echo "selected"; } ?>>Blood Mage</option>

<option value="-"></option>
<option value="-">.: Orc Hjältar :.</option>
<option value="Blademaster" <?php if($wc3_favourite_hero=="Blademaster"){ echo "selected"; } ?>>Blademaster</option>
<option value="Far Seer" <?php if($wc3_favourite_hero=="Far Seer"){ echo "selected"; } ?>>Far Seer</option>
<option value="Tauren Chieftain" <?php if($wc3_favourite_hero=="Tauren Chieftain"){ echo "selected"; } ?>>Tauren Chieftain</option>
<option value="Shadow Hunter" <?php if($wc3_favourite_hero=="Shadow Hunter"){ echo "selected"; } ?>>Shadow Hunter</option>

<option value="-"></option>
<option value="-">.: Undead Hjältar :.</option>
<option value="Death Knight" <?php if($wc3_favourite_hero=="Death Knight"){ echo "selected"; } ?>>Death Knight</option>
<option value="Dread Lord" <?php if($wc3_favourite_hero=="Dread Lord"){ echo "selected"; } ?>>Dread Lord</option>
<option value="Lich" <?php if($wc3_favourite_hero=="Lich"){ echo "selected"; } ?>>Lich</option>
<option value="Crypt Lord" <?php if($wc3_favourite_hero=="Crypt Lord"){ echo "selected"; } ?>>Crypt Lord</option>

<option value="-"></option>
<option value="-">.: Night Elf Hjältar :.</option>
<option value="Demon Hunter" <?php if($wc3_favourite_hero=="Demon Hunter"){ echo "selected"; } ?>>Demon Hunter</option>
<option value="Keeper of the Grove" <?php if($wc3_favourite_hero=="Keeper of the Grove"){ echo "selected"; } ?>>Keeper of the Grove</option>
<option value="Priestess of the Moon" <?php if($wc3_favourite_hero=="Priestess of the Moon"){ echo "selected"; } ?>>Priestess of the Moon</option>
<option value="Warden" <?php if($wc3_favourite_hero=="Warden"){ echo "selected"; } ?>>Warden</option>

<option value="-"></option>
<option value="-">.: Neutrala Hjältar :.</option>
<option value="Naga Sea Witch" <?php if($wc3_favourite_hero=="Naga Sea Witch"){ echo "selected"; } ?>>Naga Sea Witch</option>
<option value="Dark Ranger" <?php if($wc3_favourite_hero=="Dark Ranger"){ echo "selected"; } ?>>Dark Ranger</option>
<option value="Pandaren Brewmaster" <?php if($wc3_favourite_hero=="Pandaren Brewmaster"){ echo "selected"; } ?>>Pandaren Brewmaster</option>
<option value="Beastmaster" <?php if($wc3_favourite_hero=="Beastmaster"){ echo "selected"; } ?>>Beastmaster</option>
<option value="Pit Lord" <?php if($wc3_favourite_hero=="Pit Lord"){ echo "selected"; } ?>>Pit Lord</option>
<option value="Goblin Tinker" <?php if($wc3_favourite_hero=="Goblin Tinker"){ echo "selected"; } ?>>Goblin Tinker</option>
<option value="Firelord" <?php if($wc3_favourite_hero=="Firelord"){ echo "selected"; } ?>>Firelord</option>
<option value="Goblin Alchemist" <?php if($wc3_favourite_hero=="Goblin Alchemist"){ echo "selected"; } ?>>Goblin Alchemist</option>
</select>

				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					&nbsp;
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit map:</span>
				</td><td align='right'>
					<input name="wc3_favourite_map" type="text" value='<?php echo $wc3_favourite_map; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' height='20' class='admin_profile_edit_left_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					&nbsp;
				</td></tr>
				</table>

			</td><td width='50%' height='20' class='admin_profile_edit_right_dark' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Favorit spelare:</span>
				</td><td align='right'>
					<input name="wc3_favourite_player" type="text" value='<?php echo $wc3_favourite_player; ?>' style="width: 200;" autocomplete="off" class="shout_line" />
				</td></tr>
				</table>

			</td></tr>
			<tr><td width='50%' colspan='2' height='20' class='admin_profile_edit_right' valign='middle' align='center'>
				<script type="text/javascript" src="js/editor.js"></script>

					<a href="#" onclick="insert_text('^^', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/orc%5E%5E.gif" alt="^^" title="Happy Orc" height="22" hspace="2" vspace="2" width="23" border="0"></a>
					<a href="#" onclick="insert_text(':D', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_biggrin.gif" alt=":D" title="Very Happy" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_smile.gif" alt=":)" title="Smile" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':lol:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_lol.gif" alt=":lol:" title="Laughing" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':P', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_razz.gif" alt=":P" title="Razz" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':mrgreen:', true); return false;" style="line-height: 20px;">	<img src="forum/images/smilies/icon_mrgreen.gif" alt=":mrgreen:" title="Mr. Green" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(';)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_wink.gif" alt=";)" title="Wink" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':S', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_confused.gif" alt=":S" title="Confused" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':(', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_sad.gif" alt=":(" title="Sad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':cry:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_cry.gif" alt=":cry:" title="Neutral" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':|', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_neutral.gif" alt=":|" title="Neutral" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':$', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_redface.gif" alt=":$" title="Embarassed" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text('8)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_cool.gif" alt="8)" title="Cool" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':O', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_surprised.gif" alt=":O" title="Surprised" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':eek:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_eek.gif" alt=":eek:" title="Shocked" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':rolleyes:', true); return false;" style="line-height: 20px;"><img src="forum/images/smilies/icon_rolleyes.gif" alt=":rolleyes:" title="Rolling Eyes" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':mad:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_mad.gif" alt=":mad:" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':X', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_evil.gif" alt=":X" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':@', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_twisted.gif" alt=":@" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>

			</td></tr>
			<tr><td width='50%' colspan='2' height='20' class='admin_profile_edit_right_dark' valign='middle' align='center'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td align='center'>

					<input class="btnbbcode" title="Fet text: [b]text[/b]" style="font-weight: bold;" type="button" name="bold"
					onclick="javascript:addtag('b');" value="b" />

					<input class="btnbbcode" title="Kursiv text: [i]text[/i]" style="font-style: italic;" type="button" name="italic"
					onclick="javascript:addtag('i');" value="i" />

					<input class="btnbbcode" title="Understruken text: [u]text[/u]" style="text-decoration: underline;" type="button" name="inserted"
					onclick="javascript:addtag('u');" value="u" />

					<input class="btnbbcode" title="!!OBS, NAMNET MÅSTE VARA MED!! Citera text: [quote=namnet]text[/quote]" type="button" name="blockquote"
					onclick="javascript:addtag('quote');" value="Quote" />

					<input class="btnbbcode" title="Infoga länk: [url=http://url]Länktext[/url]" style="text-decoration: underline;" type="button" name="link"
					onclick="javascript:addtag('url');" value="URL" />

					<input class="btnbbcode" title="!!ENDAST TILL FILER PÅ HEMSIDAN!! Infoga länk: [url2=http://url]Länktext[/url2]" style="text-decoration: underline;" type="button" name="link2"
					onclick="javascript:addtag('url2');" value="URL(intern)" />

					<input class="btnbbcode" title="Infoga bild: [img]http://bild_url.jpg[/img]" type="button" name="image"
					onclick="javascript:addtag('img');" value="Img" />

					</td></tr>
					</table>

			</td></tr>
			<tr><td width='50%' colspan='2' height='20' class='admin_profile_edit_right' valign='middle'>

				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr><td align='center'>
					<span class='admin_orange2'>Ändra </span><span class='admin_gray'>Historia/bakgrund:</span>
				</td></tr>
				</table>

			</td></tr>

			<tr><td width='100%' colspan='2' class='admin_profile_edit_right_dark' valign='middle'>
				<textarea name="text" style="width: 100%; height: 200;" class="shout_line"><?php echo $text; ?></textarea>
			</td></tr>

			<tr><td width='100%' colspan='2' height='20' class='admin_profile_edit_right_bottom' valign='middle' align='center'>
				<input class="mainoption" type="submit" title="Ändra Profilen!" value="Ändra Profilen!" />
				</form>
			</td></tr>
			</table>


<?php

//Då ?editnews=$id&update står i adressfältet kommer följande kod att laddas
if(isset($_GET['update'])) {
/*
	//SQL kod för att uppdatera en vald nyhet
	mysql_query("UPDATE legacy_headz_medlem SET
		name = '".$_POST['name']."',
		name_last = '".$_POST['name_last']."',
		name_aka = '".$_POST['name_aka']."',
		city = '".$_POST['city']."',
		age = '".$_POST['age']."',
		gender = '".$_POST['gender']."',
		quote = '".$_POST['quote']."',
		favourite_movie = '".$_POST['favourite_movie']."',
		favourite_game = '".$_POST['favourite_game']."',
		favourite_music = '".$_POST['favourite_music']."',
		favourite_drink = '".$_POST['favourite_drink']."',
		homepage = '".$_POST['homepage']."',
		cpu = '".$_POST['cpu']."',
		graphiccard = '".$_POST['graphiccard']."',
		headset = '".$_POST['headset']."',
		mouse = '".$_POST['mouse']."',
		mousepad = '".$_POST['mousepad']."',
		memory = '".$_POST['memory']."',
		keyboard = '".$_POST['keyboard']."',
		motherboard = '".$_POST['motherboard']."',
		screen = '".$_POST['screen']."',
		css_resolution = '".$_POST['css_resolution']."',
		css_sensitivity = '".$_POST['css_sensitivity']."',
		css_steamid = '".$_POST['css_steamid']."',
		css_favourite_map = '".$_POST['css_favourite_map']."',
		css_favourite_gun = '".$_POST['css_favourite_gun']."',
		css_favourite_pistol = '".$_POST['css_favourite_pistol']."',
		css_favourite_player = '".$_POST['css_favourite_player']."',
		css_favourite_side = '".$_POST['css_favourite_side']."',
		cod4_resolution = '".$_POST['cod4_resolution']."',
		cod4_sensitivity = '".$_POST['cod4_sensitivity']."',
		cod4_favourite_gun = '".$_POST['cod4_favourite_gun']."',
		cod4_favourite_gun_attachment = '".$_POST['cod4_favourite_gun_attachment']."',
		cod4_favourite_pistol = '".$_POST['cod4_favourite_pistol']."',
		cod4_favourite_pistol_attachment = '".$_POST['cod4_favourite_pistol_attachment']."',
		cod4_favourite_map = '".$_POST['cod4_favourite_map']."',
		cod4_favourite_perk_1 = '".$_POST['cod4_favourite_perk_1']."',
		cod4_favourite_perk_2 = '".$_POST['cod4_favourite_perk_2']."',
		cod4_favourite_perk_3 = '".$_POST['cod4_favourite_perk_3']."',
		q3_resolution = '".$_POST['q3_resolution']."',
		q3_sensitivity = '".$_POST['q3_sensitivity']."',
		q3_favourite_map = '".$_POST['q3_favourite_map']."',
		q3_favourite_player = '".$_POST['q3_favourite_player']."',
		wc3_resolution = '".$_POST['wc3_resolution']."',
		wc3_favourite_race = '".$_POST['wc3_favourite_race']."',
		wc3_favourite_hero = '".$_POST['wc3_favourite_hero']."',
		wc3_favourite_map = '".$_POST['wc3_favourite_map']."',
		wc3_favourite_player = '".$_POST['wc3_favourite_player']."',
		text = '".$_POST['text']."'
		WHERE id=".$_GET['editprofile']) or exit(mysql_error());
*/
	//Skickar dig vidare till det inskrivna efter location:
	header('location: admin_profil.php');
  }
 }
}

//Om QUERY_STRING är tom laddas denna text, kolla under resurser och Server variablar för mer information
if(empty($_SERVER['QUERY_STRING'])) {

$resultat = mysql_query("SELECT * FROM legacy_headz_players WHERE user='{$_SESSION['sess_user']}'") or die (mysql_error());
if($row = mysql_fetch_array($resultat)) {
$user = $row['user'];

?>

	<table width='100%' cellspacing='0' cellpadding='0' border='0' valign='top'>
	<tr><td width='100%' height='25' valign='middle' align='center' class='historia_rubrik'>
		<span class='medlemmar_text_citat'>[ </span><span class='medlemmar_text_citat_info'><?php echo $user; ?></span><span class='medlemmar_text_citat'> ]</span>
	</td></tr>
	<tr><td width='100%' valign='top' class='admin_bg'>

		<table width='100%' height='100%' cellspacing='0' cellpadding='0' border='0'>
		<tr><td valign='top'>

			<table width='100%' cellspacing='0' cellpadding='0' border='0'>
			<tr><td class='admin_profile' colspan='4' valign='middle' align='center' width='50%' height='20'>
<?php
echo			"<span class='admin_gray'>[ </span>";
echo			"<a href='admin_profil.php?viewprofile&css' title='Se din Profil' target='main' class='admin_orange'>Se Profil</a>";
echo			"<span class='admin_gray'> ]</span><br>";
?>
			</td><td class='admin_profile_dark' colspan='4' valign='middle' align='center' width='50%' height='20'>
<?php
echo			"<span class='admin_gray'>[ </span>";
echo			"<a href='admin_profil.php?editprofile' title='Ändra din Profil' target='main' class='admin_orange'>Ändra Profil</a>";
echo			"<span class='admin_gray'> ]</span><br>";
?>
			</td></tr>
			</table>

		</td></tr>
		</table>

	</td></tr>
	</table>

<?php
 }
}
?>

</td></tr>
</table>


</body>
</html>

<?php
//Kolla under resurser och ob_end_flush()-funktionen för mer information
ob_end_flush();
?>
