<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/iframe.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3406958-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>

</head>
<body>


<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
<tr><td width="100%" height="100%" valign="top" align="left" class="main_bg">

	<table width='100%' cellspacing='0' cellpadding='0' border='0'>
	<tr><td height='25' valign='middle' align='center' class='historia_rubrik'>
		<span class='medlemmar_text_citat_info'>HeadZ Historia</span>
	</td></tr>
	<tr><td valign='top' class='hem_text_bg'>
		<span class='hem_text'>

Tänkte att det är dags att berätta storyn om Clanen <span class='text'><a href='http://www.jalba.se/headz/' title="HeadZ - A CS:S, COD4, Q3 & WC3 clan" target='_blank'>HeadZ</a></span>.
<br><br>
Det hela börja med att 2 claner, Team DC (squad inom <span class='text'><a href='http://www.disney-clan.se' title="Disney - Counterstrike Source Clan Since 2005" target='_blank'>DISNEY</a></span>) och Clan Deleters varit i kontakt med varandra många ggr. Så som på <span class='text'><a href='http://www.disney-clan.se' title="Disney - Counterstrike Source Clan Since 2005" target='_blank'>DISNEY</a></span>s Public Server ARES och i <span class='text'><a href='http://www.r60.org/' title="r60.org" target='_blank'>r60</a></span>'s Div 6. Dem flesta hade blivit mycket bra vänner väldig snabbt. Finns ju dem som hade en lite annorlunda vänskap mot dem andra, t.ex. <span class='text'><a href='medlemmar_info.php?medlem=8&css' title='Se Profil: Iiggi' target='main'>Iiggi</a></span> och XFire haha:D.
<br><br>
Efter vi båda hade spelat klart säsongen på <span class='text'><a href='http://www.r60.org/' title="r60.org" target='_blank'>r60</a></span> så hade allafall jag planer på att jag ville göra ngt annat än att ha hand om allt i Team DC. Ville gärna spela i en clan där det fanns andra som kunde ta just mina dåvarna uppgifter som Caller. Kom i kontakt med <span class='text'><a href='medlemmar_info.php?medlem=8&css' title='Se Profil: Iiggi' target='main'>Iiggi</a></span> och XFire och börja snacka om att jag själv skulle gå över till CD. Men istället för att jag själv skulle gå över så det blev snack om en sammanslagning. Berättade det för <span class='text'><a href='medlemmar_info.php?medlem=2&css' title='Se Profil: xep' target='main'>Bullseye</a></span> och han tyckte att det vore en underbar idé.
<br><br>
Det skulle innebära att vi inte skulle ha dåligt med spelare och fler personer till de olika uppgifterna som dök upp. Precis innan detta hände så var där vissa personer som lefta i CD, bland annat Striker om jag minns rätt. Och Painkiller i Team DC platsade inte för att få följa med i <span class='text'><a href='http://www.jalba.se/headz/' title="HeadZ - A CS:S, COD4, Q3 & WC3 clan" target='_blank'>HeadZ</a></span>.
<br><br>
Så jag <span class='text'><a href='medlemmar_info.php?medlem=1&css' title='Se Profil: Tulley' target='main'>Tulley</a></span>, <span class='text'><a href='medlemmar_info.php?medlem=2&css' title='Se Profil: xep' target='main'>Bullseye</a></span>, <span class='text'><a href='medlemmar_info.php?medlem=4&css' title='Se Profil: Ahl' target='main'>Ahl</a></span>, <span class='text'><a href='medlemmar_info.php?medlem=3&css' title='Se Profil: StarZinger' target='main'>StarZinger</a></span>, Bladion, gick då vidare och gick med XFire, <span class='text'><a href='medlemmar_info.php?medlem=8&css' title='Se Profil: Iiggi' target='main'>Iiggi</a></span>, Jigsaw, Byté, Lurquer, Smek, Spedsnot och Stn. Väldans många spelare hade vi helt plötsligt. Det roliga är att vi gjorde denna sammanslagning 1 dag innan det var stop på anmälan för <span class='text'><a href='http://www.r60.org/' title="r60.org" target='_blank'>r60</a></span> säsongen. Båda clanerna hade anmält sina claner tidigare, men skulle nu spela som en clan istället. Det blev akut anmälan och önsketänkande att vi skulle få en plats. XFire var på adminsen på <span class='text'><a href='http://www.r60.org/' title="r60.org" target='_blank'>r60</a></span> och ville ha svar ifall vi kunde komma med.<br><br>
Jag satt själv på Tisdagen och väntade och väntade, tills jag precis innan jag skulle lägga mig såg att vi var med i ligan! Ringde direkt till XFire, som jag tror jag väckte, kl var väl typ 01.00 eller något och berättade den underbara nyheten. <span class='text'><a href='http://www.jalba.se/headz/' title="HeadZ - A CS:S, COD4, Q3 & WC3 clan" target='_blank'>HeadZ</a></span> var på gång.
<br><br>
Detta är då hur vi startades och hur det gick till. Det har hänt mycket sen vi startade och där finns även fler berättelser att berätta. Jag kommer återkomma med info om just ligan när den är slut. Och hoppas att även de andra medlemmarna kan berätta sina tankar om det hela. =)
<br><br>// <span class='text'><a href='medlemmar_info.php?medlem=1&css' title='Se Profil: Tulley' target='main'>Tulley</a></span>

		</span>
	</td></tr>
	</table>

</td></tr>
</table>


</body>
</html>
