<?php
session_start(); // Alltid överst på sidan

require("conn.php");	// Databasanslutningen

$self = $_SERVER['PHP_SELF'];

// Kolla om inloggad = sessionen satt
if (!isset($_SESSION['sess_user'])){
  header("Location: index.php");
  exit;
}

//
if (isset($_SESSION['sess_user'])){

	//SQL kod för att uppdatera en vald nyhet
	mysql_query("UPDATE legacy_headz_medlem SET
		online = '1'
		WHERE id='{$_SESSION['sess_id']}'") or die (mysql_error());
}

// Utloggning
if (isset($_GET['logout'])){

	//SQL kod för att uppdatera en vald nyhet
	mysql_query("UPDATE legacy_headz_medlem SET
		online = '0'
		WHERE id='{$_SESSION['sess_id']}'") or die (mysql_error());

  session_unset();
  session_destroy();
  header("Location: index.php");
  exit;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><?php require("header_title.html"); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/admin.css" rel="stylesheet" type="text/css">
<link rel="icon" href="images/icons/favicon.ico">

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3406958-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>

</head>
<body>


<!-- COPYRIGHT © HeadZ 2007-2008 -->

<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
<tr><td height="100%" valign="top">

	<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
	<tr><td width="100%" height="5"></td></tr>
	<tr><td width="100%" height="140" align="center" valign="top" class="logo_bg">

		<table width="550" height="100%" cellspacing="0" cellpadding="0" border="0">
	  	<tr><td width="550" height="140" class="logo"></td></tr>
	  	</table>

	</td></tr>
	<tr><td height="5"></td></tr>
	<tr><td width="100%" height="100%" valign="top">

		<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
		<tr><td width="114" height="100%" valign="top">

			<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
			<tr><td width="100%" height="100" valign="top" align="center" class="login_top">

<?php

$resultat = mysql_query("SELECT * FROM legacy_headz_players WHERE user='{$_SESSION['sess_user']}'") or die (mysql_error());
if($row = mysql_fetch_array($resultat)) {
$user = $row['user'];

echo      "<table width='120' cellspacing='0' cellpadding='0' border='0'>
        <tr><td height='3'></td></tr>
        <tr><td class='admin_profile' align='center' height='20'>";
echo        "<span class='admin_gray'>[ </span>";
echo          "<a href=\"admin_profil.php\" title='Se din profil' target='main' class='admin_orange'>$user</a>";
echo        "<span class='admin_gray'> ]</span><br>";
echo      "</td></tr><tr><td align='center' height='20'>";
echo        "<span class='admin_gray'>[ </span>";
echo          "<a href=\"admin_nyheter.php\" title='Lägg till/Ta bort/Ändra nyheter' target='main' class='admin_orange'>Nyheter</a>";
echo        "<span class='admin_gray'> ]</span><br>";
echo      "</td></tr><tr><td align='center' height='20'>";
echo        "<del><span class='admin_gray'>[ </span>";
echo          "<a href=\"admin_matcher.php\" title='!!FUNKAR INTE ÄNNU!!' target='main' class='admin_orange'>Matcher</a>";
echo        "<span class='admin_gray'> ]</span></del><br>";
echo      "</td></tr><tr><td align='center' height='20'>";
echo        "<del><span class='admin_gray'>[ </span>";
echo          "<a href=\"admin_configs.php\" title='!!FUNKAR INTE ÄNNU!!' target='main' class='admin_orange'>Configs</a>";
echo        "<span class='admin_gray'> ]</span></del><br>";
echo      "</td></tr><tr><td align='center' height='20'>";
echo        "<del><span class='admin_gray'>[ </span>";
echo          "<a href=\"admin_demos.php\" title='!!FUNKAR INTE ÄNNU!!' target='main' class='admin_orange'>Demos</a>";
echo        "<span class='admin_gray'> ]</span></del><br>";
echo      "</td></tr>
        </table>";

}
?>

		</td></tr>
		<tr><td width='100%' height='37' valign='top' class='login_bottom2'>

			<table width='100%' cellspacing='0' cellpadding='0' border='0'>
			<tr><td height='37' valign='center' align='center'>
				<form action='index.php?logout=\' method='post' style='margin-bottom:0;'>
				<input type='submit' name='logout' value='Logga Ut' class='mainoption'><br>
				</form>
			</td></tr>
			</table>

		</td></tr>
		<tr><td width="100%" height="5"></td></tr>
		<tr><td width="100%" height="100%" valign="top" class="bg">

<?php include("menu.html"); ?>

		<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr><td height="15"></td></tr>
		</table>

			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td colspan="3" width="100%" height="31" valign="middle" align="center" class="banner_bg">
					<span class="admin_gray">Medlemmar online:</span>
				</td>
			</tr>
			<tr><td height="5" colspan="3"></td></tr>

<?php
$result = mysql_query("SELECT * FROM legacy_headz_medlem WHERE online='1' ORDER BY id ASC") or die (mysql_error());

for($i = 0; $row = mysql_fetch_array($result); $i++) {
echo		"<tr><td colspan='3' width='100%' valign='middle' align='center' class='banner_bg'>
				<a href='medlemmar_info.php?medlem={$row['id']}&css' target='main' class='admin_orange'>{$row['nick']}</a>
			</td></tr><tr><td height='5' colspan='5'></td></tr>";
}

?>
			</table>

		</td></tr>
		</table>

	</td><td width='5'></td><td valign='top' height='100%'>

		<table width='100%' height='100%' cellspacing='0' cellpadding='5' border='0'>
		<tr><td height='100%' valign='top' class='admin_bg2'>
			<iframe src='admin_profil.php' name='main' width='100%' height='100%' frameborder='0'></iframe>
		</td></tr>
		</table>

	</td><td width="5"></td><td width="202" height="100%" valign="top">

		<table width="202" height="100%" cellspacing="0" cellpadding="5" border="0">
		<tr><td width="202" height="100%" valign="top" align="center" class="bg">

			<table height="100%" cellspacing="0" cellpadding="0" border="0">
			<tr><td valign="top">
				<noscript>Enable Javascript to get full functionality of this <a href="http://www.freeshoutbox.net/">shoutbox</a><br></noscript><iframe src="http://saus.freeshoutbox.net/" width="260" height="100%" frameborder="0"></iframe>
			</td></tr>
			</table>

		</td></tr>
		</table>

	</td></tr>
	</table>

</td></tr>
</table>

<!-- COPYRIGHT © HeadZ 2007-2008 -->


</body>
</html>
