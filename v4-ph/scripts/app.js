'use strict';

angular.module('phv4App', [
  'ngRoute',
  'ngSanitize',
  'markdown'
])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        pageTitle: 'Saucy.se | v4 | Placeholder'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
