'use strict';

angular.module('phv4App')
  .controller('MainCtrl', function ($scope) {

    $scope.website = {
      'author': {
        'name': 'Saucy',
        'email': 'feedback@saucy.se'
      },
      'url': 'http://saucy.se',
      'github': 'https://github.com/Saucyboy/saucy.se',
      'versions': [
        {
          'name': 'version 1',
          'codename': 'v1',
          'url': 'http://saucy.se/v1',
          'github': 'https://github.com/Saucyboy/saucy.se/tree/v1',
          'date': '2010-05-03',
          'placeholder': {
            'name': 'placeholder-v1',
            'codename': 'ph-v1',
            'url': 'http://saucy.se/v1/placeholder',
            'github': 'https://github.com/Saucyboy/saucy.se/tree/placeholder-v1'
          }
        },
        {
          'name': 'version 2',
          'codename': 'v2',
          'url': 'http://saucy.se/v2',
          'github': 'https://github.com/Saucyboy/saucy.se/tree/v2',
          'date': '2011-02-18',
          'placeholder': {
            'name': 'placeholder-v2',
            'codename': 'ph-v2',
            'url': 'http://saucy.se/v2/placeholder',
            'github': 'https://github.com/Saucyboy/saucy.se/tree/placeholder-v2'
          }
        },
        {
          'name': 'version 3',
          'codename': 'v3',
          'url': 'http://saucy.se/v3',
          'github': 'https://github.com/Saucyboy/saucy.se/tree/v3',
          'date': '2013-02-07',
          'placeholder': {
            'name': 'placeholder-v3',
            'codename': 'ph-v3',
            'url': 'http://saucy.se/v3/placeholder',
            'github': 'https://github.com/Saucyboy/saucy.se/tree/placeholder-v3'
          }
        },
        {
          'name': 'version 4',
          'codename': 'v4',
          'url': 'http://saucy.se/v4',
          'github': 'https://github.com/Saucyboy/saucy.se/tree/v4',
          'date': '2014-01-22',
          'placeholder': {
            'name': 'placeholder-v4',
            'codename': 'ph-v4',
            'url': 'http://saucy.se/v4/placeholder',
            'github': 'https://github.com/Saucyboy/saucy.se/tree/placeholder-v4'
          }
        }
      ]
    };

    $scope.wip = {
      'header': 'Website is under construction',
      'footer': 'A footer for good measure!',
      'descriptions': [
        'Hello there! I am working on updating my website (again) to the 4:th version of my website. You can find my source code for this page and the website I am currently working on at the links below.',
        'You can view my entire website\'s source code here: [GitHub](https://github.com/Saucyboy/saucy.se)',
        'If you find any issues or errors on any of my versions, please submit an issue here: [GitHub](https://github.com/Saucyboy/saucy.se/issues).',
        'This page is built using [Yeoman](http://yeoman.io/) (using [AngularJS generator](https://github.com/yeoman/generator-angular)), [AngularJS](http://angularjs.org/), [Bower](http://bower.io/) and [Grunt](http://gruntjs.com/). The 4:th version will probably use the same tools, however, a bit customized for my [coding standards](https://github.com/Saucyboy/saucy.se/blob/master/CODING-STANDARDS.md).',
        '_**Note**: "placeholder" means the **under construction** landing page (which you\'re currently viewing!)_',
        '_**Note**: I\'m cleaning up my server, if you\'re one of the few people here who actually used some of the things I had up here and wish to use them again, send me an email at: `support [at] saucy [dot] se`. They **will** return one day!_'
      ]
    };

  });
