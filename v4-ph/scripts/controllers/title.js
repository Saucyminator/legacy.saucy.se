'use strict';

angular.module('phv4App')
  .controller('TitleCtrl', function ($scope, $route) {
    $scope.$on('$routeChangeSuccess', function () {
      $scope.pageTitle = $route.current.pageTitle;
    });
  });
