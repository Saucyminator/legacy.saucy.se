<?php

$env = 'dev';
$env_text = 'Why hello there! So, you\'ve found your way into the the pits of development. Here you will find the latest version(s) of my updated website.';

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset='UTF-8' />
		<title>Saucy.se v3 :: Development</title>
	</head>
	<body>
		<div>
			<p><?php echo ($env === 'dev') ? $env_text : '' ; ?></p>
		</div>
	</body>
</html>