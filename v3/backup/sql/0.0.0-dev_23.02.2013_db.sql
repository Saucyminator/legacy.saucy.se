-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 20, 2013 at 02:02 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `saucy_v3`
--

-- --------------------------------------------------------

--
-- Table structure for table `saucy_v3_contents`
--

CREATE TABLE IF NOT EXISTS `saucy_v3_contents` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idUser` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'author id',
  `idPage` int(11) unsigned DEFAULT NULL COMMENT 'connected to {pages}-table; where it belongs to: blogs/reviews/etc',
  `idLinks` int(11) unsigned DEFAULT NULL COMMENT '"sources", (multiple) urls; credit, other sources etc',
  `idTutorial` int(11) unsigned DEFAULT NULL COMMENT 'if not null it''s usage for: portfolios/downloads - tutorials linking',
  `dateCreated` datetime NOT NULL COMMENT 'date created, YYYY-MM-DD HH:MM:SS',
  `dateToPublish` datetime DEFAULT NULL COMMENT 'a date to publish the conents-post, if nothing is selected: post now',
  `dateEdited` datetime NOT NULL COMMENT 'date last edited, YYYY-MM-DD HH:MM:SS',
  `timesEdited` int(11) unsigned NOT NULL COMMENT 'how many times the article has been edited',
  `headline` text NOT NULL COMMENT 'article title',
  `content` text NOT NULL COMMENT 'article itself',
  `display` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `file` varchar(255) DEFAULT NULL COMMENT 'tutorials/portfolios/downloads end up in /resources/files/{pages}/{users}',
  `views` int(11) unsigned NOT NULL DEFAULT '1' COMMENT 'amount of article views',
  `reviews_rating` int(11) unsigned DEFAULT NULL COMMENT 'reviews: 1-5; 1=worst, 5=best',
  `tutorials_difficulty` int(11) unsigned DEFAULT NULL COMMENT 'tutorials: 1-5; 1=beginner, 5=expert',
  `timeToCompletion` int(11) unsigned DEFAULT NULL COMMENT 'Time to completion; minutes; for tutorials, portfolios, downloads',
  PRIMARY KEY (`id`),
  KEY `idPage` (`idPage`),
  KEY `idUser` (`idUser`),
  KEY `idTutorial` (`idTutorial`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `saucy_v3_contents`
--

INSERT INTO `saucy_v3_contents` (`id`, `idUser`, `idPage`, `idLinks`, `idTutorial`, `dateCreated`, `dateToPublish`, `dateEdited`, `timesEdited`, `headline`, `content`, `display`, `file`, `views`, `reviews_rating`, `tutorials_difficulty`, `timeToCompletion`) VALUES
(1, 1, 1, NULL, NULL, '2013-01-28 00:00:00', NULL, '2013-01-28 02:00:00', 1, 'headline here', 'content here', 1, NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `saucy_v3_pages`
--

CREATE TABLE IF NOT EXISTS `saucy_v3_pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page` varchar(255) NOT NULL COMMENT 'blogs/reviews/etc',
  `name` varchar(255) NOT NULL COMMENT 'Blogs/Reviews/etc, can have a different name than page',
  `order` int(11) unsigned NOT NULL COMMENT 'ability to change the order of the page',
  `display` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '0=hidden/1=shown, display it or not on e.g. navigation',
  PRIMARY KEY (`id`),
  UNIQUE KEY `order` (`order`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `saucy_v3_pages`
--

INSERT INTO `saucy_v3_pages` (`id`, `page`, `name`, `order`, `display`) VALUES
(1, 'blogs', 'Blog', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `saucy_v3_permissions`
--

CREATE TABLE IF NOT EXISTS `saucy_v3_permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL COMMENT 'short description',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `saucy_v3_permissions`
--

INSERT INTO `saucy_v3_permissions` (`id`, `name`, `description`) VALUES
(0, 'Superuser', 'Superusers hoo!'),
(1, 'Admin', 'Admin stuff'),
(2, 'User', 'Normal user');

-- --------------------------------------------------------

--
-- Table structure for table `saucy_v3_usergroups`
--

CREATE TABLE IF NOT EXISTS `saucy_v3_usergroups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL COMMENT 'short description',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'false=hidden/true=shown, if the usergroup is active or not, e.g. admins can put users in the group',
  `display` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'false=hidden/true=shown, display it on /usergroups or not etc',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `saucy_v3_usergroups`
--

INSERT INTO `saucy_v3_usergroups` (`id`, `name`, `description`, `enabled`, `display`) VALUES
(0, 'Superusers', 'We''re awesome!', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `saucy_v3_usergroups_permissions`
--

CREATE TABLE IF NOT EXISTS `saucy_v3_usergroups_permissions` (
  `idUsergroups` int(11) unsigned NOT NULL,
  `idPermissions` int(11) unsigned NOT NULL,
  PRIMARY KEY (`idUsergroups`,`idPermissions`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `saucy_v3_usergroups_permissions`
--

INSERT INTO `saucy_v3_usergroups_permissions` (`idUsergroups`, `idPermissions`) VALUES
(0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `saucy_v3_users`
--

CREATE TABLE IF NOT EXISTS `saucy_v3_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nameDisplay` varchar(255) NOT NULL COMMENT 'username',
  `nameFirst` varchar(255) DEFAULT NULL COMMENT 'first name',
  `nameMiddle` varchar(255) DEFAULT NULL COMMENT 'middle name',
  `nameLast` varchar(255) DEFAULT NULL COMMENT 'last name',
  `dateCreated` datetime NOT NULL COMMENT 'date registered',
  `dateLastOnline` datetime NOT NULL COMMENT 'date last logged in',
  `display` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'false=hidden/true=shown, maybe use as a ''do not display me on /users'' list?',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'false=hidden/true=shown, if the user can login/etc',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0=offline/1=online',
  `email` varchar(255) NOT NULL COMMENT 'email',
  `age` datetime DEFAULT NULL COMMENT 'age; maybe just use int instead of specific date?',
  `location` varchar(255) DEFAULT NULL COMMENT 'e.g. Sweden/Scania/Höör',
  `url` varchar(255) DEFAULT NULL COMMENT 'website/url',
  `passwordHash` varchar(255) NOT NULL COMMENT 'remove if I''ll go OAuth/facebook',
  `passwordSalt` varchar(255) NOT NULL COMMENT 'remove if I''ll go OAuth/facebook',
  `accessToken` varchar(255) DEFAULT NULL COMMENT 'not null',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `saucy_v3_users`
--

INSERT INTO `saucy_v3_users` (`id`, `nameDisplay`, `nameFirst`, `nameMiddle`, `nameLast`, `dateCreated`, `dateLastOnline`, `display`, `enabled`, `status`, `email`, `age`, `location`, `url`, `passwordHash`, `passwordSalt`, `accessToken`) VALUES
(1, 'Saucy', NULL, NULL, NULL, '2013-01-28 00:00:00', '2013-01-29 00:00:00', 1, 1, 0, 'saucy@saucy.se', '2000-01-01 00:00:00', NULL, NULL, 'test', 'test', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `saucy_v3_users_usergroup`
--

CREATE TABLE IF NOT EXISTS `saucy_v3_users_usergroup` (
  `idUser` int(11) unsigned NOT NULL,
  `idUsergroup` int(11) unsigned NOT NULL,
  PRIMARY KEY (`idUser`,`idUsergroup`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `saucy_v3_users_usergroup`
--

INSERT INTO `saucy_v3_users_usergroup` (`idUser`, `idUsergroup`) VALUES
(1, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
