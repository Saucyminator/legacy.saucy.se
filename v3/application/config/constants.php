﻿<?php

////////////////////////////////////////
// applications/config/configuration.php maybe?
//
// mb_internal_encoding("UTF-8");
// setlocale(LC_ALL, "sv_SE");
// date_default_timezone_set("Europe/Paris");


////////////////////////////////////////
// DEFAULT
define('APP_ROOT', 	realpath(dirname(__FILE__) . '/../..'));
define('APP_HOST', 	$_SERVER['SERVER_NAME']);
define('APP_PATH', 	'application');

$arrayLocal = array('localhost', '127.0.0.1', '192.168.1.86'); // one

if(in_array($_SERVER['SERVER_NAME'], $arrayLocal)) {
	define('ROOT', 		realpath(dirname(__FILE__) . '/../../..'));

	define('APP_NAME', 	strtolower(str_replace(ROOT . DIRECTORY_SEPARATOR, '', APP_ROOT)));
	define('APP_URL', 	'http://' . APP_HOST . '/' . APP_NAME);
} else {
	define('ROOT', 		$_SERVER['DOCUMENT_ROOT']);

	define('APP_NAME', 	strtolower($_SERVER['SERVER_NAME']));
	define('APP_URL', 	'http://' . APP_NAME);
}


////////////////////////////////////////
// CUSTOM
define('ENVIRONMENT', 'dev');
define('VERSION', '0.0.0');


////////////////////////////////////////
// ENVIRONMENT
if(defined('ENVIRONMENT')) {
	switch(ENVIRONMENT) {
		case 'dev':
		case 'develop':
		case 'development':
			ini_set('display_errors', 1);
			ini_set('log_errors', 1);
			error_reporting(E_ALL);
			break;
		case 'live':
		case 'production':
			ini_set('display_errors', 0);
			ini_set('log_errors', 0);
			error_reporting(0);
			break;
		case 'test':
		case 'testing':
		case 'stop':
		case 'close':
		case 'closed':
			echo $env_text;
			exit;
		default:
			throw new Exception('Unknown application environment.', 123);
	}
}


////////////////////////////////////////
// Testing

?>
<table class="table table-hover table-condensed table-bordered">
	<tbody>
		<tr>
			<td>ROOT</td>
			<td><?php echo ROOT; ?></td>
		</tr>
		<tr>
			<td>APP_ROOT</td>
			<td><?php echo APP_ROOT; ?></td>
		</tr>
		<tr>
			<td>APP_NAME</td>
			<td><?php echo APP_NAME; ?></td>
		</tr>
		<tr>
			<td>APP_URL</td>
			<td><?php echo APP_URL; ?></td>
		</tr>
		<tr>
			<td>APP_PATH</td>
			<td><?php echo APP_PATH; ?></td>
		</tr>
		<tr>
			<td>ENVIRONMENT</td>
			<td><?php echo ENVIRONMENT; ?></td>
		</tr>
		<tr>
			<td>VERSION</td>
			<td><?php echo VERSION; ?></td>
		</tr>
	</tbody>
</table>
<?php
