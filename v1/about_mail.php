<?php include("settings.php");

//$urlRefresh = "blog.php";
//header("Refresh: 1; URL=\"" . $urlRefresh . "\""); // redirect in 1 second

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>.: Saucy.se <?php echo $siteVersion; ?> &nbsp;&raquo;&nbsp; ABOUT &nbsp; ~ &nbsp; <?php echo $siteSlogan; ?> :.</title>
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/nav.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<meta name="author" content="Patrik 'S�s' Holmberg" />
<meta name="generator" content="phpDesigner 7, Photoshop CS3" />
<meta name="keywords" content="SAUCY.SE, Saucy, personal, blog, game corner, portfolio, projects, downloads, config, map, model, tutorial, video, wallpaper, forum, about, q&a, questions & answers, resources, archive" />
<meta name="description" content="Saucy.se - Personal site of Saucy! Blog, portfolio, reviews, tutorials, guides, downloadables and other stuff." />
<meta name="copyright" content="SAUCY.SE - Patrik Holmberg" />
<meta http-equiv="imagetoolbar" content="no"/>
<link rel="icon" href="image/icon/favicon.ico" />

<?php require("css/body.html"); ?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-XXXXX-X']);
  _gaq.push(['_setDomainName', 'url.example.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script language = "Javascript">
function Validate()
{
    if (document.contact.name.value == '') 
    {
        alert('You forgot to enter your name!');
        return false;
    } else if (document.contact.email.value == '') 
    {
       alert('You forgot to type your email!');
       return false;
    }
}
</script>

</head>
<body>

<?php require("preloader.html"); ?>

<center>

    <?php include("info.html"); ?>

    <div id="body">

        <a href="<?php echo $saucyURL; ?>"><div id="logo"></div></a>

        <div id="navwrapper">
            <div id="nav">
            	<ul class="menu_body" id="nav1">
            		<li class="menu_head_home"><a href="index.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav2">
            		<li class="menu_head_blog"><a href="blog.php"></a></li>
            		<li><a href="">GAME CORNER</a></li>
            		<li><a href=""><span style="color:<?php echo $dark; ?>;">REVIEWS</span></a></li>
            	</ul>
            	<ul class="menu_body" id="nav3">
            		<li class="menu_head_portfolio"><a href=""></a></li>
            		<li class="alt"><a href="">PROJECTS</a></li>
            	</ul>
            	<ul class="menu_body" id="nav4">
            		<li class="menu_head_downloads"><a href=""></a></li>
            		<li class="alt"><a href="">CONFIG</a></li>
            		<li class="altlight"><a href=""><span style="color:<?php echo $dark; ?>;">MAP</span></a></li>
            		<li class="alt"><a href="">MODEL</a></li>
            		<li class="altlight"><a href=""><span style="color:<?php echo $dark; ?>;">TUTORIAL</span></a></li>
            		<li class="alt"><a href="">VIDEO</a></li>
            		<li class="altlight"><a href=""><span style="color:<?php echo $dark; ?>;">WALLPAPER</span></a></li>
            		<li class="alt"><a href="">WOW-GUIDES</a></li>
            	</ul>
            	<ul class="menu_body" id="nav5">
            		<li class="menu_head_forum"><a href="http://forum.saucy.se/"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav6">
            		<li class="menu_head_about_location"><a href="about.php"></a></li>
            		<li><a href="">Q&A</a></li>
            	</ul>
            	<ul class="menu_body" id="nav7">
            		<li class="menu_head_resources"><a href=""></a></li>
            		<li><a href="">USEFUL LINKS</a></li>
            		<li><a href=""><span style="color:<?php echo $dark; ?>;">ARCHIVE</span></a></li>
            	</ul>
            </div>
        </div>

        <?php include("message.html"); ?>

        <div id="panel_left">
            <div style="float: left">
                <?php include("infobox.html"); ?>
                <?php include("poll.html"); ?>
                <?php include("shoutbox.html"); ?>
            </div>
        </div>
        <div id="panel_right">
            <div style="float: right">
                <?php include("search.html"); ?>
            </div>
        </div>

        <div id="contentwrapper">       <?php /* TEXTEN I div id=text M�STE HA <br /> P� SLUTET F�R INTE TEXT SKA FINNAS MELLAN COMMENTS OCH READMORE L�NKARNA */ ?>
            <div id="content">
                <div id="title"><span class="big">Who am I?</span></div>
                <div id="spacer"></div>
                <div id="text" style="text-align: center;">

<?php

// anger en variabel som kan lagra de eventuella felaktigheterna
$errors = array();

// kontrollera om ett Namn angivits
if (!$_POST["name"])
$errors[] = "&raquo; Your name";

// kontrollera om ett �mne angivits
if (!$_POST["subject"])
$errors[] = "&raquo; Your subject";

// kontrollera om en Epostadress angivits
$emailcheck = $_POST["email"];
if(!preg_match("/^[a-z0-9\�\�\�._-]+@[a-z0-9\�\�\�.-]+\.[a-z]{2,6}$/i", $emailcheck))
$errors[] = "&raquo; Your e-mail"; 

// kontrollera om ett Meddelande angivits
if (!$_POST["message"])
$errors[] = "&raquo; No message written";

//kontrollera om scriptet anropas fr�n ditt formul�r
if ($_SERVER['HTTP_REFERER']!= "http://saucy.se/about.php")
$errors[] = "&raquo; Your use of our form is not allowed!";

// om felaktig information finns visas detta meddelande
if(count($errors)>0) {
echo "<b>Following information must be filled in:</b><br /><br />";
foreach($errors as $fel)
echo "$fel<br/>";
echo "<br />Please enter the required information and try again.<br />";
echo "<a href='http://saucy.se/about.php' title='Go back' target='_self'>Click here to go back to the form</a>";
} else {
// formul�ret �r korrekt ifyllt och informationen bearbetas
$to = "info@frostapaintball.se";
$name = $_POST["name"];
$from = $_POST["email"];
$phone = $_POST["phone"];
$find = $_POST["find"];
$subject = $_POST["subject"];
$message = $_POST["message"];

	if(mail($to, $subject, $message ,"From: $name <$from>")) {
		echo nl2br("Your message has been sent!
        Subject: $subject
        Phone Number: $phone
        How you found me: $find
        Message: $message
		");
	} else {
		echo "Your message could not be sent";
	}
}

?>

                    </div>
                </div>
            </div>

            <?php require("copyright.html"); ?>

        </div>

    </div>

</center>


</body>
</html>