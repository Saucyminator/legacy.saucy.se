<?php include("settings.php");

//$urlRefresh = "index.php";
//header("Refresh: 1; URL=\"" . $urlRefresh . "\""); // redirect in 1 second

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>.: Saucy.se <?php echo $siteVersion; ?> &nbsp;&raquo;&nbsp; HOME &nbsp; ~ &nbsp; <?php echo $siteSlogan; ?> :.</title>
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/nav.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<meta name="author" content="Saucy" />
<meta name="generator" content="phpDesigner 7, Photoshop CS3" />
<meta name="keywords" content="SAUCY.SE, Saucy, personal, blog, game corner, portfolio, projects, downloads, config, map, model, tutorial, video, wallpaper, forum, about, q&a, questions & answers, resources, archive" />
<meta name="description" content="Saucy.se - Personal site of Saucy! Blog, portfolio, reviews, tutorials, guides, downloadables and other stuff." />
<meta name="copyright" content="Saucy" />
<meta http-equiv="imagetoolbar" content="no"/>
<link rel="icon" href="image/icon/favicon.ico" />

<?php require("css/body.html"); ?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-XXXXX-X']);
  _gaq.push(['_setDomainName', 'url.example.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<style type="text/css">

body {
    background: <?php echo $light; ?> url(<?php echo $portfolio_bg; ?>) repeat-x scroll 0px 233px;
}

</style>

</head>
<body>

<?php require("preloader.html"); ?>

<center>

    <?php include("info.html"); ?>

    <div id="body">

        <a href="<?php echo $saucyURL; ?>"><div id="logo"></div></a>

        <div id="navwrapper">
            <div id="nav">
            	<ul class="menu_body" id="nav1">
            		<li class="menu_head_home_location"><a href="index.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav2">
            		<li class="menu_head_blog"><a href="blog.php"></a></li>
            		<li><a href="">GAME CORNER</a></li>
            		<li><a href=""><span style="color:<?php echo $dark; ?>;">REVIEWS</span></a></li>
            	</ul>
            	<ul class="menu_body" id="nav3">
            		<li class="menu_head_portfolio"><a href=""></a></li>
            		<li class="alt"><a href="">PROJECTS</a></li>
            	</ul>
            	<ul class="menu_body" id="nav4">
            		<li class="menu_head_downloads"><a href=""></a></li>
            		<li class="alt"><a href="">CONFIG</a></li>
            		<li class="altlight"><a href=""><span style="color:<?php echo $dark; ?>;">MAP</span></a></li>
            		<li class="alt"><a href="">MODEL</a></li>
            		<li class="altlight"><a href=""><span style="color:<?php echo $dark; ?>;">TUTORIAL</span></a></li>
            		<li class="alt"><a href="">VIDEO</a></li>
            		<li class="altlight"><a href=""><span style="color:<?php echo $dark; ?>;">WALLPAPER</span></a></li>
            		<li class="alt"><a href="">WOW-GUIDES</a></li>
            	</ul>
            	<ul class="menu_body" id="nav5">
            		<li class="menu_head_forum"><a href="http://forum.saucy.se/"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav6">
            		<li class="menu_head_about"><a href="about.php"></a></li>
            		<li><a href="">Q&A</a></li>
            	</ul>
            	<ul class="menu_body" id="nav7">
            		<li class="menu_head_resources"><a href=""></a></li>
            		<li><a href="">USEFUL LINKS</a></li>
            		<li><a href=""><span style="color:<?php echo $dark; ?>;">ARCHIVE</span></a></li>
            	</ul>
            </div>
        </div>

        <div id="portfolio">
            <div id="title" style="padding-top: 15px; margin-bottom: 10px;">
                <span class="big">Latest portfolio entries:</span>
            </div>
            <div id="portfolio_arrow" style="left: -50px;"><a href=""><img src="image/main/arrow_left.png" /></a></div>
            <div id="portfolio_arrow" style="right: -50px;"><a href=""><img src="image/main/arrow_right.png" /></a></div>
                <div id="portfolio_img"><a href="image/misc/Terran.jpg" target="_blank"><img src="<?php echo $portfolio_latestimg_3; ?>" /></a></div>
                <div id="portfolio_spacer"></div>
                <div id="portfolio_img"><a href="image/misc/Zerg.jpg" target="_blank"><img src="<?php echo $portfolio_latestimg_2; ?>" /></a></div>
                <div id="portfolio_spacer"></div>
                <div id="portfolio_img"><a href="image/misc/Protoss.jpg" target="_blank"><img src="<?php echo $portfolio_latestimg_1; ?>" /></a></div>
        </div>

        <?php include("message.html"); ?>

        <div id="panel_left">
            <div style="float: left">
                <?php include("infobox.html"); ?>
                <?php include("poll.html"); ?>
                <?php include("shoutbox.html"); ?>
            </div>
        </div>
        <div id="panel_right">
            <div style="float: right">
                <?php include("search.html"); ?>
            </div>
        </div>

        <div id="contentwrapper">       <?php /* TEXTEN I div id=text M�STE HA <br /> P� SLUTET F�R INTE TEXT SKA FINNAS MELLAN COMMENTS OCH READMORE L�NKARNA */ ?>
            <div id="content">
                <div id="title">
                    <span class="big">
                        Latest video entry:
                    </span>
                </div>
                <div id="spacer"></div>
                <div id="text">
                <div id="video" style="position: relative; float: left; margin: 3px 10px 0px 0px;"><img src="image/video.png" align="left" hspace="0" vspace="0" width="320" height="240" /></div>
                    <div id="date">
                        <span class="small">
                            [ HH:MM @ DD/MM-YY | Category: <b><a href="">Website Designs</a></b> ]
                        </span>
                    </div>
<br />
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the dummy text Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electron and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.ic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the dummy textLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and type
<br /><br /><br />
                    <div id="comments_small" style="left: 332px;">
                        <span class="small">
                            [ <b><a href="">123 comments</a></b> | 123 Views | Tags: <a href="">Awesome..</a> ]
                        </span>
                    </div>
                    <div id="readmore_small">
                        <span class="small">
                            [ <a href="">Read more &raquo;</a> ]
                        </span>
                    </div>
                </div>
            </div>

            <div id="content">
                <div id="title">
                    <span class="mediumbold" style="float: left; padding-left: 10px;">
                        Latest blog entry:
                    </span>
                    <span class="big">
                        Latest blog entry headline here
                    </span>
                </div>
                <div id="spacer"></div>
                <div id="text">
                <div id="img" style="position: relative; float: left; margin: 3px 10px 0px 0px;"><img src="image/img.png" align="left" hspace="0" vspace="0" width="150" height="150" /></div>
                    <div id="date">
                        <span class="small">
                            [ HH:MM @ DD/MM-YY | Category: <b><a href="">Website Designs</a></b> ]
                        </span>
                    </div>
<br />
                     in Virginia, Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the .......
<br /><br />
                    <div id="comments_small">
                        <span class="small">
                            [ <b><a href="">123 comments</a></b> | 123 Views | Tags: <a href="">Awesome..</a> ]
                        </span>
                    </div>
                    <div id="readmore_small">
                        <span class="small">
                            [ <a href="">Read more &raquo;</a> ]
                        </span>
                    </div>
                </div>
            </div>

            <div id="content">
                <div id="title">
                    <span class="mediumbold" style="float: left; padding-left: 10px;">
                        Latest game corner entry:
                    </span>
                    <span class="big">
                        Latest game corner headline here
                    </span>
                </div>
                <div id="spacer"></div>
                <div id="text">
                <div id="img" style="position: relative; float: left; margin: 3px 10px 0px 0px;"><img src="image/img.png" align="left" hspace="0" vspace="0" width="150" height="150" /></div>
                    <div id="date">
                        <span class="small">
                            [ HH:MM @ DD/MM-YY | Category: <b><a href="">Website Designs</a></b> ]
                        </span>
                    </div>
<br />
                     in Virginia, Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the .......
<br /><br />
                    <div id="comments_small">
                        <span class="small">
                            [ <b><a href="">123 comments</a></b> | 123 Views | Tags: <a href="">Awesome..</a> ]
                        </span>
                    </div>
                    <div id="readmore_small">
                        <span class="small">
                            [ <a href="">Read more &raquo;</a> ]
                        </span>
                    </div>
                </div>
            </div>

            <div id="content">
                <div id="title">
                    <span class="mediumbold" style="float: left; padding-left: 10px;">
                        Latest projects entry:
                    </span>
                    <span class="big">
                        Latest projects entry headline here
                    </span>
                </div>
                <div id="spacer"></div>
                <div id="text">
                <div id="img" style="position: relative; float: left; margin: 3px 10px 0px 0px;"><img src="image/img.png" align="left" hspace="0" vspace="0" width="150" height="150" /></div>
                    <div id="date">
                        <span class="small">
                            [ HH:MM @ DD/MM-YY | Category: <b><a href="">Website Designs</a></b> ]
                        </span>
                    </div>
<br />
                     in Virginia, Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the .......
<br /><br />
                    <div id="comments_small">
                        <span class="small">
                            [ <b><a href="">123 comments</a></b> | 123 Views | Tags: <a href="">Awesome..</a> ]
                        </span>
                    </div>
                    <div id="readmore_small">
                        <span class="small">
                            [ <a href="">Read more &raquo;</a> ]
                        </span>
                    </div>
                </div>
            </div>

            <div id="content" style="margin-right: 454px; width: <?php echo ($contentWidth/2 - 25)."px"; ?>; height: 192px;">
                <div id="title">
                    <span class="big">
                        Latest downloads headline
                    </span>
                </div>
                <div id="spacer"></div>
                <div id="text">
                <div id="img" style="position: relative; float: left; margin: 3px 10px 0px 0px;"><img src="image/img.png" align="left" hspace="0" vspace="0" width="150" height="150" /></div>
                    <div id="date">
                        <span class="small">
                            [ HH:MM @ DD/MM-YY ]
                        </span>
                    </div>
<br />
                    It has roots in a piece of It has roots in a piece of classical Latin literature from 45 BC, It has roots in a piece of It has roots in a piece of classical Latin literature from 45 BC, It has roots in a piece of It has roots in a piece of classical Latin literature from 45 BC, It has roots in a piece of It has roots in a piece of classical Latin literature from 45 BC, It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the .......
<br />
                    <div id="comments">
                        <span class="small">
                            [ Tags: <a href="">Awesome</a>, <a href="">Awesome..</a> ]
                        </span>
                    </div>
                    <div id="readmore">
                        <span class="small">
                            [ <a href="">Read more &raquo;</a> ]
                        </span>
                    </div>
                </div>
            </div>

            <div id="content" style="margin: -222px 0px 0px 0px; left: 226px; width: <?php echo ($contentWidth/2 - 25)."px"; ?>; height: 192px;">
                <div id="title">
                    <span class="big">
                        Latest downloads headline
                    </span>
                </div>
                <div id="spacer"></div>
                <div id="text">
                <div id="img" style="position: relative; float: left; margin: 3px 10px 0px 0px;"><img src="image/img.png" align="left" hspace="0" vspace="0" width="150" height="150" /></div>
                    <div id="date">
                        <span class="small">
                            [ HH:MM @ DD/MM-YY ]
                        </span>
                    </div>
<br />
                    It has It has It has It has It has roots in a piece of It has roots in a piece of classical Latin literature from 45 BC, It has roots in a piece of It has roots in a piece of classical Latin literature from 45 BC, It has roots in a piece of It has roots in a piece of classical Latin literature from 45 BC, It has roots in a piece of It has roots in a piece of classical Latin literature from 45 BC, It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the .......
<br />
                    <div id="comments">
                        <span class="small">
                            [ Tags: <a href="">Awesome</a>, <a href="">Awesome..</a> ]
                        </span>
                    </div>
                    <div id="readmore">
                        <span class="small">
                            [ <a href="">Read more &raquo;</a> ]
                        </span>
                    </div>
                </div>
            </div>


            <?php require("copyright.html"); ?>

        </div>

    </div>

</center>


</body>
</html>