<?php
//$urlRefresh = "index.php";
//header("Refresh: 1; URL=\"" . $urlRefresh . "\""); // redirect in 1 second

/* HOME URL */
$saucyURL = "http://saucy.se";

/* COLORS */
$light = "#e6e6e6";
$dark = "#1a1a1a";
$orange = "#e68f17";

/*  font: <'font-style'>  || <'font-variant'>  || <'font-weight'>  || <'font-size'>  || / <'line-height'>  || <'font-family'>  */
$defaultfont = "normal normal bold 10px normal Tahoma, sans-serif;";

$logo = "image/logo.png";
$bg = "image/main/bg.png";
$spacer = "image/main/spacer.png";
$portfolio_spacer = "image/main/portfolio_spacer.png";
$message_bg = "image/main/message_bg.png";
$portfolio_bg = "image/main/portfolio_bg.png";

/* $siteWidth = 896; */
$siteWidth = 1340;
$siteVersion = "v.1";
$siteSlogan = "Liberty & Truth";


$navWidth = 128;
$navHeight = 30;
$navAmount = 7;


$sidebarWidth = 192;


$portfolio_imgWidth = 260;
$portfolio_imgHeight = 170;
$portfolio_arrowWidth = 40;
$portfolio_arrowHeight = 80;

$portfolio_latestimg_1 = "image/main/portfolio/Protoss_img.png";
$portfolio_latestimg_2 = "image/main/portfolio/Zerg_img.png";
$portfolio_latestimg_3 = "image/main/portfolio/Terran_img.png";


$contentWidth = 896;

$videoWidth = 320;
$videoHeight = 240;

$imgWidth = 150;
$imgHeight = 150;


$socialWidth = 32;
$socialHeight = 32;
$socialAmount = 10;
$socialGap = 10;

$socialEmail      = "mailto:saucy@saucy.se";
$socialSteam      = "http://steamcommunity.com/id/saucyboy";
$socialBattlenet  = "http://eu.battle.net/sc2/en/profile/865062/1/Saucy/";
$socialDeviantart = "http://ninjasaus.deviantart.com/";
$socialTwitter    = "http://twitter.com/Zaucy";
$socialYoutube    = "http://www.youtube.com/user/zaucyboy";
$socialLastfm     = "http://www.last.fm/user/skaUn1nG";
$socialWowhead    = "http://www.wowhead.com/user=zAce";
$socialSpotify    = "http://open.spotify.com/user/define%28s%C3%A5s%29%3b";
$socialSkype      = "skype:sgt.saucy?userinfo";


$home = "image/nav/home.png";
$home_hover = "image/nav/home_hover.png";
$home_location = "image/nav/home_location.png";
$home_location_hover = "image/nav/home_location_hover.png";

$blog = "image/nav/blog.png";
$blog_hover = "image/nav/blog_hover.png";
$blog_location = "image/nav/blog_location.png";
$blog_location_hover = "image/nav/blog_location_hover.png";

$portfolio = "image/nav/portfolio.png";
$portfolio_hover = "image/nav/portfolio_hover.png";
$portfolio_location = "image/nav/portfolio_location.png";
$portfolio_location_hover = "image/nav/portfolio_location_hover.png";

$downloads = "image/nav/downloads.png";
$downloads_hover = "image/nav/downloads_hover.png";
$downloads_location = "image/nav/downloads_location.png";
$downloads_location_hover = "image/nav/downloads_location_hover.png";

$forum = "image/nav/forum.png";
$forum_hover = "image/nav/forum_hover.png";
$forum_location = "image/nav/forum_location.png";
$forum_location_hover = "image/nav/forum_location_hover.png";

$about = "image/nav/about.png";
$about_hover = "image/nav/about_hover.png";
$about_location = "image/nav/about_location.png";
$about_location_hover = "image/nav/about_location_hover.png";

$resources = "image/nav/resources.png";
$resources_hover = "image/nav/resources_hover.png";
$resources_location = "image/nav/resources_location.png";
$resources_location_hover = "image/nav/resources_location_hover.png";


$social_img_1 = "image/social/email.png";
$social_img_1_hover = "image/social/email_hover.png";
$social_img_2 = "image/social/steam.png";
$social_img_2_hover = "image/social/steam_hover.png";
$social_img_3 = "image/social/battlenet.png";
$social_img_3_hover = "image/social/battlenet_hover.png";
$social_img_4 = "image/social/deviantart.png";
$social_img_4_hover = "image/social/deviantart_hover.png";
$social_img_5 = "image/social/twitter.png";
$social_img_5_hover = "image/social/twitter_hover.png";
$social_img_6 = "image/social/youtube.png";
$social_img_6_hover = "image/social/youtube_hover.png";
$social_img_7 = "image/social/lastfm.png";
$social_img_7_hover = "image/social/lastfm_hover.png";
$social_img_8 = "image/social/wowhead.png";
$social_img_8_hover = "image/social/wowhead_hover.png";
$social_img_9 = "image/social/spotify.png";
$social_img_9_hover = "image/social/spotify_hover.png";
$social_img_10 = "image/social/skype.png";
$social_img_10_hover = "image/social/skype_hover.png";
?>