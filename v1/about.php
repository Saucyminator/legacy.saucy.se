<?php include("settings.php");

//$urlRefresh = "blog.php";
//header("Refresh: 1; URL=\"" . $urlRefresh . "\""); // redirect in 1 second

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>.: Saucy.se <?php echo $siteVersion; ?> &nbsp;&raquo;&nbsp; ABOUT &nbsp; ~ &nbsp; <?php echo $siteSlogan; ?> :.</title>
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/nav.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<meta name="author" content="Patrik 'S�s' Holmberg" />
<meta name="generator" content="phpDesigner 7, Photoshop CS3" />
<meta name="keywords" content="SAUCY.SE, Saucy, personal, blog, game corner, portfolio, projects, downloads, config, map, model, tutorial, video, wallpaper, forum, about, q&a, questions & answers, resources, archive" />
<meta name="description" content="Saucy.se - Personal site of Saucy! Blog, portfolio, reviews, tutorials, guides, downloadables and other stuff." />
<meta name="copyright" content="SAUCY.SE - Patrik Holmberg" />
<meta http-equiv="imagetoolbar" content="no"/>
<link rel="icon" href="image/icon/favicon.ico" />

<?php require("css/body.html"); ?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-XXXXX-X']);
  _gaq.push(['_setDomainName', 'url.example.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script language = "Javascript">
function Validate()
{
    if (document.contact.name.value == '') 
    {
        alert('You forgot to enter your name!');
        return false;
    } else if (document.contact.email.value == '') 
    {
       alert('You forgot to type your email!');
       return false;
    }
}
</script>

</head>
<body>

<?php require("preloader.html"); ?>

<center>

    <?php include("info.html"); ?>

    <div id="body">

        <a href="<?php echo $saucyURL; ?>"><div id="logo"></div></a>

        <div id="navwrapper">
            <div id="nav">
            	<ul class="menu_body" id="nav1">
            		<li class="menu_head_home"><a href="index.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav2">
            		<li class="menu_head_blog"><a href="blog.php"></a></li>
            		<li><a href="">GAME CORNER</a></li>
            		<li><a href=""><span style="color:<?php echo $dark; ?>;">REVIEWS</span></a></li>
            	</ul>
            	<ul class="menu_body" id="nav3">
            		<li class="menu_head_portfolio"><a href=""></a></li>
            		<li class="alt"><a href="">PROJECTS</a></li>
            	</ul>
            	<ul class="menu_body" id="nav4">
            		<li class="menu_head_downloads"><a href=""></a></li>
            		<li class="alt"><a href="">CONFIG</a></li>
            		<li class="altlight"><a href=""><span style="color:<?php echo $dark; ?>;">MAP</span></a></li>
            		<li class="alt"><a href="">MODEL</a></li>
            		<li class="altlight"><a href=""><span style="color:<?php echo $dark; ?>;">TUTORIAL</span></a></li>
            		<li class="alt"><a href="">VIDEO</a></li>
            		<li class="altlight"><a href=""><span style="color:<?php echo $dark; ?>;">WALLPAPER</span></a></li>
            		<li class="alt"><a href="">WOW-GUIDES</a></li>
            	</ul>
            	<ul class="menu_body" id="nav5">
            		<li class="menu_head_forum"><a href="http://forum.saucy.se/"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav6">
            		<li class="menu_head_about_location"><a href="about.php"></a></li>
            		<li><a href="">Q&A</a></li>
            	</ul>
            	<ul class="menu_body" id="nav7">
            		<li class="menu_head_resources"><a href=""></a></li>
            		<li><a href="">USEFUL LINKS</a></li>
            		<li><a href=""><span style="color:<?php echo $dark; ?>;">ARCHIVE</span></a></li>
            	</ul>
            </div>
        </div>

        <?php include("message.html"); ?>

        <div id="panel_left">
            <div style="float: left">
                <?php include("infobox.html"); ?>
                <?php include("poll.html"); ?>
                <?php include("shoutbox.html"); ?>
            </div>
        </div>
        <div id="panel_right">
            <div style="float: right">
                <?php include("search.html"); ?>
            </div>
        </div>

        <div id="contentwrapper">       <?php /* TEXTEN I div id=text M�STE HA <br /> P� SLUTET F�R INTE TEXT SKA FINNAS MELLAN COMMENTS OCH READMORE L�NKARNA */ ?>
            <div id="content">
                <div id="title"><span class="big">Who am I?</span></div>
                <div id="spacer"></div>
                <div id="text" style="text-align: center;">

                    <div style="position: relative; width: 576px; height: auto; float: left;">
                    <img src="image/misc/profile.jpg" style="padding: 3px 9px 3px 0px; float:left;" align="left" hspace="0" vspace="0" width="576" height="134" />
                    </div>
                    
                    <div style="position: relative; width: 290px; float: right; z-index: 20;">
                    Ever since I played The Legend of Zelda on NES, I've been hooked to computers, technology and games. I have a strong passion for games and coding in general.
                    <br /><br />
                    I live in a small village in the southern part of Sweden. There's not much to do here and most companies does not exist here because of size of this village. However I have a dream to move a slightly larger city once I get a job and a proper education.
                    <br /><br />
                    I have never been an ''out-door'' person, so I spend much of my sparetime by my computer. Most people must think I'm crazy, but that's what I love to do, it's my hobby.
                    <br /><br />
                    But of course I love to do other things! Me and my friends have started a Paintball association for those who live in the southern parts of Sweden.
                    <br />
                    (Homepage <a href='http://www.frostapaintball.se/'>here</a>, it's in Swedish however.)
                    </div>

                    <div align="center" style="position: relative; margin-top: 250px;">
                        <span class="big">
                            Follow me<br />
                        </span>
                <div id="spacer" style="width: 60%"></div>

                    Here you can follow me, or even contact me. (Click on the icons)
                    
                        <div id="copyright" style="margin: 0px 0px -10px 0px; height: 32px; padding-bottom: 0px;">
                            <div id="social">
                                <a href="<?php echo $socialEmail; ?>"><div id="social_1"></div></a>
                                <a href="<?php echo $socialSteam; ?>"><div id="social_2"></div></a>
                                <a href="<?php echo $socialBattlenet; ?>"><div id="social_3"></div></a>
                                <a href="<?php echo $socialDeviantart; ?>"><div id="social_4"></div></a>
                                <a href="<?php echo $socialTwitter; ?>"><div id="social_5"></div></a>
                                <a href="<?php echo $socialFacebook; ?>"><div id="social_6"></div></a>
                                <a href="<?php echo $socialYoutube; ?>"><div id="social_7"></div></a>
                                <a href="<?php echo $socialLastfm; ?>"><div id="social_8"></div></a>
                                <a href="<?php echo $socialWowhead; ?>"><div id="social_9"></div></a>
                                <a href="<?php echo $socialSpotify; ?>"><div id="social_10"></div></a>
                                <a href="<?php echo $socialSkype; ?>"><div id="social_11"></div></a>
                            </div>
                        </div>

                    </div>

<br /><br /><br /><br />

                    <div align="center">
                        <span class="big">
                            Contact me<br />
                        </span>
                <div id="spacer" style="width: 60%"></div>
                    <br />

                        <div style="position: relative; left: -125px; width: 250px; text-align: center;">
                        <form style="margin-bottom:0;" name="contact" method="post" action="about_mail.php" onsubmit="return Validate();">
                			Name:<br />
                            <input name="name" type="text" title="Name" autocomplete="off" size="30" tabindex="1" /><br /><br />

                			E-mail:<br />
                			<input name="email" type="text" title="E-mail" autocomplete="off" size="30" tabindex="2" /><br /><br />

                            

                            

                        </div>
                        <div style="position: relative; left: 125px; margin-top: -92px; width: 250px; text-align: center;">

                			Phone number:<br />
                            <input name="phone" type="text" title="Phone Number" autocomplete="off" size="30" tabindex="3" /><br /><br />

                			How did you find me?:<br />
                			<input name="find" type="text" title="Find" autocomplete="off" size="30" tabindex="4" /><br /><br /> 
                         
                    </div>

                        <div style="position: relative; width: 500px; text-align: center;">

                			Subject:<br />
                			<input name="subject" type="text" title="Subject" autocomplete="off" size="60" tabindex="5" /><br /><br />   

                			Message:<br />
                			<textarea name="message" title="Message" style="width: 452px; height: 100;" tabindex="6"></textarea><br /><br />

                			<input type="submit" /> &nbsp; <input type="reset" onClick="return confirm('Are you sure you want to reset the form?')" /><br /><br />
            			</form>
                        </div>

                    </div>
                </div>
            </div>

            <?php require("copyright.html"); ?>

        </div>

    </div>

</center>


</body>
</html>