<?php
mb_internal_encoding("UTF-8");
setlocale(LC_ALL, "swedish");
date_default_timezone_set("Europe/Paris");

ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);

$saucy["Default"]["Version"]          = 2;
$saucy["Default"]["MetaCharset"]      = "UTF-8";
$saucy["Default"]["MetaAuthor"]       = "Saucy";
$saucy["Default"]["MetaGenerator"]    = "Sublime Text 2, Adobe Photoshop CS3";
$saucy["Default"]["MetaKeywords"]     = "SAUCY.SE, Saucy, saucyboy, Sås, zAce, Gamer, Geek, Geeky, Nerd, Nerdy, Brony, Personal, Website, Blog, Review, Tutorial, Portfolio, Download, Code, Config, Map, Model, Video, Wallpaper, Forum, About, Useful links, Contact, Ask me anything, Resources, Archive";
$saucy["Default"]["MetaDescription"]  = "SAUCY.SE - Saucy, the nerdy PC gaming brony who loves games, MLP:FiM, space, music, computers, art, web design and design overall!";
$saucy["Default"]["MetaCopyRight"]    = "SAUCY.SE, Saucy";
$saucy["Default"]["MetaRobots"]       = "ALL";
$saucy["Default"]["MetaImagetoolbar"] = "NO";
$saucy["Default"]["MetaIcon"]         = "image/icon/favicon.ico";

$saucy["Default"]["CSS"] = "css/body.php";

$saucy["Default"]["JS"]["jQuery"] = "https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js";
$saucy["Default"]["JS"]["Qtip"]   = "js/jquery.qtip.min.js";

$saucy["Default"]["Font"]                  = "Trebuchet MS";
$saucy["Default"]["FontHeight"]            = "150%";
$saucy["Default"]["Color"]["Dark"]         = "#141414";
$saucy["Default"]["Color"]["Light"]        = "#ebebeb";
$saucy["Default"]["Color"]["Link"]         = "#b35959";
$saucy["Default"]["Color"]["Link_visited"] = "#5c5c5c";
$saucy["Default"]["Color"]["Border"]       = "#2d2d2d";
$saucy["Default"]["Color"]["Darker"]       = "#0f0f0f";
$saucy["Default"]["Color"]["Black"]        = "#000000";

$saucy["Individual"]["Index"]["Locale"]["Title"] = "Site is under construction, it will be back soon!
- Saucy [i][18/02-2011][/i]";
$saucy["Individual"]["Index"]["Content"]         = "(I promise it will be fully functional this time!)
Here's the [urlcustom=old/,_self,,tooltip,old website]old website[/url] - Here's a preview of the [urlcustom=new/,_self,,tooltip,new website]new website[/url]

[urlcustom=mumble.php,_self,,tooltip,Mumble guide for HeadZ]Mumble guide for HeadZ[/url] - [urlcustom=text_capitalizer.php,_self,,tooltip,Text Capitalizer]Text Capitalizer[/url] - [urlcustom=word_counter.php,_self,,tooltip,Word Counter]Word Counter[/url] - [urlcustom=colors.php,_self,,tooltip,WoW Chat Colors]WoW Chat Colors[/url]
[urlcustom=ip.php,_self,,tooltip,Check your IP]Check your IP[/url] - [urlcustom=day9_newbielist.php,_self,,tooltip,Day9 Newbie Tuesday Video List]Day[9] Newbie Tuesday Video List[/url]";
