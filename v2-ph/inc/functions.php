<?php

function bbkod($text) {    
    $folder = "image/smilies";
	
    //Funktionen str_replace() drar igenom den valda texten och ers&auml;tter med respektive kommando. Sedan funktionen nl2br() l&auml;gger automatiskt in <br> vid varje enter slag. Och sist, trim funktionen tar bort onödiga mellanslag samt onödiga enter slag i slutet av texten.
    //$text = htmlentities($text);
    //$text = stripslashes($text);
    
    $text = str_replace("<", "&lt;", $text);
    $text = str_replace(">", "&gt", $text);
    $text = str_replace('"', "&quot;", $text);
	$text = str_replace("'", "&#39;", $text);
	$text = str_replace("©", "&copy;", $text);
	
	$text = preg_replace("#\[color=(.*?)\](.*?)\[/color\]#is", "<span style='color: $1;'>$2</span>", $text);
	$text = preg_replace("#\[size=(.*?)\](.*?)\[/size\]#is", "<span style='font-size: $1px;'>$2</span>", $text);
	$text = preg_replace("#\[title=(.*?)\](.*?)\[/title\]#is", "<span class='tooltip' title='$1' '>$2</span>", $text);
	
    $text = str_replace("[b]", "<b>", $text);
    $text = str_replace("[/b]", "</b>", $text);
    $text = str_replace("[i]", "<i>", $text);
    $text = str_replace("[/i]", "</i>", $text);
    $text = str_replace("[u]", "<u>", $text);
    $text = str_replace("[/u]", "</u>", $text);
    $text = str_replace("[s]", "<s>", $text);
    $text = str_replace("[/s]", "</s>", $text);
    $text = str_replace('[mail=', '<a href="mailto:', $text);
    $text = str_replace('[/mail]', '</a>', $text);
    $text = preg_replace("#\[img\](.*?)\[/img\]#is", "<a href='$1' target='_blank' class='tooltip tooltipSector tooltipArticle'' title='$1'><img src='$1' alt='' /></a>", $text);
	$text = preg_replace("#\[url=(.*?)\](.*?)\[/url\]#is", "<a href='$1' target='_blank' class='tooltip tooltipSector tooltipArticle' title='$1'>$2</a>", $text);
    $text = preg_replace("#\[urlcustom=(.*?)\,(.*?)\,(.*?)\,(.*?)\,(.*?)\](.*?)\[/url\]#is", "<a href='$1' target='$2' id='$3' class='$4' title='$5'>$6</a>", $text);
    $text = preg_replace("#\[quote=(.*?),(.*?)\](.*?)\[/quote\]#is", "<div class='quotewrapper'><div class='quotetitle'><a href='$2'>$1</a></div><div class='quotecontent'>$3</div></div>", $text);
    $text = preg_replace("#\[quote=(.*?)\](.*?)\[/quote\]#is", "<div class='quotewrapper'><div class='quotetitle'>$1</div><div class='quotecontent'>$2</div></div>", $text);
    $text = str_replace('[quote]', '<blockquote>', $text);
    $text = str_replace('[/quote]', '</blockquote>', $text);
    $text = str_replace('[br]', '<br />', $text);
    $text = str_replace('[width]', '" width="', $text);
    $text = str_replace('[height]', '" height="', $text);
    //$text = str_replace('[size=', '<font size="', $text);
    //$text = str_replace('[/size]', '</font>', $text);
    $text = preg_replace("#\[spoiler\](.*?)\[/spoiler\]#is", "<span style='background:#000000; color:#000000;'>$1</span>", $text);
    $text = preg_replace("#\[left\](.*?)\[/left\]#is", "<div style='text-align: left;'>$1</div>", $text);
    $text = preg_replace("#\[center\](.*?)\[/center\]#is", "<div align='center'>$1</div>", $text);
    $text = preg_replace("#\[right\](.*?)\[/right\]#is", "<div align='right'>$1</div>", $text);
    $text = preg_replace("#\[youtube\]http://www.youtube.com/watch?v=(.*?)\[/youtube\]#is", "<iframe title='YouTube video player' width='432' height='356' src='http://www.youtube.com/embed/$1' frameborder='0' style='margin-top: 5px;' allowfullscreen></iframe>", $text);
    
    $text = str_replace("[B]", "<b>", $text);
    $text = str_replace("[/B]", "</b>", $text);
    $text = str_replace("[I]", "<i>", $text);
    $text = str_replace("[/I]", "</i>", $text);
    $text = str_replace("[U]", "<u>", $text);
    $text = str_replace("[/U]", "</u>", $text);
    $text = str_replace("[S]", "<s>", $text);
    $text = str_replace("[/S]", "</s>", $text);
    $text = preg_replace("#\[IMG\](.*?)\[/IMG\]#is", "<a href='$1' target='_blank'><img align='left' src='$1' /></a>", $text);
    $text = preg_replace("#\[URL=(.*?)\](.*?)\[/URL\]#is", "<a href='$1' target='_blank' class='tooltip' title='$1'>$2</a>", $text);
    $text = preg_replace("#\[URLCUSTOM=(.*?)\,(.*?)\,(.*?)\,(.*?)\,(.*?)\](.*?)\[/URL\]#is", "<a href='$1' target='$2' id='$3' class='$4' title='$5'>$6</a>", $text);
    $text = preg_replace("#\[QUOTE=(.*?),(.*?)\](.*?)\[/QUOTE\]#is", "<div class='quotewrapper'><div class='quotetitle'><a href='$2'>$1</a></div><div class='quotecontent'>$3</div></div>", $text);
    $text = preg_replace("#\[QUOTE=(.*?)\](.*?)\[/QUOTE\]#is", "<div class='quotewrapper'><div class='quotetitle'>$1</div><div class='quotecontent'>$2</div></div>", $text);
    $text = str_replace('[QUOTE]', '<blockquote>', $text);
    $text = str_replace('[/QUOTE]', '</blockquote>', $text);
    $text = str_replace('[BR]', '<br />', $text);
    $text = str_replace('[WIDTH]', '" width="', $text);
    $text = str_replace('[HEIGTH]', '" heigth="', $text);
    //$text = str_replace('[SIZE=', '<font size="', $text);
    //$text = str_replace('[/SIZE]', '</font>', $text);
    $text = preg_replace("#\[LEFT\](.*?)\[/LEFT\]#is", "<div align='left'>$1</div>", $text);
    $text = preg_replace("#\[CENTER\](.*?)\[/CENTER\]#is", "<div align='center'>$1</div>", $text);
    $text = preg_replace("#\[RIGHT\](.*?)\[/RIGHT\]#is", "<div align='right'>$1</div>", $text);
    $text = preg_replace("#\[YOUTUBE\]http://www.youtube.com/watch?v=(.*?)\[/YOUTUBE\]#is", "<iframe title='YouTube video player' width='432' height='356' src='http://www.youtube.com/embed/$1' frameborder='0' style='margin-top: 5px;' allowfullscreen></iframe>", $text);
    
	$smilies = array(
	'^^',
	':|', ':-|', '=|',
	':O', ':-O', '=O',
	':S', ':-S', '=S',
	':)', ':-)', '=)',
	':(', ':-(', '=(',
	':D', ':-D', '=D',
	':P', ':-P', '=P',
	';)', ';-)',
	':X', ':-X',
	':@', ':-@',
	':$',
	'8)',
	':cry:',
	':mad:',
	':lol:',
	':eek:',
	':mrgreen:',
	':rolleyes:',
	);
    
	$img = array(
	'orc^^.gif',
	'icon_neutral.gif', 'icon_neutral.gif', 'icon_neutral.gif',
	'icon_surprised.gif', 'icon_surprised.gif', 'icon_surprised.gif',
	'icon_confused.gif', 'icon_confused.gif', 'icon_confused.gif',
	'icon_smile.gif', 'icon_smile.gif', 'icon_smile.gif',
	'icon_sad.gif', 'icon_sad.gif', 'icon_sad.gif',
	'icon_biggrin.gif', 'icon_biggrin.gif', 'icon_biggrin.gif',
	'icon_razz.gif', 'icon_razz.gif', 'icon_razz.gif',
	'icon_wink.gif', 'icon_wink.gif',
	'icon_evil.gif', 'icon_evil.gif',
	'icon_twisted.gif', 'icon_twisted.gif',
	'icon_redface.gif',
	'icon_cool.gif',
	'icon_cry.gif',
	'icon_mad.gif',
	'icon_lol.gif',
	'icon_eek.gif',
	'icon_mrgreen.gif',
	'icon_rolleyes.gif',
	);
	
	$text = ' '.$text.' ';
	$num_smilies = count($smilies);
	
	for ($smiles_index = 0; $smiles_index < $num_smilies; $smiles_index++) {
	
		$text = preg_replace("#(?<=.\W|\W.|^\W)".preg_quote($smilies[$smiles_index], '#')."(?=.\W|\W.|\W$)#m", '$1<img src="'.$folder.'/'.$img[$smiles_index].'" title="'.$smilies[$smiles_index].'" style="border: 0px;">$2', $text);

	}

	$text = nl2br($text);
	$text = trim($text);
	
	//Returnerar den genomgångna texten
	return $text;
}
