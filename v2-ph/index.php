<?php

require("inc/settings.php");
require("inc/functions.php");

?>
<!DOCTYPE html>
<html lang='sv'>
<head>
<title>.: Saucy.se v<?php echo $saucy["Default"]["Version"]; ?> :.</title>

<meta charset='<?php echo $saucy["Default"]["MetaCharset"]; ?>' />
<meta name='author' content='<?php echo $saucy["Default"]["MetaAuthor"]; ?>' />
<meta name='generator' content='<?php echo $saucy["Default"]["MetaGenerator"]; ?>' />
<meta name='keywords' content='<?php echo $saucy["Default"]["MetaKeywords"]; ?>' />
<meta name='description' content='<?php echo $saucy["Default"]["MetaDescription"]; ?>' />
<meta name='copyright' content='<?php echo $saucy["Default"]["MetaCopyRight"]; ?>' />
<meta name='robots' content='<?php echo $saucy["Default"]["MetaRobots"]; ?>' />
<meta http-equiv='imagetoolbar' content='<?php echo $saucy["Default"]["MetaImagetoolbar"]; ?>' />
<link rel="shortcut icon" href="<?php echo $saucy["Default"]["MetaIcon"]; ?>" type="image/x-icon" />

<?php

require($saucy["Default"]["CSS"]);

echo "<script src='".$saucy["Default"]["JS"]["jQuery"]."'></script>";
echo "<script src='".$saucy["Default"]["JS"]["Qtip"]."'></script>";

echo "<style>
body, html {
	height: 75%;
}
</style>";

?>

<script>
$(document).ready(function(){
	$(".tooltip").each(function() {
		$(this).qtip({
			content: {
				text: $(this).attr("title"),
			},
			position: {
				target: "mouse",
				adjust: { x: 15, y: 0, },
			},
			style: {
				tip: false
			},
			show: {
				delay: 0
			},
			hide: {
				delay: 200
			}
		});
	});
});
</script>

</head>
<body>

<?php

echo "<div id='indexContent' style='top: 75%;'>
	<div class='text'>
		<span class='huge'>
			".bbkod($saucy["Individual"]["Index"]["Locale"]["Title"])."
		</span>
	</div>
	<div class='text' style='margin-top: 10px;'>
		<span class='smalli'>
			".bbkod($saucy["Individual"]["Index"]["Content"])."
		</span>
	</div>
</div>";

?>

</body>
</html>