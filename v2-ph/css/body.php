<?php

echo "<style>

* {
    padding: 0px;
    margin: 0px;
}


body {
    background: ".$saucy["Default"]["Color"]["Dark"].";
    color: ".$saucy["Default"]["Color"]["Light"].";
    margin: 0px auto;
    font: 12px ".$saucy["Default"]["Font"].";
    line-height: ".$saucy["Default"]["FontHeight"].";
}

/* Content box used for the under construction/IP pages */
#indexContent {
    position: relative;
    margin: 0px auto;
    text-align: center;
    vertical-align: text-bottom;
    top: 100%;
}

.text {
    position: relative;
    text-align: center;
    padding: 0px 6px;
}

.huge {
    font: 18px ".$saucy["Default"]["Font"].";
    line-height: ".$saucy["Default"]["FontHeight"].";
}

.smalli {
    font: italic 10px ".$saucy["Default"]["Font"].";
    line-height: ".$saucy["Default"]["FontHeight"].";
}

a:link {
    color: ".$saucy["Default"]["Color"]["Link"].";
    line-height: ".$saucy["Default"]["FontHeight"].";
    text-decoration: none;
    cursor: pointer;
}
a:visited {
    color: ".$saucy["Default"]["Color"]["Link_visited"].";
    line-height: ".$saucy["Default"]["FontHeight"].";
    text-decoration: none;
    cursor: pointer;
}
a:hover, a:active {
    color: ".$saucy["Default"]["Color"]["Light"].";
    line-height: ".$saucy["Default"]["FontHeight"].";
    text-decoration: underline;
    cursor: pointer;
}

/* qTip (jQuery tooltip) style */
.ui-tooltip, .qtip {
 position: absolute;
 left: -28000px;
 top: -28000px;
 display: none;
 max-width: auto;
 min-width: auto;
 z-index: 15000;
}
.ui-tooltip-default .ui-tooltip-titlebar,
.ui-tooltip-default .ui-tooltip-content {
 border: 1px solid ".$saucy["Default"]["Color"]["Border"].";
 background-color: ".$saucy["Default"]["Color"]["Darker"].";
 color: ".$saucy["Default"]["Color"]["Light"].";
 outline: 1px solid ".$saucy["Default"]["Color"]["Black"].";
 outline-offset: 0px;
 padding: 3px 6px 4px 6px;
 font: 12px/16px ".$saucy["Default"]["Font"].";
}

</style>";

?>