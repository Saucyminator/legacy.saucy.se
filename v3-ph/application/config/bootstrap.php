<?php

namespace application\config;

////////////////////////////////////////
// MOVE ALL CONSTANTS TO constants.php !!!

////////////////////////////////////////
// applications/config/configuration.php maybe?
//
// mb_internal_encoding("UTF-8");
// setlocale(LC_ALL, "sv_SE");
// date_default_timezone_set("Europe/Paris");


////////////////////////////////////////
// Default
// required for localhost app_name to work
function folderSeparator($levels) {
	return str_repeat(DIRECTORY_SEPARATOR . '..', $levels);
}

$arrayLocal = array('localhost', '127.0.0.1', '192.168.1.86');
if(in_array($_SERVER['SERVER_NAME'], $arrayLocal)) {
	define('ROOT', 		realpath(dirname(__FILE__) . folderSeparator(3)));
	define('APP_ROOT', 	realpath(dirname(__FILE__) . folderSeparator(2)));
	
	define('APP_HOST', 	$_SERVER['SERVER_NAME']);
	define('APP_NAME', 	strtolower(str_replace(ROOT . DIRECTORY_SEPARATOR, '', APP_ROOT)));
	define('APP_URL', 	'http://' . APP_HOST . '/' . APP_NAME);
	define('APP_PATH', 	APP_NAME . '/application');
}
else {
	define('ROOT', 		$_SERVER['DOCUMENT_ROOT']);
	define('APP_ROOT', 	realpath(dirname(__FILE__) . folderSeparator(2)));

	define('APP_HOST', 	$_SERVER['SERVER_NAME']);
	define('APP_NAME', 	strtolower($_SERVER['SERVER_NAME']));
	define('APP_URL', 	'http://' . APP_NAME);
	define('APP_PATH', 	APP_NAME . '/application');
}


////////////////////////////////////////
// Custom
define('VERSION_1',   'http://saucy.se/v1');
define('VERSION_2',   'http://saucy.se/v2');
define('VERSION_2_1', 'http://saucy.se/v2.1');
define('VERSION_3',   'http://saucy.se/v3');

define('VERSION_1_DATABASE_TABLE', 'saucy_v1_');
define('VERSION_2_DATABASE_TABLE', 'saucy_v2_');
define('VERSION_3_DATABASE_TABLE', 'saucy_v3_');

define('ENVIRONMENT', 'development');
define('VERSION', '0.0.0');

if(defined('ENVIRONMENT')) {
	switch (ENVIRONMENT) {
		case 'development':
			ini_set('display_errors', 1);
			ini_set('log_errors', 1);
			error_reporting(E_ALL);
			break;
		case 'production':
			ini_set('display_errors', 0);
			ini_set('log_errors', 0);
			error_reporting(0);
			break;
		default:
			throw new Exception('Unknown application environment.', 123);
			exit;
	}
}


////////////////////////////////////////
// Testing
/*
<table class="table table-hover table-condensed table-bordered">
	<tbody>
		<tr>
			<td>ROOT</td>
			<td><?php echo ROOT; ?></td>
		</tr>
		<tr>
			<td>APP_ROOT</td>
			<td><?php echo APP_ROOT; ?></td>
		</tr>
		<tr>
			<td>APP_NAME</td>
			<td><?php echo APP_NAME; ?></td>
		</tr>
		<tr>
			<td>APP_URL</td>
			<td><?php echo APP_URL; ?></td>
		</tr>
		<tr>
			<td>APP_PATH</td>
			<td><?php echo APP_PATH; ?></td>
		</tr>
		<tr>
			<td>VERSION_1</td>
			<td><?php echo VERSION_1; ?></td>
		</tr>
		<tr>
			<td>VERSION_2</td>
			<td><?php echo VERSION_2; ?></td>
		</tr>
		<tr>
			<td>VERSION_2_1</td>
			<td><?php echo VERSION_2_1; ?></td>
		</tr>
		<tr>
			<td>VERSION_3</td>
			<td><?php echo VERSION_3; ?></td>
		</tr>
		<tr>
			<td>VERSION_1_DATABASE_TABLE</td>
			<td><?php echo VERSION_1_DATABASE_TABLE; ?></td>
		</tr>
		<tr>
			<td>VERSION_2_DATABASE_TABLE</td>
			<td><?php echo VERSION_2_DATABASE_TABLE; ?></td>
		</tr>
		<tr>
			<td>VERSION_3_DATABASE_TABLE</td>
			<td><?php echo VERSION_3_DATABASE_TABLE; ?></td>
		</tr>
		<tr>
			<td>ENVIRONMENT</td>
			<td><?php echo ENVIRONMENT; ?></td>
		</tr>
		<tr>
			<td>VERSION</td>
			<td><?php echo VERSION; ?></td>
		</tr>
	</tbody>
</table>
*/