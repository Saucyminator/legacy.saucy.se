<?php

namespace application;


require_once APP_ROOT . DIRECTORY_SEPARATOR . 'Quotes.php';

$quotes = new Quotes();

?>
<footer class="text-center">
	<h4>All versions of <a href="http://saucy.se">Saucy.se</a> so far:</h4>
	<ul class="unstyled inline">
		<li><a href="<?php echo VERSION_1; ?>">Version 1</a></li>
		<li><a href="<?php echo VERSION_2; ?>">Version 2</a></li>
		<li><a href="<?php echo VERSION_2_1; ?>">Version 2.1</a> (<span class="text-error">not really working</span>)</li>
		<li><a href="<?php echo VERSION_3; ?>">Version 3</a> (<span class="text-success">NEW</span>)</li>
	</ul>

	<h4>Custom scripts/pages I've made (mostly for selfish reasons):</h4>
	<ul class="unstyled inline">
		<li><a href="ip.php">Check your IP</a></li>
		<li><a href="day9_newbielist.php">Day[9] - Starcraft 2 - Newbie Tuesday Video List</a></li>
		<li><a href="uppercase_converter.php">UPPERCASE Converter</a></li>
		<li><a href="word_counter.php">Word Counter</a></li>
		<li><a href="colors.php">WoW - Chat Colors</a></li>
	</ul>

	<h4>Quote of the F5:</h4>
	<p>"<?php echo $quotes; ?>"</p>
</footer>