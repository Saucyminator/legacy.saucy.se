<?php

namespace application;


/**
 * [short ClassName description]
 *
 * [long ClassName description]
 *
 * @todo      If this file is moved, the $path will break. Find a way to better handle that (when v3 design is finished)
 * 
 * @version   0.0.2
 * @copyright Saucy.se 2013
 * @link      http://saucy.se Saucy.se
 * @author    Saucy <saucy@saucy.se>
 */
class Quotes {

	public function __constructor() {}

	public function __toString() {
		$file   = $this->file();
		$line   = $this->line($file);
		$random = $this->random($line);

		return $random;
	}

	protected function file() {
		$fileName = 'quotes.txt';
		$path     = 'resources/files/modules/quotes/';

		$fullPath = $path . $fileName;

		return file_get_contents($fullPath);
	}

	protected function line($file) {
		return explode(PHP_EOL, $file);
	}

	protected function random($line = 0) {
		$lines = count($line);

		return $line[rand(0, $lines - 1)];
	}
}
