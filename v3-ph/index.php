<?php

namespace application;


$bootstrap = str_replace('/', DIRECTORY_SEPARATOR, __DIR__ . '/application/config/bootstrap.php');
require_once $bootstrap;

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport"              content="width=device-width, initial-scale=1.0">
		<meta name="description"           content="Saucy. Gamer. Coder. Geek. Brony.">
		<meta name="author"                content="Saucy">
		<meta name="copyright"             content="Saucy - SAUCY.SE">
		<meta name="application-name"      content="Saucy.se">

		<title>Saucy.se</title>

		<link href="images/icon/favicon.ico"									rel="shortcut icon">
		<link href="http://fonts.googleapis.com/css?family=Ubuntu:400,700,400italic" 					rel="stylesheet" type="text/css">
		<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css"	rel="stylesheet">

		<style type="text/css">
			body {
				background: #141414;
				color: #EBEBEB;
				font: 100% Ubuntu, sans-serif;
				padding-top: 10em;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<header class="text-center">
				<h1>Website is under development.</h1>
				<p>- Saucy <em><del><time datetime="2011-02-18">18.02.2011</time></del> updated <time datetime="07.02.2013T20:20">07.02.2013</time></em></p>
			</header>

			<ul>
				<li>Being quite busy (and sometimes unfocused) means I have no time over for my personal website, however I'm in the <span class="text-success">information gathering and planning</span> phase for <a href="<?php echo VERSION_3; ?>">version 3</a> of my website.</li>
				<li>I want my website (and future ones) to use code styling standards that I desire.</li>
				<li>I still have some work to do with the planning stage of my website, as there's some important points I still have to think about/work on.</li>
				<li>I've regained the will and inspiration to take up the battle of designing my website completely from scratch and improving my coding style and code thinking (still need to practice more with that one!).</li>
				<li>I've found a number of 3rd-party libraries I want to learn and use for my website(s):
					<ul>
						<li><a href="http://twitter.github.com/bootstrap/" target="_blank">Bootstrap</a> (Currently running on this page!)</li>
						<li><a href="http://angularjs.org/" target="_blank">AngularJS</a> (<del><a href="http://emberjs.com/" target="_blank">Ember.js</a></del>, sorry fellas but I love Google)</li>
						<li><a href="http://lesscss.org/" target="_blank">LESS</a> (or <a href="http://sass-lang.com/" target="_blank">SASS</a>)</li>
						<li><a href="http://www.slimframework.com/news/version-2" target="_blank">Slim Framework 2</a></li>
						<li><a href="https://github.com/j4mie/paris" target="_blank">Paris</a></li>
						<li><a href="https://github.com/j4mie/idiorm"a target="_blank">Idiorm</a></li>
					</ul>
				</li>
			</ul>

			<?php require_once 'footer.php'; ?>
		</div>

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.5/angular.min.js"></script>
	</body>
</html>