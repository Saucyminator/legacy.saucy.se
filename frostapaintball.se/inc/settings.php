<?php
//$urlRefresh = "index.php";
//header("Refresh: 1; URL=\"" . $urlRefresh . "\""); // redirect in 1 second

/* HOME URL */
$saucyURL = "http://www.frostapaintball.se";

/* COLORS */
$light = "#030303";
$dark = "#c6c6c6";
$darker = "#070707";
$orange = "#c30000";

/*  font: <'font-style'>  || <'font-variant'>  || <'font-weight'>  || <'font-size'>  || / <'line-height'>  || <'font-family'>  */
$defaultfont = "normal normal normal 10px normal Tahoma, sans-serif;";

$logo = "image/logo.png";
$headzlogo = "image/headzlogo.png";
$bg = "image/main/bg.png";
$spacer = "image/main/spacer.png";

$siteWidth = 894;

$navWidth = 149;
$navHeight = 30;
$navAmount = 6;

$sidebarWidth = 192;

$contentWidth = 672;

$home = "image/nav/nyheter.png";
$home_hover = "image/nav/nyheter_hover.png";
$home_location = "image/nav/nyheter_location.png";
$home_location_hover = "image/nav/nyheter_location_hover.png";

$blog = "image/nav/forum.png";
$blog_hover = "image/nav/forum_hover.png";
$blog_location = "image/nav/forum_location.png";
$blog_location_hover = "image/nav/forum_location_hover.png";

$portfolio = "image/nav/information.png";
$portfolio_hover = "image/nav/information_hover.png";
$portfolio_location = "image/nav/information_location.png";
$portfolio_location_hover = "image/nav/information_location_hover.png";

$downloads = "image/nav/lag.png";
$downloads_hover = "image/nav/lag_hover.png";
$downloads_location = "image/nav/lag_location.png";
$downloads_location_hover = "image/nav/lag_location_hover.png";

$forum = "image/nav/kontakta.png";
$forum_hover = "image/nav/kontakta_hover.png";
$forum_location = "image/nav/kontakta_location.png";
$forum_location_hover = "image/nav/kontakta_location_hover.png";

$about = "image/nav/lankar.png";
$about_hover = "image/nav/lankar_hover.png";
$about_location = "image/nav/lankar_location.png";
$about_location_hover = "image/nav/lankar_location_hover.png";
?>