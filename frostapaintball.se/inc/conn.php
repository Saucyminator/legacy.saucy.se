<?php

// Byt ut mot dina inloggningsuppgifter och databas
$mysql_server   = 'localhost';
$mysql_database = 'database';
$mysql_user     = 'username';
$mysql_password = 'password';

$conn = new PDO(
  "mysql:host=$mysql_server;dbname=$mysql_database",
  $mysql_user,
  $mysql_password,
  [
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::MYSQL_ATTR_FOUND_ROWS   => true,
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
  ]
);

// En funktion att anv�ndas n�r magic_quotes_gpc inte �r satt. F�r att f�rhindra SQL-injections, eller i lidrigare fall MySQl-fel.
function db_escape ($post)
{
   if (is_string($post)) {
     if (get_magic_quotes_gpc()) {
        $post = stripslashes($post);
     }
     return $post;
   }

   foreach ($post as $key => $val) {
      $post[$key] = db_escape($val);
   }

   return $post;
}
