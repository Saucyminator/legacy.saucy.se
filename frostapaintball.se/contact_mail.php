<?php
session_start(); // Alltid �verst p� sidan

include("inc/settings.php");

//$urlRefresh = "index.php";
//header("Refresh: 1; URL=\"" . $urlRefresh . "\""); // redirect in 1 second

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Frosta Paintball &nbsp;&raquo;&nbsp; Kontakta oss</title>
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/nav.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<meta name="author" content="Patrik 'S�s' Holmberg" />
<meta name="generator" content="phpDesigner 7, Photoshop CS3" />
<meta name="keywords" content="FrostaPaintball.se, Frosta, FPL, FPL.se, Paintball, lag, f�rening, H��r, medlemmar, forum, nyheter, information, l�nkar, sk�ne, sverige" />
<meta name="description" content="Frostapaintball.se - Paintball f�rening i Sk�ne." />
<meta name="copyright" content="FROSTAPAINTBALL.SE - Patrik 'S�s' Holmberg" />
<meta http-equiv="imagetoolbar" content="no"/>
<link rel="icon" href="image/icon/favicon.ico" />

<?php require("css/body.html"); ?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18545916-1']);
  _gaq.push(['_setDomainName', '.frostapaintball.se']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>
<body>

<?php require("preloader.html"); ?>

<center>

    <div id="body">

        <a href="<?php echo $saucyURL; ?>"><div id="logowrapper"><div id="logo"></div></div></a>

        <div id="navwrapper" >
            <div id="nav">
            	<ul class="menu_body" id="nav1">
            		<li class="menu_head_home"><a href="index.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav2">
            		<li class="menu_head_blog"><a href="http://forum.frostapaintball.se/" target="_blank"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav3">
            		<li class="menu_head_portfolio"><a href="info.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav4">
            		<li class="menu_head_downloads"><a href="team.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav5">
            		<li class="menu_head_forum_location"><a href="contact.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav6">
            		<li class="menu_head_about"><a href="links.php"></a></li>
            	</ul>
            </div>
        </div>

        <div id="panel_left">
            <div style="float: left">

                <div id="sidebar">
                    <div id="title">
                        <span class="big">
                            Logga in
                        </span>
                    </div>
                    <div id="spacer"></div>
                    <div id="text" style="text-align: center;">

                        <?php

                        // Om inte inloggad visa formul�r, annars logga ut-l�nk
                        if (!isset($_SESSION['sess_user'])){

                        ?>
                        <form action="index.php" method="post">

                        Användarnamn:<br />
                        <input type="text" name="user" /><br />
                        Lösenord:<br />
                        <input type="password" name="passwd" /><br /><br />

                        <input type="submit" name="submit" value="Logga in" />
                        </form>
                        <?php

                          // Visa felmeddelande vid felaktig inloggning
                          if (isset($_GET['badlogin'])){
                            echo "Fel användarnamn eller lösenord!<br>\n";
                            echo "F�rs�k igen!\n";
                          }
                        } else {

                          echo "
                        V&auml;lkommen " . ucwords(strtolower($_SESSION['sess_user'])) . "<br />
                        <a href=\"index.php?addnews\">L�gg till en nyhet</a><br /><br />
                        <a href=\"index.php?logout\">Logga ut</a>
                        ";


                        }

                        ?>

                    </div>
                </div>

                <?php include("calendar.html"); ?>
                <?php include("sponsors.html"); ?>
                <?php include("facebook.html"); ?>
            </div>
        </div>

        <div id="contentwrapper">
            <div id="content">
                <div id="title">
                    <span class="big">
                        Kontakta oss
                    </span>
                </div>
                <div id="spacer"></div>
                <div id="text">

<?php

// anger en variabel som kan lagra de eventuella felaktigheterna
$errors = array();

// kontrollera om ett Namn angivits
if (!$_POST["name"])
$errors[] = "&raquo; Ditt namn";

// kontrollera om ett �mne angivits
if (!$_POST["phone"])
$errors[] = "&raquo; Ditt telefonnummer";

// kontrollera om en Epostadress angivits
$emailcheck = $_POST["email"];
if(!preg_match("/^[a-z0-9\�\�\�._-]+@[a-z0-9\�\�\�.-]+\.[a-z]{2,6}$/i", $emailcheck))
$errors[] = "&raquo; Din epostadress";

// kontrollera om ett Meddelande angivits
if (!$_POST["forumname"])
$errors[] = "&raquo; Ditt registrerade forumnamn";


//kontrollera om scriptet anropas fr�n ditt formul�r
if ($_SERVER['HTTP_REFERER']!= "http://www.frostapaintball.se/contact.php")
$errors[] = "&raquo; Din anv�ndning av v�rt formmail �r inte till�ten!";

// om felaktig information finns visas detta meddelande
if(count($errors)>0) {
echo "<b>F�ljande information m�ste anges innan du kan skicka formul�ret:</b><br /><br />";
foreach($errors as $fel)
echo "$fel<br/>";
echo "<br />Ange den information som saknas och skicka formul�ret igen. Tack!<br />";
echo "<a href='http://www.frostapaintball.se/contact.php' title='G� tillbaka' target='_self'>Klicka h�r f�r att komma tillbaka till formul�ret</a>";
} else {
// formul�ret �r korrekt ifyllt och informationen bearbetas
$to = "info@frostapaintball.se";
$name = $_POST["name"];
$phone = $_POST["phone"];
$email = $_POST["email"];
$forumname = $_POST["forumname"];
$info = $_POST["info"];
$message = "Namn: $name
Telefon: $phone
E-mail: $email
Registrerat forumnamn: $forumname
&Ouml;vrig info: $info";

	if(mail($to,"Ny medlemsans�kan: " . $forumname,$message,"From: $name <$email>")) {
		echo nl2br("Ditt meddelande har skickats!
		Namn: $name
        Telefon: $phone
        E-mail: $email
        Registrerat forumnamn: $forumname
        �vrig info: $info
		");
	} else {
		echo "Det gick inte att skicka ditt meddelande";
	}
}

?>

                </div>
            </div>
        </div>

    <?php include("copyright.html"); ?>

    </div>

</center>

</body>
</html>
