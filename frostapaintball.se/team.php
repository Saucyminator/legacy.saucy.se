<?php
session_start(); // Alltid �verst p� sidan
 
include "inc/conn.php"; // Databasanslutningen
include "inc/functions.php"; // Funktioner
include "inc/salt.php"; // Funktioner
include "inc/settings.php"; // Funktioner
 
// Inloggning
if (isset($_POST['submit'])){
 
  $_POST = db_escape($_POST);
 
  $passwd = safepass($_POST['passwd']);
  $sql = "SELECT membersID FROM members
         WHERE user='{$_POST['user']}'
         AND pass='$passwd'";
  $result = mysql_query($sql);
 
  // Hittades inte användarnamn och lösenord
  // skicka till formul�r med felmeddelande
  if (mysql_num_rows($result) == 0){
    header("Location: team.php?badlogin");
    exit;
  }
 
  // S�tt sessionen med unikt index
  $_SESSION['sess_id'] = mysql_result($result, 0, 'membersID');
  $_SESSION['sess_user'] = $_POST['user'];
  header("Location: team.php");
  exit;
}
 
// Utloggning
if (isset($_GET['logout'])){
  session_unset();
  session_destroy();
  header("Location: team.php");
  exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Frosta Paintball &nbsp;&raquo;&nbsp; Lag</title>
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/nav.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<meta name="author" content="Patrik 'S�s' Holmberg" />
<meta name="generator" content="phpDesigner 7, Photoshop CS3" />
<meta name="keywords" content="FrostaPaintball.se, Frosta, FPL, FPL.se, Paintball, lag, f�rening, H��r, medlemmar, forum, nyheter, information, l�nkar, sk�ne, sverige" />
<meta name="description" content="Frostapaintball.se - Paintball f�rening i Sk�ne." />
<meta name="copyright" content="FROSTAPAINTBALL.SE - Patrik 'S�s' Holmberg" />
<meta http-equiv="imagetoolbar" content="no"/>
<link rel="icon" href="image/icon/favicon.ico" />

<?php require("css/body.html"); ?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18545916-1']);
  _gaq.push(['_setDomainName', '.frostapaintball.se']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script language="JavaScript">
function checkFields() {
  missinginfo = "";
	if (document.bb.name.value == "") {
	missinginfo += "\n - F�rnamn";
	}
	if (document.bb.lastname.value == "") {
	missinginfo += "\n - Efternamn";
	}
	if (document.bb.user.value == "") {
	missinginfo += "\n - Nicknamn (Användarnamn)";
	}
	if (missinginfo != "") {
	missinginfo ="_____________________________\n" +
	"Du har inte fyllt i f�lten:\n" +
	missinginfo + "\n_____________________________" +
	"\nFyll i de angivna f�lten innan du l�gger till medlemmen!";
	alert(missinginfo);
	return false;
	}
	else return true;
}
</script>

</head>
<body>

<?php require("preloader.html"); ?>

<center>

    <div id="body">

        <a href="<?php echo $saucyURL; ?>"><div id="logowrapper"><div id="logo"></div></div></a>

        <div id="navwrapper" >
            <div id="nav">
            	<ul class="menu_body" id="nav1">
            		<li class="menu_head_home"><a href="index.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav2">
            		<li class="menu_head_blog"><a href="http://forum.frostapaintball.se/" target="_blank"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav3">
            		<li class="menu_head_portfolio"><a href="info.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav4">
            		<li class="menu_head_downloads_location"><a href="team.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav5">
            		<li class="menu_head_forum"><a href="contact.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav6">
            		<li class="menu_head_about"><a href="links.php"></a></li>
            	</ul>
            </div>
        </div>

        <div id="panel_left">
            <div style="float: left">

                <div id="sidebar">
                    <div id="title">
                        <span class="big">
                            Logga in
                        </span>
                    </div>
                    <div id="spacer"></div>
                    <div id="text" style="text-align: center;">
                
                        <?php
                         
                        // Om inte inloggad visa formul�r, annars logga ut-l�nk
                        if (!isset($_SESSION['sess_user'])){
                        
                        ?>
                        <form action="team.php" method="post">
                        
                        Användarnamn:<br />
                        <input type="text" name="user" /><br />
                        Lösenord:<br />
                        <input type="password" name="passwd" /><br /><br />
                        
                        <input type="submit" name="submit" value="Logga in" />
                        </form>
                        <?php
                        
                          // Visa felmeddelande vid felaktig inloggning
                          if (isset($_GET['badlogin'])){
                            echo "Fel användarnamn eller lösenord!<br>\n";
                            echo "F�rs�k igen!\n";
                          }
                        } else {
                         
                          echo "
                        V&auml;lkommen " . ucwords(strtolower($_SESSION['sess_user'])) . "<br />
                        <a href=\"index.php?addnews\">L�gg till en nyhet</a><br />
                        <a href=\"team.php?addmember\">L�gg till en medlem</a><br />
                        <a href=\"info.php?editinfo\">�ndra information</a><br />
                        <a href=\"links.php?editinfo\">�ndra l�nkar</a><br /><br />
                        <a href=\"team.php?logout\">Logga ut</a>
                        ";
                        
                         
                        }
                         
                        ?>
                
                    </div>
                </div>

                <?php include("calendar.html"); ?>
                <?php include("sponsors.html"); ?>
                <?php include("facebook.html"); ?>
            </div>
        </div>

        <div id="contentwrapper" style="min-height: 359px;">




<?php
// Skriv ut detta n�r man inte �r inloggad
if (!isset($_SESSION['sess_user'])){

echo        "<div id=\"content\">
                <div id=\"headzlogo\"></div>
                <div id=\"title\">
                    <span class=\"big\">
                        Rooster
                    </span>
                </div>
                <div id=\"spacer\"></div>
                <div id=\"text\">

                        <u>Team HeadZ</u><br /><br />";


$sql = "SELECT * FROM legacy_frostapaintball_members ORDER BY membersID";
$stmt = $conn->prepare($sql);
$stmt->execute();
while($row = $stmt->fetch()) {
$id = $row['membersID'];
$user = $row['user'];
$name = $row['name'];
$lastname = $row['lastname'];
$role = $row['role'];

if($user == 'Admin')
    $user = 'Sås';

echo "$role: $name \"$user\" $lastname <br />";
}

// Annars om man �r inloggad s� k�r detta
} else {

//Kod f�r att l�gga till genom ett foruml�r
if(isset($_GET['addmember'])) {
?>

<div id="content" style="text-align: left;">

<form action='team.php?addmember&add' onSubmit='return checkFields();' id='add' method='post' name='bb' style="margin-bottom:0;">

F�rnamn:<br />
<input name="name" title="F�rnamn" type="text" style="width: 50%;" autocomplete="on" tabindex="1" /> <br /><br />

Efternamn:<br />
<input name="lastname" title="Efternamn" type="text" style="width: 50%;" autocomplete="on" tabindex="2" /> <br /><br />

Nicknamn (Användarnamn):<br />
<input name="user" title="Nicknamn" type="text" style="width: 50%;" autocomplete="on" tabindex="3" /> <br /><br />

Roll:<br />
<select name="role">
<option value="Spelare"> Spelare </option>
<option value="Hj�lpkapten"> Hj�lpkapten </option>
<option value="Kapten"> Kapten </option>
</select><br /><br />

Lag:<br />
<select name="team">
<option value=""> - �r inte med ett lag - </option>
<option value="HeadZ"> HeadZ </option>
</select><br /><br /><br />


     
<input type="submit" title="L�gg till Medlem!" value="L�gg till Medlem!" />&nbsp;
<input type="reset" title="Rensa formul�ret!" value="Rensa" onclick="return confirm('�r du s�ker p� att du vill rensa?')" />&nbsp;
                <input type="button" value="G� tillbaka" onclick="history.go(-1)" />
</form>

</div>

<?php

//D� ?addnews=$id&add st�r i adressf�ltet kommer f�ljande kod att laddas
if(isset($_GET['add'])) {


$password = "frosta".$_POST['name'];

//SQL kod f�r att l�gga in i databasen
mysql_query("INSERT INTO members (user, pass, name, lastname, role, team) VALUES ('".$_POST['user']."', '$password', '".$_POST['name']."', '".$_POST['lastname']."', '".$_POST['role']."', '".$_POST['team']."')") or exit(mysql_error());

   //Skickar dig vidare till det inskrivna efter location:
   echo "<script type='text/javascript'>
   document.location.href = 'team.php';
    </script>"; 
  }
}

//Kod f�r att �ndra en nyhet med vald ID
if(isset($_GET['editmember'])) {

//Denna kod h�mtar ut informationen som vi ska �ndra p�
  $editmembers = mysql_query("SELECT * FROM members WHERE membersID=".$_GET['editmember']) or exit(mysql_error());
     $enyheter = mysql_fetch_array($editmembers);

//F�ljande kod �r ett foruml�r med dess respektive information i sig
?>

<div id="content" style="text-align: left;">

<form action="team.php?editmember=<?php echo $enyheter['membersID']; ?>&update" onSubmit="return checkFields();" id='edit' name="bb" method="post" style="margin-bottom:0;">

F�rnamn:<br />
<input name="name" title="F�rnamn" type="text" style="width: 50%;" value='<?php echo $enyheter['name']; ?>' autocomplete="on" tabindex="1" /><br /><br />

Efternamn:<br />
<input name="lastname" title="Efternamn" type="text" style="width: 50%;" value='<?php echo $enyheter['lastname']; ?>' autocomplete="on" tabindex="2" /><br /><br />

Nicknamn (Användarnamn):<br />
<input name="user" title="Nicknamn" type="text" style="width: 50%;" value='<?php echo $enyheter['user']; ?>' autocomplete="on" tabindex="3" /><br /><br />

Roll:<br />
<select name="role">
<option value="" <?php if($enyheter['role'] == ""){ echo "selected"; } ?>>- Ingen roll vald -</option>
<option value="Spelare" <?php if($enyheter['role'] == "Spelare"){ echo "selected"; } ?>> Spelare </option>
<option value="Hj�lpkapten" <?php if($enyheter['role'] == "Hj�lpkapten"){ echo "selected"; } ?>> Hj�lpkapten </option>
<option value="Kapten" <?php if($enyheter['role'] == "Kapten"){ echo "selected"; } ?>> Kapten </option>
</select><br /><br />

Lag:<br />
<select name="team">
<option value="" <?php if($enyheter['team'] == ""){ echo "selected"; } ?>>- Inget lag vald -</option>
<option value="HeadZ" <?php if($enyheter['team'] == "HeadZ"){ echo "selected"; } ?>> HeadZ </option>
</select><br /><br /><br />

     
<input type="submit" title="Redigera medlem!" value="Redigera Medlem!" />&nbsp;
<input type="button" value="G� tillbaka" onclick="history.go(-1)" />
</form>

</div>


<?php
//D� ?editnews=$id&update st�r i adressf�ltet kommer f�ljande kod att laddas
if(isset($_GET['update'])) {

	//SQL kod f�r att uppdatera en vald nyhet
	mysql_query("UPDATE members SET
		user = '".$_POST['user']."',
		name = '".$_POST['name']."',
		lastname = '".$_POST['lastname']."',
        role = '".$_POST['role']."',
        team = '".$_POST['team']."'
		WHERE membersID=".$_GET['editmember']) or exit(mysql_error());

	//Skickar dig vidare till det inskrivna efter location:
   echo "<script type='text/javascript'>
   document.location.href = 'team.php';
    </script>"; 
	}
}

//Kod f�r att ta bort en nyhet
if(isset($_GET['deletemember'])) {

//F�rst h�mtar vi information om den nyheten vi vill ta bort, vilket vi gjorde med denna kod 
   $qdelete = mysql_query("SELECT * FROM members WHERE membersID=".$_GET['deletemember']) or exit(mysql_error());
     $sdelete = mysql_fetch_array($qdelete);
        $id = $sdelete['membersID'];
        $user = $sdelete['user'];
        $name = $sdelete['name'];
        $lastname = $sdelete['lastname'];
     

//En bekr�ftningsfr�ga om du vill ta bort nyheten
echo "<div id=\"content\" style=\"text-align: center;\">
        �r du s�ker p� att du vill ta bort medlemmen: $name \"$user\" $lastname ?
        <br><br>
        <a href='team.php?deletemember=$id&delete'>Ja</a> | <a href='team.php' target='_self'>Nej</a>
    </div>";

//D� ?deletenews=$id&delete st�r i adressf�ltet kommer f�ljande kod att laddas
if(isset($_GET['delete'])) {

    mysql_query("DELETE FROM members WHERE membersID=".$_GET['deletemember']) or exit(mysql_error());
#	mysql_query("DELETE FROM kommentarer WHERE nid=".$_GET['deletenews']) or exit(mysql_error());

  //Skickar dig vidare till det inskrivna efter location:
   echo "<script type='text/javascript'>
   document.location.href = 'team.php';
    </script>";
 }
}

//Om QUERY_STRING �r tom laddas denna text, kolla under resurser och Server variablar f�r mer information
if(empty($_SERVER['QUERY_STRING'])) {

echo        "<div id=\"content\">
                <div id=\"headzlogo\"></div>
                <div id=\"title\">
                    <span class=\"big\">
                        Rooster
                    </span>
                </div>
                <div id=\"spacer\"></div>
                <div id=\"text\">

                        <u>Team HeadZ</u><br /><br />";

//H�r h�mtar vi ut all information om alla nyheter och loopar ut dem, dvs skriver ut alla nyheter efter varandra
$resultat2 = mysql_query("SELECT * FROM members ORDER BY membersID") or die (mysql_error());
while($row2 = mysql_fetch_array($resultat2)) {
$id = $row2['membersID'];
$user = $row2['user'];
$name = $row2['name'];
$lastname = $row2['lastname'];
$role = $row2['role'];


if($user == Admin)
    $user = S�s;


echo "<a href=\"team.php?editmember=$id\" title=\"Redigera medlem: #$id\">$role: $name \"$user\" $lastname</a> <a href=\"team.php?deletemember=$id\" title=\"Ta bort medlem: #$id\" style=\"color: $dark;\">[x]</a> <br />";
}
}
}
?>
                </div>
            </div>
        </div>

    <?php include("copyright.html"); ?>

    </div>

</center>

</body>
</html>