<?php
session_start(); // Alltid �verst p� sidan

include "inc/conn.php"; // Databasanslutningen
include "inc/functions.php"; // Funktioner
include "inc/salt.php"; // Funktioner
include "inc/settings.php"; // Funktioner

// Inloggning
if (isset($_POST['submit'])){

  $_POST = db_escape($_POST);

  $passwd = safepass($_POST['passwd']);
  $sql = "SELECT membersID FROM members
         WHERE user='{$_POST['user']}'
         AND pass='$passwd'";
  $result = mysql_query($sql);

  // Hittades inte användarnamn och lösenord
  // skicka till formul�r med felmeddelande
  if (mysql_num_rows($result) == 0){
    header("Location: index.php?badlogin");
    exit;
  }

  // S�tt sessionen med unikt index
  $_SESSION['sess_id'] = mysql_result($result, 0, 'id');
  $_SESSION['sess_user'] = $_POST['user'];
  header("Location: index.php");
  exit;
}

// Utloggning
if (isset($_GET['logout'])){
  session_unset();
  session_destroy();
  header("Location: index.php");
  exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Frosta Paintball &nbsp;&raquo;&nbsp; Nyheter </title>
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/nav.js"></script>
<script type="text/javascript" src="js/javascript.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<meta name="author" content="Patrik 'S�s' Holmberg" />
<meta name="generator" content="phpDesigner 7, Photoshop CS3" />
<meta name="keywords" content="FrostaPaintball.se, Frosta, FPL, FPL.se, Paintball, lag, f�rening, H��r, medlemmar, forum, nyheter, information, l�nkar, sk�ne, sverige" />
<meta name="description" content="Frostapaintball.se - Paintball f�rening i Sk�ne." />
<meta name="copyright" content="FROSTAPAINTBALL.SE - Patrik 'S�s' Holmberg" />
<meta http-equiv="imagetoolbar" content="no"/>
<link rel="icon" href="image/icon/favicon.ico" />

<?php require("css/body.html"); ?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18545916-1']);
  _gaq.push(['_setDomainName', '.frostapaintball.se']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script language="JavaScript">
function checkFields() {
  missinginfo = "";
	if (document.bb.title.value == "") {
	missinginfo += "\n - Rubrik";
	}
	if(document.bb.text.value == "") {
	missinginfo += "\n - Inneh�ll";
	}
	if (missinginfo != "") {
	missinginfo ="_____________________________\n" +
	"Du har inte fyllt i f�lten:\n" +
	missinginfo + "\n_____________________________" +
	"\nFyll i de angivna f�lten innan du l�gger in Nyheten!";
	alert(missinginfo);
	return false;
	}
	else return true;
}
</script>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3406958-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>

<style type="text/css">
.facebookcomments {
    color: white;
    background: orange;
}
</style>

</head>
<body xmlns:fb="http://www.facebook.com/2008/fbml">

<?php require("preloader.html"); ?>

<center>

    <div id="body">

        <a href="<?php echo $saucyURL; ?>"><div id="logowrapper"><div id="logo"></div></div></a>

        <div id="navwrapper" >
            <div id="nav">
            	<ul class="menu_body" id="nav1">
            		<li class="menu_head_home_location"><a href="index.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav2">
            		<li class="menu_head_blog"><a href="http://forum.frostapaintball.se/" target="_blank"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav3">
            		<li class="menu_head_portfolio"><a href="info.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav4">
            		<li class="menu_head_downloads"><a href="team.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav5">
            		<li class="menu_head_forum"><a href="contact.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav6">
            		<li class="menu_head_about"><a href="links.php"></a></li>
            	</ul>
            </div>
        </div>

        <div id="panel_left">
            <div style="float: left">

                <div id="sidebar">
                    <div id="title">
                        <span class="big">
                            Logga in
                        </span>
                    </div>
                    <div id="spacer"></div>
                    <div id="text" style="text-align: center;">

                        <?php

                        // Om inte inloggad visa formul�r, annars logga ut-l�nk
                        if (!isset($_SESSION['sess_user'])){

                        ?>
                        <form action="index.php" method="post">

                        Användarnamn:<br />
                        <input type="text" name="user" /><br />
                        Lösenord:<br />
                        <input type="password" name="passwd" /><br /><br />

                        <input type="submit" name="submit" value="Logga in" />
                        </form>
                        <?php

                          // Visa felmeddelande vid felaktig inloggning
                          if (isset($_GET['badlogin'])){
                            echo "Fel användarnamn eller lösenord!<br>\n";
                            echo "F�rs�k igen!\n";
                          }
                        } else {

                          echo "
                        V&auml;lkommen " . ucwords(strtolower($_SESSION['sess_user'])) . "<br />
                        <a href=\"index.php?addnews\">L�gg till en nyhet</a><br />
                        <a href=\"team.php?addmember\">L�gg till en medlem</a><br />
                        <a href=\"info.php?editinfo\">�ndra information</a><br />
                        <a href=\"links.php?editinfo\">�ndra l�nkar</a><br /><br />
                        <a href=\"index.php?logout\">Logga ut</a>
                        ";


                        }

                        ?>

                    </div>
                </div>

                <?php include("calendar.html"); ?>
                <?php include("sponsors.html"); ?>
                <?php include("facebook.html"); ?>
            </div>
        </div>

        <div id="contentwrapper">

<?php
// Skriv ut detta n�r man inte �r inloggad
if (!isset($_SESSION['sess_user'])){

    if (!isset($_GET['news'])) {
        $nyhet = 0;

        $sqlNews = "SELECT * FROM legacy_frostapaintball_news ORDER BY newsID DESC";
        $stmtNews = $conn->prepare($sqlNews);
        $stmtNews->execute();
        while($row = $stmtNews->fetch()) {
            $id = $row['newsID'];
            $title = $row['title'];
            $date = $row['date'];
            $text = $row['text'];
            $user = $row['user'];

            if($user == 'Admin')
                $user = 'Sås';

            echo        "<div id=\"content\">
                            <div id=\"title\">
                                <span class=\"big\">
                                    $title
                                </span>
                            </div>
                            <div id=\"spacer\"></div>
                            <div id=\"text\">
                                <div id=\"date\" style=\"margin-bottom: 10px;\">
                                    [ $date ]

                                    <div style=\"float: right;\">
                                        [ Skriven av: $user ]
                                    </div>
                                </div>

                                ".bbkod($text)."

                                <div id=\"date\" style=\"margin-top: 10px;\">
                                    [ <a href='index.php?news=$id' target='_self'>Kommentarer</a> ]
                                </div>
                            </div>
                        </div>";
            }




} else {
  $nyhet = intval($_GET['news']);
  /* Anv�nd intval() f�r att undvika s.k. SQL INJECTIONS,
     dvs. att folk kan typ radera din databas... */
}

$sqlNewsSingle = "SELECT * FROM legacy_frostapaintball_news WHERE newsID = $nyhet LIMIT 1";
$stmtNewsSingle = $conn->prepare($sqlNewsSingle);
$stmtNewsSingle->execute();
if($row3 = $stmtNewsSingle->fetch()) {
$id = $row3['newsID'];
$title = $row3['title'];
$date = $row3['date'];
$text = $row3['text'];
$user = $row3['user'];

            if($user == 'Admin')
                $user = 'Sås';

echo "
            <div id=\"content\">
                <div id=\"title\">
                    <span class=\"big\">
                        $title
                    </span>
                </div>
                <div id=\"spacer\"></div>
                <div id=\"text\">
                    <div id=\"date\" style=\"margin-bottom: 10px;\">
                        [ $date ]

                        <div style=\"float: right;\">
                            [ Skriven av: $user ]
                        </div>
                    </div>

                    ".bbkod($text)."

                    <div id=\"date\" style=\"margin-top: 10px;\">
                        [ <a href=\"#\" onclick=\"history.back(); return false;\">G� tillbaka</a> ]
                    </div>
                </div>
            </div>


<script src=\"http://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php\" type=\"text/javascript\"></script>

<fb:comments width=\"672px\"></fb:comments>

<script type=\"text/javascript\">
FB.init(\"12d7adc1b4d4baefa0dbc0fdd1693e74\", \"inc/xd_receiver.htm\");
</script>




";
}


// Annars om man �r inloggad s� k�r detta
} else {

//Kod f�r att l�gga till genom ett foruml�r
if(isset($_GET['addnews'])) {
?>

<div id="content" style="text-align: left;">

				Rubrik: <br />
				<form action='index.php?addnews&add' onSubmit='return checkFields();' id='add' method='post' name='bb' style="margin-bottom:0;">
				<input name="title" title="Rubrik" type="text" style="width: 50%;" autocomplete="on" tabindex="1" /> <br />

				<script type="text/javascript" src="js/editor.js"></script>

					<a href="#" onclick="insert_text('^^', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/orc%5E%5E.gif" alt="^^" title="Happy Orc" height="22" hspace="2" vspace="2" width="23" border="0"></a>
					<a href="#" onclick="insert_text(':D', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_biggrin.gif" alt=":D" title="Very Happy" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_smile.gif" alt=":)" title="Smile" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':lol:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_lol.gif" alt=":lol:" title="Laughing" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':P', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_razz.gif" alt=":P" title="Razz" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':mrgreen:', true); return false;" style="line-height: 20px;">	<img src="forum/images/smilies/icon_mrgreen.gif" alt=":mrgreen:" title="Mr. Green" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(';)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_wink.gif" alt=";)" title="Wink" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':S', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_confused.gif" alt=":S" title="Confused" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':(', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_sad.gif" alt=":(" title="Sad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':cry:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_cry.gif" alt=":cry:" title="Neutral" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':|', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_neutral.gif" alt=":|" title="Neutral" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':$', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_redface.gif" alt=":$" title="Embarassed" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text('8)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_cool.gif" alt="8)" title="Cool" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':O', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_surprised.gif" alt=":O" title="Surprised" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':eek:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_eek.gif" alt=":eek:" title="Shocked" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':rolleyes:', true); return false;" style="line-height: 20px;"><img src="forum/images/smilies/icon_rolleyes.gif" alt=":rolleyes:" title="Rolling Eyes" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':mad:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_mad.gif" alt=":mad:" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':X', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_evil.gif" alt=":X" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':@', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_twisted.gif" alt=":@" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
                <br />
				<input title="Fet text: [b]text[/b]" style="font-weight: bold;" type="button" name="bold"
				onclick="javascript:addtag('b');" value="b" />

				<input title="Kursiv text: [i]text[/i]" style="font-style: italic;" type="button" name="italic"
				onclick="javascript:addtag('i');" value="i" />

				<input title="Understruken text: [u]text[/u]" style="text-decoration: underline;" type="button" name="inserted"
				onclick="javascript:addtag('u');" value="u" />
<!--
				<input title="!!OBS, NAMNET M�STE VARA MED!! Citera text: [quote=namnet]text[/quote]" type="button" name="blockquote"
				onclick="javascript:addtag('quote');" value="Quote" />
-->
				<input title="Infoga l�nk: [url=http://url]L�nktext[/url]" type="button" name="link"
				onclick="javascript:addtag('url');" value="URL" /><br /><br />
<!--
				<input title="!!ENDAST TILL FILER P� HEMSIDAN!! Infoga l�nk: [url2=http://url]L�nktext[/url2]" style="text-decoration: underline;" type="button" name="link2"
				onclick="javascript:addtag('url2');" value="URL(intern)" />

				<input ctitle="Infoga bild: [img]http://bild_url.jpg[/img]" type="button" name="image"
				onclick="javascript:addtag('img');" value="Img" />
-->

				Inneh�ll:<br />
				<textarea name="text" onclick="storeCaret(this);" title="Text" style="width: 100%; height: 200px;" tabindex="2"></textarea><br /><br /><br />

				<input type="submit" title="L�gg till Nyheten!" value="L�gg till Nyheten!" />&nbsp;
				<input type="reset" title="Rensa formul�ret!" value="Rensa" onclick="return confirm('�r du s�ker p� att du vill rensa?')" />&nbsp;
                <input type="button" value="G� tillbaka" onclick="history.go(-1)" />
				</form>

</div>

<?php

//D� ?addnews=$id&add st�r i adressf�ltet kommer f�ljande kod att laddas
if(isset($_GET['add'])) {

//L�gger in dagens datum i variabeln $datum som ex 1/9-04
$date = date('H:i @ d/m-y'); // 00:05 @ 05/03-07

//SQL kod f�r att l�gga in i databasen
mysql_query("INSERT INTO news (title, date, text, user) VALUES ('".$_POST['title']."', '$date', '".$_POST['text']."', '".ucwords(strtolower($_SESSION['sess_user']))."')") or exit(mysql_error());

   //Skickar dig vidare till det inskrivna efter location:
   echo "<script type='text/javascript'>
   document.location.href = 'index.php';
    </script>";
  }
}

//Kod f�r att �ndra en nyhet med vald ID
if(isset($_GET['editnews'])) {

//Denna kod h�mtar ut informationen som vi ska �ndra p�
  $editnyheter = mysql_query("SELECT * FROM news WHERE newsID=".$_GET['editnews']) or exit(mysql_error());
     $enyheter = mysql_fetch_array($editnyheter);

//F�ljande kod �r ett foruml�r med dess respektive information i sig
?>

<div id="content" style="text-align: left;">

				Rubrik:
				<form action="index.php?editnews=<?php echo $enyheter['newsID']; ?>&update" onSubmit="return checkFields();" id='edit' name="bb" method="post" style="margin-bottom:0;">
				<input name="title" type="text" title="Rubrik" value='<?php echo $enyheter['title']; ?>' style="width: 50%;" autocomplete="on" /><br />

				<script type="text/javascript" src="js/editor.js"></script>

					<a href="#" onclick="insert_text('^^', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/orc%5E%5E.gif" alt="^^" title="Happy Orc" height="22" hspace="2" vspace="2" width="23" border="0"></a>
					<a href="#" onclick="insert_text(':D', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_biggrin.gif" alt=":D" title="Very Happy" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_smile.gif" alt=":)" title="Smile" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':lol:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_lol.gif" alt=":lol:" title="Laughing" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':P', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_razz.gif" alt=":P" title="Razz" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':mrgreen:', true); return false;" style="line-height: 20px;">	<img src="forum/images/smilies/icon_mrgreen.gif" alt=":mrgreen:" title="Mr. Green" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(';)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_wink.gif" alt=";)" title="Wink" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':S', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_confused.gif" alt=":S" title="Confused" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':(', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_sad.gif" alt=":(" title="Sad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':cry:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_cry.gif" alt=":cry:" title="Neutral" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':|', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_neutral.gif" alt=":|" title="Neutral" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':$', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_redface.gif" alt=":$" title="Embarassed" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text('8)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_cool.gif" alt="8)" title="Cool" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':O', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_surprised.gif" alt=":O" title="Surprised" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':eek:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_eek.gif" alt=":eek:" title="Shocked" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':rolleyes:', true); return false;" style="line-height: 20px;"><img src="forum/images/smilies/icon_rolleyes.gif" alt=":rolleyes:" title="Rolling Eyes" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':mad:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_mad.gif" alt=":mad:" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':X', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_evil.gif" alt=":X" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':@', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_twisted.gif" alt=":@" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
                <br />
				<input title="Fet text: [b]text[/b]" style="font-weight: bold;" type="button" name="bold"
				onclick="javascript:addtag('b');" value="b" />

				<input title="Kursiv text: [i]text[/i]" style="font-style: italic;" type="button" name="italic"
				onclick="javascript:addtag('i');" value="i" />

				<input title="Understruken text: [u]text[/u]" style="text-decoration: underline;" type="button" name="inserted"
				onclick="javascript:addtag('u');" value="u" />
<!--
				<input title="!!OBS, NAMNET M�STE VARA MED!! Citera text: [quote=namnet]text[/quote]" type="button" name="blockquote"
				onclick="javascript:addtag('quote');" value="Quote" />
-->
				<input title="Infoga l�nk: [url=http://url]L�nktext[/url]" type="button" name="link"
				onclick="javascript:addtag('url');" value="URL" /><br /><br />
<!--
				<input title="!!ENDAST TILL FILER P� HEMSIDAN!! Infoga l�nk: [url2=http://url]L�nktext[/url2]" style="text-decoration: underline;" type="button" name="link2"
				onclick="javascript:addtag('url2');" value="URL(intern)" />

				<input ctitle="Infoga bild: [img]http://bild_url.jpg[/img]" type="button" name="image"
				onclick="javascript:addtag('img');" value="Img" />
-->

				Inneh�ll:
				<textarea name="text" title="Text" style="width: 100%; height: 200px;"><?php echo $enyheter['text']; ?></textarea><br /><br /><br />

				<input type="submit" title="Redigera Nyhet!" value="Redigera Nyhet!" />&nbsp;
                <input type="button" value="G� tillbaka" onclick="history.go(-1)" />
				</form>

</div>

<?php
//D� ?editnews=$id&update st�r i adressf�ltet kommer f�ljande kod att laddas
if(isset($_GET['update'])) {

	//SQL kod f�r att uppdatera en vald nyhet
	mysql_query("UPDATE news SET
		title = '".$_POST['title']."',
		text = '".$_POST['text']."'
		WHERE newsID=".$_GET['editnews']) or exit(mysql_error());

	//Skickar dig vidare till det inskrivna efter location:
   echo "<script type='text/javascript'>
   document.location.href = 'index.php';
    </script>";
	}
}

//Kod f�r att ta bort en nyhet
if(isset($_GET['deletenews'])) {

//F�rst h�mtar vi information om den nyheten vi vill ta bort, vilket vi gjorde med denna kod
   $qdelete = mysql_query("SELECT * FROM news WHERE newsID=".$_GET['deletenews']) or exit(mysql_error());
     $sdelete = mysql_fetch_array($qdelete);
     $id = $sdelete['newsID'];
     $title = $sdelete['title'];


//En bekr�ftningsfr�ga om du vill ta bort nyheten
echo "<div id=\"content\" style=\"text-align: center;\">
        �r du s�ker p� att du vill ta bort nyheten: $title?
        <br><br>
        <a href='index.php?deletenews=$id&delete'>Ja</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href='index.php' target='_self'>Nej</a>
    </div>";

//D� ?deletenews=$id&delete st�r i adressf�ltet kommer f�ljande kod att laddas
if(isset($_GET['delete'])) {

    mysql_query("DELETE FROM news WHERE newsID=".$_GET['deletenews']) or exit(mysql_error());
#	mysql_query("DELETE FROM kommentarer WHERE nid=".$_GET['deletenews']) or exit(mysql_error());

  //Skickar dig vidare till det inskrivna efter location:
   echo "<script type='text/javascript'>
   document.location.href = 'index.php';
    </script>";
 }
}

//Om QUERY_STRING �r tom laddas denna text, kolla under resurser och Server variablar f�r mer information
if(empty($_SERVER['QUERY_STRING'])) {

//H�r h�mtar vi ut all information om alla nyheter och loopar ut dem, dvs skriver ut alla nyheter efter varandra
$resultat2 = mysql_query("SELECT * FROM news ORDER BY newsID DESC") or die (mysql_error());
while($row2 = mysql_fetch_array($resultat2)) {
$id = $row2['newsID'];
$title = $row2['title'];
$date = $row2['date'];
$text = $row2['text'];
$user = $row2['user'];

if($user == Admin)
    $user = S�s;

echo "
            <div id=\"content\">
                <div id=\"title\">
                    <span class=\"big\">
                        <a href=\"index.php?editnews=$id\" title=\"Redigera nyhet: #$id\">$title</a> ($user)<br />
                    </span>
                    <div align=\"right\" style=\"margin: -16px 0px -2px 0px;\"><a href=\"index.php?deletenews=$id\" title=\"Ta bort nyhet: #$id\"><img src='image/icon/icon_admin_remove.gif' border='0' width='18' height='16' vspace='0' /></a></div>
                </div>
                <div id=\"spacer\"></div>
                <div id=\"text\">
                    <div id=\"date\" style=\"margin-bottom: 10px;\">
                        [ $date ]

                        <div style=\"float: right;\">
                            [ Skriven av: $user ]
                        </div>
                    </div>

                    ".bbkod($text)."

                </div>
            </div>
";

}

}




}
?>

        </div>

    <?php include("copyright.html"); ?>

    </div>

</center>

</body>
</html>
