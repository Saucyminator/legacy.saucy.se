<?php
session_start(); // Alltid �verst p� sidan
 
include "inc/conn.php"; // Databasanslutningen
include "inc/functions.php"; // Funktioner
include "inc/salt.php"; // Funktioner
include "inc/settings.php"; // Funktioner
 
// Inloggning
if (isset($_POST['submit'])){
 
  $_POST = db_escape($_POST);
 
  $passwd = safepass($_POST['passwd']);
  $sql = "SELECT membersID FROM members
         WHERE user='{$_POST['user']}'
         AND pass='$passwd'";
  $result = mysql_query($sql);
 
  // Hittades inte användarnamn och lösenord
  // skicka till formul�r med felmeddelande
  if (mysql_num_rows($result) == 0){
    header("Location: info.php?badlogin");
    exit;
  }
 
  // S�tt sessionen med unikt index
  $_SESSION['sess_id'] = mysql_result($result, 0, 'membersID');
  $_SESSION['sess_user'] = $_POST['user'];
  header("Location: info.php");
  exit;
}
 
// Utloggning
if (isset($_GET['logout'])){
  session_unset();
  session_destroy();
  header("Location: info.php");
  exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Frosta Paintball &nbsp;&raquo;&nbsp; Information</title>
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/nav.js"></script>
<script type="text/javascript" src="js/javascript.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<meta name="author" content="Patrik 'S�s' Holmberg" />
<meta name="generator" content="phpDesigner 7, Photoshop CS3" />
<meta name="keywords" content="FrostaPaintball.se, Frosta, FPL, FPL.se, Paintball, lag, f�rening, H��r, medlemmar, forum, nyheter, information, l�nkar, sk�ne, sverige" />
<meta name="description" content="Frostapaintball.se - Paintball f�rening i Sk�ne." />
<meta name="copyright" content="FROSTAPAINTBALL.SE - Patrik 'S�s' Holmberg" />
<meta http-equiv="imagetoolbar" content="no"/>
<link rel="icon" href="image/icon/favicon.ico" />

<?php require("css/body.html"); ?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18545916-1']);
  _gaq.push(['_setDomainName', '.frostapaintball.se']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>
<body>

<?php require("preloader.html"); ?>

<center>

    <div id="body">

        <a href="<?php echo $saucyURL; ?>"><div id="logowrapper"><div id="logo"></div></div></a>

        <div id="navwrapper" >
            <div id="nav">
            	<ul class="menu_body" id="nav1">
            		<li class="menu_head_home"><a href="index.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav2">
            		<li class="menu_head_blog"><a href="http://forum.frostapaintball.se/" target="_blank"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav3">
            		<li class="menu_head_portfolio_location"><a href="info.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav4">
            		<li class="menu_head_downloads"><a href="team.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav5">
            		<li class="menu_head_forum"><a href="contact.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav6">
            		<li class="menu_head_about"><a href="links.php"></a></li>
            	</ul>
            </div>
        </div>

        <div id="panel_left">
            <div style="float: left">

                <div id="sidebar">
                    <div id="title">
                        <span class="big">
                            Logga in
                        </span>
                    </div>
                    <div id="spacer"></div>
                    <div id="text" style="text-align: center;">
                
                        <?php
                         
                        // Om inte inloggad visa formul�r, annars logga ut-l�nk
                        if (!isset($_SESSION['sess_user'])){
                        
                        ?>
                        <form action="info.php" method="post">
                        
                        Användarnamn:<br />
                        <input type="text" name="user" /><br />
                        Lösenord:<br />
                        <input type="password" name="passwd" /><br /><br />
                        
                        <input type="submit" name="submit" value="Logga in" />
                        </form>
                        <?php
                        
                          // Visa felmeddelande vid felaktig inloggning
                          if (isset($_GET['badlogin'])){
                            echo "Fel användarnamn eller lösenord!<br>\n";
                            echo "F�rs�k igen!\n";
                          }
                        } else {
                         
                          echo "
                        V&auml;lkommen " . ucwords(strtolower($_SESSION['sess_user'])) . "<br />
                        <a href=\"index.php?addnews\">L�gg till en nyhet</a><br />
                        <a href=\"team.php?addmember\">L�gg till en medlem</a><br />
                        <a href=\"info.php?editinfo\">�ndra information</a><br />
                        <a href=\"links.php?editinfo\">�ndra l�nkar</a><br /><br />
                        <a href=\"info.php?logout\">Logga ut</a>
                        ";
                        
                         
                        }
                         
                        ?>
                
                    </div>
                </div>

                <?php include("calendar.html"); ?>
                <?php include("sponsors.html"); ?>
                <?php include("facebook.html"); ?>
            </div>
        </div>

        <div id="contentwrapper">

<?php
// Skriv ut detta n�r man inte �r inloggad
if (!isset($_SESSION['sess_user'])){

echo        "<div id=\"content\">
                <div id=\"title\">
                    <span class=\"big\">
                        Information
                    </span>
                </div>
                <div id=\"spacer\"></div>
                <div id=\"text\">";

$sql = "SELECT * FROM legacy_frostapaintball_info WHERE infoID = 1";
$stmt = $conn->prepare($sql);
$stmt->execute();
while($row = $stmt->fetch()) {
$text = $row['text'];

echo bbkod($text);

}

// Annars om man �r inloggad s� k�r detta
} else {

//Kod f�r att �ndra en nyhet med vald ID
if(isset($_GET['editinfo'])) {

//Denna kod h�mtar ut informationen som vi ska �ndra p�
  $editinfo = mysql_query("SELECT * FROM info WHERE infoID = 1") or exit(mysql_error());
     $enyheter = mysql_fetch_array($editinfo);

//F�ljande kod �r ett foruml�r med dess respektive information i sig
?>

<div id="content" style="text-align: left;">

				<form action="info.php?editinfo=<?php echo $enyheter['infoID']; ?>&update" onsubmit="return checkFields();" id='edit' name="bb" method="post" style="margin-bottom:0;">
                
				<script type="text/javascript" src="js/editor.js"></script>

					<a href="#" onclick="insert_text('^^', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/orc%5E%5E.gif" alt="^^" title="Happy Orc" height="22" hspace="2" vspace="2" width="23" border="0"></a>
					<a href="#" onclick="insert_text(':D', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_biggrin.gif" alt=":D" title="Very Happy" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_smile.gif" alt=":)" title="Smile" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':lol:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_lol.gif" alt=":lol:" title="Laughing" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':P', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_razz.gif" alt=":P" title="Razz" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':mrgreen:', true); return false;" style="line-height: 20px;">	<img src="forum/images/smilies/icon_mrgreen.gif" alt=":mrgreen:" title="Mr. Green" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(';)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_wink.gif" alt=";)" title="Wink" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':S', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_confused.gif" alt=":S" title="Confused" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':(', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_sad.gif" alt=":(" title="Sad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':cry:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_cry.gif" alt=":cry:" title="Neutral" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':|', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_neutral.gif" alt=":|" title="Neutral" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':$', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_redface.gif" alt=":$" title="Embarassed" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text('8)', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_cool.gif" alt="8)" title="Cool" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':O', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_surprised.gif" alt=":O" title="Surprised" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':eek:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_eek.gif" alt=":eek:" title="Shocked" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':rolleyes:', true); return false;" style="line-height: 20px;"><img src="forum/images/smilies/icon_rolleyes.gif" alt=":rolleyes:" title="Rolling Eyes" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':mad:', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_mad.gif" alt=":mad:" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':X', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_evil.gif" alt=":X" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
					<a href="#" onclick="insert_text(':@', true); return false;" style="line-height: 20px;">		<img src="forum/images/smilies/icon_twisted.gif" alt=":@" title="Mad" height="15" hspace="2" vspace="2" width="15" border="0"></a>
                <br />
				<input title="Fet text: [b]text[/b]" style="font-weight: bold;" type="button" name="bold" 
				onclick="javascript:addtag('b');" value="b" />

				<input title="Kursiv text: [i]text[/i]" style="font-style: italic;" type="button" name="italic" 
				onclick="javascript:addtag('i');" value="i" />

				<input title="Understruken text: [u]text[/u]" style="text-decoration: underline;" type="button" name="inserted" 
				onclick="javascript:addtag('u');" value="u" />
<!--
				<input title="!!OBS, NAMNET M�STE VARA MED!! Citera text: [quote=namnet]text[/quote]" type="button" name="blockquote" 
				onclick="javascript:addtag('quote');" value="Quote" />
-->
				<input title="Infoga l�nk: [url=http://url]L�nktext[/url]" type="button" name="link" 
				onclick="javascript:addtag('url');" value="URL" /><br /><br />
<!--
				<input title="!!ENDAST TILL FILER P� HEMSIDAN!! Infoga l�nk: [url2=http://url]L�nktext[/url2]" style="text-decoration: underline;" type="button" name="link2" 
				onclick="javascript:addtag('url2');" value="URL(intern)" />

				<input ctitle="Infoga bild: [img]http://bild_url.jpg[/img]" type="button" name="image" 
				onclick="javascript:addtag('img');" value="Img" />
-->

				Inneh�ll:
				<textarea name="text" title="Text" style="width: 100%; height: 200px;"><?php echo $enyheter['text']; ?></textarea><br /><br /><br />

				<input type="submit" title="Redigera Information!" value="Redigera Information!" />&nbsp;
                <input type="button" value="G� tillbaka" onclick="history.go(-1)" />
				</form>

</div>


<?php
//D� ?editnews=$id&update st�r i adressf�ltet kommer f�ljande kod att laddas
if(isset($_GET['update'])) {

	//SQL kod f�r att uppdatera en vald nyhet
	mysql_query("UPDATE info SET
		text = '".$_POST['text']."'
		WHERE infoID=".$_GET['editinfo']) or exit(mysql_error());

	//Skickar dig vidare till det inskrivna efter location:
   echo "<script type='text/javascript'>
   document.location.href = 'info.php';
    </script>"; 
	}
}


//Om QUERY_STRING �r tom laddas denna text, kolla under resurser och Server variablar f�r mer information
if(empty($_SERVER['QUERY_STRING'])) {

echo        "<div id=\"content\">
                <div id=\"title\">
                    <span class=\"big\">
                        Information
                    </span>
                </div>
                <div id=\"spacer\"></div>
                <div id=\"text\">";

//H�r h�mtar vi ut all information om alla nyheter och loopar ut dem, dvs skriver ut alla nyheter efter varandra
$resultat3 = mysql_query("SELECT * FROM info WHERE infoID = 1") or die (mysql_error());
while($row3 = mysql_fetch_array($resultat3)) {
$text = $row3['text'];

echo bbkod($text);

}

}
}
?>

                </div>
            </div>
        </div>

    <?php include("copyright.html"); ?>

    </div>

</center>

</body>
</html>