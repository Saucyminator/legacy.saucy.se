<?php
session_start(); // Alltid �verst p� sidan

// Kolla om inloggad = sessionen satt
if (!isset($_SESSION['sess_user'])){
  header("Location: index.php");
  exit;
}

include "inc/conn.php"; // Databasanslutningen
include "inc/functions.php"; // Funktioner
include "inc/salt.php"; // Funktioner
include "inc/settings.php"; // Funktioner

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Frosta Paintball &nbsp;&raquo;&nbsp; Nyheter</title>
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/nav.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<meta name="author" content="Patrik 'S�s' Holmberg" />
<meta name="generator" content="phpDesigner 7, Photoshop CS3" />
<meta name="keywords" content="FrostaPaintball.se, Frosta, FPL, FPL.se, Paintball, lag, f�rening, H��r, medlemmar, forum, nyheter, information, l�nkar, sk�ne, sverige" />
<meta name="description" content="Frostapaintball.se - Paintball f�rening i Sk�ne." />
<meta name="copyright" content="FROSTAPAINTBALL.SE - Patrik 'S�s' Holmberg" />
<meta http-equiv="imagetoolbar" content="no"/>
<link rel="icon" href="image/icon/favicon.ico" />

<?php require("css/body.html"); ?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18545916-1']);
  _gaq.push(['_setDomainName', '.frostapaintball.se']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>
<body>

<?php require("preloader.html"); ?>

<center>

    <div id="body">

        <a href="<?php echo $saucyURL; ?>"><div id="logowrapper"><div id="logo"></div></div></a>

        <div id="navwrapper" >
            <div id="nav">
            	<ul class="menu_body" id="nav1">
            		<li class="menu_head_home_location"><a href="index.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav2">
            		<li class="menu_head_blog"><a href="http://forum.frostapaintball.se/" target="_blank"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav3">
            		<li class="menu_head_portfolio"><a href="info.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav4">
            		<li class="menu_head_downloads"><a href="team.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav5">
            		<li class="menu_head_forum"><a href="contact.php"></a></li>
            	</ul>
            	<ul class="menu_body" id="nav6">
            		<li class="menu_head_about"><a href="links.php"></a></li>
            	</ul>
            </div>
        </div>

        <div id="panel_left">
            <div style="float: left">






<div id="sidebar">
    <div id="title">
        <span class="big">
            Logga in
        </span>
    </div>
    <div id="spacer"></div>
    <div id="text" style="text-align: center;">

V&auml;lkommen <?php echo $_SESSION['sess_user']; ?><br>
<a href="index.php?logout=">Logga ut</a>

    </div>
</div>




                <?php include("calendar.html"); ?>
                <?php include("sponsors.html"); ?>
                <?php include("facebook.html"); ?>
            </div>
        </div>

        <div id="contentwrapper">

<?php

$resultat = mysql_query("SELECT * FROM news ORDER BY id DESC") or die (mysql_error());
while($row = mysql_fetch_array($resultat)) {
$id = $row['id'];
$title = $row['title'];
$date = $row['date'];
$text = $row['text'];
$user = $row['user'];

echo "
            <div id=\"content\">
                <div id=\"title\">
                    <span class=\"big\">
                        $title
                    </span>
                </div>
                <div id=\"spacer\"></div>
                <div id=\"text\">
                    <div id=\"date\" style=\"margin-bottom: 10px;\">
                        <span class=\"small\">
                            [ $date ]
                        </span>
                    </div>

                    $text

                </div>
            </div>
";

}

?>


    </div>

        <?php require("copyright.html"); ?>

</center>


</body>
</html>