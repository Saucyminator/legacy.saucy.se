# Frostapaintball.se
Sadly the database structure (.sql file) is nowhere to be found. :(
However, I tried to recreate it, and it is found in the /sql folder.


## Changes to made the website display on a PHP v7 configuration
Some updates are made to make the website usable/visible on PHP version 7. I've tried to change as little as possible because I still want it to show how my code looked back then, and I don't want to spend time updating something that's (for me) completely useless:

/inc/conn.php:
- Change mysql connection to PDO.
- Remove mysql_real_escape_string().

/inc/functions.php:
- Remove annoying error.

/index.php:
- Change mysql functions to PDO.
- Fix error about magic keywords - but in reality I forgot to stringify what I was checking `$username` against...

/info.php:
- Change mysql functions to PDO.

/links.php:
- Change mysql functions to PDO.

/team.php:
- Change mysql functions to PDO.
- Fix error about magic keywords - but in reality I forgot to stringify what I was checking `$username` against...

/sql/frostapaintball.sql:
- Add a best-I-could-to-recreate-the-database-structure file. Mostly so I could get the website to display correctly.
