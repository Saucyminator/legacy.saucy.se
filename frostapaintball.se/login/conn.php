<?php

// Byt ut mot dina inloggningsuppgifter och databas
$mysql_server   = 'localhost';
$mysql_database = 'database';
$mysql_user     = 'username';
$mysql_password = 'password';

$conn = mysql_connect($mysql_server, $mysql_user, $mysql_password);
mysql_select_db($mysql_database, $conn);


// En funktion att anv�ndas n�r magic_quotes_gpc inte �r satt. F�r att f�rhindra SQL-injections, eller i lidrigare fall MySQl-fel.
function db_escape ($post)
{
   if (is_string($post)) {
     if (get_magic_quotes_gpc()) {
        $post = stripslashes($post);
     }
     return mysql_real_escape_string($post);
   }

   foreach ($post as $key => $val) {
      $post[$key] = db_escape($val);
   }

   return $post;
}
