<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
</head>
<body>

<?php
/* user config variables */
$max_items = 5; /* max number of news items to show */

/* make database connection */
$db = mysql_connect ('localhost','root','pade');
mysql_select_db ('test',$db);

function displayNews($all = 0) {
    /* bring in two variables
     * $db is our database connection
     * $max_items is the maximum number
     * of news items we want to display */
    global $db, $max_items;
    
    /* query for news items */
    if ($all == 0) {
        /* this query is for up to $max_items */
        $query = "SELECT id,title,newstext," . 
                 "DATE_FORMAT(postdate, '%Y-%m-%d') as date " . 
                 "FROM news ORDER BY postdate DESC LIMIT $max_items";
    } else {
        /* this query will get all news */
        $query = "SELECT id,title,newstext," . 
                 "DATE_FORMAT(postdate, '%Y-%m-%d') as date " .
                 "FROM news ORDER BY postdate DESC";
    }
    $result = mysql_query ($query);
    while ($row = mysql_fetch_assoc ($result)) {
        /* display news in a simple table */
        echo "<TABLE border=\"1\" width=\"300\">";

        /* place table row data in 
         * easier to use variables.
         * Here we also make sure no
         * HTML tags, other than the
         * ones we want are displayed */
        $date = $row['date'];        
        $title = htmlentities ($row['title']);
        $news = nl2br (strip_tags ($row['newstext'], '<a><b><i><u>'));
        
        /* display the data */
        echo "<TR><TD><b>$title</b posted on $date</TD></TR>";
        echo "<TR><TD>$news</TD></TR>";
        
        /* get number of comments */
        $comment_query = "SELECT count(*) FROM news_comments " .
                         "WHERE news_id={$row['id']}";
        $comment_result = mysql_query ($comment_query);
        $comment_row = mysql_fetch_row($comment_result);
        
        /* display number of comments with link */
        echo "<tr><td> <a href=\"{$_SERVER['PHP_SELF']}" .
             "?action=show&id={$row['id']}\">Comments</a>" .
             "($comment_row[0]}</td></tr>";
        
        /* finish up table*/
        echo "</TABLE>";
        echo "<BR>";
    }
    
    /* if we aren't displaying all news, 
     * then give a link to do so */
    if ($all == 0) {
        echo "<a href=\"{$_SERVER['PHP_SELF']}" .
             "?action=all\">View all news</a>";
    }
}

function displayOneItem($id) {
    global $db;
    
    /* query for item */
    $query = "SELECT * FROM news WHERE id=$id";
    $result = mysql_query ($query);
    
    /* if we get no results back, error out */
    if (mysql_num_rows ($result) == 0) {
        echo "Bad news id";
        return;
    }
    $row = mysql_fetch_assoc($result);
    echo "<TABLE border=\"1\" width=\"300\">";

    /* easier to read variables and 
     * striping out tags */
    $title = htmlentities ($row['title']);
    $news = nl2br (strip_tags ($row['newstext'], '<a><b><i><u>'));
    
    /* display the items */
    echo "<tr><td><b>$title</b></td></tr>";
    echo "<tr><td>$news</td></tr>";
    
    echo "</TABLE>";
    echo "<BR>";
    
    /* now show the comments */
    displayComments($id);
}

function displayComments($id) {
    /* bring db connection variable into scope */
    global $db;
    
    /* query for comments */
    $query = "SELECT * FROM news_comments WHERE news_id=$id";
    $result = mysql_query ($query);
    echo "Comments<BR><HR width=\"300\">";
    
    /* display the all the comments */
    while ($row = mysql_fetch_assoc ($result)) {
        echo "<TABLE border=\"1\" width=\"300\">";
        
        $name = htmlentities ($row['name']);
        echo "<tr><td><b>by: $name</b></td></tr>";
    
        $comment = strip_tags ($row['comment'], '<a><b><i><u>');
        $comment = nl2br ($comment);
        echo "<tr><td>$comment</td></tr>";
    
        echo "</TABLE>";
        echo "<BR>";
    }
    
    /* add a form where users can enter new comments */
    echo "<HR width=\"300\">";
    echo "<FORM action=\"{$_SERVER['PHP_SELF']}" .
         "?action=addcomment&amp;id=$id\" method=POST>\n";
    echo "Name: <input type=\"text\" " .
         "width=\"30\" name=\"name\"><BR>\n";
    echo "<TEXTAREA cols=\"40\" rows=\"5\" " .
         "name=\"comment\"></TEXTAREA><BR>\n";
    echo "<input type=\"submit\" name=\"submit\" " .
         "value=\"Add Comment\"\n";
    echo "</FORM>\n";
     
}

function addComment($id) {
    global $db;
    
    /* insert the comment */
    $query = "INSERT INTO news_comments " .
             "VALUES('',$id,'{$_POST['name']}'," .
             "'{$_POST['comment']}')";
    mysql_query($query);
    
    echo "Comment entered. Thanks!<BR>\n";
    echo "<a href=\"{$_SERVER['PHP_SELF']}" .
         "?action=show&amp;id=$id\">Back</a>\n";
}

/* this is where the script decides what do do */

echo "<CENTER>\n";
switch($_GET['action']) {
    
    case 'show':
        displayOneItem($_GET['id']);
        break;
    case 'all':
        displayNews(1);
        break;
    case 'addcomment':
        addComment($_GET['id']);
        break;
    default:
        displayNews();
}
echo "</CENTER>\n";
?>

</body>
</html>