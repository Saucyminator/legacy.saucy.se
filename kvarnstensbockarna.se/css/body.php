<?php

echo "<style>

* {
	padding: 0px;
	margin: 0px;
}

/* BODY */
body {
    background: ".$kvarnstensbockarna["Default"]["Color"]["White"].";
    color: ".$kvarnstensbockarna["Default"]["Color"]["Dark"].";
    font: 10px ".$kvarnstensbockarna["Default"]["Font"].";
	line-height: ".$kvarnstensbockarna["Default"]["FontHeight"].";
	width: 100%;
	display: -webkit-box;
	display: -moz-box;
	display: box;
	-webkit-box-pack: center;
	-moz-box-pack: center;
	box-pack: center;
}

/* FONTS */
.IP {
    font: bold 50px ".$kvarnstensbockarna["Default"]["Font"].";
	line-height: ".$kvarnstensbockarna["Default"]["FontHeight"].";
}
.massive {
    font: 30px ".$kvarnstensbockarna["Default"]["Font"].";
	line-height: ".$kvarnstensbockarna["Default"]["FontHeight"].";
}
.huge {
    font: 24px ".$kvarnstensbockarna["Default"]["Font"].";
	line-height: ".$kvarnstensbockarna["Default"]["FontHeight"].";
}
.big {
    font: 16px ".$kvarnstensbockarna["Default"]["Font"].";
	line-height: ".$kvarnstensbockarna["Default"]["FontHeight"].";
}
.normal {
    font: 14px ".$kvarnstensbockarna["Default"]["Font"].";
	line-height: ".$kvarnstensbockarna["Default"]["FontHeight"].";
}
.normali {
    font: italic 12px ".$kvarnstensbockarna["Default"]["Font"].";
	line-height: ".$kvarnstensbockarna["Default"]["FontHeight"].";
}
.small {
    font: 12px ".$kvarnstensbockarna["Default"]["Font"].";
	line-height: ".$kvarnstensbockarna["Default"]["FontHeight"].";
}
.smalli {
    font: italic 12px ".$kvarnstensbockarna["Default"]["Font"].";
	line-height: ".$kvarnstensbockarna["Default"]["FontHeight"].";
}
.tiny {
    font: 10px ".$kvarnstensbockarna["Default"]["Font"].";
	-webkit-text-size-adjust:none;
}
.tinyi {
    font: italic 10px ".$kvarnstensbockarna["Default"]["Font"].";
	-webkit-text-size-adjust:none;
}

.title {
	color: ".$kvarnstensbockarna["Default"]["Color"]["Red"].";
	text-decoration: underline;
}
.image_nav {
	color: ".$kvarnstensbockarna["Default"]["Color"]["Red"].";
}


/* LINKS */
a:link, a:visited {
    color: ".$kvarnstensbockarna["Default"]["Color"]["Red"].";
    text-decoration: none;
    cursor: pointer;
}
a:hover, a:active {
    color: ".$kvarnstensbockarna["Default"]["Color"]["Dark"].";
    text-decoration: underline;
    cursor: pointer;
}

#site_wrapper {
	max-width: 1000px;
	display: -webkit-box;
	display: -moz-box;
	display: box;
	-webkit-box-orient: vertical;
	-moz-box-orient: vertical;
	box-orient: vertical;
	-webkit-box-flex: 1;
	-moz-box-flex: 1;
	box-flex: 1;
}

#logo {
    background: url('".$kvarnstensbockarna["Default"]["Logo"]."');
    position: relative;
    width: 1000px;
    height: 185px;
}
#logo a {
    position: absolute;
    top: 0;
    left: 0;
    width: 1000px;
    height: 185px;
}
#logo a h1 {
    display: none;
}

#content_wrapper {
    background: ".$kvarnstensbockarna["Default"]["Color"]["Gray"].";
	display: -webkit-box;
	display: -moz-box;
	display: box;
	-webkit-box-orient: horizontal;
	-moz-box-orient: horizontal;
	box-orient: horizontal;
}
#navigation {
	width: 200px;
	margin-top: 22px;
	margin-bottom: 20px;
}
#space {
	border-left: 1px solid ".$kvarnstensbockarna["Default"]["Color"]["Dark"].";
	width: 1px;
	margin: 20px;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}
#subcontent_wrapper {
	-webkit-box-flex: 1;
	-moz-box-flex: 1;
	box-flex: 1;
	min-height: 300px;
}
#copyright {
    background: ".$kvarnstensbockarna["Default"]["Color"]["Dark"].";
	text-align: center;
    color: ".$kvarnstensbockarna["Default"]["Color"]["Gray"].";
    padding: 10px;
    font: 10px ".$kvarnstensbockarna["Default"]["Font"].";
	line-height: ".$kvarnstensbockarna["Default"]["FontHeight"].";
}

#big_box {
	max-width: 720px;
	display: -webkit-box;
	display: box;
	-webkit-box-orient: vertical;
	-moz-box-orient: vertical;
	box-orient: vertical;
	-webkit-box-flex: 1;
	-moz-box-flex: 1;
	box-flex: 1;
	padding: 0px 20px;
	margin-right: 20px;
}
#new_box {
	display: -webkit-box;
	display: -moz-box;
	display: box;
	-webkit-box-orient: horizontal;
	-moz-box-orient: horizontal;
	box-orient: horizontal;
	margin-bottom: 20px;
	width: 100%;
}
#side_paragraphs {
	width: 50px;
	text-align: right;
	margin-right: 10px;
}
#content {
	-webkit-box-flex: 1;
	-moz-box-flex: 1;
	box-flex: 1;
	padding-bottom: 10px;
}
#header {
	text-align: center;
	border-bottom: 1px solid ".$kvarnstensbockarna["Default"]["Color"]["Dark"].";
	margin-bottom: 20px;
	padding: 10px 0px 0px 0px;
}

#default_image_text {
	background: ".$kvarnstensbockarna["Default"]["Color"]["DarkGray"].";
	padding: 5px;
}

#updates {
	position: absolute;
	background: ".$kvarnstensbockarna["Default"]["Color"]["Gray"].";
	width: 200px;
	float: right;
	top: 185px;
	margin-left: 1020px;
	text-align: center;
}
#updates_title {
	border-bottom: 1px solid ".$kvarnstensbockarna["Default"]["Color"]["Dark"].";
	margin: 0px 20px;
	padding: 11px;
}
#updates_text {
	text-align: justify;
	padding: 10px;
}


/* IMAGES */
a img {
    border: 1px solid ".$kvarnstensbockarna["Default"]["Color"]["Dark"].";
}

.image_img {
    position: relative;
    width: 100%;
}
#img_links {
    position: relative;
    width: 100%;
    height: 25px;
    text-align: center;

}
.img_first {
    position: relative;
    float: left;
    margin-left: 75px;
}
.img_previous {
    position: relative;
    float: left;
    margin-left: 75px;
}
.img_next{
    position: relative;
    float: right;
    right: 215px;
}
.img_last {
    position: relative;
    float: right;
    right: 35px;
}

.img_text {
    background: ".$kvarnstensbockarna["Default"]["Color"]["DarkGray"].";
    position: relative;
    padding: 5px;
    margin-top: 5px;
}

/* MEMBERS */
.member_wrapper {
    position: relative;
    width: 338px;
    margin-bottom: 20px;
	float: left;
}
.member_info {
	width: 100%;
	height: 100px;
	margin-bottom: 5px;
}
.member_img {
    background: ".$kvarnstensbockarna["Default"]["Color"]["Dark"].";
    position: relative;
    width: 100px;
    height: 100px;
	margin-right: 10px;
    float: left;
    z-index: 5;
}
.member_text {
    position: relative;
	float: left;
}
.member_name {
}
.member_name_text {
    margin-top: 3px;
}
.member_role {
    margin-top: 3px;
}
.member_role_text {
    margin-top: 3px;
}
.member_email {
    background: url('".$kvarnstensbockarna["Nav"]["Medlemmar"]["Image"]."');
    position: relative;
    width: 32px;
    height: 32px;
	clear: both;
}
.member_email a {
    position: absolute;
    top: 0;
    left: 0;
    width: 32px;
    height: 32px;
}
.member_email a h1 {
    display: none;
}
.member_email:hover {
    background: url('".$kvarnstensbockarna["Nav"]["Medlemmar"]["ImageHover"]."');
}

/* NAVIGATION */
.treeview {
    position: relative;
    width: 200px;
}
.treeview a {
    color: ".$kvarnstensbockarna["Default"]["Color"]["Dark"].";
    text-decoration: none;
    display: block;
    width: 200px;
    text-align: right;
}
.treeview ul { /*CSS for Simple Tree Menu*/
    text-align: right;
}
.treeview ul a { /*CSS for Simple Tree Menu*/
    position: relative;
    text-align: right;
}
.treeview li { /*Style for LI elements in general (excludes an LI that contains sub lists)*/
    position: relative;
    list-style-type: none;
    margin-bottom: 3px;
}
.treeview li a { /*Style for LI elements in general (excludes an LI that contains sub lists)*/
    position: relative;
    list-style-type: none;
    margin-bottom: 3px;
}
.treeview li.submenu{ /* Style for LI that contains sub lists (other ULs). */
    position: relative;
    text-align: right;
}
.treeview li.submenu ul{ /*Style for ULs that are children of LIs (submenu) */
    display: none; /*Hide them by default. Don't delete. */
    position: relative;
    right: 15px;
}
.treeview .submenu ul li{ /*Style for LIs of ULs that are children of LIs (submenu) */
    position: relative;
}
.li_border, .submenu {
	border-bottom: 1px solid ".$kvarnstensbockarna["Default"]["Color"]["Dark"].";
}

</style>";

?>