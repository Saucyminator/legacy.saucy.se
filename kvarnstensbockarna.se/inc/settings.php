<?php
mb_internal_encoding("UTF-8");
setlocale(LC_ALL, "sv.UTF-8");
date_default_timezone_set("Europe/Stockholm");

ini_set('display_errors', 0);
ini_set('log_errors', 1);
error_reporting(E_ALL);

/* FOLDERS */
$folder_css			= "css";
$folder_image		= "image";
$folder_include		= "inc";
$folder_javaScript	= "js";
$folder_medlemmar	= "$folder_image/medlemmar";
$folder_bilder		= "$folder_image/bilder";
$folder_icon		= "$folder_image/icon";

$accesslist = array(
	"default",
	"medlemmar",
	"bilder",
	"stadgar",
	"bockaiader",
	"milleniumfesten",
	"utdrag_2010",
	"utdrag_2005",
	"utdrag_2000",
	"utdrag_1984",
	"lankar",
	"kontakta",
	"reseskildring",
	"gastbok",
);

$kvarnstensbockarna = array(

	// DEFAULT
	"Default" => array(
		"Name"					=> "Kvarnstensbockarna",
		"URL"					=> "http://www.kvarnstensbockarna.se/",
		"Logo"					=> "$folder_image/logo.png",
		"CopyYear"				=> 2010,
		"CurYear"				=> date("Y"),
		"Font"					=> "Trebuchet MS",
		"FontHeight"			=> "150%",
		"JS" => array(
			"jQuery"			=> "https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js",
			"SimpleTreeMenu"	=> "$folder_javaScript/simpletreemenu.js",
			"gen_validatorv31"	=> "$folder_javaScript/gen_validatorv31.js",
		),
		"Conn"					=> "$folder_include/conn.php",
		"Functions"				=> "$folder_include/functions.php",
		"CSS"					=> "$folder_css/body.php",
		//"Width"					=> 1000,
		//"WidthPanel"			=> 248,
		//"WidthContent"			=> 480,
		"MetaCharset"			=> "UTF-8",
		"MetaAuthor"			=> "Patrik 'Saucy' Holmberg",
		"MetaGenerator"			=> "Notepad++, Photoshop CS3",
		"MetaKeywords"			=> "Kvarnstensbockarna.se, Kvarnstensbockarna, Höör, Herrklubb",
		"MetaDescription"		=> "Kvarnstensbockarna - Vinst är det enda som räknas, allt annat är shit!!!",
		"MetaCopyRight"			=> "KVARNSTENSBOCKARNA.SE, - Patrik 'Saucy' Holmberg",
		"MetaRobots"			=> "ALL",
		"MetaImagetoolbar"		=> "NO",
		"MetaIcon"				=> "$folder_icon/favicon.ico",
		"Color" => array(
			"White"				=> "#ffffff",
			"Dark"				=> "#3a3b3c",
			"Gray"				=> "#f1f1f1",
			"DarkGray"			=> "#cccccc",
			"Red"				=> "#e03d3d",
			"Orange"			=> "#e68f17",
		),
	),
	"Nav" => array(
		"Default"			=> "information.php?page=default", // Start
		"Medlemmar" => array(
			"URL"			=> "information.php?page=medlemmar",
			"Image"			=> "$folder_image/email.png",
			"ImageHover"	=> "$folder_image/email_hover.png",
			"Medlem" => array(
				"Mats Nilsson" => array(
					"Roll" => "President",
					"Email" => "mats",
				),
				"Uffe Persson" => array(
					"Roll" => "Sekreterare",
					"Email" => "uffe",
				),
				"Torsten Centerdal" => array(
					"Roll" => "Kassör",
					"Email" => "totte.c",
				),
				"Per Nilsson" => array(
					"Roll" => "Vice vid övriga poster",
					"Email" => "per",
				),
				"Jan Malmström" => array(
					"Roll" => "Jag tömmer baren!",
					"Email" => "jan",
				),
				"Kjell Olsson" => array(
					"Roll" => "Jag tömmer baren!",
					"Email" => "kjell",
				),
				"Tommy Olsson" => array(
					"Roll" => "Jag tömmer baren!",
					"Email" => "tommy",
				),
				"Göran Brandt" => array(
					"Roll" => "Jag tömmer baren!",
					"Email" => "goran",
				),
				"Torsten Ericsson" => array(
					"Roll" => "Jag tömmer baren!",
					"Email" => "totte.e",
				),
				"Jörgen Sollvén" => array(
					"Roll" => "Jag tömmer baren!",
					"Email" => "jorgen",
				),
			),
		),
		"Protokoll" => array(
			"Utdrag_2010",
			"Utdrag_2005",
			"Utdrag_2000",
			"Utdrag_1984",
		),
		"Bilder" => array(),
		"Information" => array(
			"Stadgar",
			"Bockaiader",
			"Milleniumfesten",
		),
		"Gästbok" => "information.php?page=gastbok",
		"Länkar" => "information.php?page=lankar",
		"Kontakta oss" => "information.php?page=kontakta",
		//"Forum" => "http://forum.kvarnstensbockarna.se",
	),
	"Updates" =>
		"* Årsmöte hos Uffe 17/11.

		Vill ni veta vad som händer på våra möte? Läs protokollen."
	,
);
$copyright = array(
	"Content"					=> "&copy; ".$kvarnstensbockarna["Default"]["CopyYear"] . (($kvarnstensbockarna["Default"]["CopyYear"] != $kvarnstensbockarna["Default"]["CurYear"]) ? '-' . $kvarnstensbockarna["Default"]["CurYear"] : '')." - [url=".$kvarnstensbockarna["Default"]["URL"]."]KVARNSTTENSBOCKARNA.SE[/url]
		ALL RIGHTS RESERVED - DESIGN AV [url=http://saucy.se/]SAUCY.SE[/url]",
);










?>
