<?php
header('Content-Type: text/html; charset=utf-8');

require("inc/settings.php");
require($kvarnstensbockarna["Default"]["Conn"]);
require($kvarnstensbockarna["Default"]["Functions"]);

$p = isset($_GET["p"]) ? intval($_GET["p"]) : 0;
$page = isset($_GET["page"]) ? mysql_real_escape_string($_GET["page"]) : NULL;

$dir = "image/bilder/";
foreach(array_reverse(glob("$dir*")) as $folders) {
	
	$folders_clean = $folders;
	$folders       = swe__urlencode(utf8_encode($folders));
	$folders_key   = basename($folders);
	
	//$folders				= utf8_encode($folders);
	//$folders_key			= basename($folders);
	$folder[$folders_key]	= "$folders/";
	
	if(is_array(glob("$folders_clean/*"))) {
		foreach(glob("$folders_clean/*") as $subfolders) {
			if(!is_file($subfolders)) {
			
				$subfolders_clean = $subfolders;
				$subfolders       = swe__urlencode(utf8_encode($subfolders));
				$subfolders_key   = basename($subfolders);
				$subfolders_check = eng(strtolower(swe__urldecode($subfolders_key)));
							
				//$subfolders                       = utf8_encode($subfolders);
				//$subfolders_key                   = basename($subfolders);
				$subfolder[$subfolders_key]       = "$subfolders_clean/";
				//$subfolders_check                 = eng(strtolower($subfolders_key));
				$subfolder_check[$subfolders_key] = $subfolders_check;
				
				//echo ">> [SUBFOLDERS]: $subfolders_key<br />";
				//echo ">> [SUBFOLDERS PATH]: $subfolders<br />";
			} else {
				
				$subfolders_clean = $subfolders;
				$subfolders       = swe__urlencode(utf8_encode($subfolders));
				$subfolders_key   = basename($subfolders);
				$subfolders_check = eng(strtolower(swe__urldecode($subfolders_key)));
				
				//$subfolders                               = utf8_encode($subfolders);
				//$subfolders_key                           = basename($subfolders);
				$subfolder_images_text                    = preg_replace("#^\d+#",'', str_replace("_", " ", str_replace(array(".jpg",".JPG","Bild ",". ","kopia"), "", $subfolders_key)));
				$subfolder_images[$subfolder_images_text] = "$subfolders_clean/";
				//echo ">> [SUBFOLDERS IMG] Subfolders_key image: $subfolders_key<br />";
				//echo ">> [SUBFOLDERS IMG PATH] Subfolders_key image path: $subfolders<br />";
				//echo ">> [SUBFOLDERS IMG TEXT] Subfolders_key image text: $subfolder_images_text<br />";
			}
			
			/*
			foreach(glob(utf8_decode($dir."$folders_key/$subfolders_key/*")) as $images) {
				$images				= utf8_encode($images);
				$images_key			= basename($images);
				$images_text		= preg_replace("#^\d+#",'', str_replace("_", " ", str_replace(array(".jpg",".JPG","Bild ",". ","kopia"), "", $images_key)));
				$image[$images_key] = "$images";
				//$image[$folders_key."/".$subfolders_key] = "$images/";
				//echo ">> >> [IMAGES]: $images_key<br />";
				//echo ">> >> [IMAGES PATH]: $images<br />";
			}
			*/
		}
	}
}

$access = array_merge($accesslist, $subfolder_check);
if(!in_array($page, $access)) {

	echo "Sidan du söker finns inte.";
	echo "<br />".$page;
	exit;
	
}
?>
<!DOCTYPE html>
<html lang='sv'>
<head>
<title></title>

<meta charset='<?php echo $kvarnstensbockarna["Default"]["MetaCharset"]; ?>' />
<meta name='author' content='<?php echo $kvarnstensbockarna["Default"]["MetaAuthor"]; ?>' />
<meta name='generator' content='<?php echo $kvarnstensbockarna["Default"]["MetaGenerator"]; ?>' />
<meta name='keywords' content='<?php echo $kvarnstensbockarna["Default"]["MetaKeywords"]; ?>' />
<meta name='description' content='<?php echo $kvarnstensbockarna["Default"]["MetaDescription"]; ?>' />
<meta name='copyright' content='<?php echo $kvarnstensbockarna["Default"]["MetaCopyRight"]; ?>' />
<meta name='robots' content='<?php echo $kvarnstensbockarna["Default"]["MetaRobots"]; ?>' />
<meta http-equiv='imagetoolbar' content='<?php echo $kvarnstensbockarna["Default"]["MetaImagetoolbar"]; ?>' />
<link rel="shortcut icon" href="<?php echo $kvarnstensbockarna["Default"]["MetaIcon"]; ?>" type="image/x-icon" />

<?php

echo "<script src='".$kvarnstensbockarna["Default"]["JS"]["jQuery"]."'></script>";
echo "<script src='".$kvarnstensbockarna["Default"]["JS"]["gen_validatorv31"]."'></script>";

?>
</head>
<body>

<?php

if(in_array($page, $access)) {

	echo "<div id='header'>
		<span class='huge'>";
			if($page == "default") {
				echo "Start";
			} elseif($page == "stadgar") {
				echo "Bockarnas Stadgar";
			} elseif($page == "bockaiader") {
				echo "Våra kända bockaiader";
			} elseif($page == "utdrag_2010") {
				echo "Utdrag från år: 2010 - 2015";
			} elseif($page == "utdrag_2005") {
				echo "Utdrag från år: 2005 - 2010";
			} elseif($page == "utdrag_2000") {
				echo "Utdrag från år: 2000 - 2005";
			} elseif($page == "utdrag_1984") {
				echo "Utdrag från år: 1984 - 2000";
			} elseif($page == "lankar") {
				echo "Länkar";
			} elseif($page == "gastbok") {
				echo "Gästbok";
			} elseif($page == "kontakta") {
				echo ucfirst($page)." oss";
			} elseif(in_array($page, $subfolder_check)) {
				foreach($subfolder_check as $subfolder_title_key => $subfolder_title_value) {
					if($subfolder_title_value == $page) {
						echo "Bilder: ".swe__urldecode($subfolder_title_key);
					}
				}
			} else {
				echo ucfirst($page);
			}
		echo "</span>
	</div>";
	
	if($page == "default") {
	
		$startbild = swe__urlencode("image/bilder/Övrigt/Gott & Blandat/Bild 26. Kvarnstensbockarna.jpg");
		
		/*
		$replace = "image/bilder/Övrigt/Gott & Blandat/Bild 26. Kvarnstensbockarna.jpg";
		
		echo "<a href='".$replace."'>".$replace."</a> : ".mb_detect_encoding($replace)." : FUNKAR LOKALT";
		echo "<br />";
		$replace = swe__urlencode($replace);
		echo "<a href='".$replace."'>".$replace."</a> : ".mb_detect_encoding($replace)." : FUNKAR REMOTE";
		echo "<br />";
		echo "<br />";
		echo "<br />";
		echo "<br />";
		echo "<br />";
		*/
		
		/*	
		$dir = "image/bilder/";
		foreach(array_reverse(glob("$dir*"),GLOB_ONLYDIR) as $folders) {
		
			//$folders				= swe__urldecode(urlencode($folders));
			//$folders				= utf8_encode(($folders));
			//$folders_key			= basename($folders); // behöver ändra filens namn från ASCII till UTF-8
			
			$rep = "/Gott & Blandat/Bild 26. Kvarnstensbockarna.jpg";
			$subrep = "/Bild 26. Kvarnstensbockarna.jpg";
			$rep = "";
			$subrep = "";
			
			$folders     = ($folders);
			$folders_key = basename($folders);
			
			$folders14     = swe__urlencode(utf8_encode($folders_key));
			echo "#14 : <a href='".$folders14.$rep."'>".$folders14.$rep."</a> : ".mb_detect_encoding($folders14)." : FUNKAR REMOTE <br />";
			$folders16     = swe__urldecode(utf8_encode($folders_key));
			echo "#16 : <a href='".$folders16.$rep."'>".$folders16.$rep."</a> : ".mb_detect_encoding($folders16)." : FUNKAR LOKALT <br />";
						
			echo "<br />";
			echo "<br />";
			
			$folders_local      = utf8_encode($folders_key);
			$folders_local_key  = utf8_encode(basename($folders_key));
			$folders_remote     = swe__urlencode(utf8_encode($folders_key));
			$folders_remote_key = swe__urlencode(utf8_encode(basename($folders_key)));
			echo "FOLDERS : <a href='$folders_local'>$folders_local</a> : FUNKAR LOKALT : <br />";
			echo "FOLDERS_KEY : <a href='$folders_local_key'>$folders_local_key</a> : <br />";
			
			echo "FOLDERS : <a href='$folders_remote'>$folders_remote</a> : FUNKAR REMOTE : ANVÄND<br />";
			echo "FOLDERS_KEY : <a href='$folders_remote_key'>$folders_remote_key</a> : ANVÄND<br />";
			
			
			$folders15     = swe__urlencode(utf8_decode($folders_key));
			echo "#15 : <a href='".$folders15.$rep."'>".$folders15.$rep."</a> : ".mb_detect_encoding($folders15)." : <br />";
			$folders17     = swe__urldecode(utf8_decode($folders_key));
			echo "#17 : <a href='".$folders17.$rep."'>".$folders17.$rep."</a> : ".mb_detect_encoding($folders17)." : <br />";
			
			echo "#1 : <a href='".$folders_key.$rep."'>".$folders_key.$rep."</a> : ".mb_detect_encoding($folders_key)." : <br />";
			echo "<br />";
			
			$folders2     = urlencode($folders_key);
			echo "#2 : <a href='".$folders2.$rep."'>".$folders2.$rep."</a> : ".mb_detect_encoding($folders2)." : <br />";
			$folders3     = urldecode($folders_key);
			echo "#3 : <a href='".$folders3.$rep."'>".$folders3.$rep."</a> : ".mb_detect_encoding($folders3)." : <br />";
			echo "<br />";
			
			$folders4     = mb_convert_encoding(swe__urlencode($folders_key), "ASCII");
			echo "#4 : <a href='".$folders4.$rep."'>".$folders4.$rep."</a> : ".mb_detect_encoding($folders4)." : <br />";
			$folders5     = mb_convert_encoding(swe__urldecode($folders_key), "ASCII");
			echo "#5 : <a href='".$folders5.$rep."'>".$folders5.$rep."</a> : ".mb_detect_encoding($folders5)." : <br />";
			echo "<br />";
			
			$folders6     = utf8_encode(swe__urlencode($folders_key));
			echo "#6 : <a href='".$folders6.$rep."'>".$folders6.$rep."</a> : ".mb_detect_encoding($folders6)." : FUNKAR LOKALT <br />";
			$folders7     = utf8_encode(swe__urldecode($folders_key));
			echo "#7 : <a href='".$folders7.$rep."'>".$folders7.$rep."</a> : ".mb_detect_encoding($folders7)." : FUNKAR LOKALT <br />";
			$folders8     = utf8_decode(swe__urlencode($folders_key));
			echo "#8 : <a href='".$folders8.$rep."'>".$folders8.$rep."</a> : ".mb_detect_encoding($folders8)." : <br />";
			$folders9     = utf8_decode(swe__urldecode($folders_key));
			echo "#9 : <a href='".$folders9.$rep."'>".$folders9.$rep."</a> : ".mb_detect_encoding($folders9)." : <br />";
			echo "<br />";
			
			$folders10     = utf8_encode($folders_key);
			echo "#10 : <a href='".$folders10.$rep."'>".$folders10.$rep."</a> : ".mb_detect_encoding($folders10)." : FUNKAR LOKALT <br />";
			$folders11     = utf8_decode($folders_key);
			echo "#11 : <a href='".$folders11.$rep."'>".$folders11.$rep."</a> : ".mb_detect_encoding($folders11)." : <br />";
			echo "<br />";
			
			$folders12     = swe__urlencode($folders_key);
			echo "#12 : <a href='".$folders12.$rep."'>".$folders12.$rep."</a> : ".mb_detect_encoding($folders12)." : <br />";
			$folders13     = swe__urldecode($folders_key);
			echo "#13 : <a href='".$folders13.$rep."'>".$folders13.$rep."</a> : ".mb_detect_encoding($folders13)." : <br />";
			echo "<br />";
			
			
			echo "FOLDERS : <a href='".($folders)."'>".($folders)."</a> <br />";
			echo "FOLDERS_KEY : <a href='".($folders_key)."'>".($folders_key)."</a> <br />";
			echo "FOLDERS : <a href='".urlencode($folders)."'>".urlencode($folders)."</a> <br />";
			echo "FOLDERS_KEY : <a href='".urlencode($folders_key)."'>".urlencode($folders_key)."</a> <br />";
			echo "FOLDERS : <a href='".urldecode($folders)."'>".urldecode($folders)."</a> <br />";
			echo "FOLDERS_KEY : <a href='".urldecode($folders_key)."'>".urldecode($folders_key)."</a> <br />";
			echo "FOLDERS : <a href='".utf8_decode($folders)."'>".utf8_decode($folders)."</a> <br />";
			echo "FOLDERS_KEY : <a href='".utf8_decode($folders_key)."'>".utf8_decode($folders_key)."</a> <br />";
			echo "FOLDERS : <a href='".swe__urlencode($folders)."'>".swe__urlencode($folders)."</a> <br />";
			echo "FOLDERS_KEY : <a href='".swe__urlencode($folders_key)."'>".swe__urlencode($folders_key)."</a> <br />";
			echo "FOLDERS : <a href='".swe__urldecode($folders)."'>".swe__urldecode($folders)."</a> <br />";
			echo "FOLDERS_KEY : <a href='".swe__urldecode($folders_key)."'>".swe__urldecode($folders_key)."</a> <br />";
			echo "<br />";
			echo "FOLDERS_KEY : <a href='".swe__urldecode(urlencode($folders_key))."'>".swe__urldecode(urlencode($folders_key))."</a> <br />";
			
			
			/*
			echo "<br />";
			echo $dir;
			echo "<br />";
			echo $folders;
			echo "<br />";
			echo $folders_key;
			echo "<br />";
			*/
			
			//echo "<li style='border: 0px;'><a><span class='normali'>$folders_key</span></a>"; // Övrigt, 2012, 2011 etc
			//echo "<ul>";
			//if(is_array(swe__urldecode(urlencode($dir."$folders_key/*")))) {
			//	foreach(glob(swe__urldecode(urlencode($dir."$folders_key/*")),GLOB_ONLYDIR) as $subfolders) {
			/*
			if(is_array(glob("$folders/*",GLOB_ONLYDIR))) {
				//foreach(glob($dir."$folders_key/*",GLOB_ONLYDIR) as $subfolders) {
				foreach(glob("$folders/*",GLOB_ONLYDIR) as $subfolders) {
					if(!is_file($subfolders)) {
						//$subfolders		= utf8_encode($subfolders);
						//$subfolders_key	= basename($subfolders);
						//echo "<li><a id='information.php?page=".eng(strtolower($subfolders_key))."' class='link_click'><span class='normali'>".preg_replace("#[^a-zåäöÅÄÖ &]#i", "", $subfolders_key)."</span></a></li>"; // Gott & Blandat, Möte hos otte E 2012-03-12 etc
						
						
						$subfolders_local      = utf8_encode($subfolders.$subrep);
						$subfolders_local_key  = utf8_encode(basename($subfolders.$subrep));
						$subfolders_remote     = swe__urlencode(utf8_encode($subfolders.$subrep));
						$subfolders_remote_key = swe__urlencode(utf8_encode(basename($subfolders.$subrep)));
						echo ">> SUBFOLDERS : <a href='$subfolders_local'>$subfolders_local</a> : FUNKAR LOKALT : <br />";
						echo ">> SUBFOLDERS_KEY : <a href='$subfolders_local_key'>$subfolders_local_key</a> : <br />";
						
						echo ">> SUBFOLDERS : <a href='$subfolders_remote'>$subfolders_remote</a> : FUNKAR REMOTE : ANVÄND<br />";
						echo ">> SUBFOLDERS_KEY : <a href='$subfolders_remote_key'>$subfolders_remote_key</a> : ANVÄND<br />";
						
						
						
						
					} else {
						//$subfolders		= utf8_encode($subfolders);
						$subfolders_key	= basename($subfolders);
						
					}
					//foreach(glob(utf8_decode($dir."$folders_key/$subfolders_key/*")) as $images) {
					//	$images				= utf8_encode($images);
					//	$images_key			= basename($images);
					//}
					echo "<br />";
					echo "<br />";
				}
			}
			echo "<br />";
			echo "<br />";
			echo "<br />";
			//echo "</ul>";
		}
		*/
		
		
		echo "<div id='big_box'>
			<div id='new_box'>
				<div id='content'>
					<div style='text-align: center;'>
						<span class='big'>
							Kvarnstensbockarna år 1985
						</span>
					</div>
					<div>
						<a href='$startbild' target='_blank'>
							<img src='$startbild' style='width: 100%;' />
						</a>
					</div>
					<div class='img_text' style='margin-top: 0px;'>
						<span class='tinyi'>
							Uffe, Mats, Tommy, Per, Jörgen, Torsten E, Torsten C
							<br />
							Jan, Bo, Göran, Kjell
						</span>
					</div>
				</div>
			</div>
		</div>";
		
		
	} elseif($page == "medlemmar") {
	
		echo "<div id='big_box' style='display: block; width: 720px;'>
			<div id='new_box'>
				<div id='content'>";
				
					foreach($kvarnstensbockarna as $key1 => $value1) {
						if(is_array($value1)) {
							foreach($value1 as $key2 => $value2) {
								if($key2 == "Medlemmar") {
									foreach($value2 as $key3 => $value3) {
										if($key3 == "Medlem") {
											foreach($value3 as $key4 => $value4) {
											
												/*
												$key = (($key4));
												echo "<a href='$folder_medlemmar/$key.jpg'>$key</a> <br />";
												
												$key = (urlencode($key4));
												echo "<a href='$folder_medlemmar/$key.jpg'>$key</a> <br />";
												$key = (urldecode($key4));
												echo "<a href='$folder_medlemmar/$key.jpg'>$key</a> <br />";
												
												$key = urlencode(urlencode($key4));
												echo "<a href='$folder_medlemmar/$key.jpg'>$key</a> <br />";
												$key = urlencode(urldecode($key4));
												echo "<a href='$folder_medlemmar/$key.jpg'>$key</a> <br />";
												$key = urldecode(urlencode($key4));
												echo "<a href='$folder_medlemmar/$key.jpg'>$key</a> <br />";
												$key = urldecode(urldecode($key4));
												echo "<a href='$folder_medlemmar/$key.jpg'>$key</a> <br />";
												
												$key = swe__urlencode(($key4));
												echo ">> <a href='$folder_medlemmar/$key.jpg'>$key</a> <br />";
												$key = swe__urldecode(($key4));
												echo ">> <a href='$folder_medlemmar/$key.jpg'>$key</a> <br />";
												
												$key = swe__urlencode(utf8_encode($key4));
												echo "<a href='$folder_medlemmar/$key.jpg'>$key</a> <br />";
												$key = swe__urlencode(utf8_decode($key4));
												echo "<a href='$folder_medlemmar/$key.jpg'>$key</a> <br />";
												$key = swe__urldecode(utf8_encode($key4));
												echo "<a href='$folder_medlemmar/$key.jpg'>$key</a> <br />";
												$key = swe__urldecode(utf8_decode($key4));
												echo "<a href='$folder_medlemmar/$key.jpg'>$key</a> <br />";
												
												echo "<br />";
												*/
												
												$key4 = swe__urlencode($key4);
												
												echo "<div class='member_wrapper'>
													<div class='member_info'>
														<div class='member_img'><a href='$folder_medlemmar/".$key4."_big.jpg' target='_blank'><img src='$folder_medlemmar/".$key4.".jpg' /></a></div>
														<div class='member_text'>
															<div class='member_name'><span class='normal' style='text-decoration: underline;'>Namn:</span></div>
															<div class='member_name_text'><span class='normal'>".swe__urldecode($key4)."</span></div>
															
															<div class='member_role'><span class='normal' style='text-decoration: underline;'>Roll:</span></div>
															<div class='member_role_text'><span class='normal'>".$value4["Roll"]."</span></div>
														</div>
													</div>
													<div class='member_email'>
														<a href='mailto:".$value4["Email"]."@kvarnstensbockarna.se' title='Skicka ett email till: $key4'><h1>$key4</h1></a>
													</div>
												</div>";
											}
										}
									}
								}
							}
						}
					}
					
				echo "</div>
			</div>
		</div>";
		
	} elseif($page == "utdrag_2010" OR $page == "utdrag_2005" OR $page == "utdrag_2000" OR $page == "utdrag_1984") {
	
		$pageExtra = str_replace("utdrag_", "", $page);
		
		echo "<div id='big_box'>";
			$resultat = mysql_query("
			SELECT
				id,
				utdrag,
				datum,
				plats,
				text
			FROM legacy_kvarnstensbockarna_protokoll
			WHERE utdrag = '$pageExtra'
			ORDER BY datum DESC") or die (mysql_error());
			while($row = mysql_fetch_array($resultat)) {
			
				$id		= $row['id'];
				$datum	= $row['datum'];
				$plats	= $row['plats'];
				$text	= $row['text'];
				
				echo "<div id='new_box'>
					<div id='content' style='border-bottom: 1px solid black;'>
						<div>
							<span class='big title'>
								$datum ";
								
								if($datum == '0309??') {
									echo "- $plats";
								} elseif($datum == '011117' OR $datum == '020706' OR $datum == '030707' OR $datum == '040701' OR $datum == '0508??') {
									echo "($plats)";
								} else {
									echo "(hos $plats)";
								}
								
							echo "</span>
						</div>
						<div>
							<span class='normal'>";
							
								if(!empty($text)) {
									echo bbkod($text);
								} else {
									echo "SAKNAR PROTOKOLL";
								}
							
							echo "</span>
						</div>
					</div>
				</div>";
			}
		echo "</div>";
		
	} elseif(in_array($page, $subfolder_check)) {
	
		echo "<script>
		$('a.link_image').on('click', function(e){
			e.preventDefault();
			$('#subcontent_wrapper').load($(this).attr('id'));
		});
		</script>";
		
		echo "<style>
		#subcontent_wrapper {
			min-height: 1200px;
		}
		</style>";
	
		echo "<div id='big_box' style='text-align: center;'>
			<div id='new_box'>
				<div id='content'>
					<span class='normal'>";
					
						$image_amount = 0;
						$image_text_characters = 9; // Hur många bokstäver som skall tas bort på bildnamnet
						$image = array();
						
						$image_text_exclude = array(
							"VM fotboll 2006-06-09",
							"Korpskytte 2003-05-29",
							"Fiske Månstorp 2003-07-05",
						);
						
						foreach($subfolder_check as $subfolder_title_key => $subfolder_title_value) {
							if($subfolder_title_value == $page) {
								foreach($subfolder as $key => $value) {
									if($key == $subfolder_title_key) {
										if($key == $subfolder_title_key) {
											foreach(glob("$value*") as $images) {
											
												$images_clean = $images;
												$images       = swe__urlencode(utf8_encode($images));
												$images_key   = basename($images);
												$images_key   = swe__urldecode(basename($images));
												
												$image_amount++;
												//$images				= utf8_encode($images);
												//$images_key			= basename($images);
												//$images_text		= preg_replace("#^\d+#",'', str_replace("_", " ", str_replace(array(".jpg",".JPG","Bild ",". ","kopia"), "", $images_key)));
												$images_text		= str_replace("_", " ", str_replace(array(".jpg",".JPG","kopia"), "", substr($images_key,$image_text_characters)));;
												
												$key_encoded = swe__urldecode($key);
												if(!in_array($key_encoded, $image_text_exclude)) {
													$images_texts[]	= $images_text;
												}
												$image[] = $images;
											}
											
											echo "<div id='img_links'>";
											
												if($p != 0) {
													echo "<div class='img_first'>
														<a id='information.php?page=$page&p=0' class='link_image image_nav'>
															<span style='font-weight: bold;'>
																« Första
															</span>
														</a>
													</div>";
												}
												
												if($p > 0) {
												   echo "<div class='img_previous'>
													   <a id='information.php?page=$page&p=".($p - 1)."' class='link_image image_nav'>
															<span style='font-weight: bold;'>
																‹ Föregående
															</span>
													   </a>
												   </div>";
												}
												
												if($p < ($image_amount - 1)) {
												   echo "<div class='img_next'>
														<a id='information.php?page=$page&p=".($p + 1)."' class='link_image image_nav'>
															<span style='font-weight: bold;'>
																Nästa ›
															</span>
														</a>
													</div>";
												}
												
												if($p != ($image_amount - 1)) {
													echo "<div class='img_last' align='right'>
														<a id='information.php?page=$page&p=".($image_amount - 1)."' class='link_image image_nav'>
															<span style='font-weight: bold;'>
																Sista »
															</span>
														</a>
													</div>";
												}
												
											echo "</div>";
											
											// Visa bilden
											echo "<div>
												<a href='".$image[$p]."' target='_blank'><img src='".$image[$p]."' class='image_img' /></a>
											</div>";
											
											// Visa bild nr
											echo "<div>
												Bild nr: <span style='font-weight: bold;'>".($p+1)."</span> av <span style='font-weight: bold;'>$image_amount</span>
											</div>";
											
											// Om det finns nån text till bilden så visas det, annars är det döljt
											if(!empty($images_texts[$p])) {
												echo "<div class='img_text'>".$images_texts[$p]."</div>";
											}
											
										}
									}
								}
							}
						}
						
					echo "</span>
				</div>
			</div>
		</div>";
		
	} elseif($page == "bockaiader" OR $page == "lankar" OR $page == "stadgar") {
	
		echo "<div id='big_box' "; if($page == "lankar") { echo "style='text-align: center;'"; } echo ">";
			$resultat = mysql_query("
			SELECT
				id,
				section,
				text
			FROM legacy_kvarnstensbockarna_information
			WHERE section = '$page'
			ORDER BY id ASC") or die (mysql_error());
			while($row = mysql_fetch_array($resultat)) {
			
				$id		= $row['id'];
				$text	= $row['text'];
				
				echo "<div id='new_box'>";
					if($page == "stadgar") {
						echo "<div id='side_paragraphs'>
							<span class='normal'>
								$id"."§
							</span>
						</div>";
					}
					echo "<div id='content'>
						<span class='normal'>";
							echo bbkod($text);
						echo "</span>
					</div>
				</div>";
			}
		echo "</div>";
		
	} elseif($page == "gastbok") {
	
		echo "<div id='big_box' style='text-align: center;'>
			<div id='new_box'>
				<div id='content'>
					<iframe src='http://www.gastbok.nu/gb/55579/'' width='100%'' height='800'></iframe>
				</div>
			</div>
		</div>";
		
	} elseif($page == "kontakta") {
	
		echo "<script>
		$('a.link_kontakta').on('click', function(e){
			e.preventDefault();
			$('#subcontent_wrapper').load($(this).attr('id'));
		});
		</script>";
		
		echo "<div id='big_box' style='text-align: center;'>
			<div id='new_box'>
				<div id='content'>
					<span class='normal'>
						Hemsidan får vara utan kontakt from tills webmastern kommit på ett bättre system.
						Ni kan alltid maila medlemmarna i herrklubben genom <a id='information.php?page=medlemmar' class='link_kontakta' style='color: ".$kvarnstensbockarna["Default"]["Color"]["Red"].";'>Medlemssidan</a>.
					</span>
				</div>
			</div>
		</div>";
		
	} elseif($page == "milleniumfesten") {
	
		echo "<div id='big_box'>
			<div id='new_box'>
				<div id='content'>
					<span class='normal'>
						<span style='font-weight: bold;'>Arbetsfördelning till nyårsfesten.</span>
						<br />
						<br />
						Då är det dax att sätta igång att förbereda inför nyårsfirandet. Nu jävlar så........... Den utökade fusionskommittén har verkligen kommit igång så här i seklets elfte timme. Vi har delats upp i olika arbetsgrupper (handplockade). Sammansättningen är enfaldigt utsedd av kommittén och går inte att överklaga.
						<br />
						<br />
						
						<table style='width: 100%;' class='normal'>
						
						<tr>
						<td>Musikgruppen (Blues Brothers)</td>
						<td>Totte E & Totte C</td>
						</tr>
						
						
						<tr>
						<td>Bar-sprit (A-laget)</td>
						<td>Jörgen & Jan</td>
						</tr>
						
						
						<tr>
						<td>Dukning (Bröderna Duk)</td>
						<td>Tommy & Kjell</td>
						</tr>
						
						
						<tr>
						<td>Raket&Bomb (Apollo 19)</td>
						<td>Mats & Totte E</td>
						</tr>
						
						
						<tr>
						<td>Snacks-Godis-Kaffe (Gottegrisarna)</td>
						<td>Jörgen & Jan</td>
						</tr>
						
						
						<tr>
						<td>TV-Video (Studio 4)</td>
						<td>Tommy & Jan</td>
						</tr>
						
						
						<tr>
						<td>Eftermiddagsaktiviteter (Playmates)</td>
						<td>Per & Göran & Ulf</td>
						</tr>
						
						
						<tr>
						<td>Matbeställning</td>
						<td>Jan & BorisMöller</td>
						</tr>
						
						
						<tr>
						<td>Pynt, hattar o sånghäfte.</td>
						<td>Kjell Lönnå & Totte José C (areras)</td>
						</tr>
						
						
						<tr>
						<td>Nattamad (Werner&Werner)</td>
						<td>Mats & Per</td>
						</tr>
						
						
						<tr>
						<td>Transportavdelningen</td>
						<td>Ulf \"Hopenofire\"</td>
						</tr>
						
						
						<tr>
						<td>Städning dan efter avdelning</td>
						<td>Bo \"Moppen\" Nilsson</td>
						</tr>
						
						</table>
						<br />
						Frågor om arbetsuppgifterna till Jan. Kommittén önskar lycka till med era arbetsuppgifter och hälsar att ni ska ligga i utav bara helvete så vi får en blixtrande trevlig avslutning på århundradet.
					</span>
				</div>
			</div>
		</div>";
	
	} else {
	
		echo "<div id='big_box'>
			<span class='normal'>
				else
			</span>
		</div>";
		
	}
	
} else {

	echo "något är fel";
	
}

?>

</body>
</html>