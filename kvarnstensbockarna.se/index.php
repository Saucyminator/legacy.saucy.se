<?php
header('Content-Type: text/html; charset=utf-8');

// Första ÅÄÖ bokstaven försvinner av nån anledning, Årsmöte blir rsmöte etc

require("inc/settings.php");
require($kvarnstensbockarna["Default"]["Conn"]);
require($kvarnstensbockarna["Default"]["Functions"]);

?>
<!DOCTYPE html>
<html lang='sv'>
<head>
<title>Kvarnstensbockarna.se</title>

<meta charset='<?php echo $kvarnstensbockarna["Default"]["MetaCharset"]; ?>' />
<meta name='author' content='<?php echo $kvarnstensbockarna["Default"]["MetaAuthor"]; ?>' />
<meta name='generator' content='<?php echo $kvarnstensbockarna["Default"]["MetaGenerator"]; ?>' />
<meta name='keywords' content='<?php echo $kvarnstensbockarna["Default"]["MetaKeywords"]; ?>' />
<meta name='description' content='<?php echo $kvarnstensbockarna["Default"]["MetaDescription"]; ?>' />
<meta name='copyright' content='<?php echo $kvarnstensbockarna["Default"]["MetaCopyRight"]; ?>' />
<meta name='robots' content='<?php echo $kvarnstensbockarna["Default"]["MetaRobots"]; ?>' />
<meta http-equiv='imagetoolbar' content='<?php echo $kvarnstensbockarna["Default"]["MetaImagetoolbar"]; ?>' />
<link rel="shortcut icon" href="<?php echo $kvarnstensbockarna["Default"]["MetaIcon"]; ?>" type="image/x-icon" />

<?php

require($kvarnstensbockarna["Default"]["CSS"]);

echo "<script src='".$kvarnstensbockarna["Default"]["JS"]["jQuery"]."'></script>";
echo "<script src='".$kvarnstensbockarna["Default"]["JS"]["SimpleTreeMenu"]."'></script>";

?>

<script>
$(document).ready(function(){
    $("#subcontent_wrapper").load("information.php?page=default");
	
    $("a.link_click").on("click",function(e){
    	e.preventDefault();
        $("#subcontent_wrapper").load($(this).attr("id"));
    });
});
</script>
</head>
<body>

<?php


echo "<div id='site_wrapper'>
	<header id='logo'>
		<a href='".$kvarnstensbockarna["Default"]["URL"]."' title='".$kvarnstensbockarna["Default"]["Name"]."'><h1>".$kvarnstensbockarna["Default"]["Name"]."</h1></a>
	</header>
	
	<div id='content_wrapper'>
	
		<nav id='navigation'>
		
			<ul id='treemenu1' class='treeview'>";
			
				foreach($kvarnstensbockarna as $test_key => $test_value) {
				
					switch($test_key) {
						case "Nav":
							foreach($test_value as $key => $value) {
								switch($key) {
									case "Default":
										echo "<li class='li_border'><a id='$value' class='link_click'><span class='normal'>Start</span></a></li>";
										break;
									case "Protokoll":
										echo "<li class='li_border'><a><span class='normal'>$key</span></a>
											<ul>";
											foreach($value as $vkey => $vvalue) {
												switch($vvalue) {
													case "Utdrag_2010":
														echo "<li><a id='information.php?page=".strtolower($vvalue)."' class='link_click'><span class='normali'>Utdrag 2010-2015</span></a></li>";
														break;
													case "Utdrag_2005":
														echo "<li><a id='information.php?page=".strtolower($vvalue)."' class='link_click'><span class='normali'>Utdrag 2005-2010</span></a></li>";
														break;
													case "Utdrag_2000":
														echo "<li><a id='information.php?page=".strtolower($vvalue)."' class='link_click'><span class='normali'>Utdrag 2000-2005</span></a></li>";
														break;
													case "Utdrag_1984":
														echo "<li><a id='information.php?page=".strtolower($vvalue)."' class='link_click'><span class='normali'>Utdrag 1984-2000</span></a></li>";
														break;
													default:
														echo "<li><a id='information.php?page=".strtolower($vvalue)."' class='link_click'><span class='normali'>$vvalue</span></a></li>";
												}
											}
											echo "</ul>
										</li>";
										break;
									case "Bilder":
										echo "<li class='li_border'><a><span class='normal'>$key</span></a>"; // BILDER
											echo "<ul>";
											
												$dir = "image/bilder/";
												foreach(array_reverse(glob("$dir*"),GLOB_ONLYDIR) as $folders) {
												
													/*
													$folders15     = swe__urlencode(utf8_decode($folders));
													echo "#15 : <a href='".$folders15.$rep."'>".$folders15.$rep."</a> : ".mb_detect_encoding($folders15)." : <br />";
													$folders17     = swe__urldecode(utf8_decode($folders));
													echo "#17 : <a href='".$folders17.$rep."'>".$folders17.$rep."</a> : ".mb_detect_encoding($folders17)." : <br />";
													
													echo "#1 : <a href='".$folders.$rep."'>".$folders.$rep."</a> : ".mb_detect_encoding($folders)." : <br />";
													echo "<br />";
													
													$folders2     = urlencode($folders);
													echo "#2 : <a href='".$folders2.$rep."'>".$folders2.$rep."</a> : ".mb_detect_encoding($folders2)." : <br />";
													$folders3     = urldecode($folders);
													echo "#3 : <a href='".$folders3.$rep."'>".$folders3.$rep."</a> : ".mb_detect_encoding($folders3)." : <br />";
													echo "<br />";
													
													$folders4     = mb_convert_encoding(swe__urlencode($folders), "ASCII");
													echo "#4 : <a href='".$folders4.$rep."'>".$folders4.$rep."</a> : ".mb_detect_encoding($folders4)." : <br />";
													$folders5     = mb_convert_encoding(swe__urldecode($folders), "ASCII");
													echo "#5 : <a href='".$folders5.$rep."'>".$folders5.$rep."</a> : ".mb_detect_encoding($folders5)." : <br />";
													echo "<br />";
													
													$folders6     = utf8_encode(swe__urlencode($folders));
													echo "#6 : <a href='".$folders6.$rep."'>".$folders6.$rep."</a> : ".mb_detect_encoding($folders6)." : FUNKAR LOKALT <br />";
													$folders7     = utf8_encode(swe__urldecode($folders));
													echo "#7 : <a href='".$folders7.$rep."'>".$folders7.$rep."</a> : ".mb_detect_encoding($folders7)." : FUNKAR LOKALT <br />";
													$folders8     = utf8_decode(swe__urlencode($folders));
													echo "#8 : <a href='".$folders8.$rep."'>".$folders8.$rep."</a> : ".mb_detect_encoding($folders8)." : <br />";
													$folders9     = utf8_decode(swe__urldecode($folders));
													echo "#9 : <a href='".$folders9.$rep."'>".$folders9.$rep."</a> : ".mb_detect_encoding($folders9)." : <br />";
													echo "<br />";
													
													$folders10     = utf8_encode($folders);
													echo "#10 : <a href='".$folders10.$rep."'>".$folders10.$rep."</a> : ".mb_detect_encoding($folders10)." : FUNKAR LOKALT <br />";
													$folders11     = utf8_decode($folders);
													echo "#11 : <a href='".$folders11.$rep."'>".$folders11.$rep."</a> : ".mb_detect_encoding($folders11)." : <br />";
													echo "<br />";
													
													$folders12     = swe__urlencode($folders);
													echo "#12 : <a href='".$folders12.$rep."'>".$folders12.$rep."</a> : ".mb_detect_encoding($folders12)." : <br />";
													$folders13     = swe__urldecode($folders);
													echo "#13 : <a href='".$folders13.$rep."'>".$folders13.$rep."</a> : ".mb_detect_encoding($folders13)." : <br />";
													echo "<br />";
													*/
													/*
													echo "FOLDERS : <a href='".($folders)."'>".($folders)."</a> <br />";
													echo "FOLDERS_KEY : <a href='".($folders_key)."'>".($folders_key)."</a> <br />";
													echo "FOLDERS : <a href='".urlencode($folders)."'>".urlencode($folders)."</a> <br />";
													echo "FOLDERS_KEY : <a href='".urlencode($folders_key)."'>".urlencode($folders_key)."</a> <br />";
													echo "FOLDERS : <a href='".urldecode($folders)."'>".urldecode($folders)."</a> <br />";
													echo "FOLDERS_KEY : <a href='".urldecode($folders_key)."'>".urldecode($folders_key)."</a> <br />";
													echo "FOLDERS : <a href='".utf8_decode($folders)."'>".utf8_decode($folders)."</a> <br />";
													echo "FOLDERS_KEY : <a href='".utf8_decode($folders_key)."'>".utf8_decode($folders_key)."</a> <br />";
													echo "FOLDERS : <a href='".swe__urlencode($folders)."'>".swe__urlencode($folders)."</a> <br />";
													echo "FOLDERS_KEY : <a href='".swe__urlencode($folders_key)."'>".swe__urlencode($folders_key)."</a> <br />";
													echo "FOLDERS : <a href='".swe__urldecode($folders)."'>".swe__urldecode($folders)."</a> <br />";
													echo "FOLDERS_KEY : <a href='".swe__urldecode($folders_key)."'>".swe__urldecode($folders_key)."</a> <br />";
													*/
													
													/*
													echo "<br />";
													echo $dir;
													echo "<br />";
													echo $folders;
													echo "<br />";
													echo $folders_key;
													echo "<br />";
													*/
													
													//$folders				= swe__urldecode(urlencode($folders));
													//$folders				= utf8_encode(($folders));
													//$folders_key			= basename($folders); // behöver ändra filens namn från ASCII till UTF-8
													
													//$rep = "/Gott & Blandat/Bild 26. Kvarnstensbockarna.jpg";
													//$subrep = "/Bild 26. Kvarnstensbockarna.jpg";
													//$rep = "";
													//$subrep = "";
													
													//$folders     = ($folders);
													//$folders_key = basename($folders);
													
													//$folders14     = swe__urlencode(utf8_encode($folders));
													//echo "#14 : <a href='".$folders14.$rep."'>".$folders14.$rep."</a> : ".mb_detect_encoding($folders14)." : FUNKAR REMOTE <br />";
													//$folders16     = swe__urldecode(utf8_encode($folders));
													//echo "#16 : <a href='".$folders16.$rep."'>".$folders16.$rep."</a> : ".mb_detect_encoding($folders16)." : FUNKAR LOKALT <br />";
													
													$folders_clean = $folders;
													$folders       = swe__urlencode(utf8_encode($folders));
													$folders_key   = basename($folders);
													/*
													if(!empty($kvarnstensbockarna["Local"])) {
														$folders     = utf8_encode($folders);
														$folders_key = basename($folders);
													} else {
														$folders     = swe__urlencode(utf8_encode($folders));
														$folders_key = basename($folders);
													}
													*/
													
													//echo "FOLDERS : <a href='$folders'>$folders</a> : <br />";
													//echo "FOLDERS_KEY : <a href='$folders_key'>$folders_key</a> : <br />";
													/*
													echo "FOLDERS : <a href='$folders_local'>$folders_local</a> : FUNKAR LOKALT : <br />";
													echo "FOLDERS_KEY : <a href='$folders_local_key'>$folders_local_key</a> : <br />";
													echo "FOLDERS : <a href='$folders_remote'>$folders_remote</a> : FUNKAR REMOTE : ANVÄND<br />";
													echo "FOLDERS_KEY : <a href='$folders_remote_key'>$folders_remote_key</a> : ANVÄND<br />";
													*/
													
													echo "<li style='border: 0px;'><a><span class='normali'>".swe__urldecode(utf8_encode($folders_key))."</span></a>";
													
													
													//echo "<li style='border: 0px;'><a><span class='normali'>$folders_key</span></a>"; // Övrigt, 2012, 2011 etc
													echo "<ul>";
													if(is_array(glob("$folders_clean/*",GLOB_ONLYDIR))) {
														foreach(glob("$folders_clean/*",GLOB_ONLYDIR) as $subfolders) {
															if(!is_file($subfolders)) {
																//$subfolders		= utf8_encode($subfolders);
																//$subfolders_key	= basename($subfolders);
																//echo "<li><a id='information.php?page=".eng(strtolower($subfolders_key))."' class='link_click'><span class='normali'>".preg_replace("#[^a-zåäöÅÄÖ &]#i", "", $subfolders_key)."</span></a></li>"; // Gott & Blandat, Möte hos otte E 2012-03-12 etc
																
																$subfolders_clean = $subfolders;
																$subfolders       = swe__urlencode(utf8_encode($subfolders));
																$subfolders_key   = basename($subfolders);
																
																//echo swe__urldecode($subfolders_key);
																//echo "<br />";
																//echo eng(strtolower(swe__urldecode($subfolders_key)));
																
																/*
																echo "<br />";
																echo "<br />";
																echo $q = basename($subfolders)." : ".mb_detect_encoding($q)."<br />";
																echo $w = swe__urlencode(utf8_encode(basename($subfolders)))." : ".mb_detect_encoding($w)."<br />";
																echo $e = swe__urlencode(utf8_decode(basename($subfolders)))." : ".mb_detect_encoding($e)."<br />";
																echo $r = swe__urldecode(utf8_encode(basename($subfolders)))." : ".mb_detect_encoding($r)."<br />";
																echo $t = swe__urldecode(utf8_decode(basename($subfolders)))." : ".mb_detect_encoding($t)."<br />";
																echo $y = utf8_encode(basename($subfolders))." : ".mb_detect_encoding($y)."<br />";
																echo $a = utf8_decode(basename($subfolders))." : ".mb_detect_encoding($a)."<br />";
																echo $u = utf8_encode(swe__urlencode(basename($subfolders)))." : ".mb_detect_encoding($u)."<br />";
																echo $p = utf8_encode(swe__urldecode(basename($subfolders)))." : ".mb_detect_encoding($p)."<br />";
																echo $s = utf8_decode(swe__urlencode(basename($subfolders)))." : ".mb_detect_encoding($s)."<br />";
																echo $i = utf8_decode(swe__urldecode(basename($subfolders)))." : ".mb_detect_encoding($i)."<br />";
																echo $d = swe__urlencode(basename($subfolders))." : ".mb_detect_encoding($d)."<br />";
																echo $f = swe__urldecode(basename($subfolders))." : ".mb_detect_encoding($f)."<br />";
																echo $o = mb_convert_encoding(basename($subfolders), "ASCII")." : ".mb_detect_encoding($o)."<br />";
																echo $g = mb_convert_encoding(basename($subfolders), "UTF-8")." : ".mb_detect_encoding($g)."<br />";
																*/
																
																/*
																if(!empty($kvarnstensbockarna["Local"])) {
																	$subfolders     = utf8_encode($subfolders);
																	$subfolders_key = basename($subfolders);
																} else {
																	$subfolders     = swe__urlencode(utf8_encode($subfolders));
																	$subfolders_key = basename($subfolders);
																}
																*/
																
																echo "<li><a id='information.php?page=".eng(strtolower(swe__urldecode($subfolders_key)))."' class='link_click'><span class='normali'>".preg_replace("#[^a-zåäöÅÄÖ &]#i", "", swe__urldecode($subfolders_key))."</span></a></li>";
																//echo "<li><a id='information.php?page=".eng(strtolower($subfolders_key))."' class='link_click'><span class='normali'>".preg_replace("#[^a-zåäöÅÄÖ &]#i", "", $subfolders_key)."</span></a></li>";
																//echo "<li><a id='information.php?page=".eng(strtolower($subfolders_key))."' class='link_click'><span class='normali'>".preg_replace("#[^a-zåäöÅÄÖ &]#i", "", $subfolders_key)."</span></a></li>"; // Gott & Blandat, Möte hos otte E 2012-03-12 etc
																
																/*
																$subfolders_local      = utf8_encode($subfolders);
																$subfolders_local_key  = utf8_encode(basename($subfolders));
																$subfolders_remote     = swe__urlencode(utf8_encode($subfolders));
																$subfolders_remote_key = swe__urlencode(utf8_encode(basename($subfolders)));
																echo ">> SUBFOLDERS : <a href='$subfolders_local'>$subfolders_local</a> : FUNKAR LOKALT : <br />";
																echo ">> SUBFOLDERS_KEY : <a href='$subfolders_local_key'>$subfolders_local_key</a> : <br />";
																
																echo ">> SUBFOLDERS : <a href='$subfolders_remote'>$subfolders_remote</a> : FUNKAR REMOTE : ANVÄND<br />";
																echo ">> SUBFOLDERS_KEY : <a href='$subfolders_remote_key'>$subfolders_remote_key</a> : ANVÄND<br />";
																*/
																
																
																
															} else {
																//$subfolders		= utf8_encode($subfolders);
																//$subfolders_key	= basename($subfolders);
																echo "bör inte visas";
															}
															//foreach(glob(utf8_decode($dir."$folders_key/$subfolders_key/*")) as $images) {
															//	$images				= utf8_encode($images);
															//	$images_key			= basename($images);
															//}
														}
													}
													/*
													*/
													echo "</ul>";
												}
												
												/*
												$dir = "image/bilder/";
												foreach(array_reverse(glob("$dir*"),GLOB_ONLYDIR) as $folders) {
												
													//$folders				= swe__urldecode(urlencode($folders));
													//$folders				= utf8_encode(($folders));
													//$folders_key			= basename($folders); // behöver ändra filens namn från ASCII till UTF-8
													
													echo "<a href='".($folders)."' style='display: inline;'>".($folders)."</a> : Folders";
													echo "<br />";
													echo "<a href='".utf8_encode($folders)."' style='display: inline;'>".utf8_encode($folders)."</a> : Folders encoded";
													echo "<br />";
													echo "<br />";
													
													$folders_key = basename($folders);
													echo "$folders_key : Folders_key";
													echo "<br />";
													echo utf8_encode(basename($folders))." : Folders_key encoded";
													echo "<br />";
													echo "<br />";
													
													echo mb_detect_encoding($folders)." : Folders detected";
													echo "<br />";
													echo mb_detect_encoding($folders_key)." : Folders_key detected";
													echo "<br />";
													$folders_key = mb_convert_encoding(basename($folders), "UTF-8", "ASCII");
													echo mb_detect_encoding($folders_key)." : Converted Folders_key";
													echo "<br />";
													echo "<br />";
													
													echo "<li style='border: 0px;'><a><span class='normali'>$folders_key</span></a>"; // Övrigt, 2012, 2011 etc
													echo "<ul>";
													//if(is_array(swe__urldecode(urlencode($dir."$folders_key/*")))) {
													//	foreach(glob(swe__urldecode(urlencode($dir."$folders_key/*")),GLOB_ONLYDIR) as $subfolders) {
													if(is_array($dir."$folders_key/*")) {
														foreach(glob($dir."$folders_key/*",GLOB_ONLYDIR) as $subfolders) {
															if(!is_file($subfolders)) {
																//$subfolders		= utf8_encode($subfolders);
																$subfolders_key	= basename($subfolders);
																echo "<li><a id='information.php?page=".eng(strtolower($subfolders_key))."' class='link_click'><span class='normali'>".preg_replace("#[^a-zåäöÅÄÖ &]#i", "", $subfolders_key)."</span></a></li>"; // Gott & Blandat, Möte hos otte E 2012-03-12 etc
															} else {
																//$subfolders		= utf8_encode($subfolders);
																$subfolders_key	= basename($subfolders);
															}
															//foreach(glob(utf8_decode($dir."$folders_key/$subfolders_key/*")) as $images) {
															//	$images				= utf8_encode($images);
															//	$images_key			= basename($images);
															//}
														}
													}
													echo "</ul>";
												}
												*/
											echo "</ul>
										</li>";
										break;
									case "Medlemmar":
										foreach($value as $vkey => $vvalue) {
											switch($vkey) {
												case "Medlem":
													break;
												case "Image":
													break;
												case "ImageHover":
													break;
												default:
													echo "<li class='li_border'><a id='$vvalue' class='link_click'><span class='normal'>$key</span></a></li>";
											}
										}
										break;
									case "Information":
										echo "<li class='li_border'><a><span class='normal'>$key</span></a>
											<ul>";
											foreach($value as $vkey => $vvalue) {
												switch($vvalue) {
													case "Utdrag_2005":
														echo "<li><a id='information.php?page=".strtolower($vvalue)."' class='link_click'><span class='normali'>Utdrag 2005-2010</span></a></li>";
														break;
													case "Utdrag_2000":
														echo "<li><a id='information.php?page=".strtolower($vvalue)."' class='link_click'><span class='normali'>Utdrag 2000-2005</span></a></li>";
														break;
													case "Utdrag_1984":
														echo "<li><a id='information.php?page=".strtolower($vvalue)."' class='link_click'><span class='normali'>Utdrag 1984-2000</span></a></li>";
														break;
													default:
														echo "<li><a id='information.php?page=".strtolower($vvalue)."' class='link_click'><span class='normali'>$vvalue</span></a></li>";
												}
											}
											echo "</ul>
										</li>";
										break;
									//case "Forum":
									//	echo "<li class='li_border'><a href='$value'><span class='normal'>$key</span></a></li>";
									//	break;
									default:
										echo "<li class='li_border'><a id='$value' class='link_click'><span class='normal'>$key</span></a></li>";
								}
							}
							break;
						default:
					}
				}
				
			echo "</ul>

			<script>
				ddtreemenu.createTree('treemenu1', true)
			</script>

		</nav>
		
		<div id='space'></div>
		
		<section id='subcontent_wrapper'></section>
	</div>
	
	<div id='updates'>
	<div id='updates_title'>
		<span class='big'>
			Updates
		</span>
	</div>
	<div id='updates_text'>
		<span class='small'>
			".bbkod($kvarnstensbockarna["Updates"])."
		</span>
	</div>
</div>
	
	<footer id='copyright'>
		".bbkod($copyright["Content"])."
	</footer>
</div>";

echo "";



?>

</body>
</html>