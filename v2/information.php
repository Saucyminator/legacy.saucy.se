<?php

require("inc/settings.php");
require($saucy["Default"]["Conn"]);
require($saucy["Default"]["Functions"]);
//require_once($saucy["Default"]["SubmitRating"]);

$page	= isset($_GET["page"]) ? $_GET["page"] : NULL;
$pagelist_amount = count($informationlist);
if(!in_array($page, $informationlist)) {

	echo "You are trying to access a page that does not exist.";
	exit;
	
}

?>
<!DOCTYPE html>
<html lang='sv'>
<head>
<title></title>

<?php

echo "<script src='".$saucy["Default"]["JS"]["jQuery"]."'></script>";
//echo "<script src='".$saucy["Default"]["JS"]["MaxRating"]."'></script>";
echo "<script src='".$saucy["Default"]["JS"]["TimeAgo"]."'></script>";
echo "<script src='".$saucy["Default"]["JS"]["Qtip"]."'></script>";

?>

<script>
$(document).ready(function(){
    jQuery(".timeagoInformation").timeago();
    jQuery.timeago.settings.allowFuture = true;
	
    $(".showInformation").bind("click",function() {
        $("."+this.id ).stop(true, true).slideToggle("<?php echo $saucy["Default"]["ToggleSpeed"]; ?>");
    });
	
	$(".tooltipInformation, .tooltipMenu").each(function() {
		$(this).qtip({
			content: {
				text: $(this).attr("title"),
			},
			position: {
				target: "mouse",
				adjust: { x: 15, y: 0, },
			},
			style: {
				tip: false
			},
			show: {
				delay: 0
			},
			hide: {
				delay: 200
			}
		});
	});
});
</script>

</head>
<body>

<?php

$parent = str_replace(".php", "", strtolower(basename($_SERVER["PHP_SELF"])));

include("subMenu.php");

// -------------------------------------------------------------------------------------------------------------------

if($page == "cookies" OR $page == "about" OR $page == "statistics" OR $page == "askmeanything" OR $page == "contact") {

	echo "<div class='box'>
		<div class='text big'>
			<a id='$page' class='showInformation tooltipInformation' title='".bbkod($saucy["Panel"][ucfirst($page)]["Locale"]["TitleHover"])."'>
				.: ".bbkod($saucy["Panel"][ucfirst($page)]["Locale"]["Title"])." :.
			</a>
		</div>
		<div class='$page'>
			<div class='spacer'></div>
			<div class='text' style='text-align: justify;'>";
			
				// FIX #################################################################
				if($page == "contact") {
			
					echo "<div style='margin-top: 3px; width: 100%;'>
						<div style='width: 30%; float: left; text-align: right;'>
							<div style='margin-bottom: 20px;'><span class='big'>Name:</span><br /><span class='smalli' style='float: right;'>Required</span></div>
							<div style='margin-bottom: 20px;'><span class='big'>Email:</span><br /><span class='smalli' style='float: right;'>Required</span></div>
							<div style='margin-bottom: 20px;'><span class='big'>Website:</span><br /><span class='smalli' style='float: right;'>Optional</span></div>
							<div style='margin-bottom: 34px;'><span class='big'>Category:</span></div>
							<div style='margin-bottom: 32px;'><span class='big'>How you found me:</span><br /><span class='smalli' style='float: right;'>Optional</span></div>
							<div style='margin-bottom: 22px;'><span class='big'>Subject:</span><br /><span class='smalli' style='float: right;'>Required</span></div>
							<div style='margin-bottom: 127px;'><span class='big'>Message:</span><br /><span class='smalli' style='float: right;'>Required</span></div>
							<div style='margin-bottom: 6px;'><span class='big'>Captcha:</span><br /><span class='smalli' style='float: right;'>What is 10 + 20?</span></div>
						</div>
						<div style='width: 65%; float: right; margin-top: 2px;'>
						<form>
							<div style='margin-bottom: 24px; padding-right: 4px;'><input type='text' class='form' /></div>
							<div style='margin-bottom: 24px; padding-right: 4px;'><input type='text' class='form' /></div>
							<div style='margin-bottom: 24px; padding-right: 4px;'><input type='text' class='form' /></div>
							<div style='margin-bottom: 24px; padding-right: 2px;'>
								<select class='form' style='height: 24px; font-size: 15px;'>
									<optgroup label='My message is about the websites:'>
										<option value=''>Content</option>
										<option value=''>Design (graphics)</option>
										<option value=''>Layout</option>
										<option value='' selected=''>Other..</option>
									</optgroup>
									<optgroup label='Report anything broken:'>
										<option value=''>I found a spelling error(s)</option>
										<option value=''>One/many of your links are broken</option>
										<option value=''>Your website looks weird on my web browser</option>
									</optgroup>
									<optgroup label='Miscellaneous:'>
										<option value=''>I like your website because..</option>
										<option value=''>I dislike your website because..</option>
									</optgroup>
								</select>
							</div>
							<div style='margin-bottom: 36px; padding-right: 4px;'><input type='text' class='form' /></div>
							<div style='margin-bottom: 24px; padding-right: 4px;'><input type='text' class='form' /></div>
							<div style='margin-bottom: 22px; padding-right: 6px;'><textarea style='height: 125px;' class='form'></textarea></div>
							<div style='margin-bottom: 6px; margin-right: -3px;'><input type='text' class='form' style='width: 35%;' /><input type='submit' class='submit' value='Submit' style='float: right;' /></div>
							<input type='hidden' name='ip' value='' />
						</form>
						</div>                        
					</div>
					<div style='clear: both;'></div>";
				
				} else {
				
					echo "<span class='normal'>".
						bbkod($saucy["Panel"][ucfirst($page)]["Content"])
					."</span>";
					
				}
				
			echo "</div>
		</div>
	</div>";
	
} elseif($page == "link") {

	echo "<div class='box'>";
	
		// Hämta alla rader du vill ha med.
    $sql = "SELECT link_category, link_title, link_url
		FROM {$saucy["DevDB"]["Name"][$page]}
		ORDER BY link_category ASC";
    $stmt = $conn->prepare($sql);
    $stmt->execute();

		// Skapa en tom array för att lagra resultatet i
		$tbl_array = array();
		
		// Loopa igenom alla rader i PHP
		while ($row = $stmt->fetch()) {

			// Lägg dem i en multidimensionell array med datumet som nyckel i första ledet.
			// Hämta datumet som vi skall använda som "nyckel"
			$category = $row['link_category'];
			// Lägg den här raden i en array för datumet
			$tbl_array[$category][] = $row;
			
		// Avsluta loopen
		}
		
		$l = 0;
		$len = count($tbl_array);
		
		// För att skriva ut loopar du igenom arrayen (så du får grupperna) och sedan loopar du igenom gruppens rader (så du får fonderna).
		foreach($tbl_array as $category => $rader) {
		
			echo "<div"; if($l != $len - 1) { echo " style='margin-bottom: ".($box_pad*2)."px;'"; } $l++; echo ">
				<div class='text big'>
					$category
				</div>
				<div class='spacer' style='margin-bottom: ".($box_pad*2)."px;'></div>";
				
				foreach($rader as $rad) {
					echo "<a href='".$rad['link_url']."' target='_blank' class='tooltipInformation' title='".bbkod($rad['link_title'])." : ".$rad['link_url']."'>
						<div class='linkz small'>
							<div style='float: left; color: ".$saucy["Default"]["Color"]["Light"].";'>.:</div>
							<div style='float: right; color: ".$saucy["Default"]["Color"]["Light"].";'>:.</div>
							<div style='text-align: center;'>".bbkod($rad['link_title'])."</div>
						</div>
					</a>";
					
				}
				
			echo "</div>";
			
		}
		
	echo "</div>";

} elseif($page == "ip") {

	//$saucy["Default"]["MetaKeywords"]		= "SAUCY.SE, Saucy, Check, External, IP, What, Is, What's, My";
	//$saucy["Default"]["MetaDescription"]	= "Saucy.se - Check your external IP! - Saucy, the geeky gamer who loves games, music, computers, web design and design overall!";

	//<style>
	//body, html {
	//	height: 50%;
	//}
	//</style>

	//</head>
	//<body>
	
	echo "<div id='indexContent'>
		<div class='big' style='font-weight: bold; font-style: italic;'>
			Your IP is:
		</div>
		<div class='IP'>
			".getIP()."
		</div>
	</div>";
	
} elseif($page == "text_capitalizer") {

	//$saucy["Default"]["MetaKeywords"]		= "SAUCY.SE, Saucy, Text, Capitalizer, Capitalize";
	//$saucy["Default"]["MetaDescription"]	= "Saucy.se - Capitalize your text! - Saucy, the geeky gamer who loves games, music, computers, web design and design overall!";
	
	echo "<div id='content_wrapper'>
		<div class='box' style='margin: 10px 0px;'>
			<div class='text'>
				<span class='huge'>
					Text capitalizer
				</span>
			</div>
		</div>
		<div class='box' style='width: 471px; margin: 0px; padding: $boxpad"."px; float: left;'>
			<form action='".basename($_SERVER["PHP_SELF"])."' method='post'>		
				<div style='margin-bottom: ".($boxpad*2)."px; padding-right: $boxpad"."px;'><textarea class='form' name='text' cols='1' style='width: 100%; height: ".(24*5)."px;'></textarea></div>
				<input type='submit' class='submit' value='Submit' />			
			</form>
		</div>
		<div class='box' style='width: 471px; margin: 0px 0px 0px 10px; padding: $boxpad"."px; float: left; text-align: center;'>
			<span class='normal'>
				Text will show up here: <br />
				<div class='spacer'></div>
				".mb_strtoupper(bbkod($_POST['text']))."
			</span>
		</div>
	</div>";
	
} elseif($page == "word_counter") {

	//$siteMeta_keywords = "SAUCY.SE, Saucy, Text, Word, Count";
	//$siteMeta_description = "Saucy.se - Count the words in your text! - Saucy, the geeky gamer who loves games, music, computers, web design and design overall!";
	
	echo "<div id='content_wrapper'>
		<div class='box' style='margin: 10px 0px; text-align: center;'>
			<span class='huge'>
				Word counter
			</span>
		</div>
		<div class='box' style='width: 471px; margin: 0px; padding: 6px; float: left;'>
			<form action='".basename($_SERVER["PHP_SELF"])."' method='post'>		
				<div style='margin-bottom: 12px; padding-right: 6px;'><textarea class='form' name='text' cols='1' style='width: 100%; height: ".(24*5)."px;'></textarea></div>
				<input type='submit' class='submit' value='Submit' />			
			</form>
		</div>
		<div class='box' style='width: 471px; margin: 0px 0px 10px 10px; padding: 6px; float: left; text-align: center;'>
			<span class='normal'>
				Number of words: '".count_words($_POST["text"])." (".mb_strlen(trim($_POST["text"]))." characters)
			</span>
		</div>
		<div class='box' style='width: 471px; margin: 0px 0px 0px 10px; padding: 6px; float: left; text-align: center;'>
			<span class='normal'>Text inputted:<br />
				<div class='spacer'></div>
				".$_POST["text"]."
			</span>
		</div>
	</div>";
	
} elseif($page == "personality") {

	/*
	<div id='content_wrapper'>

		<div class="box" style="margin: 10px 0px;">
			<div class='text'>
				<span class="huge">
					Personality
				</span>
			</div>
		</div>
		
		<div id='conten_left'>
		
			<div class='box'>
				test
			</div>
		
		</div>
		
		<div id='content_right'>
		
			<div class='box'>
				test
			</div>
			
		</div>
		
		<div class="box" style="width: 471px; margin: 0px; padding: <?php echo $boxpad; ?>px; float: left;">
			<form action="<?php echo basename($_SERVER["PHP_SELF"]); ?>" method="post">		
				<div style="margin-bottom: <?php echo ($boxpad*2); ?>px; padding-right: <?php echo $boxpad; ?>px;"><textarea class="form" name="text" cols="1" style="width: 100%; height: <?php echo (24*5); ?>px;"></textarea></div>
				<input type="submit" class="submit" value="Submit" />			
			</form>
		</div>
		
		<div class="box" style="width: 471px; margin: 0px 0px 0px 10px; padding: <?php echo $boxpad; ?>px; float: left; text-align: center;">
			<span class="normal">Text will show up here: <br />
			<div class="spacer"></div>
			<?php
			
			echo mb_strtoupper(bbkod($_POST['text']));
			
			?>
			</span>
		</div>
	</div>
	/*
	*/
	
	$wrap = $saucy["Default"]["Width"];
	$left = (($saucy["Default"]["Width"]/2)-5);
	$right = (($saucy["Default"]["Width"]/2)-5);
	
	// COLUMNS ALL
	if(!empty($saucy["DevShow"]["Column"]["All"])) {
	
		echo "<div id='content_wrapper' style='background: green;'>";
		
		echo "<div class='box' style='margin: 10px 0px 10px 0px;'>
			<div class='text'>
				<span class='big'>
					Personality
				</span>
			</div>
		</div>";
		
			// POSITIVE
			if(!empty($saucy["DevShow"]["Column"]["Left"])) {
			
				echo "<div id='content_left' style='width: $left"."px; background: red;'>
				$left";
				
				// POSITIVE POSTS
				if(!empty($saucy["Panel"]["Webmaster"]["Show"])) {
				
					echo "<div class='box'>
						<div class='text'>
							<span class='big'>
								<a id='webmaster' class='showMore' style='color: ".$saucy["Default"]["Color"]["Light"].";' title='".bbkod($saucy["Panel"]["Webmaster"]["Locale"]["TitleHover"])."'>
									POSITIVE POSTS
								</a>
							</span>
						</div>
						<div class='webmaster' "; if(empty($saucy["Panel"]["Webmaster"]["Enabled"])) { echo "style='display: none;'"; } echo ">
							<div class='spacer'></div>
							<div class='text' style='text-align: justify;'>
								<span class='normal'>
									".bbkod($saucy["Panel"]["Webmaster"]["Content"])."
								</span>
							</div>
						</div>
					</div>";
					
				}
				
				// POSITIVE ADD
				if(!empty($saucy["Panel"]["Webmaster"]["Show"])) {
				
					echo "<div class='box'>
						<div class='text'>
							<span class='big'>
								<a id='webmaster' class='showMore' style='color: ".$saucy["Default"]["Color"]["Light"].";' title='".bbkod($saucy["Panel"]["Webmaster"]["Locale"]["TitleHover"])."'>
									POSITIVE ADD
								</a>
							</span>
						</div>
						<div class='webmaster' "; if(empty($saucy["Panel"]["Webmaster"]["Enabled"])) { echo "style='display: none;'"; } echo ">
							<div class='spacer'></div>
							<div class='text' style='text-align: justify;'>
								<span class='normal'>
									".bbkod($saucy["Panel"]["Webmaster"]["Content"])."
								</span>
							</div>
						</div>
					</div>";
					
				}
				
				echo "</div>";
				
			}
			
			// NEGATIVE
			if(!empty($saucy["DevShow"]["Column"]["Right"])) {
			
				echo "<div id='content_right' style='width: $right"."px; background: orange;'>
				$right";
				
				// NEGATIVE POSTS
				if(!empty($saucy["Panel"]["Webmaster"]["Show"])) {
				
					echo "<div class='box'>
						<div class='text'>
							<span class='big'>
								<a id='webmaster' class='showMore' style='color: ".$saucy["Default"]["Color"]["Light"].";' title='".bbkod($saucy["Panel"]["Webmaster"]["Locale"]["TitleHover"])."'>
									NEGATIVE POSTS
								</a>
							</span>
						</div>
						<div class='webmaster' "; if(empty($saucy["Panel"]["Webmaster"]["Enabled"])) { echo "style='display: none;'"; } echo ">
							<div class='spacer'></div>
							<div class='text' style='text-align: justify;'>
								<span class='normal'>
									".bbkod($saucy["Panel"]["Webmaster"]["Content"])."
								</span>
							</div>
						</div>
					</div>";
					
				}
				
				// NEGATIVE ADD
				if(!empty($saucy["Panel"]["Webmaster"]["Show"])) {
				
					echo "<div class='box'>
						<div class='text'>
							<span class='big'>
								<a id='webmaster' class='showMore' style='color: ".$saucy["Default"]["Color"]["Light"].";' title='".bbkod($saucy["Panel"]["Webmaster"]["Locale"]["TitleHover"])."'>
									NEGATIVE ADD
								</a>
							</span>
						</div>
						<div class='webmaster' "; if(empty($saucy["Panel"]["Webmaster"]["Enabled"])) { echo "style='display: none;'"; } echo ">
							<div class='spacer'></div>
							<div class='text' style='text-align: justify;'>
								<span class='normal'>
									".bbkod($saucy["Panel"]["Webmaster"]["Content"])."
								</span>
							</div>
						</div>
					</div>";
					
				}
				
				echo "</div>";
				
			}
			
		echo "</div>";
		
	}
	
}

?>

</body>
</html>