<?php

require("inc/settings.php");
require($saucy["Default"]["Conn"]);
require($saucy["Default"]["Functions"]);
//require_once($saucy["Default"]["SubmitRating"]);

if(!empty($saucy["Default"]["Reload"])) {
	echo $saucy["Default"]["Reload"];
}

/*
// Function to strip out all non asci characters
function strip_utf($string) {
  for($i = 0; $i < strlen ($string); $i++)
    if (ord($string[$i]) > 127)
      $string[$i] = "";
  return $string;
}

echo strip_utf("¯\_(ツ)_/¯");
*/
/*	FILE STRUCTURE

.: /INC :.
conn
functions
quotes
settings

.: /CSS :.
body
jquery.qtip.css
jquery.qtip.min.css

.: /JS :.
jquery.qtip.js
jquery.qtip.min.js
jquery.timeago.js


.: ARCHIVE :.
blog
review
tutorial
portfolio
list
poll
download


archive_blog
archive_reviews
archive_tutorials
archive_portfolio
archive_list
archive_poll
archive_code
archive_config
archive_map
archive_model
archive_video
archive_wallpaper
archive_question

.: INFO :.
info_blog
info_reviews
info_tutorials
info_portfolio
info_list
-- info_poll (inte fixad än)
info_code
info_config
info_map
info_model
info_video
info_wallpaper


.: FILES :.
.htaccess
about
askmeanything
contact
links
subMenu
statistics
cookies

blog
reviews
tutorials
portfolio
downloads
list
poll

.: Comments :.
getItemsFromDatabase.php
indexComment.php
leaveComment.php
conn.php >> define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']));
loader.gif

.: DB :.
list
list_content
-- poll
category
blog
reviews

.: PARENT :.
$search			= array("archive_", "info_", ".php");			// ord som ska hittas
$string			= strtolower(basename($_SERVER["PHP_SELF"]));	// leta i $string; dvs denna filen (basename($_SERVER["PHP_SELF"]))
$parent			= str_replace($search, "", $string);			// hitta orden i $search (archive_, info_, .php) -> ersätt med inget ("") -> leta efter orden från $search i $string (basename($_SERVER["PHP_SELF"]))
$parent_extra	= str_replace(".php", "", $string);				// leta efter (.php) -> ersätt med inget ("") -> i stringen (strtolower(basename($_SERVER["PHP_SELF"])))

*/

// QUOTES NOT USED
$quotes_file			= file_get_contents($saucy["Default"]["Quotes"]);
$quotes_lines			= explode("\n", $quotes_file); //Use \r\n Instead of \n if used on Windows
$quotes_lines_amount	= count($quotes_lines);
$quotes_text			= $quotes_lines[rand(0,$quotes_lines_amount-1)];


$dev_text = array_map("bbkod", $dev_text);

foreach($saucy["DevSymbol"] as $symbol => $class) {
    $dev_text = preg_replace("#(\\" . $symbol . ".*?(<br \/>|$))#", "<span class='normal dev_text_" . $class. "'>$1</span>", $dev_text);
}

?>
<!DOCTYPE html>
<html lang='sv'>
<head>
<title>.: Saucy.se v<?php echo $saucy["Default"]["Version"]; ?> :.</title>

<meta charset='<?php echo $saucy["Default"]["MetaCharset"]; ?>' />
<meta name='author' content='<?php echo $saucy["Default"]["MetaAuthor"]; ?>' />
<meta name='generator' content='<?php echo $saucy["Default"]["MetaGenerator"]; ?>' />
<meta name='keywords' content='<?php echo $saucy["Default"]["MetaKeywords"]; ?>' />
<meta name='description' content='<?php echo $saucy["Default"]["MetaDescription"]; ?>' />
<meta name='copyright' content='<?php echo $saucy["Default"]["MetaCopyRight"]; ?>' />
<meta name='robots' content='<?php echo $saucy["Default"]["MetaRobots"]; ?>' />
<meta http-equiv='imagetoolbar' content='<?php echo $saucy["Default"]["MetaImagetoolbar"]; ?>' />
<link rel="shortcut icon" href="<?php echo $saucy["Default"]["MetaIcon"]; ?>" type="image/x-icon" />

<?php

//<link href='http://fonts.googleapis.com/css?family=Ubuntu+Mono|Droid+Sans+Mono|Open+Sans|Yanone+Kaffeesatz|Ubuntu|Lato|Molengo|Nobile|News+Cycle|Maven+Pro|Terminal+Dosis|Francois+One|Istok+Web|Ubuntu+Condensed|Orbitron|Geo|Jockey+One|Signika' rel='stylesheet' type='text/css'>

//$font = "Segoe UI";
//$font = "Helvetica";
//$font = "Verdana";
//$font = "Consolas";
//$font = "Trebuchet MS";

//$font = "Ubuntu Mono";
///$font = "Droid Sans Mono";
//$font = "Open Sans";
///$font = "Yanone Kaffeesatz";
//$font = "Ubuntu";
///$font = "Lato";
///$font = "Molengo";
///$font = "Nobile";
///$font = "News Cycle";
//$font = "Maven Pro";
///$font = "Terminal Dosis";
///$font = "Francois One";
//$font = "Istok Web";
///$font = "Ubuntu Condensed";
///$font = "Orbitron";
///$font = "Geo";
///$font = "Jockey One";
///$font = "Signika";

require($saucy["Default"]["CSS"]);

echo "<script src='".$saucy["Default"]["JS"]["jQuery"]."'></script>";
//echo "<script src='".$saucy["Default"]["JS"]["MaxRating"]."'></script>";
echo "<script src='".$saucy["Default"]["JS"]["TimeAgo"]."'></script>";
echo "<script src='".$saucy["Default"]["JS"]["Qtip"]."'></script>";

?>

<script>
$(document).ready(function(){
    $("#html").load("sector.php?page=blog");
	
	$(document).on("click","a.link_click",function(event){
        event.preventDefault();
        $("#html").load($(this).attr("id"));
    });
	
    jQuery(".timeago").timeago();
    jQuery.timeago.settings.allowFuture = true;
	
    $(".show").bind("click",function() {
        $("."+this.id ).stop(true, true).slideToggle("<?php echo $saucy["Default"]["ToggleSpeed"]; ?>");
    });
	
	$(".tooltip, .tooltipMenu").each(function() {
		$(this).qtip({
			content: {
				text: $(this).attr("title"),
			},
			position: {
				target: "mouse",
				adjust: { x: 15, y: 0, },
			},
			style: {
				tip: false
			},
			show: {
				delay: 0
			},
			hide: {
				delay: 200
			}
		});
	});
});
</script>

</head>
<body>

<?php
/*
CSS
text-shadow: rgb(0, 0, 0) 1px 1px 0px;
color: white;

//$hover = "image/icon/social_hover.png";
//$image = "/saucy/old/image/main/portfolio/Protoss_img.png";
//echo "<img style='background:url($image)' src='$hover' />";


while (list($var, $val) = each($array)) {
	print "$var is $val\n";
}
*/


/*
function maxLen($input) {
    $length = 0;
    foreach($input as $entry)
        if(strlen($entry[0])>$length) $length = strlen($entry[0]);
    return $length;
}
$len = maxLen($array);
foreach( $array as $entry ) {
    $data .=  sprintf("%-$len"."s\t%s\n", $entry[0], $entry[1]);
}
echo "<pre>$data</pre>";

#!/usr/bin/env python2
import sys
def wadsworth(original): 
if len(sys.argv) != 0: phrase = ’ ’.join(original[1:])
else: phrase = original
output = phrase[int(len(phrase)-len(phrase)*.70):]
return output
if name == ‘main’:
print wadsworth(sys.argv)



$files = array(
	"archive.php",
	"index.php",
);

$files_count = count($files);
$lines = 0;
$i=1;
while($i <= $files_count) {
	echo "The number is " . $i . "<br />";
	$lines = count(file($files[$files_count]));
	$i++;
}

//$lines = count(file($files));
echo "There are $lines lines in $files_count files"; 
*/


// DEV TODO
if(!empty($saucy["DevEnabled"]["Todo"]) AND !empty($dev_text["Todo"])) {

	echo "<div class='dev' style='left: 0px; top: ".$saucy["DevEnabled"]["Top"]."px;'>
		<div class='text big'>
			<a id='todo' class='show'>
				Todo list:
			</a>
		</div>
		<div class='todo'>
			<div class='spacer'></div>
			<div class='text normal' style='text-align: justify;'>
				".$dev_text["Todo"]."
			</div>
		</div>
	</div>";
	
}
// DEV DONE
if(!empty($saucy["DevEnabled"]["Done"]) AND !empty($dev_text["Done"])) {

	echo "<div class='dev' style='right: 0px; top: ".$saucy["DevEnabled"]["Top"]."px;'>
		<div class='text big'>
			<a id='done' class='show'>
				Completely done:
			</a>
		</div>
		<div class='done'>
			<div class='spacer'></div>
			<div class='text normal' style='text-align: justify;'>
				".$dev_text["Done"]."
			</div>
		</div>
	</div>";
	
}
// DEV INFO
if(!empty($saucy["DevEnabled"]["Info"]) AND !empty($dev_text["Info"])) {

	echo "<div class='dev' style='position: fixed; width: 100%; bottom: 0px; z-index: 100;'>
		<div class='text normal'>
			".$dev_text["Info"]."
		</div>
	</div>";
	
}

// NAV
if(!empty($saucy["DevShow"]["Nav"])) {

	echo "<div id='content_nav_background'>
		<div id='content_nav'>";
		
			foreach($nav as $nav_key => $nav_value) {
			
				echo "<div class='nav'>";
				
				echo "<div class='huge' style='display: block;'>
					<a ";
					
					if($nav_key == "Forum") {
					
						echo "href='$nav_value' target='_blank' class='tooltip' style='color: ".$saucy["Default"]["Color"]["Light"].";'"; // style:color behövs här
						
					} elseif($nav_key == "About" OR $nav_key == "Contact") {
					
						echo "id='information.php?page=$nav_value' class='link_click tooltip'";
						
					} else {
					
						echo "id='sector.php?page=$nav_value' class='link_click tooltip'";
						
					}
					
					echo " title='$nav_key'>".
						$nav_key
					."</a>
				</div>";
				
				switch($nav_key) {
					case "Forum":
						break;
					case "About":
						echo "<span class='tiny'>
							<a id='information.php?page=link' class='link_click tooltipMenu' title='Collection of useful links'>
								Useful links
							</a>
						</span>";
						break;
					case "Contact":
						echo "<span class='tiny'>
							<a id='information.php?page=askmeanything' class='link_click tooltipMenu' title='Ask me anything questions'>
								Ask me anything
							</a>
						</span>";
						break;
						//isset($array($key))
					default:
						echo "<span class='tiny'>
							<a id='archive.php?page=";
							
							if($nav_key == "Reviews" OR $nav_key == "Tutorials" OR $nav_key == "Downloads") {
								$nav_key = substr($nav_key, 0, -1);
							}
							
							echo strtolower($nav_key);
							
							echo "' class='link_click tooltipMenu' title='".ucfirst($nav_key)." archive'>
								$nav_key archive
							</a>
						</span>";
					
				}
				
				echo "</div>";
				
			}
			
		echo "</div>
	</div>";
	
}

// SOCIAL ALL
if(!empty($saucy["DevShow"]["Social"]["All"])) {

	echo "<div id='content_social'>";
	
		// ICONS
		if(!empty($saucy["DevShow"]["Social"]["Icons"])) {
		
			foreach($social_icon as $social_icon_key => $social_icon_value) {
			
				if(!empty($social_icon_value["Enabled"])) {
				
					echo "<div class='social'"; if($social_index+1 == $social_amount) { echo "style='margin-right: ".$social_space."px;'"; } echo ">
						<a href='".$social_icon_value["URL"]."' target='_blank' class='tooltip' title='".$social_icon_value["Desc"]; if($social_icon_key != "E-mail") { echo " $social_icon_key"; } echo "'>
							<img src='$folder_icon/".lcfirst($social_icon_key).".gif' class='social_icon' />
							<img class='hoverimage' />
						</a>
					</div>";
					
					$social_index++;
					
				}
				
			}
			
		}
		
		// STATISTICS
		if(!empty($saucy["DevShow"]["Social"]["Statistics"])) {
		
			foreach($social_info as $social_info_key => $social_info_value) {
			
				if(!empty($social_info_value["Enabled"])) {
				
					echo "<div class='social'>
						<a id='information.php?page=".$social_info_value["URL"]."' class='link_click tooltip' title='".$social_info_value["Desc"]."'>
							<img src='$folder_icon/".lcfirst($social_info_key).".gif' class='social_icon' />
							<img class='hoverimage' src='$social_hover' />
						</a>
					</div>";
					
				}
				
			}
			
		}
		
		// SEARCH
		if(!empty($saucy["DevShow"]["Social"]["Search"])) {
		
			echo "<div id='search'>";
		
				echo "<div class='box' style='height: $social"."px; padding: 0px; margin: 0px; float: left;'>
					<form method='post' action='' id='searchform'>
					<input name='search' type='search' class='searchfield' results='5' placeholder='".bbkod($saucy["Default"]["Locale"]["SearchPlaceholder"])."' />
				</div>
				
				<div class='box' style='height: $social"."px; padding: 0px; margin-left: 3px; float: left;'>
					<input name='submit' type='submit' class='search tooltip' value='Search' title='".bbkod($saucy["Default"]["Locale"]["Search"])."' disabled='disabled' />
					</form>
				</div>";
			
			echo "</div>";
			
		}
		
	echo "</div>";
	
}

// COLUMNS ALL
if(!empty($saucy["DevShow"]["Column"]["All"])) {

	echo "<div id='content_wrapper'>";
	
		// COLUMN LEFT
		if(!empty($saucy["DevShow"]["Column"]["Left"])) {
		
			echo "<div id='content_left'>";
			
				// WEBMASTER
				if(!empty($saucy["Panel"]["Webmaster"]["Enabled"])) {
				
					echo "<div class='box'>
						<div class='text big'>
							<a id='webmaster' class='show tooltip' title='".bbkod($saucy["Panel"]["Webmaster"]["Locale"]["TitleHover"])."'>
								".$saucy["Panel"]["Webmaster"]["Locale"]["Title"]."
							</a>
						</div>
						<div class='webmaster' "; if(empty($saucy["Panel"]["Webmaster"]["Show"])) { echo "style='display: none;'"; } echo ">
							<div class='spacer'></div>
							<div class='text normal' style='text-align: justify;'>
								".bbkod($saucy["Panel"]["Webmaster"]["Content"])."
							</div>
						</div>
					</div>";
					
				}
				
				// LIST
				if(!empty($saucy["Panel"]["List"]["Enabled"])) {

          $sql = "SELECT list_id, list_title, date
					FROM {$saucy["DevDB"]["Name"]["list"]}
					ORDER BY date DESC
					LIMIT {$saucy["Panel"]["List"]["Limit"]["Articles"]}";
          $stmt = $conn->prepare($sql);
          $stmt->execute();
          while($row_list_index = $stmt->fetch()) {

						$list_id_index		= $row_list_index["list_id"];
						$list_title_index	= bbkod($row_list_index["list_title"]);
						$list_date_index	= $row_list_index["date"];

						// LIST COMMENTS
            $sql = "SELECT comment_parent_id, comment_parent, comment_approved
            FROM {$saucy["DevDB"]["Name"]["comment"]}
            WHERE comment_parent_id = :id AND comment_parent = 'list' AND comment_approved = 1
            ORDER BY date DESC";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id', $list_id_index, PDO::PARAM_INT);
            $stmt->execute();
            $row_list_comments_amount_index = $stmt->rowCount();

						echo "<div class='box'>
							<div class='text big'>
								<a id='list' class='show tooltip' title='".bbkod($saucy["Panel"]["List"]["Locale"]["TitleHover"])."'>".
									$saucy["Panel"]["List"]["Locale"]["Title"].$list_title_index
								."</a>
							</div>
							<div class='list'"; if(empty($saucy["Panel"]["List"]["Show"])) { echo "style='display: none;'"; } echo ">
								<div class='spacer'></div>
								<div class='content_small smalli'>
									[ <span class='timeago tooltip' title='$list_date_index'>".date('H:i @ jS \of F Y', strtotime($list_date_index))."</span> ]
								</div>";
								
								// LIST CONTENT
								$list_counter_index = 1;
                $sql = "SELECT list_content_id, list_content_title, list_content_text
								FROM {$saucy["DevDB"]["Name"]["listcontent"]}
								WHERE list_content_parent_id = :id
								ORDER BY list_content_id ASC
								LIMIT {$saucy["Panel"]["List"]["Limit"]["Posts"]}";
                $stmt = $conn->prepare($sql);
                $stmt->bindParam(':id', $list_id_index, PDO::PARAM_INT);
                $stmt->execute();
                while ($row_list_content_index = $stmt->fetch()) {

									$list_content_id_index		= $row_list_content_index["list_content_id"];
									$list_content_title_index	= $row_list_content_index["list_content_title"];
									$list_content_text_index	= bbkod($row_list_content_index["list_content_text"]);
									
									echo "<span class='normal tooltip' title='$list_content_text_index'>
										<div class='linkz'>
											#$list_counter_index : ".truncate_str($list_content_title_index, $saucy["Panel"]["List"]["Limit"]["Text"])."
										</div>
									</span>";
									
									$list_counter_index++;
									
								}
								
								echo "<div class='content_small smalli' style='text-align: center; line-height: 0%;'>
									[ <a href='' id='article.php?page=list&id=$list_id_index' class='link_click tooltip' title='".bbkod($saucy["Default"]["Locale"]["CommentsRead"])."'>$row_list_comments_amount_index ".$saucy["Default"]["Locale"]["Comments"]."</a> | <a href='' id='archive.php?page=list' class='link_click tooltip' title='".bbkod($saucy["Panel"]["List"]["Locale"]["TextHover"])."'>".$saucy["Panel"]["List"]["Locale"]["Text"]."</a> ]
								</div>
							</div>
						</div>";
						
					}
					
				}
				
				// POLL
				if(!empty($saucy["Panel"]["Poll"]["Enabled"])) {
				
					$poll_id_index = 1;
					
					echo "<div class='box'"; if(!empty($saucy["DevEnabled"]["Info"]) AND !empty($dev_text["Info"])) { echo "style='margin-bottom: 81px;'"; } echo ">
						<div class='text big'>
							<a id='poll' class='show tooltip' title='".bbkod($saucy["Panel"]["Poll"]["Locale"]["TitleHover"])."'>
								".$saucy["Panel"]["Poll"]["Locale"]["Title"].bbkod($dev_poll_title)."
							</a>
						</div>
						<div class='poll' "; if(empty($saucy["Panel"]["Poll"]["Show"])) { echo "style='display: none;'"; } echo ">
							<div class='spacer'></div>
							<div class='content_small smalli'>
								[ <span class='timeago tooltip' title='$dev_twitter_date'>".date("H:i @ jS \of F Y", strtotime($dev_twitter_date))."</span> ]
							</div>";

							for($dev_poll_content = 0; $dev_poll_content < $saucy["Panel"]["Poll"]["Limit"]["Posts"]; $dev_poll_content++) {
							
								echo "<a href='' id='article.php?page=".$saucy["DevDB"]["Name"]["poll"]."&id=$dev_poll_content' class='link_click tooltip' title='".bbkod($saucy["Default"]["Locale"]["VoteHover"])." ".bbkod($dev_poll[$dev_poll_content])."'>
									<div class='linkz normal'>
										<span style='color: ".$saucy["Default"]["Color"]["Light"].";'>.:</span> ".truncate_str($dev_poll[$dev_poll_content], $saucy["Panel"]["Poll"]["Limit"]["Text"])."
									</div>
								</a>";
								
							}

							echo "<div class='content_small smalli' style='text-align: center; line-height: 0%;'>
								[ <a href='' id='article.php?page=".$saucy["DevDB"]["Name"]["poll"]."&id=$poll_id_index' class='link_click tooltip' title='".bbkod($saucy["Default"]["Locale"]["CommentsRead"])."'>9001 ".$saucy["Default"]["Locale"]["Comments"]."</a> | 9001 ".$saucy["Default"]["Locale"]["Votes"]." | <a href='' id='archive.php?page=poll' class='link_click tooltip' title='".bbkod($saucy["Panel"]["Poll"]["Locale"]["TextHover"])."'>".$saucy["Panel"]["Poll"]["Locale"]["Text"]."</a> ]
							</div>
						</div>
					</div>";
					
				}
				
			echo "</div>";
			
		}
		
		
		// COLUMN MIDDLE
		if(!empty($saucy["DevShow"]["Column"]["Middle"])) {
		
			echo "<div id='content_middle'>";
			
				// TWITTER
				if(!empty($saucy["Panel"]["Twitter"]["Enabled"])) {
				
					echo "<div class='box'>
						<div class='content_small smalli' style='padding: ".($box_pad-3)."px $box_pad"."px ".($box_pad-6)."px $box_pad"."px; height: 18px;'>
							<span style='float: left;'>
								<a id='twitter' class='show'>
									[ <span class='timeago tooltip' title='$dev_twitter_date'>".date("H:i @ jS \of F Y", strtotime($dev_twitter_date))."</span> ]
								</a>
							</span>
							<span style='float: right;'>
								<a id='twitter' class='show tooltip' title='".bbkod($saucy["Panel"]["Twitter"]["Locale"]["TextHover"])."'>
									[ ".$saucy["Panel"]["Twitter"]["Locale"]["Text"]." <a href='".$social_icon["Twitter"]["URL"]."' target='_blank' class='tooltip' title='".$social_icon["Twitter"]["URL"]."'>".$saucy["Default"]["Name"]."</a>! ]
								</a>
							</span>
						</div>
						<div class='twitter' "; if(empty($saucy["Panel"]["Twitter"]["Show"])) { echo "style='display: none;'"; } echo ">
							<div class='spacer'></div>
							<div class='text normal' style='text-align: justify;'>
								".bbkod($saucy["Panel"]["Twitter"]["Content"])."
							</div>
						</div>
					</div>";
					
				}
				
				// CONTENT
				if(!empty($saucy["Panel"]["Content"]["Show"])) {
				
					echo "<div id='html'></div>";
					
				}
				
				// COPYRIGHT
				if(!empty($copyright["Enabled"])) {
				
					echo "<div class='box'"; if(!empty($saucy["DevEnabled"]["Info"]) AND !empty($dev_text["Info"])) { echo "style='margin-bottom: 81px;'"; } echo ">
						<div class='text small'>
							".bbkod($copyright["Content"])."
						</div>
					</div>";
					
				}
				
			echo "</div>";
			
		}
		
		
		// COLUMN RIGHT
		if(!empty($saucy["DevShow"]["Column"]["Right"])) {
		
			echo "<div id='content_right'>";
			
				// CATEGORIES
				if(!empty($saucy["Panel"]["Category"]["Enabled"])) {
				
					echo "<div class='box'>
						<div class='text big'>
							<a id='search_category' class='show tooltip' title='".bbkod($saucy["Panel"]["Category"]["Locale"]["TitleHover"])."'>
								".$saucy["Panel"]["Category"]["Locale"]["Title"]."
							</a>
						</div>
						<div class='search_category' "; if(empty($saucy["Panel"]["Category"]["Show"])) { echo "style='display: none;'"; } echo ">
							<div class='spacer' style='margin-bottom: ".($box_pad*2)."px;'></div>";

              $sql = "SELECT DISTINCT {$saucy["DevDB"]["Name"]["category"]}.category_id, category_title, category_popular
							FROM {$saucy["DevDB"]["Name"]["category"]}
							INNER JOIN {$saucy["DevDB"]["Name"]["categoryparent"]} ON {$saucy["DevDB"]["Name"]["category"]}.category_id = {$saucy["DevDB"]["Name"]["categoryparent"]}.category_id
							ORDER BY category_popular DESC
							LIMIT {$saucy["Panel"]["Category"]["Limit"]["Posts"]}";
              $stmt = $conn->prepare($sql);
              $stmt->execute();
							while($row_categories_index	= $stmt->fetch()) {

								$categories_id_index	= $row_categories_index["category_id"];
								$categories_title_index	= bbkod($row_categories_index["category_title"]);
								
								echo "<a href='' id='search.php?page=category&id=$categories_id_index' class='link_click tooltip' title='".bbkod($saucy["Panel"]["Category"]["Locale"]["TextHover"])." $categories_title_index"."'>
									<div class='linkz normal'>
										<span style='color: ".$saucy["Default"]["Color"]["Light"].";'>.:</span> $categories_title_index
									</div>
								</a>";
								
							}
							
						echo "</div>
					</div>";
					
				}
				
				// TOOLS
				if(!empty($saucy["Panel"]["Tool"]["Enabled"])) {
				
					echo "<div class='box'>
						<div class='text big'>
							<a id='search_tool' class='show tooltip' title='".bbkod($saucy["Panel"]["Tool"]["Locale"]["TitleHover"])."'>
								".$saucy["Panel"]["Tool"]["Locale"]["Title"]."
							</a>
						</div>
						<div class='search_tool' "; if(empty($saucy["Panel"]["Tool"]["Show"])) { echo "style='display: none;'"; } echo ">
							<div class='spacer' style='margin-bottom: ".($box_pad*2)."px;'></div>";

              $sql = "SELECT DISTINCT {$saucy["DevDB"]["Name"]["tool"]}.tool_id, tool_title, tool_popular
							FROM {$saucy["DevDB"]["Name"]["tool"]}
							INNER JOIN {$saucy["DevDB"]["Name"]["toolparent"]} ON {$saucy["DevDB"]["Name"]["tool"]}.tool_id = {$saucy["DevDB"]["Name"]["toolparent"]}.tool_id
							ORDER BY tool_popular DESC
							LIMIT {$saucy["Panel"]["Tool"]["Limit"]["Posts"]}";
              $stmt = $conn->prepare($sql);
              $stmt->execute();
							while($row_tools_index	= $stmt->fetch()) {

								$tools_id_index		= $row_tools_index["tool_id"];
								$tools_title_index	= bbkod($row_tools_index["tool_title"]);
								
								echo "<a href='' id='search.php?page=tool&id=$tools_id_index' class='link_click tooltip' title='".bbkod($saucy["Panel"]["Category"]["Locale"]["TextHover"])." $tools_title_index"."'>
									<div class='linkz normal'>
										<span style='color: ".$saucy["Default"]["Color"]["Light"].";'>.:</span> $tools_title_index
									</div>
								</a>";
								
							}
							
						echo "</div>
					</div>";
					
				}
				
				// LATEST
				if(!empty($saucy["Panel"]["Latest"]["Enabled"])) {
				
					echo "<div class='box'>
						<div class='text big'>
							<a id='latest' class='show tooltip' title='".bbkod($saucy["Panel"]["Latest"]["Locale"]["TitleHover"])."'>
								".$saucy["Panel"]["Latest"]["Locale"]["Title"]."
							</a>
						</div>
						<div class='latest' "; if(empty($saucy["Panel"]["Latest"]["Show"])) { echo "style='display: none;'"; } echo ">
							<div class='spacer' style='margin-bottom: ".($box_pad*2)."px;'></div>";

              $sql = "SELECT blog_id, blog_title, date, blog_parent
							FROM {$saucy["DevDB"]["Name"]["blog"]}
							UNION
							SELECT review_id, review_title, date, review_parent
							FROM {$saucy["DevDB"]["Name"]["review"]}
							ORDER BY date DESC
							LIMIT {$saucy["Panel"]["Latest"]["Limit"]["Posts"]}";
              $stmt = $conn->prepare($sql);
              $stmt->execute();
							while($row_latest_index = $stmt->fetch()) {

								$latest_id_index			= $row_latest_index["blog_id"];
								$latest_title_index			= bbkod($row_latest_index["blog_title"]);
								$latest_parent_index		= $row_latest_index["blog_parent"];
								
								echo "<a href='' id='article.php?page=$latest_parent_index&id=$latest_id_index' class='link_click tooltip' title='".bbkod($saucy["Default"]["Locale"]["ReadMoreHover"])." $latest_title_index'>
									<div class='linkz normal'>
										<span style='color: ".$saucy["Default"]["Color"]["Light"].";'>.: ".ucfirst($latest_parent_index)." : </span>";
										
										if($latest_parent_index == "blog") {
										
											echo truncate_str($latest_title_index, ($saucy["Panel"]["Latest"]["Limit"]["Text"]+4));
											
										} elseif($latest_parent_index == "review") {
										
											echo truncate_str($latest_title_index, $saucy["Panel"]["Latest"]["Limit"]["Text"]);
											
										}
										
									echo "</div>
								</a>";
								
							}
							
						echo "</div>
					</div>";
					
				}
				
				// ARCHIVES
				if(!empty($saucy["Panel"]["Archive"]["Enabled"])) {
				
					echo "<div class='box'>
						<div class='text big'>
							<a id='archives' class='show tooltip' title='".bbkod($saucy["Panel"]["Archive"]["Locale"]["TitleHover"])."'>
								".$saucy["Panel"]["Archive"]["Locale"]["Title"]."
							</a>
						</div>
						<div class='archives' "; if(empty($saucy["Panel"]["Archive"]["Show"])) { echo "style='display: none;'"; } echo ">
							<div class='spacer' style='margin-bottom: ".($box_pad*2)."px;'></div>";
							
							for($archive_content_index = 0; $archive_content_index < $saucy["Panel"]["Archive"]["Limit"]["Posts"]; $archive_content_index++) {
							
								echo "<span class='normal'>
									<a href='' id='archive.php?page=".$archivelist[$archive_content_index]."' class='link_click tooltip' title='".bbkod($saucy["Panel"]["Archive"]["Locale"]["TextHover"])." ".ucfirst($archivelist[$archive_content_index])."'>
										<div class='linkz normal'>
											<span style='color: ".$saucy["Default"]["Color"]["Light"].";'>.: ".$saucy["Panel"]["Archive"]["Locale"]["Text"]."</span>".ucfirst($archivelist[$archive_content_index])."
										</div>
									</a>
								</span>";
								
							}
							
						echo "</div>
					</div>";
					
				}
				
				// BANNER
				if(!empty($saucy["Panel"]["Banner"]["Enabled"])) {
				
					echo "<div class='box'"; if(!empty($saucy["DevEnabled"]["Info"]) AND !empty($dev_text["Info"])) { echo "style='margin-bottom: 81px;'"; } echo ">
						<div class='text big'>
							<a id='banners' class='show tooltip' title='".bbkod($saucy["Panel"]["Banner"]["Locale"]["TitleHover"])."'>
								".$saucy["Panel"]["Banner"]["Locale"]["Title"]."
							</a>
						</div>
						<div class='banners' "; if(empty($saucy["Panel"]["Banner"]["Show"])) { echo "style='display: none;'"; } echo ">
							<div class='spacer'></div>
							<div class='text'>";
							
								foreach($saucy["Panel"]["Banner"]["Content"] as $banner_key => $banner_value) {
								
									if(!empty($banner_value["Enabled"])) {
									
										echo "<a href='".$banner_value["URL"]."' target='_blank' class='tooltip' title='$banner_key : ".$banner_value["URL"]."'><img src='$folder_banner/".$banner_value["Image"]."' class='banner' /></a>";
										
									}
									
								}
								
							echo "</div>
						</div>
					</div>";
					
				}
				
			echo "</div>";
			
		}
		
	echo "</div>";
	
}

?>

</body>
</html>