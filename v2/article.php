<?php

require("inc/settings.php");
require($saucy["Default"]["Conn"]);
require($saucy["Default"]["Functions"]);
require_once($saucy["Default"]["SubmitRating"]);

/*	SHARES CODE WITH:

.: Past name : info_blog.php :.

.: FILES :.
info_blog
info_reviews
info_tutorials
info_portfolio
info_list
-- info_poll (inte fixad än)
info_code
info_config
info_map
info_model
info_video
info_wallpaper


.: DB :.
blog_id
blog_parent
blog_title
date
blog_text
blog_source
blog_views

review_id
review_parent
review_title
date
review_text
review_rating
review_source
review_views

tutorial_id
tutorial_title
date
tutorial_text
tutorial_difficulty
tutorial_time
tutorial_file
tutorial_source
tutorial_views

portfolio_id
tutorial_id
portfolio_title
date
portfolio_text
portfolio_file
portfolio_views

list_id
list_title
date
list_views

download_id
download_parent
download_title
date
download_text
download_file
download_source
download_views

comment_id
comment_parent_id
comment_parent
comment_reply
comment_name
comment_ip
date
comment_website
comment_text
comment_approved

*/

//if($parent == "downloads") { echo $download_parent; } else { echo $parent; }

$id		= isset($_GET["id"]) ? intval($_GET["id"]) : 0;
$page	= isset($_GET["page"]) ? $_GET["page"] : NULL;

/*
if(isset($_GET["page"])) { // check if page even exist, otherwise set it to null

    $page = $_GET["page"];

} else {

    $page = NULL;
    
}
*/

$pagelist_amount = count($articlelist);
if(!in_array($page, $articlelist)) {

	echo "You are trying to access a page that does not exist.";
	exit;
	
}
/*
if(isset($_GET["id"])) {

    $id = intval($_GET["id"]);
    
} else {

    $id = 0;
    
}
*/

?>
<!DOCTYPE html>
<html lang='sv'>
<head>
<title></title>

<?php

echo "<script src='".$saucy["Default"]["JS"]["jQuery"]."'></script>";
//echo "<script src='".$saucy["Default"]["JS"]["MaxRating"]."'></script>";
echo "<script src='".$saucy["Default"]["JS"]["TimeAgo"]."'></script>";
echo "<script src='".$saucy["Default"]["JS"]["Qtip"]."'></script>";

?>

<script>
$(document).ready(function(){
    jQuery(".timeagoArticle").timeago();
    jQuery.timeago.settings.allowFuture = true;
	
    $(".showArticle").bind("click",function() {
        $("."+this.id ).stop(true, true).slideToggle("<?php echo $saucy["Default"]["ToggleSpeed"]; ?>");
    });
	
	$(".tooltipArticle, .tooltipMenu").each(function() {
		$(this).qtip({
			content: {
				text: $(this).attr("title"),
			},
			position: {
				target: "mouse",
				adjust: { x: 15, y: 0, },
			},
			style: {
				tip: false
			},
			show: {
				delay: 0
			},
			hide: {
				delay: 200
			}
		});
	});
});
</script>

</head>
<body>

<?php

$parent = str_replace(".php", "", strtolower(basename($_SERVER["PHP_SELF"])));

include "subMenu.php";

// -------------------------------------------------------------------------------------------------------------------

if($page == "code" OR $page == "map" OR $page == "video" OR $page == "config" OR $page == "model" OR $page == "wallpaper") {

    $page = "download";
	
}

$sql = "UPDATE {$saucy["DevDB"]["Name"][$page]}
SET {$page}_views = {$page}_views + 1
WHERE {$page}_id = :id";
$stmt = $conn->prepare($sql);
$stmt->bindParam(':id', $id, PDO::PARAM_INT);
$stmt->execute();

if($page == "blog") {

	// BLOG
  $tbl = "SELECT {$page}_id, {$page}_title, date, {$page}_text, {$page}_source, {$page}_views
	FROM {$saucy["DevDB"]["Name"][$page]}
	WHERE {$page}_id = :id
	LIMIT {$saucy["Default"]["Limit"]["Articles"]}";

} elseif($page == "review") {

	// REVIEW
  $tbl = "SELECT {$page}_id, {$page}_title, date, {$page}_text, {$page}_rating, {$page}_source, {$page}_views
	FROM {$saucy["DevDB"]["Name"][$page]}
	WHERE {$page}_id = :id
	LIMIT {$saucy["Default"]["Limit"]["Articles"]}";

} elseif($page == "tutorial") {

	// TUTORIAL
  $tbl = "SELECT {$page}_id, {$page}_title, date, {$page}_text, {$page}_difficulty, {$page}_time, {$page}_file, {$page}_source, {$page}_views
	FROM {$saucy["DevDB"]["Name"][$page]}
	WHERE {$page}_id = :id
	LIMIT {$saucy["Default"]["Limit"]["Articles"]}";

} elseif($page == "portfolio") {

	// PORTFOLIO
  $tbl = "SELECT {$page}_id, tutorial_id, {$page}_title, date, {$page}_text, {$page}_file, {$page}_views
	FROM {$saucy["DevDB"]["Name"][$page]}
	WHERE {$page}_id = :id
	LIMIT {$saucy["Default"]["Limit"]["Other"]}";


} elseif($page == "list") {

	// LIST
  $tbl = "SELECT {$page}_id, {$page}_title, date, {$page}_views
	FROM {$saucy["DevDB"]["Name"][$page]}
	WHERE {$page}_id = :id
	ORDER BY date DESC
	LIMIT {$saucy["Default"]["Limit"]["Other"]}";

} elseif($page == "download") {

	// DOWNLOAD
  $tbl = "SELECT {$page}_id, {$page}_parent, {$page}_title, date, {$page}_text, {$page}_file, {$page}_source, {$page}_views
	FROM {$saucy["DevDB"]["Name"][$page]}
	WHERE {$page}_id = :id
	LIMIT {$saucy["Default"]["Limit"]["Other"]}";

}

$stmt = $conn->prepare($tbl);
$stmt->bindParam(':id', $id, PDO::PARAM_INT);
$stmt->execute();
while ($row = $stmt->fetch()) {

	if($page == "blog"){
	
		// BLOG
		$id					= $row[$page."_id"];
		$title				= $row[$page."_title"];
		$date				= $row["date"];
		$text				= $row[$page."_text"];
		$source				= $row[$page."_source"];
		$views				= $row[$page."_views"];
		
	} elseif($page == "review") {
	
		// REVIEW
		$id					= $row[$page."_id"];
		$title				= $row[$page."_title"];
		$date				= $row["date"];
		$text				= $row[$page."_text"];
		$rating				= $row[$page."_rating"];
		$source				= $row[$page."_source"];
		$views				= $row[$page."_views"];
		
	} elseif($page == "tutorial") {
	
		// TUTORIAL
		$id					= $row[$page."_id"];
		$title				= $row[$page."_title"];
		$date				= $row["date"];
		$text				= $row[$page."_text"];
		$difficulty			= $row[$page."_difficulty"];
		$time				= $row[$page."_time"];
		$file				= $row[$page."_file"];
		$source				= $row[$page."_source"];
		$views				= $row[$page."_views"];
		
	} elseif($page == "portfolio") {
	
		// PORTFOLIO
		$id					= $row[$page."_id"];
		$id_tutorial		= $row["tutorial_id"];
		$title				= $row[$page."_title"];
		$date				= $row["date"];
		$text				= $row[$page."_text"];
		$file				= $row[$page."_file"];
		$views				= $row[$page."_views"];
		
	} elseif($page == "list") {
	
		// LIST
		$id					= $row[$page."_id"];
		$title				= $row[$page."_title"];
		$date				= $row["date"];
		$views				= $row[$page."_views"];
		
	} elseif($page == "download") {
	
		// DOWNLOAD
		$id					= $row[$page."_id"];
		$download_parent	= $row["download_parent"];
		$title				= $row[$page."_title"];
		$date				= $row["date"];
		$text				= $row[$page."_text"];
		$file				= $row[$page."_file"];
		$source				= $row[$page."_source"];
		$views				= $row[$page."_views"];
		
	}
	
	// COMMENT
    $sql = "SELECT comment_id, comment_parent_id, comment_parent, comment_reply, comment_name, comment_ip, date, comment_website, comment_text, comment_approved
  	FROM {$saucy["DevDB"]["Name"]["comment"]}
  	WHERE comment_parent_id = :id AND comment_parent = :page AND comment_approved = 1
  	ORDER BY date DESC";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->bindParam(':page', $page, PDO::PARAM_STR);
    $stmt->execute();
    $comments_amount = $stmt->rowCount();

    echo "<div class='box'>
        <div class='text big'>
			<a id='article_$id' class='showArticle tooltipArticle' title='".bbkod($saucy["Default"]["Locale"]["Info"]." ".$title)."'>";
				if($page == "download") { echo strtoupper($download_parent)." : "; } echo truncate_str($title, $saucy["Default"]["Limit"]["Title"])."
			</a>
        </div>
		<div class='article_$id'>
			<div class='spacer'></div>
			<div class='content_small smalli' style='margin-bottom: ".$box_pad."px;'>
				<span style='float: left;'>
					[ <span class='timeagoArticle tooltipArticle' title='$date'>".date("H:i @ jS \of F Y", strtotime($date))."</span> ]
				</span>
				<span style='float: right;'>";

          $sql = "SELECT category_parent_id, {$saucy["DevDB"]["Name"]["categoryparent"]}.category_id, {$saucy["DevDB"]["Name"][$page]}.{$page}_id, category_title
					FROM {$saucy["DevDB"]["Name"]["categoryparent"]}
					INNER JOIN {$saucy["DevDB"]["Name"]["category"]} ON {$saucy["DevDB"]["Name"]["category"]}.category_id = {$saucy["DevDB"]["Name"]["categoryparent"]}.category_id
					INNER JOIN {$saucy["DevDB"]["Name"][$page]} ON {$saucy["DevDB"]["Name"][$page]}.{$page}_id = {$saucy["DevDB"]["Name"]["categoryparent"]}.{$page}_id
					WHERE {$saucy["DevDB"]["Name"]["categoryparent"]}.{$page}_id = :id";
          $stmt = $conn->prepare($sql);
          $stmt->bindParam(':id', $id, PDO::PARAM_INT);
          $stmt->execute();
          $category_amount = $stmt->rowCount();

					// CATEGORY
					if(!empty($category_amount)) {
					
						echo "[ Cat: ";

            while ($row_category = $stmt->fetch()) {

							$category_id	= $row_category["category_id"];
							$category_title	= bbkod($row_category["category_title"]);
							
							$category_amount--;
							
							echo "<a href='' id='search.php?page=category&id=$category_id' class='link_click tooltipArticle' title='".bbkod($saucy["Default"]["Locale"]["Category"])." $category_title'>$category_title</a>";
							
							if(!empty($category_amount)) {
							
								echo ", ";
								
							}
							
						}
						
						echo " ]";
						
					}
					
				echo "</span>
			</div>
			<div class='text normal' style='text-align: justify;'>";
				
					/*
					if($page == "blog") {
					
						echo "<div class='disclaimer'><span class='massive' style='font-weight: bold; color: red;'>DISCLAIMER: <span style='color: ".$saucy["Default"]["Color"]["Light"].";'>My Reviews</span></span><br />
						<span class='small'>This is <i>my</i> review of things, you may or may not agree with what <i>I</i> think.<br /> I have as much right to express my thoughts and feelings as you.</span></div>";
						
					} else
					*/
					
					// PORTFOLIO
					if($page == "portfolio") {
					
						echo "<div style='padding: 0px 2px 3px 0px;' align='center'>
							<a href='$folder_portfolio/$file' target='_blank' class='tooltipArticle' title='".bbkod($saucy["Default"]["Locale"]["Portfolio"])."'>
								<img style='max-width: 100%;' src='$folder_portfolio/$file' />
							</a>
						</div>";
						
					// LIST
					} elseif($page == "list") {
											
						$list_counter = 1;
            $sql = "SELECT {$page}_content_id, {$page}_content_title, {$page}_content_text, {$page}_content_parent_id
						FROM {$saucy["DevDB"]["Name"][$page]}_content
						WHERE {$page}_content_parent_id = :id";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            while ($row_list = $stmt->fetch()) {

							$list_id	= $row_list[$page."_content_id"];
							$list_title	= bbkod($row_list[$page."_content_title"]);
							$list_text	= bbkod($row_list[$page."_content_text"]);
							
							echo "<a id='content".$list_id."' class='showArticle tooltipArticle' title='$list_text'>
								<div class='linkz big'>
									#$list_counter : $list_title
									<div class='content".$list_id." normal' style='padding: ".($box_pad*4)."px ".$box_pad."px 0px ".$box_pad."px; display: none;'>
										$list_text
									</div>
								</div>
							</a>";
								
							$list_counter++;
							
						}
						
					}
					
					// TEXT
					if(!empty($text)) {
						echo bbkod($text);
					}
					
					// SOURCE
					if(!empty($source)) {
					
						echo "<br /><br />";
						echo "[ <a href='$source' target='_blank' class='tooltipArticle' title='Source: $source'>Source</a> ]";
						
					}
					
			echo "</div>
			<div class='spacer'></div>";
			
			if($page == "tutorial" OR $page == "portfolio" OR $page == "download") {

        $sql = "SELECT
					tool_parent_id,
					{$saucy["DevDB"]["Name"]["toolparent"]}.tool_id,
					{$saucy["DevDB"]["Name"][$page]}.{$page}_id,
					tool_title
				FROM {$saucy["DevDB"]["Name"]["toolparent"]}
				INNER JOIN {$saucy["DevDB"]["Name"]["tool"]} ON {$saucy["DevDB"]["Name"]["tool"]}.tool_id = {$saucy["DevDB"]["Name"]["toolparent"]}.tool_id
				INNER JOIN {$saucy["DevDB"]["Name"][$page]} ON {$saucy["DevDB"]["Name"][$page]}.{$page}_id = {$saucy["DevDB"]["Name"]["toolparent"]}.{$page}_id
				WHERE {$saucy["DevDB"]["Name"]["toolparent"]}.{$page}_id = :id";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $tbl_tool_amount = $stmt->rowCount();

				if(!empty($tbl_tool_amount) OR !empty($file)) {
				
					echo "<div class='content_small smalli' style='margin-bottom: $box_pad"."px;'>
						<span style='float: left;'>";
							
							// FILENAME
							if(!empty($file)) {
							
								if($page == "portfolio" OR $page == "tutorial" OR $page == "download") {
								
									echo "[ Filename: <a href='";
									
									if($page == "tutorial") {
									
										echo $folder_tutorial;
										
									} elseif($page == "portfolio") {
									
										echo $folder_portfolio;
										
									} elseif($page == "download") {
									
										echo "$folder_download/$download_parent";
										
									}
									
									echo "/$file' target='_blank' class='tooltipArticle' title='".bbkod($saucy["Default"]["Locale"]["DownloadHover"])." $file'>$file</a>";
									
								}
								
							}
							
							// TOOLS
							if(!empty($tbl_tool_amount)) {
							
								echo (!empty($file) ? " | " : " [ ");
								
								echo "Tools: ";

								while($row_tutorials_tools = $stmt->fetch) {

									$id_tool	= $row_tutorials_tools["tool_id"];
									$tool_title	= bbkod($row_tutorials_tools["tool_title"]);
									
									$tbl_tool_amount--;
									
									echo "<a href='' id='search.php?page=tool&id=$id_tool' class='link_click tooltipArticle' title='".bbkod($saucy["Default"]["Locale"]["ToolHover"])." $tool_title'>$tool_title</a>";
									
									if(!empty($tbl_tool_amount)) {
									
										echo ", ";
										
									}
									
								}
								
							}
							
						echo " ]</span>";
						
						// DOWNLOAD
						if(!empty($file)) {
						
							echo "<span style='float: right;'>[ <a href='";
							
							if($page == "tutorial") {
							
								echo $folder_tutorial;
								
							} elseif($page == "portfolio") {
							
								echo $folder_portfolio;
								
							} elseif($page == "download") {
							
								echo "$folder_download/$download_parent";
								
							}
							
							echo "/$file' target='_blank' class='tooltipArticle' title='".bbkod($saucy["Default"]["Locale"]["DownloadHover"])." $file'>".$saucy["Default"]["Locale"]["Download"]."</a> ]</span>";
							
						}
						
					echo "</div>";
					
				}
				
			}
			
			// RATE
			echo "<div class='content_small smalli' style='margin-bottom: 6px;'>
				<span style='float: left;'>";
					echo "[ Rate: ";
					echo $handler->displayRating(strip_utf(eng_char($title)),10);
					echo " | ";
					echo $handler->displayTotalNumberOfRatings(strip_utf(eng_char($title)));
					echo " votes, average: ";
					echo $handler->displayRateValue(strip_utf(eng_char($title)));
					echo " out of 10 ]";
				echo "</span>";
				
				// RATING
				if($page == "review") {
				
					echo "<span style='float: right;'>";
						
							if(empty($rating)) {
							
								echo "[ ".$saucy["Default"]["Locale"]["Rating"]["Text"].$saucy["Default"]["Locale"]["Rating"]["NotRated"]." ]";
								
							} else {
							
								echo "[ ".$saucy["Default"]["Locale"]["Rating"]["Text"];
								
									for($rating_index = 0; $rating_index < $rating; $rating_index++) {
									
										if($rating == 5) {
										
											echo "<span class='tooltipArticle' title='".bbkod($saucy["Default"]["Locale"]["Rating"]["Terrific"])."'><img src='".$saucy["Default"]["Image"]["Rating"][$rating_index]."' style='border: 0px solid;' /></span>";
											
										} elseif($rating == 4) {
										
											echo "<span class='tooltipArticle' title='".bbkod($saucy["Default"]["Locale"]["Rating"]["Good"])."'><img src='".$saucy["Default"]["Image"]["Rating"][$rating_index]."' style='border: 0px solid;' /></span>";
											
										} elseif($rating == 3) {
										
											echo "<span class='tooltipArticle' title='".bbkod($saucy["Default"]["Locale"]["Rating"]["OK"])."'><img src='".$saucy["Default"]["Image"]["Rating"][$rating_index]."' style='border: 0px solid;' /></span>";
											
										} elseif($rating == 2) {
										
											echo "<span class='tooltipArticle' title='".bbkod($saucy["Default"]["Locale"]["Rating"]["Bad"])."'><img src='".$saucy["Default"]["Image"]["Rating"][$rating_index]."' style='border: 0px solid;' /></span>";
											
										} elseif($rating == 1) {
										
											echo "<span class='tooltipArticle' title='".bbkod($saucy["Default"]["Locale"]["Rating"]["Horrible"])."'><img src='".$saucy["Default"]["Image"]["Rating"][$rating_index]."' style='border: 0px solid;' /></span>";
											
										}
										
									}
									
								echo " ]";
								
							}
							
						echo "</span>";
					
				// DIFFICULTY
				} elseif($page == "tutorial") {
				
					echo "<span style='float: right;'>";
					
						if(empty($difficulty)) {
						
							echo "[ ".$saucy["Default"]["Locale"]["Difficulty"]["Text"].$saucy["Default"]["Locale"]["Difficulty"]["NotRated"]." ]";
							
						} else {
						
							echo "[ ".$saucy["Default"]["Locale"]["Difficulty"]["Text"];
							
							for($difficulty_index = 0; $difficulty_index < $difficulty; $difficulty_index++) {
							
								if($difficulty == 5) {
								
									echo "<a href='' class='tooltipArticle' title='".bbkod($saucy["Default"]["Locale"]["Difficulty"]["Expert"])."'><img src='".$saucy["Default"]["Image"]["Difficulty"][$difficulty_index]."' style='border: 0px solid;' /></a>";
									
								} elseif($difficulty == 4) {
								
									echo "<a href='' class='tooltipArticle' title='".bbkod($saucy["Default"]["Locale"]["Difficulty"]["Hard"])."'><img src='".$saucy["Default"]["Image"]["Difficulty"][$difficulty_index]."' style='border: 0px solid;' /></a>";
									
								} elseif($difficulty == 3) {
								
									echo "<a href='' class='tooltipArticle' title='".bbkod($saucy["Default"]["Locale"]["Difficulty"]["Medium"])."'><img src='".$saucy["Default"]["Image"]["Difficulty"][$difficulty_index]."' style='border: 0px solid;' /></a>";
									
								} elseif($difficulty == 2) {
								
									echo "<a href='' class='tooltipArticle' title='".bbkod($saucy["Default"]["Locale"]["Difficulty"]["Easy"])."'><img src='".$saucy["Default"]["Image"]["Difficulty"][$difficulty_index]."' style='border: 0px solid;' /></a>";
									
								} elseif($difficulty == 1) {
								
									echo "<a href='' class='tooltipArticle' title='".bbkod($saucy["Default"]["Locale"]["Difficulty"]["Beginner"])."'><img src='".$saucy["Default"]["Image"]["Difficulty"][$difficulty_index]."' style='border: 0px solid;' /></a>";
									
								}
								
							}
							
							echo " ]";
							
						}
						
					echo "</span>";
					
				// TUTORIAL AVAILABLE
				} elseif($page == "portfolio" OR $page == "downloads") {
				
					if(!empty($id_tutorial)) {
					
						echo "<span style='float: right;'>[ <a href='' id='article.php?page=tutorial&id=$id_tutorial' class='link_click tooltipArticle' title='".bbkod($saucy["Default"]["Locale"]["TutorialHover"])." $title'>".$saucy["Default"]["Locale"]["Tutorial"]."</a> ]</span>";
					
					}
					
				}
				
			echo "</div>
				
			<div class='content_small smalli'>
				<span style='float: left;'>";
				
					// COMMENTS + VIEWS + DOWNLOADS + SIZE + COMPLETION TIME
					echo "[ <a href='' id='article.php?page=$page&id=$id' class='link_click tooltipArticle' title='".bbkod($saucy["Default"]["Locale"]["CommentsRead"])."'>$comments_amount ".$saucy["Default"]["Locale"]["Comments"]."</a> | $views ".$saucy["Default"]["Locale"]["Views"]." ";
					
					if($page == "blog" OR $page == "review" OR $page == "list" OR $page == "poll") {
					
						echo "]";
						
					} elseif($page == "portfolio" OR $page == "download") {
					
						echo "| 0 Downloads";
						
						if(!empty($file)) {
						
							echo " | Size: <span style='color: ".$saucy["Default"]["Color"]["Link_completed"].";'>";
							
							if($page == "portfolio") {
							
								echo round((filesize("$folder_portfolio/$file")/1024)/1024, 2)." MB (".round(filesize("$folder_portfolio/$file")/1024);
								
							} elseif($page == "download") {
							
								echo round((filesize("$folder_download/$download_parent/$file")/1024)/1024, 2)." MB (".round(filesize("$folder_download/$download_parent/$file")/1024);
								
							}
							
							echo " KB)</span> ]";
							
						}
						
					} elseif($page == "tutorial") {
					
						echo "| Completion Time: <span style='color: ".$saucy["Default"]["Color"]["Link_completed"].";'>"; if(!empty($time)) { echo "Unknown"; } else { echo $time; }  echo "</span> ]";
						
					}
				
				echo "</span>";
				
			echo "</div>
		</div>
    </div>
</div>";

}

// COMMENTS
if(!empty($saucy["Panel"]["Comments"]["Enabled"])) {

	if(!empty($comments_amount)) {
	
		echo "<div class='box'>
			<div class='text big'>
				<a id='comments_$id' class='showArticle tooltipArticle' style='color: ".$saucy["Default"]["Color"]["Light"].";' title='".bbkod($saucy["Default"]["Locale"]["CommentsHover"])."'>
					.: $comments_amount ".$saucy["Default"]["Locale"]["Comments"]." :.
				</a>
			</div>
		</div>
		<div class='comments_$id' "; if(empty($saucy["Panel"]["Comments"]["Show"])) { echo "style='display: none;'"; } echo ">";
		
			/*
			$times = 1;
			for($i = 0; $i < $times; $i++) {
				echo "<div style='background: orange; margin: 0px ".($box_pad*2)."px ".($box_pad*2)."px ".($box_pad*2)."px; border: 1px solid ".$saucy["Default"]["Color"]["Border"]."; outline: 1px solid black;'>
					<div style='background: ".$saucy["Default"]["Color"]["Border"]."; padding: $box_pad"."px;'>1. Saucy said for 2 days ago:</div>
					<div style='background: ".$saucy["Default"]["Color"]["Darker"]."; padding: $box_pad"."px;'>text</div>
				</div>";
			}
			*/
			
			/*
			// getting the comments from mysql, I'm obviously not bothering
			// to check the return value, but in your code you should do it
			$result = mysql_query("SELECT id, parent FROM comments");

			$comments = array();
			while ($row = mysql_fetch_array($result)) {
			  $row['childs'] = array();
			  $comments['id'] = $row;
			}

			// This is the array you get after your mysql query
			// Order is non important, I put the parents first here simply to make it clearer.
			
			$comments = array(
				// some top level (parent == 0)
				1 => array('id' => 1, 'parent' => 0, 'childs' => array()),
				5 => array('id' => 5, 'parent' => 0, 'childs' => array()),
				2 => array('id' => 2, 'parent' => 0, 'childs' => array()),
				10 => array('id' => 10, 'parent' => 0, 'childs' => array()),
				// and some childs
				3 => array('id' => 3, 'parent' => 1, 'childs' => array()),
				6 => array('id' => 6, 'parent' => 2, 'childs' => array()),
				4 => array('id' => 4, 'parent' => 2, 'childs' => array()),
				7 => array('id' => 7, 'parent' => 3, 'childs' => array()),
				8 => array('id' => 8, 'parent' => 7, 'childs' => array()),
				9 => array('id' => 9, 'parent' => 6, 'childs' => array()),
			);
			

			// now loop your comments list, and everytime you find a child, push it 
			//   into its parent
			foreach ($comments as $k => &$v) {
			  if ($v['parent'] != 0) {
				$comments[$v['parent']]['childs'][] =& $v;
			  }
			}
			unset($v);

			// delete the childs comments from the top level
			foreach ($comments as $k => $v) {
			  if ($v['parent'] != 0) {
				unset($comments[$k]);
			  }
			}

			// now we display the comments list, this is a basic recursive function
			function display_comments(array $comments, $level = 0) {
			  foreach ($comments as $info) {
				echo str_repeat('-', $level + 1).' comment '.$info['id']."<br />";
				if (!empty($info['childs'])) {
				  display_comments($info['childs'], $level + 1);
				}
			  }
			}

			display_comments($comments);
			*/
			
			// class Threaded_comments
			// {

			// 	public $parents  = array();
			// 	public $children = array();

			// 	/**
			// 	 * @param array $comments
			// 	 */
			// 	function __construct($comments)
			// 	{
			// 		foreach ($comments as $comment)
			// 		{
			// 			if ($comment['parent_id'] === NULL)
			// 			{
			// 				$this->parents[$comment['id']][] = $comment;
			// 			}
			// 			else
			// 			{
			// 				$this->children[$comment['parent_id']][] = $comment;
			// 			}
			// 		}
			// 	}

			// 	/**
			// 	 * @param array $comment
			// 	 * @param int $depth
			// 	 */
			// 	private function format_comment($comment, $depth)
			// 	{
			// 		for ($depth; $depth > 0; $depth--)
			// 		{
			// 			echo "\t";
			// 		}

			// 		echo $comment['text'];
			// 		echo "<br />";
			// 	}

			// 	/**
			// 	 * @param array $comment
			// 	 * @param int $depth
			// 	 */
			// 	private function print_parent($comment, $depth = 0)
			// 	{
			// 		foreach ($comment as $c)
			// 		{
			// 			$this->format_comment($c, $depth);

			// 			if (isset($this->children[$c['id']]))
			// 			{
			// 				$this->print_parent($this->children[$c['id']], $depth + 1);
			// 			}
			// 		}
			// 	}

			// 	public function print_comments()
			// 	{
			// 		foreach ($this->parents as $c)
			// 		{
			// 			$this->print_parent($c);
			// 		}
			// 	}

			// }


			// $comments = array(  array('id'=>1, 'parent_id'=>NULL,   'text'=>'Parent'),
			// 					array('id'=>2, 'parent_id'=>1,      'text'=>'Child'),
			// 					array('id'=>3, 'parent_id'=>2,      'text'=>'Child Third level'),
			// 					array('id'=>4, 'parent_id'=>NULL,   'text'=>'Second Parent'),
			// 					array('id'=>5, 'parent_id'=>4,   'text'=>'Second Child')
			// 				);

			// $threaded_comments = new Threaded_comments($comments);

			// $threaded_comments->print_comments();

			while($row_comments = $stmt->fetch()) {

				$comments_id		= $row_comments["comment_id"];
				$comments_name		= bbkod($row_comments["comment_name"]);
				$comments_date		= $row_comments["date"];
				$comments_website	= $row_comments["comment_website"];
				$comments_text		= bbkod($row_comments["comment_text"]);
				
				// echo "<div style='position: relative; width: 20px; height: 20px; background: orange; z-index: 20;'>";
				// echo "<sup>#$comments_id </sup>";
				// echo "</div>";
				
				echo "<div class='box' style='margin: 0px ".($box_pad*2)."px 10px ".($box_pad*2)."px;"; if($comments_name == "Saucy") { echo "background: ".$saucy["Default"]["Color"]["Darker_green"].";'"; } echo "'>";
				
					echo "<div class='content_small' style='padding: ".($box_pad-3)."px $box_pad"."px ".($box_pad-6)."px $box_pad"."px; height: 18px;'>
						<span class='big' style='float: left; margin-top: -3px;'>";
							//<a id='comment_".$comments_id."' class='showArticle tooltipArticle' style='color: ".$saucy["Default"]["Color"]["Light"].";' title='".bbkod($saucy["Default"]["Locale"]["CommentHover"])."'>";
							
								if(!empty($comments_name)) {
								
									if(!empty($comments_website)) {
									
										echo "<a href='$comments_website' target='_blank' class='tooltipArticle' title='URL : $comments_website'>
											$comments_name
										</a>";
										
									} else {
									
										echo $comments_name;
										
									}
									
								} else {
								
									if(!empty($comments_website)) {
									
										echo "<a href='$comments_website' target='_blank' class='tooltipArticle' title='URL : $comments_website'>
											<span style='font-style: italic;'>
												Anonymous
											</span>
										</a>";
										
									} else {
									
										echo "<span style='font-style: italic;'>Anonymous</span>";
										
									}
									
								}
								
							//echo "</a>
						echo "</span>
						<span class='smalli' style='float: right;'>
							[ <span class='timeagoArticle tooltipArticle' title='$comments_date'>".date("H:i @ jS \of F Y", strtotime($comments_date))."</span> ]
						</span>
					</div>
					<div class='comment_".$comments_id."'>
						<div class='spacer'></div>
						<div class='text' style='text-align: justify;'>
							<span class='normal'>
								$comments_text
							</span>
						</div>
					</div>
				</div>";
				
			}
				
			
		echo "</div>";
		
	} else {
	
		echo "<div class='box'>
			<div class='text big'>
				.: ".$saucy["Default"]["Locale"]["NoComments"]." :.
			</div>
		</div>";
		
	}
	
}


$num_1 = 10;
$num_2 = 20;

	/*
$sum = ($num_1 + $num_2);

    if(validate == $sum) {
        // skicka in datan till databasen
    } else {
        // annars returna false
        return false;
    }
	*/


// DISPLAY FORM IF FORM HAS NOT BEEN SUBMITTED, OTHERWISE HIDE

// ADD COMMENT
if(empty($saucy["Panel"]["AddComment"]["Enabled"])) {

echo "<div class='box'>
    <div class='text big'>
        <a id='addcomment' class='showArticle tooltipArticle' style='color: ".$saucy["Default"]["Color"]["Light"].";' title='".bbkod($saucy["Default"]["Locale"]["CommentAdd"])."'>
            .: Add comment :.
        </a>
    </div>
	<div class='addcomment' style='"; if(empty($saucy["Panel"]["AddComment"]["Show"])) { echo "display: none;"; } echo "'>
		<div class='spacer'></div>
        <div class='text' style='text-align: justify;'>
			<div style='background: none; width: 140px; text-align: right; float: left;'>
			
				<div class='addcomment_text'>
					<div class='big'>Name:</div>
					<div class='smalli'>Optinal</div>
				</div>
				
				<div class='addcomment_text'>
					<div class='big'>Email:</div>
					<div class='smalli'>Required (will not be shown)</div>
				</div>
				
				<div class='addcomment_text'>
					<div class='big'>Website:</div>
					<div class='smalli'>Optional</div>
				</div>
				
				<div class='addcomment_text' style='height: ".(($addcomment_margin*10)+16)."px;'>
					<div class='big'>Message:</div>
					<div class='smalli'>Required</div>
				</div>
				
				<div class='addcomment_text'>
					<div class='big'>Are you human?</div>
					<div class='smalli'>Enter the sum of ".($num_1 + $num_2).":</div>
				</div>
				
			</div>
			
			<div style='background: none; width: 280px; float: right; margin-top: 6px; box-sizing: content-box;'>
			
				<form method='post' name='comment_form'>
				
					<div class='addcomment_input'>
						<input name='name' type='text' form='comment_form' class='form' autocomplete='on' />
					</div>
					
					<div class='addcomment_input'>
						<input name='email' type='email' form='comment_form' class='form' required />
					</div>
					
					<div class='addcomment_input'>
						<input name='website' type='url' form='comment_form' class='form' autocomplete='on' />
					</div>
					
					<div class='addcomment_input' style='height: ".(($addcomment_margin*10)+16)."px;'>
						<textarea name='message' form='comment_form' class='form' style='height: ".($addcomment_margin*10)."px;' required></textarea>
					</div>
					
					<div class='addcomment_input'>
						<input name='human' type='text' form='comment_form' class='form' style='width: 25%;' autocomplete='off' required />
						
						<input name='ip' type='hidden' form='comment_form' value='".ip2long(getIP())."' />
						
					</div>
						<input name='submit' type='submit' form='comment_form' class='submit' value='Submit comment!' />
					
				</form>
				
			</div>
			
        </div>
    </div>
    <div style='clear: both;'></div>
</div>";

}

/*
$ip = getIP();
$ip2int = ip2long($ip);
$ip2show = long2ip($ip2int);
echo $ip2show."<br />"$ip2int;

$username	= mysql_real_escape_string($_POST['name']);
$ip			= mysql_real_escape_string($_POST['ip']);
$website	= mysql_real_escape_string($_POST['website']);
$text		= mysql_real_escape_string($_POST['text']);

if($_POST) {
$date = date('Y-m-d H:i:s');
mysql_query("INSERT INTO comments (comments_parent_id, comments_parent, comments_name, comments_ip, date, comments_website, comments_text) VALUES ('$id','$page','$name','$ip','$date','$website','$text')") or die (mysql_error());
echo "Comment sent!";
}
*/

?>

</body>
</html>