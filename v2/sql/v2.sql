-- phpMyAdmin SQL Dump
-- version 4.3.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 02, 2015 at 05:07 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.2

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `database_name`
--
CREATE DATABASE IF NOT EXISTS `database_name` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `database_name`;

-- --------------------------------------------------------

--
-- Table structure for table `legacy_v2_blog`
--

CREATE TABLE IF NOT EXISTS `legacy_v2_blog` (
  `blog_id` int(10) unsigned NOT NULL,
  `blog_parent` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'blog',
  `blog_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `blog_text` text COLLATE utf8_unicode_ci NOT NULL,
  `blog_source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blog_views` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_v2_blog`
--

INSERT INTO `legacy_v2_blog` (`blog_id`, `blog_parent`, `blog_title`, `date`, `blog_text`, `blog_source`, `blog_views`) VALUES
(1, 'blog', 'blog title', '2010-01-01 10:02:01', 'blog text', NULL, 13);

-- --------------------------------------------------------

--
-- Table structure for table `legacy_v2_category`
--

CREATE TABLE IF NOT EXISTS `legacy_v2_category` (
  `category_id` int(10) unsigned NOT NULL,
  `category_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_popular` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_v2_category`
--

INSERT INTO `legacy_v2_category` (`category_id`, `category_title`, `category_popular`) VALUES
(1, 'Uncategorized', 10),
(2, 'HTML5', 7),
(3, 'CSS3', 4),
(4, 'PHP', 5),
(5, 'MySQL', 2),
(6, 'Web design', 10),
(7, 'AJAX', 7),
(8, 'jQuery', 3),
(9, 'Photoshop', 1),
(10, 'JavaScript', 4),
(11, 'Illustrator', 7);

-- --------------------------------------------------------

--
-- Table structure for table `legacy_v2_category_parent`
--

CREATE TABLE IF NOT EXISTS `legacy_v2_category_parent` (
  `category_parent_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `blog_id` int(10) unsigned DEFAULT NULL,
  `review_id` int(10) unsigned DEFAULT NULL,
  `list_id` int(10) unsigned DEFAULT NULL,
  `tutorial_id` int(10) unsigned DEFAULT NULL,
  `portfolio_id` int(10) unsigned DEFAULT NULL,
  `download_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_v2_category_parent`
--

INSERT INTO `legacy_v2_category_parent` (`category_parent_id`, `category_id`, `blog_id`, `review_id`, `list_id`, `tutorial_id`, `portfolio_id`, `download_id`) VALUES
(1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(2, 2, NULL, 1, NULL, NULL, NULL, NULL),
(3, 11, NULL, NULL, 1, NULL, NULL, NULL),
(4, 8, NULL, NULL, 1, NULL, NULL, NULL),
(5, 6, NULL, NULL, 1, NULL, NULL, NULL),
(6, 8, NULL, NULL, NULL, 1, NULL, NULL),
(7, 6, NULL, NULL, NULL, NULL, 1, NULL),
(8, 2, NULL, NULL, NULL, NULL, NULL, 1),
(9, 6, NULL, NULL, NULL, NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `legacy_v2_comment`
--

CREATE TABLE IF NOT EXISTS `legacy_v2_comment` (
  `comment_id` int(10) NOT NULL,
  `comment_parent_id` int(10) NOT NULL COMMENT 'id for blog, review, tutorial, portfolio, download',
  `comment_parent` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'name of the parent, blog, review, tutorial, portfolio, download',
  `comment_reply` int(10) unsigned DEFAULT NULL,
  `comment_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment_ip` int(10) unsigned DEFAULT NULL,
  `date` datetime NOT NULL,
  `comment_website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment_text` text COLLATE utf8_unicode_ci NOT NULL,
  `comment_approved` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_v2_comment`
--

INSERT INTO `legacy_v2_comment` (`comment_id`, `comment_parent_id`, `comment_parent`, `comment_reply`, `comment_name`, `comment_ip`, `date`, `comment_website`, `comment_text`, `comment_approved`) VALUES
(1, 1, 'blog', NULL, 'comments name', NULL, '2011-03-06 01:00:00', 'http://saucy.se', 'blog comment text', '1'),
(2, 1, 'review', NULL, 'comments name', NULL, '2011-03-08 18:05:28', NULL, 'review comment text', '1'),
(3, 1, 'tutorial', NULL, NULL, NULL, '2011-03-16 22:56:13', NULL, 'tutorial comment text', '1'),
(4, 1, 'portfolio', NULL, NULL, NULL, '2011-03-22 01:04:02', NULL, 'portfolio comment text', '1'),
(5, 1, 'download', NULL, 'comment name', NULL, '2014-01-17 00:00:00', NULL, 'download comment text', '1'),
(6, 1, 'list', NULL, NULL, NULL, '2011-03-08 19:00:00', NULL, 'list comment text', '1'),
(7, 1, 'poll', NULL, 'comment name', NULL, '2014-01-17 00:00:00', NULL, 'poll comment text (doesn''t work)', '1');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_v2_download`
--

CREATE TABLE IF NOT EXISTS `legacy_v2_download` (
  `download_id` int(10) unsigned NOT NULL,
  `download_parent` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `download_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `download_text` text COLLATE utf8_unicode_ci NOT NULL,
  `download_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'download filename',
  `download_source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `download_views` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_v2_download`
--

INSERT INTO `legacy_v2_download` (`download_id`, `download_parent`, `download_title`, `date`, `download_text`, `download_file`, `download_source`, `download_views`) VALUES
(1, 'code', 'downloads title code', '2011-03-18 01:00:00', 'download text code', NULL, NULL, 119),
(2, 'config', 'downloads title config', '2011-03-18 02:00:00', 'download text config', NULL, NULL, 8),
(3, 'map', 'downloads title map', '2011-03-18 03:00:00', 'download text map', NULL, NULL, 7),
(4, 'model', 'downloads title model', '2011-03-18 04:00:00', 'download text model', NULL, 'http://saucy.se', 4),
(5, 'video', 'downloads title video', '2011-03-18 05:00:00', 'download text video', NULL, NULL, 8),
(6, 'wallpaper', 'downloads title wallpaper', '2011-03-18 06:00:00', 'download text wallpaper', NULL, NULL, 23);

-- --------------------------------------------------------

--
-- Table structure for table `legacy_v2_link`
--

CREATE TABLE IF NOT EXISTS `legacy_v2_link` (
  `link_id` int(10) NOT NULL,
  `link_category` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Uncategorized',
  `link_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_v2_link`
--

INSERT INTO `legacy_v2_link` (`link_id`, `link_category`, `link_title`, `link_url`) VALUES
(1, 'Uncategorized', 'SAUCYLOL', 'http://www.saucy.se'),
(2, 'Uncategorized', 'SAUCY', 'http://saucy.se'),
(3, 'Uncategorized', 'SAUCYHEHE', 'http://saucy.se/'),
(4, 'Web design', 'WEBBENLOL', 'http://saucy.se/new/'),
(5, 'Science', 'SIENCELOL', 'http://www.saucy.se/new/'),
(6, 'Web design', 'löl', 'http://saucy.se/new');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_v2_list`
--

CREATE TABLE IF NOT EXISTS `legacy_v2_list` (
  `list_id` int(10) unsigned NOT NULL,
  `list_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `list_views` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_v2_list`
--

INSERT INTO `legacy_v2_list` (`list_id`, `list_title`, `date`, `list_views`) VALUES
(1, 'List title', '2011-03-08 14:00:00', 118);

-- --------------------------------------------------------

--
-- Table structure for table `legacy_v2_list_content`
--

CREATE TABLE IF NOT EXISTS `legacy_v2_list_content` (
  `list_content_id` int(10) NOT NULL,
  `list_content_parent_id` int(10) NOT NULL,
  `list_content_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `list_content_text` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_v2_list_content`
--

INSERT INTO `legacy_v2_list_content` (`list_content_id`, `list_content_parent_id`, `list_content_title`, `date`, `list_content_text`) VALUES
(1, 1, 'List content title 1', '2011-03-10 21:56:47', 'List content text'),
(2, 1, 'List content title 2', '2011-10-24 00:00:00', 'List content text'),
(3, 1, 'List content title 3', '2011-03-10 21:56:47', 'List content text'),
(4, 1, 'List content title 4', '2011-03-10 21:56:47', 'List content text'),
(5, 1, 'List content title 5', '2011-03-10 21:56:47', 'List content text'),
(6, 1, 'List content title 6', '2011-03-10 21:56:47', 'List content text'),
(7, 1, 'List content title 7', '2011-03-10 21:56:47', 'List content text'),
(8, 1, 'List content title 8', '2011-03-10 21:56:47', 'List content text'),
(9, 1, 'List content title 9', '2011-03-10 21:56:47', 'List content text'),
(10, 1, 'List content title 10', '2011-03-10 21:56:47', 'List content text');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_v2_portfolio`
--

CREATE TABLE IF NOT EXISTS `legacy_v2_portfolio` (
  `portfolio_id` int(10) unsigned NOT NULL,
  `tutorial_id` int(10) DEFAULT NULL,
  `portfolio_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `portfolio_text` text COLLATE utf8_unicode_ci NOT NULL,
  `portfolio_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `portfolio_views` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_v2_portfolio`
--

INSERT INTO `legacy_v2_portfolio` (`portfolio_id`, `tutorial_id`, `portfolio_title`, `date`, `portfolio_text`, `portfolio_file`, `portfolio_views`) VALUES
(1, 1, 'portfolio title', '2011-03-17 03:36:41', 'Some random text about some random things that should be in this text. Wicked fun to come up absolutely nothing in a text! How would the world look if there were no random people writing about random stuff? Some random text about some random things that should be in this text. Wicked fun to come up absolutely nothing in a text! How would the world look if there were no random people writing about random stuff?', 'Protoss.jpg', 11);

-- --------------------------------------------------------

--
-- Table structure for table `legacy_v2_question`
--

CREATE TABLE IF NOT EXISTS `legacy_v2_question` (
  `question_id` int(10) unsigned NOT NULL,
  `question_parent` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'question',
  `question_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `question_text` text COLLATE utf8_unicode_ci NOT NULL,
  `question_source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_v2_question`
--

INSERT INTO `legacy_v2_question` (`question_id`, `question_parent`, `question_title`, `date`, `question_text`, `question_source`) VALUES
(1, 'question', 'question title', '2011-03-20 16:01:57', 'question text', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `legacy_v2_review`
--

CREATE TABLE IF NOT EXISTS `legacy_v2_review` (
  `review_id` int(10) unsigned NOT NULL,
  `review_parent` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'review',
  `review_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `review_text` text COLLATE utf8_unicode_ci NOT NULL,
  `review_rating` int(2) unsigned DEFAULT NULL,
  `review_source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `review_views` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_v2_review`
--

INSERT INTO `legacy_v2_review` (`review_id`, `review_parent`, `review_title`, `date`, `review_text`, `review_rating`, `review_source`, `review_views`) VALUES
(1, 'review', 'review title', '2011-03-08 14:00:00', 'review text', 1, NULL, 30);

-- --------------------------------------------------------

--
-- Table structure for table `legacy_v2_tool`
--

CREATE TABLE IF NOT EXISTS `legacy_v2_tool` (
  `tool_id` int(10) NOT NULL,
  `tool_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tool_popular` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_v2_tool`
--

INSERT INTO `legacy_v2_tool` (`tool_id`, `tool_title`, `tool_popular`) VALUES
(1, 'Notepad', 10),
(2, '7-zip', 5);

-- --------------------------------------------------------

--
-- Table structure for table `legacy_v2_tool_parent`
--

CREATE TABLE IF NOT EXISTS `legacy_v2_tool_parent` (
  `tool_parent_id` int(10) unsigned NOT NULL,
  `tool_id` int(10) unsigned DEFAULT NULL,
  `list_id` int(10) unsigned DEFAULT NULL,
  `tutorial_id` int(10) unsigned DEFAULT NULL,
  `portfolio_id` int(10) unsigned DEFAULT NULL,
  `download_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_v2_tool_parent`
--

INSERT INTO `legacy_v2_tool_parent` (`tool_parent_id`, `tool_id`, `list_id`, `tutorial_id`, `portfolio_id`, `download_id`) VALUES
(1, 1, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `legacy_v2_tutorial`
--

CREATE TABLE IF NOT EXISTS `legacy_v2_tutorial` (
  `tutorial_id` int(10) unsigned NOT NULL,
  `tutorial_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `tutorial_text` text COLLATE utf8_unicode_ci NOT NULL,
  `tutorial_difficulty` int(2) unsigned DEFAULT NULL,
  `tutorial_time` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'completion time',
  `tutorial_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'download filename',
  `tutorial_source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tutorial_views` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `legacy_v2_tutorial`
--

INSERT INTO `legacy_v2_tutorial` (`tutorial_id`, `tutorial_title`, `date`, `tutorial_text`, `tutorial_difficulty`, `tutorial_time`, `tutorial_file`, `tutorial_source`, `tutorial_views`) VALUES
(1, 'tutorial title', '2011-03-16 20:20:00', 'tutorial text', 3, '1-2 hours', 'profile.png', NULL, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `legacy_v2_blog`
--
ALTER TABLE `legacy_v2_blog`
ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `legacy_v2_category`
--
ALTER TABLE `legacy_v2_category`
ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `legacy_v2_category_parent`
--
ALTER TABLE `legacy_v2_category_parent`
ADD PRIMARY KEY (`category_parent_id`);

--
-- Indexes for table `legacy_v2_comment`
--
ALTER TABLE `legacy_v2_comment`
ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `legacy_v2_download`
--
ALTER TABLE `legacy_v2_download`
ADD PRIMARY KEY (`download_id`);

--
-- Indexes for table `legacy_v2_link`
--
ALTER TABLE `legacy_v2_link`
ADD PRIMARY KEY (`link_id`);

--
-- Indexes for table `legacy_v2_list`
--
ALTER TABLE `legacy_v2_list`
ADD PRIMARY KEY (`list_id`);

--
-- Indexes for table `legacy_v2_list_content`
--
ALTER TABLE `legacy_v2_list_content`
ADD PRIMARY KEY (`list_content_id`);

--
-- Indexes for table `legacy_v2_portfolio`
--
ALTER TABLE `legacy_v2_portfolio`
ADD PRIMARY KEY (`portfolio_id`);

--
-- Indexes for table `legacy_v2_question`
--
ALTER TABLE `legacy_v2_question`
ADD PRIMARY KEY (`question_id`);

--
-- Indexes for table `legacy_v2_review`
--
ALTER TABLE `legacy_v2_review`
ADD PRIMARY KEY (`review_id`);

--
-- Indexes for table `legacy_v2_tool`
--
ALTER TABLE `legacy_v2_tool`
ADD PRIMARY KEY (`tool_id`);

--
-- Indexes for table `legacy_v2_tool_parent`
--
ALTER TABLE `legacy_v2_tool_parent`
ADD PRIMARY KEY (`tool_parent_id`);

--
-- Indexes for table `legacy_v2_tutorial`
--
ALTER TABLE `legacy_v2_tutorial`
ADD PRIMARY KEY (`tutorial_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `legacy_v2_blog`
--
ALTER TABLE `legacy_v2_blog`
MODIFY `blog_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `legacy_v2_category`
--
ALTER TABLE `legacy_v2_category`
MODIFY `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `legacy_v2_category_parent`
--
ALTER TABLE `legacy_v2_category_parent`
MODIFY `category_parent_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `legacy_v2_comment`
--
ALTER TABLE `legacy_v2_comment`
MODIFY `comment_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `legacy_v2_download`
--
ALTER TABLE `legacy_v2_download`
MODIFY `download_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `legacy_v2_link`
--
ALTER TABLE `legacy_v2_link`
MODIFY `link_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `legacy_v2_list`
--
ALTER TABLE `legacy_v2_list`
MODIFY `list_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `legacy_v2_list_content`
--
ALTER TABLE `legacy_v2_list_content`
MODIFY `list_content_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `legacy_v2_portfolio`
--
ALTER TABLE `legacy_v2_portfolio`
MODIFY `portfolio_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `legacy_v2_question`
--
ALTER TABLE `legacy_v2_question`
MODIFY `question_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `legacy_v2_review`
--
ALTER TABLE `legacy_v2_review`
MODIFY `review_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `legacy_v2_tool`
--
ALTER TABLE `legacy_v2_tool`
MODIFY `tool_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `legacy_v2_tool_parent`
--
ALTER TABLE `legacy_v2_tool_parent`
MODIFY `tool_parent_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `legacy_v2_tutorial`
--
ALTER TABLE `legacy_v2_tutorial`
MODIFY `tutorial_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
