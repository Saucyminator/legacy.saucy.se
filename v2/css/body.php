<?php

echo "<style>

* {
	padding: 0px;
	margin: 0px;
}

body {
    background: ".$saucy["Default"]["Color"]["Dark"].";
    color: ".$saucy["Default"]["Color"]["Light"].";
    margin: 0px auto;
    font: 12px ".$saucy["Default"]["Font"].";
	line-height: ".$saucy["Default"]["FontHeight"].";
}

/* Content box used for the under construction/IP pages */
#indexContent {
    position: relative;
	margin: 0px auto;
    text-align: center;
    vertical-align: text-bottom;
	top: 100%;
}

/* FONT STYLES & LINKS */
.IP {
    font: bold 50px ".$saucy["Default"]["Font"].";
	line-height: ".$saucy["Default"]["FontHeight"].";
}
.massive {
    font: 22px ".$saucy["Default"]["Font"].";
	line-height: ".$saucy["Default"]["FontHeight"].";
}
.huge {
    font: 18px ".$saucy["Default"]["Font"].";
	line-height: ".$saucy["Default"]["FontHeight"].";
}
.big {
    font: bold 14px ".$saucy["Default"]["Font"].";
	line-height: ".$saucy["Default"]["FontHeight"].";
}
.normal {
    font: 12px ".$saucy["Default"]["Font"].";
	line-height: ".$saucy["Default"]["FontHeight"].";
}
.normali {
    font: italic 12px ".$saucy["Default"]["Font"].";
	line-height: ".$saucy["Default"]["FontHeight"].";
}
.small {
    font: 10px ".$saucy["Default"]["Font"].";
	line-height: ".$saucy["Default"]["FontHeight"].";
}
.smalli {
    font: italic 10px ".$saucy["Default"]["Font"].";
	line-height: ".$saucy["Default"]["FontHeight"].";
}
.tiny {
    font: 10px ".$saucy["Default"]["Font"].";
	-webkit-text-size-adjust:none;
}
a:link {
    color: ".$saucy["Default"]["Color"]["Link"].";
	line-height: ".$saucy["Default"]["FontHeight"].";
    text-decoration: none;
    cursor: pointer;
}
a:visited {
    color: ".$saucy["Default"]["Color"]["Link_visited"].";
	line-height: ".$saucy["Default"]["FontHeight"].";
    text-decoration: none;
    cursor: pointer;
}
a:hover, a:active {
    color: ".$saucy["Default"]["Color"]["Light"].";
	line-height: ".$saucy["Default"]["FontHeight"].";
    text-decoration: underline;
    cursor: pointer;
}

img, video, iframe {
    border: 1px solid ".$saucy["Default"]["Color"]["Black"].";
}

/*
.disclaimer {
    margin: 10px 45px;
    padding: 6px;
    text-align: center;
}
.disclaimer:hover {
    background: ".$saucy["Default"]["Color"]["Darker"]." url('".$saucy["Default"]["Image"]["Validate"]["Close"]."') 98% 15% no-repeat;
    cursor: pointer;
}
*/

/* INPUT */
input[type='search']::-webkit-search-decoration,
input[type='search']::-webkit-search-cancel-button,
input[type='search']::-webkit-search-results-button,
input[type='search']::-webkit-search-results-decoration {
  display: none;
}
/* Needed?
input[type='search'] {
      -webkit-appearance: textfield;
}
*/
input.submit {
    cursor: pointer;
    color: ".$saucy["Default"]["Color"]["Light"].";
    float: right;
    background: ".$saucy["Default"]["Color"]["Darker"].";
    position: relative;
    width: 120px;
    outline: 1px solid ".$saucy["Default"]["Color"]["Black"].";
    outline-offset: 0px;
	border: 1px solid ".$saucy["Default"]["Color"]["Border"].";
    padding: $box_pad"."px;
    z-index: 10;
    margin: 0px 3px 0px 0px;
}
input.submit:hover {
    text-decoration: underline;
}
/*
#search {
	
}
*/
input.searchfield {
    background: ".$saucy["Default"]["Color"]["Darker"].";
    color: ".$saucy["Default"]["Color"]["Light"].";
	width: ".(($saucy["Default"]["WidthPanel"] - $box_search_width) - ((2*5) + 7))."px;
    height: $social"."px;
    font: 19px ".$saucy["Default"]["Font"].";
    padding: 0px 5px;
    cursor: text;
    border: 0px;
	-webkit-box-sizing: content-box;
}
input.search {
    background: ".$saucy["Default"]["Color"]["Darker"].";
    color: ".$saucy["Default"]["Color"]["Light"].";
    width: $box_search_width"."px;
    height: $social"."px;
    font: 18px ".$saucy["Default"]["Font"].";
    border: 0px;
}
input.search:hover {
    background: ".$saucy["Default"]["Color"]["Dark"].";
    outline: 1px solid ".$saucy["Default"]["Color"]["Link_completed"].";
    cursor: pointer;
}
input.searchfield:hover, input.searchfield:focus {
    background: ".$saucy["Default"]["Color"]["Dark"].";
    outline: 1px solid ".$saucy["Default"]["Color"]["Link_completed"].";
    outline-offset: 0px;
    cursor: text;
}
input, select, option {
    font: ".($box_pad*2)."px ".$saucy["Default"]["Font"].";
	color: ".$saucy["Default"]["Color"]["Dark"].";
	background: ".$saucy["Default"]["Color"]["Light"].";
}
textarea {
	background: ".$saucy["Default"]["Color"]["Light"].";
	color: ".$saucy["Default"]["Color"]["Dark"].";
}
optgroup {
	font: bolder italic 14px ".$saucy["Default"]["Font"].";
}
input:focus, select:focus, option:focus, textarea:focus {
    outline: 3px solid ".$saucy["Default"]["Color"]["Link_completed"].";
    outline-offset: -1px;
}

input:required:invalid, input:focus:invalid, textarea:required:invalid, textarea:focus:invalid {
    background: ".$saucy["Default"]["Color"]["Light"]." url('".$saucy["Default"]["Image"]["Validate"]["Error"]."') top right no-repeat;
    background-size: 16px;
    color: ".$saucy["Default"]["Color"]["Dark"].";
}
input:required:valid, textarea:required:valid {
    background: ".$saucy["Default"]["Color"]["Light"]." url('".$saucy["Default"]["Image"]["Validate"]["Success"]."') top right no-repeat;
    background-size: 16px;
    color: ".$saucy["Default"]["Color"]["Dark"].";
}
/* PLACEHOLDER (must be seperated) */
input::-webkit-input-placeholder, textarea::-webkit-input-placeholder {
    color: ".$saucy["Default"]["Color"]["Link_visited"].";
	font-style: italic;
}
input:-moz-placeholder, textarea:-moz-placeholder {
    color: ".$saucy["Default"]["Color"]["Link_visited"].";
	font-style: italic;
}

.form {
    background: ".$saucy["Default"]["Color"]["Light"].";
    font: 12px ".$saucy["Default"]["Font"].";
    border: 1px solid ".$saucy["Default"]["Color"]["Border"].";
    color: ".$saucy["Default"]["Color"]["Dark"].";
    width: 100%;
    height: 20px;
}

.social {
    background: ".$saucy["Default"]["Color"]["Darker"].";
    position: relative;
    width: $social"."px;
    height: $social"."px;
    border: 1px solid ".$saucy["Default"]["Color"]["Border"].";
    outline: 1px solid ".$saucy["Default"]["Color"]["Black"].";
    margin-right: $social_margin"."px;
    float: left;
}

.social_icon {

	border: 0;
}
.hoverimage {
	background-image: url('$social_hover');
	position: absolute;
	width: 38px;
	height: 38px;
	top: 0;
	left: 0;
	border: 0px;
	display: none;
	opacity: 0.05;
}
.social:hover .hoverimage {
	display: block;
}

.box {
    background: ".$saucy["Default"]["Color"]["Darker"].";
    position: relative;
    outline: 1px solid ".$saucy["Default"]["Color"]["Black"].";
    outline-offset: 0px;
	border: 1px solid ".$saucy["Default"]["Color"]["Border"].";
    padding: $box_pad"."px;
    z-index: 10;
    margin-bottom: ".($box_pad*2)."px;
}
.box_small {
    background: ".$saucy["Default"]["Color"]["Darker"].";
    position: relative;
    width: $box_small_width"."px;
    height: $box_small_height"."px;
    outline: 1px solid ".$saucy["Default"]["Color"]["Black"].";
    outline-offset: 0px;
	border: 1px solid ".$saucy["Default"]["Color"]["Border"].";
    padding: $box_pad"."px 0px $box_pad"."px 0px;
    z-index: 10;
}
.box_small_wrapper {
    position: relative;
    width: ".$saucy["Default"]["WidthContent"]."px;
    height: ".($box_small_height+14)."px;
    margin-bottom: ".($box_pad*2)."px;
}

#content_nav_background {
    background: ".$saucy["Default"]["Color"]["Darker"].";
    position: relative;
    width: 100%;
	border-bottom: 1px solid ".$saucy["Default"]["Color"]["Border"].";
    outline: 1px solid ".$saucy["Default"]["Color"]["Black"].";
    z-index: 10;
    text-align: center;
    margin: 0px 0px ".($box_pad*2)."px 0px;
}
#content_nav {
	width: ".$saucy["Default"]["Width"]."px;
	margin: 0px auto;
    padding: ".($box_pad*3)."px 0px $box_pad"."px 0px;
    display: -webkit-box;
    display: -moz-box;
	display: box;
	-webkit-box-orient: horizontal;
	-moz-box-orient: horizontal;
	box-orient: horizontal;
}
.nav {
	-webkit-box-flex: 1;
	-moz-box-flex: 1;
	box-flex: 1;
	display: block;
}
#content_social {
    position: relative;
    width: ".$saucy["Default"]["Width"]."px;
    height: 40px;
    margin: 0px auto ".($box_pad*2)."px auto;
}
#content_wrapper {
    position: relative;
    width: ".$saucy["Default"]["Width"]."px;
    margin: 0px auto;
}
#content_left {
    position: relative;
    width: ".$saucy["Default"]["WidthPanel"]."px;
    float: left;
}
#content_middle {
    position: relative;
    width: ".$saucy["Default"]["WidthContent"]."px;
    margin-left: ".($box_pad*2)."px;
    float: left;
}
#content_right {
    position: relative;
    width: ".$saucy["Default"]["WidthPanel"]."px;
    float: right;
}
.box:hover, .box_small:hover, #content_nav_background:hover {
    background: ".$saucy["Default"]["Color"]["Quote"].";
}

.banner {
    width: ".$saucy["Panel"]["Banner"]["Size"]["Width"].";
    height: ".$saucy["Panel"]["Banner"]["Size"]["Height"].";
    position: relative;
	margin: $box_pad"."px $box_pad"."px 0px $box_pad"."px;
    z-index: 10;
	border: 0px;
	text-align: center;
}

.profile_img {
    background: url('".$saucy["Default"]["Image"]["Profile"]["Image"]."') no-repeat top center;
    position: relative;
    width: ".$saucy["Default"]["Image"]["Profile"]["Size"]."px;
    height: ".$saucy["Default"]["Image"]["Profile"]["Size"]."px;
    margin: ".($box_pad-2)."px $box_pad"."px 0px $box_pad"."px;
    outline: 1px solid ".$saucy["Default"]["Color"]["Black"].";
    float: right;
    z-index: 10;
}
.spacer {
    background: url('".$saucy["Default"]["Image"]["Spacer"]."') repeat-x;
    position: relative;
    height: 2px;
    margin: $box_pad"."px 2px $box_pad"."px 2px;
    z-index: 20;
}
.text {
    position: relative;
    text-align: center;
    padding: 0px 6px;
}
.text_small {
    position: relative;
    text-align: justify;
    padding: 0px 6px;
}
.content_small {
    position: relative;
    text-align: justify;
    height: 15px;
    padding: 0px 6px;
}

.linkz {
    background: ".$saucy["Default"]["Color"]["Dark"].";
    position: relative;
    border: 1px solid ".$saucy["Default"]["Color"]["Black"].";
    margin: $box_pad"."px;
    padding: 3px $box_pad"."px;
    z-index: 40;
}
.linkz:hover {
    background: ".$saucy["Default"]["Color"]["Darker"].";
    z-index: 40;
}

/*
a.linkz {
    color: ".$saucy["Default"]["Color"]["Link"].";
}
a.linkz:hover {
    color: ".$saucy["Default"]["Color"]["Light"].";
    text-decoration: underline;
}
*/


blockquote {
    background: ".$saucy["Default"]["Color"]["Quote"].";
    margin: 0px $box_pad"."px 2px $box_pad"."px;
    color: ".$saucy["Default"]["Color"]["Link_completed"].";
    border-left: 2px solid ".$saucy["Default"]["Color"]["Light"].";
    border-top: 1px solid ".$saucy["Default"]["Color"]["Dark"].";
    border-right: 1px solid ".$saucy["Default"]["Color"]["Dark"].";
    border-bottom: 1px solid ".$saucy["Default"]["Color"]["Dark"].";
    padding: ".($box_pad*2)."px;
    font-style: italic;
}

.quotewrapper {
	background: ".$saucy["Default"]["Color"]["Light"].";
    margin: 0px $box_pad"."px;
	padding-left: 2px;
	border: 1px solid ".$saucy["Default"]["Color"]["Dark"].";
	clear: both;
}
.quotetitle {
    background: ".$saucy["Default"]["Color"]["Quote"].";
	color: ".$saucy["Default"]["Color"]["Link"].";
	padding: $box_pad"."px ".($box_pad*2)."px;
	border: 1px solid ".$saucy["Default"]["Color"]["Dark"].";
}
.quotecontent {
    background: ".$saucy["Default"]["Color"]["Quote"].";
    color: ".$saucy["Default"]["Color"]["Link_completed"].";
	padding: ".($box_pad*2)."px;
	font-style: italic;
}
.quotewrapper, .quotetitle { 
	margin: 0px; 
	border-width: 0px 0px 1px 0px;
}
.quotewrapper, .quotecontent {
	margin: 0px; 
	border-width: 1px 1px 1px 0px;
}

/*
#img_wrapper {}
#img_text {
    background: ".$saucy["Default"]["Color"]["Light"].";
}
*/

/* ARTICLE - ADD COMMENT */
.addcomment_text {
    background: none;
    margin-bottom: $addcomment_margin"."px;
}
.addcomment_input {
    background: none;
    margin-bottom: ".($addcomment_margin)."px;
    height: 36px;
    padding-right: 2px;
}



/* qTip (jQuery tooltip) style */
.ui-tooltip, .qtip {
	position: absolute;
	left: -28000px;
	top: -28000px;
	display: none;
	max-width: auto;
	min-width: auto;
	z-index: 15000;
}
.ui-tooltip-default .ui-tooltip-titlebar,
.ui-tooltip-default .ui-tooltip-content {
	border: 1px solid ".$saucy["Default"]["Color"]["Border"].";
	background-color: ".$saucy["Default"]["Color"]["Darker"].";
	color: ".$saucy["Default"]["Color"]["Light"].";
	outline: 1px solid ".$saucy["Default"]["Color"]["Black"].";
	outline-offset: 0px;
	padding: 3px 6px 4px 6px;
	font: 12px/16px ".$saucy["Default"]["Font"].";
}

.dev {
    background: ".$saucy["Default"]["Color"]["Darker"].";
    position: absolute;
    width: $dev_text_width"."px;
    padding: $box_pad"."px;
    outline: 1px solid ".$saucy["Default"]["Color"]["Black"].";
	border: 1px solid ".$saucy["Default"]["Color"]["Border"].";
    z-index: 120;
}
.dev_text_important {
	background: ".$saucy["DevColors"]["Color"]["Important"].";
	color: ".$saucy["Default"]["Color"]["Light"].";
}
.dev_text_development {
	background: ".$saucy["DevColors"]["Color"]["Development"].";
	color: ".$saucy["Default"]["Color"]["Light"].";
}
.dev_text_add {
	background: ".$saucy["DevColors"]["Color"]["Add"].";
	color: ".$saucy["Default"]["Color"]["Dark"].";
}
.dev_text_remove {
	background: ".$saucy["DevColors"]["Color"]["Remove"].";
	color: ".$saucy["Default"]["Color"]["Dark"].";
}
.dev_text_fix {
	background: ".$saucy["DevColors"]["Color"]["Fix"].";
	color: ".$saucy["Default"]["Color"]["Dark"].";
}
.dev_text_think {
	background: ".$saucy["DevColors"]["Color"]["Think"].";
	color: ".$saucy["Default"]["Color"]["Dark"].";
}
.dev_text_change {
	background: ".$saucy["DevColors"]["Color"]["Change"].";
	color: ".$saucy["Default"]["Color"]["Dark"].";
}
.dev_text_undone {
	background: ".$saucy["DevColors"]["Color"]["Undone"].";
	color: ".$saucy["Default"]["Color"]["Dark"].";
	text-decoration: line-through;
}
.dev_text_strike {
	text-decoration: line-through;
}";

if(!empty($saucy["DevColors"]["Enabled"])) {

	echo "
	.IP {
		background: ".$saucy["DevColors"]["Color"]["IP"].";
	}
	.massive {
		background: ".$saucy["DevColors"]["Color"]["Massive"].";
	}
	.huge {
		background: ".$saucy["DevColors"]["Color"]["Huge"].";
	}
	.big {
		background: ".$saucy["DevColors"]["Color"]["Big"].";
	}
	.normal {
		background: ".$saucy["DevColors"]["Color"]["Normal"].";
	}
	.normali {
		background: ".$saucy["DevColors"]["Color"]["Normali"].";
	}
	.smalli {
		background: ".$saucy["DevColors"]["Color"]["Smalli"].";
	}
	.small {
		background: ".$saucy["DevColors"]["Color"]["Small"].";
	}
	.spacer {
		background: ".$saucy["DevColors"]["Color"]["Spacer"].";
	}
	.content {
		background: ".$saucy["DevColors"]["Color"]["Content"].";
	}
	.text {
		background: ".$saucy["DevColors"]["Color"]["Text"].";
	}
	.text_small {
		background: ".$saucy["DevColors"]["Color"]["Text_small"].";
	}
	.content_small {
		background: ".$saucy["DevColors"]["Color"]["Content_small"].";
	}
	.linkz {
		background: ".$saucy["DevColors"]["Color"]["Linkz"].";
	}
	input.search {
		background:  ".$saucy["DevColors"]["Color"]["InputSearch"].";
	}";
	
}

echo "</style>";

?>