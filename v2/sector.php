<?php

require("inc/settings.php");
require($saucy["Default"]["Conn"]);
require($saucy["Default"]["Functions"]);
require_once($saucy["Default"]["SubmitRating"]);

/*	SHARES CODE WITH:

.: Past name : blog.php :.

.: FILES :.
blog
review
tutorial
portfolio
list
poll
download


.: DB :.
blog_id
blog_parent
blog_title
date
blog_text
blog_source
blog_views

review_id
review_parent
review_title
date
review_text
review_rating
review_source
review_views

tutorial_id
tutorial_title
date
tutorial_text
tutorial_difficulty
tutorial_time
tutorial_file
tutorial_source
tutorial_views

portfolio_id
tutorial_id
portfolio_title
date
portfolio_text
portfolio_file
portfolio_views

list_id
list_title
date
list_views

download_id
download_parent
download_title
date
download_text
download_file
download_source
download_views

comment_id
comment_parent_id
comment_parent
comment_reply
comment_name
comment_ip
date
comment_website
comment_text
comment_approved

*/

$page	= isset($_GET["page"]) ? $_GET["page"] : NULL;
$pagelist_amount = count($sectorlist);
if(!in_array($page, $sectorlist)) {

	echo "You are trying to access a page that does not exist.";
	exit;
	
}
/*
if(isset($_GET["page"])) { // check if page even exist, otherwise set it to null

    $page = $_GET["page"];

} else {

    $page = NULL;
    
}
$pagelist_amount = count($sectorlist);
if(!in_array($page, $sectorlist)) {

	echo "You are trying to access a page that does not exist.";
	exit;
	
}
*/

?>
<!DOCTYPE html>
<html lang='sv'>
<head>
<title></title>

<?php

echo "<script src='".$saucy["Default"]["JS"]["jQuery"]."'></script>";
//echo "<script src='".$saucy["Default"]["JS"]["MaxRating"]."'></script>";
echo "<script src='".$saucy["Default"]["JS"]["TimeAgo"]."'></script>";
echo "<script src='".$saucy["Default"]["JS"]["Qtip"]."'></script>";

?>

<script>
$(document).ready(function(){
	jQuery(".timeagoSector").timeago();
	jQuery.timeago.settings.allowFuture = true;
	
    $(".showSector").bind("click",function() {
        $("."+this.id ).stop(true, true).slideToggle("<?php echo $saucy["Default"]["ToggleSpeed"]; ?>");
    });
	
	$(".tooltipSector, .tooltipMenu").each(function() {
		$(this).qtip({
			content: {
				text: $(this).attr("title"),
			},
			position: {
				target: "mouse",
				adjust: { x: 15, y: 0, },
			},
			style: {
				tip: false
			},
			show: {
				delay: 0
			},
			hide: {
				delay: 200
			}
		});
	});
});
</script>

</head>
<body>

<?php

$parent = str_replace(".php", "", strtolower(basename($_SERVER["PHP_SELF"])));

include("subMenu.php");

// -------------------------------------------------------------------------------------------------------------------

if($page == "blog") {

	// BLOG
  $tbl = "SELECT {$page}_id, {$page}_title, date, {$page}_text, {$page}_source, {$page}_views
  FROM {$saucy["DevDB"]["Name"][$page]}
  ORDER BY date DESC
  LIMIT {$saucy["Default"]["Limit"]["Articles"]}";

} elseif($page == "review") {

	// REVIEW
  $tbl = "SELECT {$page}_id, {$page}_title, date, {$page}_text, {$page}_rating, {$page}_source, {$page}_views
	FROM {$saucy["DevDB"]["Name"][$page]}
	ORDER BY date DESC
	LIMIT {$saucy["Default"]["Limit"]["Articles"]}";

} elseif($page == "tutorial") {

	// TUTORIAL
  $tbl = "SELECT {$page}_id, {$page}_title, date, {$page}_text, {$page}_difficulty, {$page}_time, {$page}_file, {$page}_source, {$page}_views
	FROM {$saucy["DevDB"]["Name"][$page]}
	ORDER BY date DESC
	LIMIT {$saucy["Default"]["Limit"]["Articles"]}";

} elseif($page == "portfolio") {

	// PORTFOLIO
  $tbl = "SELECT {$page}_id, tutorial_id, {$page}_title, date, {$page}_text, {$page}_file, {$page}_views
	FROM {$saucy["DevDB"]["Name"][$page]}
	ORDER BY date ASC
	LIMIT {$saucy["Default"]["Limit"]["Other"]}";

} elseif($page == "list") {

	// LIST
  $tbl = "SELECT {$page}_id, {$page}_title, date, {$page}_views
	FROM {$saucy["DevDB"]["Name"][$page]}
	ORDER BY date DESC
	LIMIT {$saucy["Default"]["Limit"]["Articles"]}";

} elseif($page == "download") {

	// DOWNLOAD
  $tbl = "SELECT {$page}_id, {$page}_parent, {$page}_title, date, {$page}_text, {$page}_file, {$page}_source, {$page}_views
	FROM {$saucy["DevDB"]["Name"][$page]}
	ORDER BY date DESC
	LIMIT {$saucy["Default"]["Limit"]["Articles"]}";

}

$stmt = $conn->prepare($tbl);
$stmt->execute();
while ($row = $stmt->fetch()) {

	if($page == "blog"){
	
		// BLOG
		$id					= $row[$page."_id"];
		$title				= $row[$page."_title"];
		$date				= $row["date"];
		$text				= $row[$page."_text"];
		$source				= $row[$page."_source"];
		$views				= $row[$page."_views"];
		
	} elseif($page == "review") {
	
		// REVIEW
		$id					= $row[$page."_id"];
		$title				= $row[$page."_title"];
		$date				= $row["date"];
		$text				= $row[$page."_text"];
		$rating				= $row[$page."_rating"];
		$source				= $row[$page."_source"];
		$views				= $row[$page."_views"];
		
	} elseif($page == "tutorial") {
	
		// TUTORIAL
		$id					= $row[$page."_id"];
		$title				= $row[$page."_title"];
		$date				= $row["date"];
		$text				= $row[$page."_text"];
		$difficulty			= $row[$page."_difficulty"];
		$time				= $row[$page."_time"];
		$file				= $row[$page."_file"];
		$source				= $row[$page."_source"];
		$views				= $row[$page."_views"];
		
	} elseif($page == "portfolio") {
	
		// PORTFOLIO
		$id					= $row[$page."_id"];
		$id_tutorial		= $row["tutorial_id"];
		$title				= $row[$page."_title"];
		$date				= $row["date"];
		$text				= $row[$page."_text"];
		$file				= $row[$page."_file"];
		$views				= $row[$page."_views"];
		
	} elseif($page == "list") {
	
		// LIST
		$id					= $row[$page."_id"];
		$title				= $row[$page."_title"];
		$date				= $row["date"];
		$views				= $row[$page."_views"];
		
	} elseif($page == "download") {
	
		// DOWNLOAD
		$id					= $row[$page."_id"];
		$download_parent	= $row[$page."_parent"];
		$title				= $row[$page."_title"];
		$date				= $row["date"];
		$text				= $row[$page."_text"];
		$file				= $row[$page."_file"];
		$source				= $row[$page."_source"];
		$views				= $row[$page."_views"];
		
	}
	
	// COMMENT
  $sql = "SELECT comment_parent_id, comment_parent, comment_approved
  FROM {$saucy["DevDB"]["Name"]["comment"]}
  WHERE comment_parent_id = :id AND comment_parent = :page AND comment_approved = 1
  ORDER BY date DESC";
  $stmt = $conn->prepare($sql);
  $stmt->bindParam(':id', $list_id_index, PDO::PARAM_INT);
  $stmt->bindParam(':page', $list_id_index, PDO::PARAM_STR);
  $stmt->execute();
  $comments_amount = $stmt->rowCount();

    echo "<div class='box'>
        <div class='text big'>
			<a id='article.php?page=$page&id=$id' class='link_click tooltipSector' title='".bbkod($saucy["Default"]["Locale"]["ReadMoreHover"]." ".$title)."'>";
				if($page == "download") { echo strtoupper($download_parent)." : "; } echo truncate_str($title, $saucy["Default"]["Limit"]["Title"])."
			</a>
        </div>
		<div class='spacer'></div>
		<div class='content_small smalli' style='margin-bottom: ".$box_pad."px;'>
			<span style='float: left;'>
				[ <span class='timeagoSector tooltipSector' title='$date'>".date("H:i @ jS \of F Y", strtotime($date))."</span> ]
			</span>
			<span style='float: right;'>";

        $sql = "SELECT category_parent_id, {$saucy["DevDB"]["Name"]["categoryparent"]}.category_id, {$saucy["DevDB"]["Name"][$page]}.{$page}_id, category_title
        FROM {$saucy["DevDB"]["Name"]["categoryparent"]}
        INNER JOIN {$saucy["DevDB"]["Name"]["category"]} ON {$saucy["DevDB"]["Name"]["category"]}.category_id = {$saucy["DevDB"]["Name"]["categoryparent"]}.category_id
        INNER JOIN {$saucy["DevDB"]["Name"][$page]} ON {$saucy["DevDB"]["Name"][$page]}.{$page}_id = {$saucy["DevDB"]["Name"]["categoryparent"]}.category_id
        WHERE {$saucy["DevDB"]["Name"]["categoryparent"]}.category_id = :id";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $category_amount = $stmt->rowCount();

				// CATEGORY
				if(!empty($category_amount)) {
				
					echo "[ Cat: ";

          while ($row_category = $stmt->fetch()) {

						$category_id	= $row_category["category_id"];
						$category_title	= bbkod($row_category["category_title"]);
						
						$category_amount--;
						
						echo "<a href='' id='search.php?page=category&id=$category_id' class='link_click tooltipSector' title='".bbkod($saucy["Default"]["Locale"]["Category"])." $category_title'>$category_title</a>";
						
						if(!empty($category_amount)) {
						
							echo ", ";
							
						}
						
					}
					
					echo " ]";
					
				}
				
			echo "</span>
		</div>
		<div class='text normal' style='text-align: justify;'>";
			
				/*
				if($page == "blog") {
				
					echo "<div class='disclaimer'><span class='massive' style='font-weight: bold; color: red;'>DISCLAIMER: <span style='color: ".$saucy["Default"]["Color"]["Light"].";'>My Reviews</span></span><br />
					<span class='small'>This is <i>my</i> review of things, you may or may not agree with what <i>I</i> think.<br /> I have as much right to express my thoughts and feelings as you.</span></div>";
					
				} else
				*/
				
				// PORTFOLIO
				if($page == "portfolio") {
				
					echo "<div style='padding: 0px 2px 3px 0px;' align='center'>
						<a href='$folder_portfolio/$file' target='_blank' class='tooltipSector' title='".bbkod($saucy["Default"]["Locale"]["Portfolio"])."'>
							<img style='max-width: 100%;' src='$folder_portfolio/$file' />
						</a>
					</div>";
					
				// LIST
				} elseif($page == "list") {
					
					$list_counter = 1;
          $sql = "SELECT {$page}_content_id, {$page}_content_title, {$page}_content_text, {$page}_content_parent_id
					FROM {$saucy["DevDB"]["Name"]["listcontent"]}
					WHERE {$page}_content_parent_id = :id";
          $stmt = $conn->prepare($sql);
          $stmt->bindParam(':id', $id, PDO::PARAM_INT);
          $stmt->execute();
          while ($row_list = $stmt->fetch()) {

						$list_id	= $row_list[$page."_content_id"];
						$list_title	= bbkod($row_list[$page."_content_title"]);
						$list_text	= bbkod($row_list[$page."_content_text"]);
						
						echo "<a id='content".$list_id."' class='showSector tooltipSector' title='$list_text'>
							<div class='linkz big'>
								#$list_counter : $list_title
								<div class='content".$list_id." normal' style='padding: ".($box_pad*4)."px ".$box_pad."px 0px ".$box_pad."px; display: none;'>
									$list_text
								</div>
							</div>
						</a>";
						
						$list_counter++;
						
					}
					
				}
				
					
				// TEXT
				if(!empty($text)) {
					echo bbkod(truncate_str($text, $saucy["Default"]["Limit"]["Text"]));
				}
				
				// SOURCE
				if(!empty($source)) {
				
					echo "<br /><br />";
					echo "[ <a href='$source' target='_blank' class='tooltipSector' title='Source: $source'>Source</a> ]";
					
				}
				
		echo "</div>
		<div class='spacer'></div>";
		
		if($page == "tutorial" OR $page == "portfolio" OR $page == "download") {


      $sql = "SELECT
				tool_parent_id,
				{$saucy["DevDB"]["Name"]["toolparent"]}.tool_id,
				{$saucy["DevDB"]["Name"][$page]}.{$page}_id,
				tool_title
			FROM {$saucy["DevDB"]["Name"]["toolparent"]}
			INNER JOIN {$saucy["DevDB"]["Name"]["tool"]} ON {$saucy["DevDB"]["Name"]["tool"]}.tool_id = {$saucy["DevDB"]["Name"]["toolparent"]}.tool_id
			INNER JOIN {$saucy["DevDB"]["Name"][$page]} ON {$saucy["DevDB"]["Name"][$page]}.{$page}_id = {$saucy["DevDB"]["Name"]["toolparent"]}.{$page}_id
			WHERE {$saucy["DevDB"]["Name"]["toolparent"]}.{$page}_id = :id";
      $stmt = $conn->prepare($sql);
      $stmt->bindParam(':id', $id, PDO::PARAM_INT);
      $stmt->execute();
      $tbl_tool_amount = $stmt->rowCount();

			if(!empty($tbl_tool_amount) OR !empty($file)) {
			
				echo "<div class='content_small smalli' style='margin-bottom: $box_pad"."px;'>
					<span style='float: left;'>";
					
						// FILENAME
						if(!empty($file)) {
						
							if($page == "portfolio" OR $page == "tutorial" OR $page == "download") {
							
								echo "[ Filename: <a href='";
								
								if($page == "tutorial") {
								
									echo $folder_tutorial;
									
								} elseif($page == "portfolio") {
								
									echo $folder_portfolio;
									
								} elseif($page == "download") {
								
									echo "$folder_download/$download_parent";
									
								}
								
								echo "/$file' target='_blank' class='tooltipSector' title='".bbkod($saucy["Default"]["Locale"]["DownloadHover"])." $file'>$file</a>";
								
							}
							
						}
						
						// TOOLS
						if(!empty($tbl_tool_amount)) {
						
							echo (!empty($file) ? " | " : " [ ");
							
							echo "Tools: ";

              while ($row_tutorials_tools = $stmt->fetch()) {

								$id_tool	= $row_tutorials_tools["tool_id"];
								$tool_title	= bbkod($row_tutorials_tools["tool_title"]);
								
								$tbl_tool_amount--;
								
								echo "<a href='' id='search.php?page=tool&id=$id_tool' class='link_click tooltipSector' title='".bbkod($saucy["Default"]["Locale"]["ToolHover"])." $tool_title'>$tool_title</a>";
								
								if(!empty($tbl_tool_amount)) {
								
									echo ", ";
									
								}
								
							}
							
						}
						
					echo " ]</span>";
					
					// DOWNLOAD
					if(!empty($file)) {
					
						echo "<span style='float: right;'>[ <a href='";
						
						if($page == "tutorial") {
						
							echo $folder_tutorial;
							
						} elseif($page == "portfolio") {
						
							echo $folder_portfolio;
							
						} elseif($page == "download") {
						
							echo "$folder_download/$download_parent";
							
						}
						
						echo "/$file' target='_blank' class='tooltipSector' title='".bbkod($saucy["Default"]["Locale"]["DownloadHover"])." $file'>".$saucy["Default"]["Locale"]["Download"]."</a> ]</span>";
						
					}
					
				echo "</div>";
				
			}
			
		}
		
		// RATE
		echo "<div class='content_small smalli' style='margin-bottom: 6px;'>
			<span style='float: left;'>";
				echo "[ Rate: ";
				echo $handler->displayRating(strip_utf(eng_char($title)),10);
				echo " | ";
				echo $handler->displayTotalNumberOfRatings(strip_utf(eng_char($title)));
				echo " votes, average: ";
				echo $handler->displayRateValue(strip_utf(eng_char($title)));
				echo " out of 10 ]";
			echo "</span>";
			
			// RATING
			if($page == "review") {
			
				echo "<span style='float: right;'>";
					
						if(empty($rating)) {
						
							echo "[ ".$saucy["Default"]["Locale"]["Rating"]["Text"].$saucy["Default"]["Locale"]["Rating"]["NotRated"]." ]";
							
						} else {
						
							echo "[ ".$saucy["Default"]["Locale"]["Rating"]["Text"];
							
								for($rating_index = 0; $rating_index < $rating; $rating_index++) {
								
									if($rating == 5) {
									
										echo "<span class='tooltipSector' title='".bbkod($saucy["Default"]["Locale"]["Rating"]["Terrific"])."'><img src='".$saucy["Default"]["Image"]["Rating"][$rating_index]."' style='border: 0px solid;' /></span>";
										
									} elseif($rating == 4) {
									
										echo "<span class='tooltipSector' title='".bbkod($saucy["Default"]["Locale"]["Rating"]["Good"])."'><img src='".$saucy["Default"]["Image"]["Rating"][$rating_index]."' style='border: 0px solid;' /></span>";
										
									} elseif($rating == 3) {
									
										echo "<span class='tooltipSector' title='".bbkod($saucy["Default"]["Locale"]["Rating"]["OK"])."'><img src='".$saucy["Default"]["Image"]["Rating"][$rating_index]."' style='border: 0px solid;' /></span>";
										
									} elseif($rating == 2) {
									
										echo "<span class='tooltipSector' title='".bbkod($saucy["Default"]["Locale"]["Rating"]["Bad"])."'><img src='".$saucy["Default"]["Image"]["Rating"][$rating_index]."' style='border: 0px solid;' /></span>";
										
									} elseif($rating == 1) {
									
										echo "<span class='tooltipSector' title='".bbkod($saucy["Default"]["Locale"]["Rating"]["Horrible"])."'><img src='".$saucy["Default"]["Image"]["Rating"][$rating_index]."' style='border: 0px solid;' /></span>";
										
									}
									
								}
								
							echo " ]";
							
						}
						
					echo "</span>";
				
			// DIFFICULTY
			} elseif($page == "tutorial") {
			
				echo "<span style='float: right;'>";
				
					if(empty($difficulty)) {
					
						echo "[ ".$saucy["Default"]["Locale"]["Difficulty"]["Text"].$saucy["Default"]["Locale"]["Difficulty"]["NotRated"]." ]";
						
					} else {
					
						echo "[ ".$saucy["Default"]["Locale"]["Difficulty"]["Text"];
						
						for($difficulty_index = 0; $difficulty_index < $difficulty; $difficulty_index++) {
						
							if($difficulty == 5) {
							
								echo "<a href='' class='tooltipSector' title='".bbkod($saucy["Default"]["Locale"]["Difficulty"]["Expert"])."'><img src='".$saucy["Default"]["Image"]["Difficulty"][$difficulty_index]."' style='border: 0px solid;' /></a>";
								
							} elseif($difficulty == 4) {
							
								echo "<a href='' class='tooltipSector' title='".bbkod($saucy["Default"]["Locale"]["Difficulty"]["Hard"])."'><img src='".$saucy["Default"]["Image"]["Difficulty"][$difficulty_index]."' style='border: 0px solid;' /></a>";
								
							} elseif($difficulty == 3) {
							
								echo "<a href='' class='tooltipSector' title='".bbkod($saucy["Default"]["Locale"]["Difficulty"]["Medium"])."'><img src='".$saucy["Default"]["Image"]["Difficulty"][$difficulty_index]."' style='border: 0px solid;' /></a>";
								
							} elseif($difficulty == 2) {
							
								echo "<a href='' class='tooltipSector' title='".bbkod($saucy["Default"]["Locale"]["Difficulty"]["Easy"])."'><img src='".$saucy["Default"]["Image"]["Difficulty"][$difficulty_index]."' style='border: 0px solid;' /></a>";
								
							} elseif($difficulty == 1) {
							
								echo "<a href='' class='tooltipSector' title='".bbkod($saucy["Default"]["Locale"]["Difficulty"]["Beginner"])."'><img src='".$saucy["Default"]["Image"]["Difficulty"][$difficulty_index]."' style='border: 0px solid;' /></a>";
								
							}
							
						}
						
						echo " ]";
						
					}
					
				echo "</span>";
				
			// TUTORIAL AVAILABLE
			} elseif($page == "portfolio" OR $page == "download") {
			
				if(!empty($id_tutorial)) {
				
					echo "<span style='float: right;'>[ <a href='' id='article.php?page=tutorial&id=$id_tutorial' class='link_click tooltipSector' title='".bbkod($saucy["Default"]["Locale"]["TutorialHover"])." $title'>".$saucy["Default"]["Locale"]["Tutorial"]."</a> ]</span>";
					
				}
				
			}
			
		echo "</div>
			
		<div class='content_small smalli'>
			<span style='float: left;'>";
			
				// COMMENTS + VIEWS + DOWNLOADS + SIZE + COMPLETION TIME
				echo "[ <a href='' id='article.php?page=$page&id=$id' class='link_click tooltipSector' title='".bbkod($saucy["Default"]["Locale"]["CommentsRead"])."'>$comments_amount ".$saucy["Default"]["Locale"]["Comments"]."</a> | $views ".$saucy["Default"]["Locale"]["Views"]." ";
				
				if($page == "blog" OR $page == "review" OR $page == "list" OR $page == "poll") {
				
					echo "]";
					
				} elseif($page == "portfolio" OR $page == "download") {
				
					echo "| 0 Downloads";
					
					if(!empty($file)) {
					
						echo " | Size: <span style='color: ".$saucy["Default"]["Color"]["Link_completed"].";'>";
						
						if($page == "portfolio") {
						
							echo round((filesize("$folder_portfolio/$file")/1024)/1024, 2)." MB (".round(filesize("$folder_portfolio/$file")/1024);
							
						} elseif($page == "download") {
						
							echo round((filesize("$folder_download/$download_parent/$file")/1024)/1024, 2)." MB (".round(filesize("$folder_download/$download_parent/$file")/1024);
							
						}
						
						echo " KB)</span> ]";
						
					}
					
				} elseif($page == "tutorial") {
				
					echo "| Completion Time: <span style='color: ".$saucy["Default"]["Color"]["Link_completed"].";'>"; if(!empty($time)) { echo "Unknown"; } else { echo $time; }  echo "</span> ]";
					
				}
			
			echo "</span>";
			
			// READ MORE
			if($page != "list") {
			
				if(mb_strlen($text) >= $saucy["Default"]["Limit"]["Text"]) {
				
					echo "<span style='float: right;'>
						[ <a href='' id='article.php?page=$page&id=$id' class='link_click tooltipSector' title='".bbkod($saucy["Default"]["Locale"]["ReadMoreHover"])." $title'>".$saucy["Default"]["Locale"]["ReadMore"]."</a> ]
					</span>";
					
				}
				
			}
			
		echo "</div>
    </div>
</div>";

}

?>

</body>
</html>