<?php

require("inc/settings.php");
require($saucy["Default"]["Conn"]);
require($saucy["Default"]["Functions"]);
//require_once($saucy["Default"]["SubmitRating"]);

$page            = isset($_GET["page"]) ? $_GET["page"] : NULL;
$pagelist_amount = count($searchlist);
if(!in_array($page, $searchlist)) {

	echo "You are trying to access a page that does not exist.";
	exit;
	
}

$id_get = $_GET["id"];
$id_url = ((isset($id_get) AND is_numeric($id_get)) ? intval($id_get) : 0);

$sql = "SELECT
	{$saucy["DevDB"]["Name"][$searchlist[0]]}.{$searchlist[0]}_id,
	{$saucy["DevDB"]["Name"][$searchlist[1]]}.{$searchlist[1]}_id
FROM {$saucy["DevDB"]["Name"][$searchlist[0]]}, {$saucy["DevDB"]["Name"][$searchlist[1]]}
WHERE {$saucy["DevDB"]["Name"][$searchlist[0]]}.{$searchlist[0]}_id = :id_url OR {$saucy["DevDB"]["Name"][$searchlist[1]]}.{$searchlist[1]}_id = :id_url";
$stmt = $conn->prepare($sql);
$stmt->bindParam(':id_url', $id_url, PDO::PARAM_INT);
$stmt->execute();

if($stmt->rowCount() > 0){

	$page_id = $id_get;
	
} else {

	echo "You are trying to access a page that does not exist.";
	exit;
	
}

/*
echo "SELECT ";
	//echo "<br />";
for($b = 0; $b < $pagelist_amount; $b++) {
	echo $searchlist[$b].".".$searchlist[$b]."_id";
	if ($b < ($pagelist_amount - 1)) {
		echo ',';
		//echo "<br />";
	}
	echo " ";
}
//echo "<br />";

echo "FROM ";
for($b = 0; $b < $pagelist_amount; $b++) {
	echo $searchlist[$b];
	if ($b < ($pagelist_amount - 1)) {
		echo ', ';
	}
	echo " ";
}
//echo "<br />";

echo "WHERE ";
for($b = 0; $b < $pagelist_amount; $b++) {
	echo $searchlist[$b].".".$searchlist[$b]."_id=$id_url";
	if ($b < ($pagelist_amount - 1)) {
		echo ' OR ';
	}
}
*/

?>
<!DOCTYPE html>
<html lang='sv'>
<head>
<title></title>

<?php

echo "<script src='".$saucy["Default"]["JS"]["jQuery"]."'></script>";
//echo "<script src='".$saucy["Default"]["JS"]["MaxRating"]."'></script>";
echo "<script src='".$saucy["Default"]["JS"]["TimeAgo"]."'></script>";
echo "<script src='".$saucy["Default"]["JS"]["Qtip"]."'></script>";

?>

<script>
$(document).ready(function(){
    jQuery(".timeagoSearch").timeago();
    jQuery.timeago.settings.allowFuture = true;
	
    $(".showSearch").bind("click",function() {
        $("."+this.id ).stop(true, true).slideToggle("<?php echo $saucy["Default"]["ToggleSpeed"]; ?>");
    });
	
	$(".tooltipSearch, .tooltipMenu").each(function() {
		$(this).qtip({
			content: {
				text: $(this).attr("title"),
			},
			position: {
				target: "mouse",
				adjust: { x: 15, y: 0, },
			},
			style: {
				tip: false
			},
			show: {
				delay: 0
			},
			hide: {
				delay: 200
			}
		});
	});
});
</script>

</head>
<body>

<?php

$sql = "UPDATE {$saucy["DevDB"]["Name"][$page]}
SET {$page}_popular = {$page}_popular + 1
WHERE {$page}_id = :page_id";
$stmt = $conn->prepare($sql);
$stmt->bindParam(':page_id', $page_id, PDO::PARAM_INT);
$stmt->execute();

$sql = "SELECT {$page}_id, {$page}_title
FROM {$saucy["DevDB"]["Name"][$page]}
WHERE {$page}_id = :page_id";
$stmt = $conn->prepare($sql);
$stmt->bindParam(':page_id', $page_id, PDO::PARAM_INT);
$stmt->execute();

while($row_page = $stmt->fetch()) {

	$id = $row_page[$page."_id"];
	$title = bbkod($row_page[$page."_title"]);
	
	echo "<div class='box'>
		<div class='text big'>
				.: ".$saucy["Panel"][ucfirst($page)]["Locale"]["Text"]." $title :.
		</div>
	</div>";
	
}

if($page == "category") {

	foreach($categories as $category) {

    $sql = "SELECT {$category}_id
		FROM {$saucy["DevDB"]["Name"]["categoryparent"]}
		WHERE category_id = $page_id AND {$category}_id IS NOT NULL
		ORDER BY {$category}_id DESC";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $tbl_amount = $stmt->rowCount();

		if(!empty($tbl_amount)) {

			echo "<div class='box'>
				<div class='text big'>
					<a id='search_$category$page_id' class='showSearch tooltipSearch' title='".bbkod($saucy["Default"]["Locale"]["Toggle"])."'>
						".ucfirst($category)."
					</a>
				</div>
				<div class='search_$category$page_id'>
					<div class='spacer' style='margin-bottom: ".($box_pad*2)."px;'></div>";

					while($row = $stmt->fetch()) {

						$id_category = $row[$category."_id"];
            $sql = "SELECT {$category}_id, {$category}_title, date
						FROM {$saucy["DevDB"]["Name"][$category]}
						WHERE {$category}_id = :id_category";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id_category', $id_category, PDO::PARAM_INT);
            $stmt->execute();
						while($row_content = $stmt->fetch()) {

							$id_content		= $row_content[$category."_id"];
							$title_content	= bbkod($row_content[$category."_title"]);
							$date			= $row_content["date"];
							
							echo "<a href='' id='article.php?page=$category&id=$id_content' class='link_click'>
								<div class='linkz small'>"
									."<span style='color: ".$saucy["Default"]["Color"]["Light"].";'>.: </span>"
									."<span class='showSearch tooltipSearch' title='".bbkod($saucy["Default"]["Locale"]["Categories"])." $title_content'>".truncate_str($title_content, $saucy["Panel"]["Category"]["Limit"]["Text"])."</span>"
									."<span style='color: ".$saucy["Default"]["Color"]["Light"]."; float: right;'>"
									."<span class='timeagoSearch tooltipSearch' title='$date'>".date("H:i @ jS \of F Y", strtotime($date))."</span> :.</span>"
								."</div>
							</a>";
							
						}
						
					}
					
					echo "</div>
				</div>
			</div>";
			
		}
		
	}

} elseif($page == "tool") {

	foreach($tools as $tool) {

    $sql = "SELECT {$tool}_id
		FROM {$saucy["DevDB"]["Name"]["toolparent"]}
		WHERE tool_id = :page_id AND {$tool}_id IS NOT NULL
		ORDER BY {$tool}_id DESC";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':page_id', $page_id, PDO::PARAM_INT);
    $stmt->execute();
    $tbl_amount = $stmt->rowCount();

		if(!empty($tbl_amount)) {
		
			echo "<div class='box'>
				<div class='text big'>
					<a id='search_$tool$page_id' class='showSearch tooltipSearch' title='".bbkod($saucy["Default"]["Locale"]["Toggle"])."'>
						".ucfirst($tool)."
					</a>
				</div>
				<div class='search_$tool$page_id'>
					<div class='spacer' style='margin-bottom: ".($box_pad*2)."px;'></div>";

					while($row = $stmt->fetch()) {

						$id_tool = $row[$tool."_id"];

            $sql = "SELECT {$tool}_id, {$tool}_title, date
						FROM {$saucy["DevDB"]["Name"][$tool]}
						WHERE {$tool}_id = :id_tool";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id_tool', $id_tool, PDO::PARAM_INT);
            $stmt->execute();
						while($row_content = $stmt->fetch()) {

							$id_content		= $row_content[$tool."_id"];
							$title_content	= bbkod($row_content[$tool."_title"]);
							$date			= $row_content["date"];
							
							echo "<a href='' id='article.php?page=$tool&id=$id_content' class='link_click'>
								<div class='linkz small'>"
									."<span style='color: ".$saucy["Default"]["Color"]["Light"].";'>.: </span>"
									."<span class='showSearch tooltipSearch' title='".bbkod($saucy["Default"]["Locale"]["Categories"])." $title_content'>".truncate_str($title_content, $saucy["Panel"]["Category"]["Limit"]["Text"])."</span>"
									."<span style='color: ".$saucy["Default"]["Color"]["Light"]."; float: right;'>"
									."<span class='timeagoSearch tooltipSearch' title='$date'>".date("H:i @ jS \of F Y", strtotime($date))."</span> :.</span>"
								."</div>
							</a>";
							
						}
						
					}
					
					echo "</div>
				</div>
			</div>";
			
		}
		
	}

}

?>

</body>
</html>