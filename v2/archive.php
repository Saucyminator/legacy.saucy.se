<?php

require("inc/settings.php");
require($saucy["Default"]["Conn"]);
require($saucy["Default"]["Functions"]);
//require_once($saucy["Default"]["SubmitRating"]);

/*	ARCHIVE STRUCTURE

blog
review
tutorial
portfolio
poll
list
download

*/

$page	= isset($_GET["page"]) ? $_GET["page"] : NULL;
$pagelist_amount = count($archivelist);
if(!in_array($page, $archivelist)) {

	echo "You are trying to access a page that does not exist.";
	exit;
	
}

?>
<!DOCTYPE html>
<html lang='sv'>
<head>
<title></title>

<?php

echo "<script src='".$saucy["Default"]["JS"]["jQuery"]."'></script>";
//echo "<script src='".$saucy["Default"]["JS"]["MaxRating"]."'></script>";
echo "<script src='".$saucy["Default"]["JS"]["TimeAgo"]."'></script>";
echo "<script src='".$saucy["Default"]["JS"]["Qtip"]."'></script>";

?>

<script>
$(document).ready(function(){
    jQuery(".timeagoArchive").timeago();
    jQuery.timeago.settings.allowFuture = true;
	
    $(".showArchive").bind("click",function() {
        $("."+this.id ).stop(true, true).slideToggle("<?php echo $saucy["Default"]["ToggleSpeed"]; ?>");
    });
	
	$(".tooltipArchive, .tooltipMenu").each(function() {
		$(this).qtip({
			content: {
				text: $(this).attr("title"),
			},
			position: {
				target: "mouse",
				adjust: { x: 15, y: 0, },
			},
			style: {
				tip: false
			},
			show: {
				delay: 0
			},
			hide: {
				delay: 200
			}
		});
	});
});
</script>

</head>
<body>

<?php

$parent = str_replace(".php", "", strtolower(basename($_SERVER["PHP_SELF"])));

include("subMenu.php");

// -------------------------------------------------------------------------------------------------------------------

if($page == "portfolio") {

    echo "<div class='box'>
        <div class='text' style='padding: 0px; margin: 0px;'>";

      $sql = "SELECT {$page}_id, {$page}_title, {$page}_text, {$page}_file, date
			FROM {$saucy["DevDB"]["Name"][$page]}
			ORDER BY date DESC";
      $stmt = $conn->prepare($sql);
      $stmt->bindParam(':id', $id, PDO::PARAM_INT);
      $stmt->execute();
			while($row_portfolio = $stmt->fetch()) {

				$id_portfolio		= $row_portfolio[$page."_id"];
				$title_portfolio	= bbkod($row_portfolio[$page."_title"]);
				$text_portfolio		= $row_portfolio[$page."_text"];
				$file_portfolio		= $row_portfolio[$page."_file"];
				
				echo "<a href='' id='article.php?page=$page&id=$id_portfolio' class='link_click tooltipArchive' title='".bbkod($saucy["Default"]["Locale"]["ArchivesPortfolioHover"])." $title_portfolio'>".
					"<img src='$folder_portfolio/$file_portfolio' style='width: ".$saucy["Panel"]["Archive"]["Width"]."px; margin: $box_pad"."px $box_pad"."px 0px 0px;' />"
				."</a>";
				
			}
			
        echo "</div>
    </div>";
	
} elseif($page == "download") {

	foreach($downloadables as $category) {

    $sql = "SELECT DISTINCT download_id, download_parent, download_title, date
		FROM {$saucy["DevDB"]["Name"][$page]}
		WHERE download_parent = :category
		ORDER BY download_id DESC";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':category', $category, PDO::PARAM_STR);
    $stmt->execute();
    $tbl_amount = $stmt->rowCount();

		if(!empty($tbl_amount)) {
		
			echo "<div class='box'>
				<div class='text big'>
					<a id='$category' class='showArchive tooltipArchive' title='".bbkod($saucy["Default"]["Locale"]["Toggle"])."'>
						".ucfirst($category)."
					</a>
				</div>
				<div class='$category'>
					<div class='spacer' style='margin-bottom: ".($box_pad*2)."px;'></div>";

					while($row = $stmt->fetch()) {

						$id		= $row["download_id"];
						$title	= bbkod($row["download_title"]);
						$date	= $row["date"];
						
						echo "<a href='' id='article.php?page=download&id=$id' class='link_click tooltipArchive'>
							<div class='linkz small'>"
								."<span style='color: ".$saucy["Default"]["Color"]["Light"].";'>.: </span>"
								."<span class='showSearch tooltipArchive' title='".bbkod($saucy["Default"]["Locale"]["Categories"])." $title'>".truncate_str($title, $saucy["Panel"]["Category"]["Limit"]["Text"])."</span>"
								."<span style='color: ".$saucy["Default"]["Color"]["Light"]."; float: right;'>"
								."<span class='timeagoArchive tooltipArchive' title='$date'>".date("H:i @ jS \of F Y", strtotime($date))."</span> :.</span>"
							."</div>
						</a>";
						
					}
					
					echo "</div>
				</div>
			</div>";
			
		}
		
	}
	
} else {

	if($page == "code" OR $page == "map" OR $page == "video" OR $page == "config" OR $page == "model" OR $page == "wallpaper") {
	
		$page = "download";
		
	}

  $sql = "SELECT DISTINCT EXTRACT(YEAR FROM date) AS date_year
	FROM {$saucy["DevDB"]["Name"][$page]}
	ORDER BY date_year DESC";
  $stmt = $conn->prepare($sql);
  $stmt->execute();

    while($row_year = $stmt->fetch()) {

        $date_year = $row_year['date_year'];
		
        echo "<div class='box'>
            <div class='text massive'>
                <a id='archive_$date_year' class='showArchive tooltipArchive' title='".bbkod($saucy["Default"]["Locale"]["ArchivesYearHover"])."'>
                    $date_year
                </a>
            </div>
            <div class='archive_$date_year'"; if($date_year != date("Y")) { echo " style='display: none;'"; } echo ">
                <div class='spacer'></div>";

        $sql = "SELECT DISTINCT EXTRACT(YEAR FROM date) AS date_year, EXTRACT(MONTH FROM date) AS date_month
				FROM {$saucy["DevDB"]["Name"][$page]}
				WHERE YEAR(date) = :date_year
				ORDER BY {$page}_id DESC";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':date_year', $date_year, PDO::PARAM_INT);
        $stmt->execute();

				while($row_month = $stmt->fetch()) {

          $sql = "SELECT date
					FROM {$saucy["DevDB"]["Name"][$page]}
					WHERE YEAR(date) = :date_year AND MONTH(date) = :date_month";
          $stmt = $conn->prepare($sql);
          $stmt->bindParam(':date_year', $date_year, PDO::PARAM_INT);
          $stmt->bindParam(':date_month', $date_month, PDO::PARAM_INT);
          $stmt->execute();
          $count_month = $stmt->rowCount();

					echo "<div class='text big'>
						<a id='archive_$date_year-$date_month' class='showArchive tooltipArchive' title='".bbkod($saucy["Default"]["Locale"]["ArchivesMonthHover"])."'>
							".date('F : Y',mktime(0,0,0,$date_month+1,0,$date_year))." : [ $count_month ]
						</a>
					</div>
					<div class='archive_$date_year-$date_month'"; if($date_year != date("Y") OR $date_month != date("m")) { echo " style='display: none;'"; } echo ">
						<div class='spacer' style='margin: $box_pad"."px ".($box_pad*8)."px ".($box_pad*2)."px ".($box_pad*8)."px'></div>";
						
						if($page == "list" OR $page == "tutorial") {

              $sql = "SELECT DISTINCT EXTRACT(YEAR FROM date) AS date_year, EXTRACT(MONTH FROM date) AS date_month, {$page}_id, {$page}_title, date
							FROM {$saucy["DevDB"]["Name"][$page]}
							WHERE YEAR(date) = :date_year AND MONTH(date) = :date_month
							ORDER BY {$page}_id DESC";

						} else {

              $sql = "SELECT DISTINCT EXTRACT(YEAR FROM date) AS date_year, EXTRACT(MONTH FROM date) AS date_month, {$page}_id, {$page}_parent, {$page}_title, date
							FROM {$saucy["DevDB"]["Name"][$page]}
							WHERE YEAR(date) = :date_year AND MONTH(date) = :date_month
							ORDER BY {$page}_id DESC";

						}

            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':date_year', $date_year, PDO::PARAM_INT);
            $stmt->bindParam(':date_month', $date_month, PDO::PARAM_INT);
            $stmt->execute();

						while($row_day = $stmt->fetch()) {

							$id_day		= $row_day[$page."_id"];
							$title		= bbkod($row_day[$page."_title"]);
							$date		= $row_day["date"];
							
							echo "<a href='' id='article.php?page=$page&id=$id_day' class='link_click'>
								<div class='linkz small'>"
									."<span style='color: ".$saucy["Default"]["Color"]["Light"].";'>.: </span>"
									."<span class='showArchive tooltipArchive' title='".bbkod($saucy["Default"]["Locale"]["ArchivesInfoHover"])." $title'>".truncate_str($title, $saucy["Panel"]["Archive"]["Limit"]["Text"])."</span>"
									."<span style='color: ".$saucy["Default"]["Color"]["Light"]."; float: right;'>"
									."<span class='timeagoArchive tooltipArchive' title='$date'>".date("H:i @ jS \of F Y", strtotime($date))."</span> :.</span>"
								."</div>
							</a>";
							
						}
						
					echo "</div>";
					
				}
				
            echo "</div>
        </div>";
		
    }
	
}

?>

</body>
</html>