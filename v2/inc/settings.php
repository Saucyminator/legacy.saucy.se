<?php
mb_internal_encoding("UTF-8");
setlocale(LC_ALL, "swedish");
date_default_timezone_set("Europe/Paris");

// ini_set('display_errors', 1);
// ini_set('log_errors', 1);
// error_reporting(E_ALL);

//calculate years of age (input string: YYYY-MM-DD)
function birthday($birthday) {

	list($year,$month,$day) = explode("-",$birthday);
	$year_diff  = date("Y") - $year;
	$month_diff = date("m") - $month;
	$day_diff   = date("d") - $day;

	if($month_diff < 0) {

		$year_diff--;

	} elseif(($month_diff == 0) AND ($day_diff < 0)) {

		$year_diff--;

	}

	return $year_diff;

}

/*
.HTACCESS www to non-www:
RewriteEngine On
RewriteCond %{HTTP_HOST} !^my-domain\.com$ [NC]
RewriteRule ^(.*)$ http://my-domain.com/$1 [R=301,L]

.HTACCESS non-www to www:
RewriteEngine On
RewriteCond %{HTTP_HOST} !^www\.
RewriteRule ^(.*)$ http://www.%{HTTP_HOST}/$1 [R=301,L]
*/


// DEVELOPMENT - ALLT MED DEV_ SKA TAS BORT ELLER KAN ÄNDRAS I ADMIN SIDAN
/* RULES

.: A :.
href -> target -> id -> class -> style:width,height,padding,margin,text-align,display,float -> title

.: IMG :.
src -> id -> class -> style

.: SPAN :.
id -> class -> style:width,height,padding,margin,text-align,display,float -> title

.: DIV :.
id -> class -> style:width,height,padding,margin,text-align,display,float -> align

.: INPUT :.
name -> type -> value -> form -> id -> class -> style:width,height,padding,margin,text-align,display,float -> value -> title -> results -> onclick -> autocomplete > required

.: FORM :.
name -> method -> id -> action

*/

/* FOLDERS */
$folder_css			= "css";
$folder_download	= "download";
$folder_image		= "image";
$folder_include		= "inc";
$folder_javaScript	= "js";

$folder_blog		= "$folder_image/blog";
$folder_review		= "$folder_image/review";
$folder_tutorial	= "$folder_image/tutorial";
$folder_portfolio	= "$folder_image/portfolio";
$folder_list		= "$folder_image/list";
$folder_poll		= "$folder_image/poll"; // may not be needed
$folder_banner		= "$folder_image/banner";
$folder_icon		= "$folder_image/icon";
$folder_profile		= "$folder_image/profile";
$folder_smilies		= "$folder_image/smilies";
$folder_code		= "$folder_download/code";
$folder_config		= "$folder_download/config";
$folder_map			= "$folder_download/map";
$folder_model		= "$folder_download/model";
$folder_video		= "$folder_download/video";
$folder_wallpaper	= "$folder_download/wallpaper";

$folder_cards		= "$folder_icon/cards";


// LOCALIZATION
// Posted = Skriven
/*
$locale_hover = array(
	"Toggle"			=> "Click to toggle expand/collapse",
	"Search"			=> "Search the website",
	"Twitter"			=> "Click to show/hide the latest Twitter message",
	"Latest"			=> "Click to show/hide the latest posts panel",
	"Archives"			=> "Click to show/hide the archives panel",
	"ArchivesText"		=> "Click to view the archives of",
	"ArchivesInfo"		=> "Click to view the post",
	"ArchivesMonth"		=> "Click to show/hide the month",
	"ArchivesYear"		=> "Click to show/hide the year",
	"Poll"				=> "Click to show/hide the poll panel",
	"PollResults"		=> "Click to view the results",
	"Category"			=> "Click to search for more posts with the categoryname",
	"Categories"		=> "Click to show/hide the categories panel",
	"CategoriesText"	=> "Click to search for blog, reviews, tutorials, portfolio, downloads,<br /> lists and polls containing the category name",
	"Tool"				=> "Click to search for more posts with the same used tool: ",
	"List"				=> "Click to show/hide the list panel",
	"Vote"				=> "Click to vote for:",
	"ReadList"			=> "View the full list",
	"ReadMe"			=> "Click to read more",
	"ReadComments"		=> "Click to read all comments",
	"Info"				=> "Click to show/hide post",
	"Download"			=> "Click to download the file",
	"Tutorial"			=> "Click to read the tutorial",
	"Webmaster"			=> "Click to show/hide webmaster panel",
	"Banner"			=> "Click to show/hide the banner panel",
	"Comments"			=> "Click to show/hide comments",
	"Comment"			=> "Click to show/hide comment",
	"AddComment"		=> "Click to show/hide add comment",
	"CopyRight"			=> "View information about cookies",
	"Portfolio"			=> "Click to view the image (opens in new window)",
	"PortfolioInfo"		=> "Click to view more information about",
);
/*
*/

/*
// LIMITS
// limits the posts of specific part on index
$limit = array(
	"Title"			=> 70,
	"Text"			=> 300,
	"List"			=> 3,
	"Portfolio"		=> 1,
	"Other"			=> 3,
	"Poll"			=> 5,
	"Categories"	=> 10,
	"Latest"		=> 10,
	"Archives"		=> 13, // total of 13 achives
	"Info"		=> array(
		"List"		=> 35,
		"Latest"	=> 25,
		"Archives"	=> 70,
		"Poll"		=> 35),
);
*/

$sectorlist = array(
	"blog",
	"review",
	"tutorial",
	"portfolio",
	"download",
	"list",
	"poll",
);
$articlelist = array(
	"blog",
	"review",
	"tutorial",
	"portfolio",
	"download",
	"list",
);
$informationlist = array(
	"cookies",
	"about",
	"statistics",
	"contact",
	"askmeanything",
	"link",
	"ip",
	"text_capitalizer",
	"word_counter",
	"personality",
	"sitemap",
);
$sitemaplist = array(
	"link",
	"askmeanything",
);
$archivelist = array(
	"blog",
	"review",
	"tutorial",
	"portfolio",
	"poll",
	"list",
	"download",
);
$searchlist = array(
	"category",
	"tool",
);



$downloadables = array(
	"code",
	"config",
	"map",
	"model",
	"video",
	"wallpaper",
);
// USED IN search_category
$categories = array(
	"blog",
	"review",
	"list",
	"tutorial",
	"portfolio",
	"download",
);
// USED IN search_tool
$tools = array(
	"list",
	"tutorial",
	"portfolio",
	"download",
);
// NAV
$nav = array(
	"Blog"						=> "blog",
	"Reviews"					=> "review",
	"Tutorials"					=> "tutorial",
	"Portfolio"					=> "portfolio",
	"Downloads"					=> "download",
	"Forum"						=> "http://forum.saucy.se/",
	"About"						=> "about",
	"Contact"					=> "contact",
);






$saucy = array(


	// DEVELOPMENT
	"DevColors" => array(
		"Enabled"				=> FALSE,
		"Color" => array(
			"IP"				=> "Green",
			"Massive"			=> "Blue",
			"Huge"				=> "Gray",
			"Big"				=> "Purple",
			"Normal"			=> "Navy",
			"Normali"			=> "Indigo",
			"Smalli"			=> "SlateBlue",
			"Small"				=> "Cyan",
			"Spacer"			=> "Red",
			"Content"			=> "Green",
			"Text"				=> "Orange",
			"Text_small"		=> "Silver",
			"Content_small"		=> "Maroon",
			"Linkz"				=> "Teal",
			"InputSearch"		=> "LimeGreen",

			"Important"			=> "MidnightBlue",
			"Development"		=> "#2d2d2d", // found no color name, is the same as ["Default"]["Colors"]["Border"]
			"Add"				=> "Chartreuse",
			"Remove"			=> "Crimson",
			"Fix"				=> "LightSteelBlue",
			"Think"				=> "Gold",
			"Change"			=> "Hotpink",
			"Undone"			=> "BlueViolet",

		),
	),
	"DevEnabled" => array(
		"Top"					=> 82,
		"Todo"					=> FALSE,
		"Info"					=> TRUE,
		"Done"					=> FALSE,
	),
	"DevShow" => array(
		"Nav"					=> TRUE,
		"Social" => array(
			"All"				=> TRUE,
			"Icons"				=> TRUE,
			"Statistics"		=> TRUE,
			"Search"			=> TRUE,
		),
		"Column" => array(
			"All"				=> TRUE,
			"Left"				=> TRUE,
			"Middle"			=> TRUE,
			"Right"				=> TRUE,
		),
		"Day9" => array(
			"Nav"				=> TRUE,
			"All"				=> TRUE, // Column
			"Left"				=> TRUE, // Column
			"Right"				=> TRUE, // Column
			"Updates"			=> TRUE, // Panel
			"Episodes"			=> TRUE, // Panel
			"Featured"			=> TRUE, // Panel
		),
		"Mumble" => array(
			"Nav"				=> TRUE,
			"All"				=> TRUE, // Column
		),
		"Cards" => array(
			"Nav"				=> TRUE,
			"All"				=> TRUE, // Column
		),
	),
	"DevSymbol" => array(
		"!"						=> "important",
		"@"						=> "development",
		"+"						=> "add",
		"~"						=> "remove",
		"^"						=> "fix",
		"§"						=> "think",
		"½"						=> "change",
		"£"						=> "undone",
		"|"						=> "strike",
	),
	"DevDB" => array(
		"Name" => array(
			"blog"				=> "legacy_v2_blog",
			"category"			=> "legacy_v2_category",
			"categoryparent"	=> "legacy_v2_category_parent",
			"comment"			=> "legacy_v2_comment",
			"download"			=> "legacy_v2_download",
			"link"				=> "legacy_v2_link",
			"list"				=> "legacy_v2_list",
			"listcontent"		=> "legacy_v2_list_content",
			"portfolio"			=> "legacy_v2_portfolio",
			"askmeanything"		=> "legacy_v2_askmeanything",
			"review"			=> "legacy_v2_review",
			"tool"				=> "legacy_v2_tool",
			"toolparent"		=> "legacy_v2_tool_parent",
			"tutorial"			=> "legacy_v2_tutorial",
			"poll"				=> "legacy_v2_poll",
		),
	),
	"DevNew" => array(			// fixa med? om några i listan heter det i basename, lägg till 'new/' framför CONN, JS etc
		"ip"					=> TRUE,
		"mumble"				=> TRUE,
		"text_capitalizer"		=> TRUE,
		"word_counter"			=> TRUE,
		"day9_newbielist"		=> TRUE,
		"colors"				=> TRUE,
	),


	// DEFAULT
	"Default" => array(
		//"Reload"				=> header("Refresh: 1; URL='".basename($_SERVER["PHP_SELF"])."'"), // redirect in 1 second
		"Name"					=> "Saucy",
		"URL"					=> "http://saucy.se/",
		"Version"				=> "2",
		//"Slogan"				=> "Liberty & Truth",
		"CopyYear"				=> 2010,
		"CurYear"				=> date("Y"),
		//"Created"				=> date("H:i:s @ d/m-Y", filectime(basename($_SERVER["PHP_SELF"]))),
		//"Modified"				=> date("H:i:s @ d/m-Y", getlastmod()),
		"Font"					=> "Trebuchet MS",
		"FontHeight"			=> "150%",
		"ToggleSpeed"			=> "slow",
		"JS" => array(
			"jQuery"			=> "https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js",
			"MaxRating"			=> "jquery.maxrating.js",
			"TimeAgo"			=> "$folder_javaScript/jquery.timeago.js",
			"Qtip"				=> "$folder_javaScript/jquery.qtip.min.js",
			/*
			"Pages" => array(
				"Index"			=> "$folder_javaScript/jquery.p.index.js", // index.php
				"Sector"		=> "$folder_javaScript/jquery.p.sector.js", // sector_*.php
				"Article"		=> "$folder_javaScript/jquery.p.article.js", // article_*.php
				"Archive"		=> "$folder_javaScript/jquery.p.archive.js", // archive_*.php
				"Search"		=> "$folder_javaScript/jquery.p.search.js", // search_category, search_tool
				"Information"	=> "$folder_javaScript/jquery.p.information.js", // cookies, about, statistics, contact, askmeanything, link, ip, text_capitalizer, word_counter, personality
			),
			*/
		),
		"Conn"					=> "$folder_include/conn.php",
		//"Settings"				=> "$folder_include/settings.php",
		"Functions"				=> "$folder_include/functions.php",
		"SubmitRating"			=> "submitRating.php",
		//"Analyticstracking"		=> "$folder_include/analyticstracking.php",
		"Quotes"				=> "$folder_include/quotes.txt",
		"CSS"					=> "$folder_css/body.php",
		"Width"					=> 1000,
		"WidthPanel"			=> 248,
		"WidthContent"			=> 480,
		"MetaCharset"			=> "UTF-8",
		"MetaAuthor"			=> "Saucy",
		"MetaGenerator"			=> "Sublime Text 2, Adobe Photoshop CS3",
		"MetaKeywords"			=> "SAUCY.SE, Saucy, saucyboy, Sås, zAce, Gamer, Geek, Geeky, Nerd, Nerdy, Brony, Personal, Website, Blog, Review, Tutorial, Portfolio, Download, Code, Config, Map, Model, Video, Wallpaper, Forum, About, Useful links, Contact, Ask me anything, Resources, Archive",
		"MetaDescription"		=> "SAUCY.SE - Saucy, the nerdy PC gaming brony who loves games, MLP:FiM, space, music, computers, art, web design and design overall!",
		"MetaCopyRight"			=> "SAUCY.SE, Saucy",
		"MetaRobots"			=> "ALL",
		"MetaImagetoolbar"		=> "NO",
		"MetaIcon"				=> "$folder_icon/favicon.ico",

		"Locale" => array(
			"Rating" => array(
				"Text"			=> "My rating: ",
				"NotRated"		=> "Not yet rated",
				"Terrific"		=> "Game/Program rated as Terrific",
				"Good"			=> "Game/Program rated as Good",
				"OK"			=> "Game/Program rated as O.K.",
				"Bad"			=> "Game/Program rated as Bad",
				"Horrible"		=> "Game/Program rated as Horrible",
			),
			"Difficulty" => array(
				"Text"			=> "Difficulty: ",
				"NotRated"		=> "Not yet rated",
				"Expert"		=> "Tutorial rated as Expert",
				"Hard"			=> "Tutorial rated as Hard",
				"Medium"		=> "Tutorial rated as Medium",
				"Easy"			=> "Tutorial rated as Easy",
				"Beginner"		=> "Tutorial rated as Beginner",
			),

			"Toggle"			=> "Click to toggle expand/collapse",

			"Comment"			=> "Comment",
			"CommentHover"		=> "Click to show/hide the comment",
			"CommentAdd"		=> "Click to show/hide add comment",
			"Comments"			=> "Comments",
			"CommentsHover"		=> "Click to show/hide comments",
			"CommentsRead"		=> "Read all comments",
			"NoComments"		=> "This article has no comments",

			"Download"			=> "Download",
			"DownloadHover"		=> "Download the file:",
			"Tutorial"			=> "Tutorial available",
			"TutorialHover"		=> "Read the tutorial for:",

			"ArchivesMonthHover"	=> "Click to show/hide the month",
			"ArchivesYearHover"		=> "Click to show/hide the year",
			"ArchivesInfoHover"		=> "View the post:",
			"ArchivesPortfolioHover"=> "View more information about:",

			"Category"			=> "Search for more posts with the categoryname:",
			"Categories"		=> "View:",
			"Info"				=> "Click to show/hide article:",
			"Portfolio"			=> "View the image (opens in new tab)",

			"Search"			=> "Search the website",
			"SearchPlaceholder"	=> "Work in progress",

			"CopyRight"			=> "View information about cookies",

			"Votes"				=> "Votes",
			"VoteHover"			=> "Vote for:",

			"ToolSearch"		=> "Search by tools :",
			"ToolHover"			=> "Search for more posts with the same used tool:",

			"Views"				=> "Views",

			"ReadMore"			=> "Read more",
			"ReadMoreHover"		=> "Read more about:",
		),
		"Color" => array(
			"Light"				=> "#ebebeb",
			"Dark"				=> "#141414",
			"Darker"			=> "#0f0f0f",
			"Darker_red"		=> "#161212",
			"Darker_green_dark"	=> "#101410",
			"Darker_green"		=> "#121612",
			"Darker_blue"		=> "#121416",
			"Black"				=> "#000000",
			"Border"			=> "#2d2d2d",
			"Quote"				=> "#0d0d0d",
			"Link"				=> "#b35959",
			"Link_visited"		=> "#5c5c5c",
			"Link_completed"	=> "#969696",
		),
		"Image" => array(
			"Profile" => array(
				"Size"			=> 100,
				"Image"			=> "$folder_profile/profile.png",
			),
			"Spacer"			=> "$folder_icon/spacer.gif",
			"Validate" => array(
				"Success"		=> "$folder_icon/validate_success.png",
				"Error"			=> "$folder_icon/validate_error.png",
				"Close"			=> "$folder_icon/validate_close.png", // unused
			),
			"Rating" => array(
				"$folder_icon/rate_05.gif",
				"$folder_icon/rate_04.gif",
				"$folder_icon/rate_03.gif",
				"$folder_icon/rate_02.gif",
				"$folder_icon/rate_01.gif",
			),
			"Difficulty" => array(
				"$folder_icon/rate_01.gif",
				"$folder_icon/rate_02.gif",
				"$folder_icon/rate_03.gif",
				"$folder_icon/rate_04.gif",
				"$folder_icon/rate_05.gif",
			),
		),
		"Limit" => array(
			"Title"				=> 50,
			"Articles"			=> 3,
			"Text"				=> 300,
			"Other"				=> 1,
		),
	),


	// PANELS
	"Panel" => array(

		"Webmaster" => array(
			"Enabled"			=> TRUE,
			"Show"				=> FALSE,
			"Locale" => array(
				"Title"			=> "About the Webmaster",
				"TitleHover"	=> "Click to show/hide webmaster panel",
			),
			"Content"			=> "Hello there and welcome to my new website! I go by the alias \"[color=orange]Saucy[/color]\" (I guess you've figured that out already..).

				Ever since I played [color=orange]The Legend of Zelda[/color] on [color=orange]NES[/color], I've been hooked on games, computers and technology.

				But my ultimate passion is the [color=orange]universe[/color]! Stars, nebulae and galaxies are the prettiest and coolest things I have ever seen. Just the bare thought of how things work or behaves makes me want become an [color=orange]astronomer[/color]; however I'm not to bright when it comes to calculate things.. However it's still my ultimate dream!

				I am [color=orange]".birthday("1988-03-31")."[/color] years old and I live in a small town in the southern part of [color=orange]Sweden[/color]. I spend many hours in front of my computer, doing the things I like and having fun whilst doing so.

				Some information about me and who I am, basically me in a nutshell:
				.: [color=orange]Brony[/color]
				.: [color=orange]A nerdy PC gamer[/color]
				.: [color=orange]Huge passion for games[/color]
				.: [color=orange]Organized[/color] & [color=orange]a quick learner[/color]
				.: [color=orange]Open minded[/color] & [color=orange]got a sense of humor[/color]
				.: [color=orange]Shy[/color], [color=orange]quiet[/color] & [color=orange]calm[/color]
				.: [color=orange]Perfectionist[/color]
				.: [color=orange]Always listen to music[/color]

				During the years I've spent at my computer (also been building my own computers), I've picked up some things when it comes to computers and web design:
				.: [color=orange]XHTML[/color], [color=orange]CSS3[/color]
				.: [color=orange]PHP[/color], [color=orange]MySQL[/color]
				.: [color=orange]JS[/color], [color=orange]jQuery[/color], [color=orange]AJAX[/color], [color=orange]JSON (Learning)[/color]
				.: [color=orange]Adobe Photoshop & Illustrator[/color]
				.: [color=orange]Windows OS (Win 98 - Win 7)[/color]

				[color=orange]FYI[/color]: English isn't my main language, so excuse me for any spelling/grammar errors (if you'd want, you can always email me about your findings).",
		),

		"List" => array(
			"Enabled"			=> TRUE,
			"Show"				=> TRUE,
			"Folder"			=> "$folder_image/list", // may not be needed
			"Locale" => array(
				"Title"			=> "List : ",
				"TitleHover"	=> "Click to show/hide the list panel",
				"Text"			=> "List archive",
				"TextHover"		=> "View the list archive",
			),
			"Limit" => array(
				"Articles"		=> 1,
				"Posts"			=> 3,
				"Text"			=> 35,
			),
		),

		"Poll" => array(
			"Enabled"			=> TRUE,
			"Show"				=> TRUE,
			"Folder"			=> "$folder_image/poll", // may not be needed
			"Locale" => array(
				"Title"			=> "Poll : ",
				"TitleHover"	=> "Click to show/hide the poll panel",
				"Text"			=> "Poll archive",
				"TextHover" 	=> "View the poll archive",
			),
			"Limit" => array(
				"Posts"			=> 5,
				"Text"			=> 35,
			),
		),



		"Twitter" => array(
			"Enabled"			=> TRUE,
			"Show"				=> FALSE,
			"Locale" => array(
				"Text"			=> "The latest tweet from ",
				"TextHover" 	=> "Click to show/hide the latest Twitter message",
			),
			"Content"			=> "Short message here which most likely should be linked with my twitter/facebook maybe? Who knows? Some dummy text in here please! ...Bumblebee tuna! åäö ' ¯\_(ツ)_/¯",
		),

		"Content" => array(
			"Enabled"			=> TRUE, // may not be needed
			"Show"				=> TRUE,
		),
/*
"Content"=> array(
	"Show"					=> TRUE,
	"Blog"=> array(
		"Nav"=> array(
			TRUE,
			"URL"=> "blog.php",
		),
		"Latest"=> TRUE,
		"Archive"=> TRUE,




		"Locale"=> array(
			"Hover"=> "Click to show/hide the banner panel",
			"HoverText" => "Click to view the results",
			"Title"=> "Banner : Friends",
			"Text"=> "View results",
		),
		"Limit"=> array(
			"Posts"=> 10,
			"Info"=> 300,
		),
		"Size"=> array(
			"Width"=> 88,
			"Height"=> 31,
		),
		"Content"=> array(
			"headz"=> "http://twitter.com/Zaucy/",
			"headz2"=> "http://saucy.se/",
			"headz3"=> "Follow me on:",
		),
	),
),
*/

		"Category" => array(
			"Enabled"			=> TRUE,
			"Show"				=> FALSE,
			"Locale" => array(
				"Singluar"		=> "Category",
				"Plural"		=> "Categories",
				"Title"			=> "Categories : Search",
				"TitleHover"	=> "Click to show/hide the categories panel",
				"Text"			=> "Searched by category :", // used in search_category.php
				"TextHover"		=> "Search for blog, reviews, tutorials, portfolio, downloads,<br /> lists and polls containing the category name: ",
			),
			"Limit" => array(
				"Posts"			=> 10,
				"Text"			=> 55, // used in archive_*.php ###################################### PROBLEM MED LÄNGDEN
			),
		),

		"Tool" => array(
			"Enabled"			=> TRUE,
			"Show"				=> FALSE,
			"Locale" => array(
				"Singluar"		=> "Tool",
				"Plural"		=> "Tools",
				"Title"			=> "Tools : Search",
				"TitleHover"	=> "Click to show/hide the tools panel",
				"Text"			=> "Searched by tool :", // used in search_category.php
				"TextHover"		=> "Search for blog, reviews, tutorials, portfolio, downloads,<br /> lists and polls containing the tool name: ",
			),
			"Limit" => array(
				"Posts"			=> 10,
				"Text"			=> 55, // used in archive_*.php ###################################### PROBLEM MED LÄNGDEN
			),
		),

		"Latest" => array(
			"Enabled"			=> TRUE,
			"Show"				=> TRUE,
			"Locale" => array(
				"Singluar"		=> "",
				"Plural"		=> "",
				"Title"			=> "Latest : Posts",
				"TitleHover"	=> "Click to show/hide the latest posts panel",
				"Text"			=> "",
				"TextHover"		=> "Click to read more about: ",
			),
			"Limit"=> array(
				"Posts"			=> 10,
				"Text"			=> 25,
			),
		),

		"Archive" => array(
			"Enabled"			=> TRUE,
			"Show"				=> FALSE,
			"Width"				=> 64,
			"Locale" => array(
				"Singluar"		=> "Archive",
				"Plural"		=> "Archives",
				"Title"			=> "Archives",
				"TitleHover"	=> "Click to show/hide the archives panel",
				"Text"			=> "Archive : ",
				"TextHover"		=> "Click to view the archives of: ",
			),
			"Limit" => array(
				"Posts"			=> 7, // was 13
				"Text"			=> 55, // used in archive_*.php ###################################### PROBLEM MED LÄNGDEN
			),
		),

		"Banner" => array(
			"Enabled"			=> TRUE,
			"Show"				=> TRUE,
			"Folder"			=> "$folder_image/banner",
			"Locale" => array(
				"Title"			=> "Banner : Friends",
				"TitleHover"	=> "Click to show/hide the banner panel",
			),
			"Limit" => array(
				"Posts"			=> 10,
			),
			"Size" => array(
				"Width"			=> 88,
				"Height"		=> 31,
			),
			"Content" => array(
				"Title #1" => array(
					"Enabled"				=> TRUE,
					"URL"					=> "http://saucy.se/",
					"Image"					=> "headz.gif"),
				"Title #2" => array(
					"Enabled"				=> TRUE,
					"URL"					=> "http://saucy.se/",
					"Image"					=> "headz2.gif"),
			),
		),

		"About" => array( // ÄNDRAS TILL CONTENT ARRAY NÄR JAG GJORT DEN
			"Locale" => array(
				"Title"			=> "About me",
				"TitleHover"	=> "Click to show/hide about me",
			),
			"Content"			=> "Some random text about some random things that should be in this text. Wicked fun to come up absolutely nothing in a text! How would the world look if there were no random people writing about random stuff?

				I could give it a try to be a random person who likes to create websites and write random stuff, just like this.

				I would like to try some quotations and maybe a source URL in here? I have to use the force for this one!",
		),

		"Cookies" => array( // ÄNDRAS TILL CONTENT ARRAY NÄR JAG GJORT DEN
			"Locale" => array(
				"Title"			=> "Information about cookies",
				"TitleHover"	=> "Click to show/hide information about cookies",
			),
			"Content"			=> "More information about cookies on this site  - Will be updated.

				Don't worry - I will only log IP when:
				Posting comments (to ban/prevent spam).
				Voting on the poll and the rating system (from abusing the 'system').",
		),

		"Statistics" => array( // ÄNDRAS TILL CONTENT ARRAY NÄR JAG GJORT DEN
			"Locale" => array(
				"Title"			=> "Statistics",
				"TitleHover"	=> "Click to show/hide statistics",
			),
			"Content"			=> "Will be updated",
		),

		"Contact" => array( // ÄNDRAS TILL CONTENT ARRAY NÄR JAG GJORT DEN
			"Locale" => array(
				"Title"			=> "Send me an email! (or click the mail icon at the top left)",
				"TitleHover"	=> "Click to show/hide contact information",
			),
			"Content"			=> "", // not needed
		),

		"Information" => array( // ÄNDRAS TILL CONTENT ARRAY NÄR JAG GJORT DEN
			"Locale" => array(
				"Title"			=> "Information",
				"TitleHover"	=> "Click to show/hide information",
			),
			"Content"			=> "Will be updated",
		),

		"Askmeanything" => array( // ÄNDRAS TILL CONTENT ARRAY NÄR JAG GJORT DEN
			"Locale" => array(
				"Title"			=> "Ask me anything",
				"TitleHover"	=> "Click to show/hide Ask me anything",
			),
			"Content"			=> "Will be updated",
		),

		"Comments" => array(
			"Enabled"			=> TRUE, // disables all comments on cross every article
			"Show"				=> TRUE,
		),

		"AddComment" => array(
			"Enabled"			=> FALSE, // disables all add comment on cross every article
			"Show"				=> TRUE,
		),

	),


	// INDIVIDUAL
	"Individual" => array(

		"Index" => array(
			"Locale"=> array(
				"Title"=> "Site is under construction, it will be back soon!
					- Saucy [i][18/02-2011][/i]",
			),
			"Content"=> "(I promise it will be fully functional this time!)
				Here's the [urlcustom=old/,_self,,tooltip,old website]old website[/url] - Here's a preview of the [urlcustom=new/,_self,,tooltip,new website]new website[/url]

				[urlcustom=mumble.php,_self,,tooltip,Mumble guide for HeadZ]Mumble guide for HeadZ[/url] - [urlcustom=text_capitalizer.php,_self,,tooltip,Text Capitalizer]Text Capitalizer[/url] - [urlcustom=word_counter.php,_self,,tooltip,Word Counter]Word Counter[/url] - [urlcustom=colors.php,_self,,tooltip,WoW Chat Colors]WoW Chat Colors[/url]
				[urlcustom=ip.php,_self,,tooltip,Check your IP]Check your IP[/url] - [urlcustom=day9_newbielist.php,_self,,tooltip,Day9 Newbie Tuesday Video List]Day[9] Newbie Tuesday Video List[/url]",
		),

		"IP" => array(
			"Locale"=> array(
				"Title"=> "Check your external IP",
			),
			"Content"=> "[b]Your IP is:[/b]",
		),

		"LoL" => array(
			"Locale"=> array(
				"Title"=> "League of Legends Runes",
			),
		),

		"Colors" => array(
			"Locale"=> array(
				"Title"=> "WoW chat color codes",
				"Text"=> "WOW CHAT NAME - [url=http://www.wowinterface.com/downloads/info18732-WoWChatChannelColorCodes.html]SOURCE[/url]",
				"Colors"=> "[color=red]RED[/color],[color=green]GREEN[/color],[color=blue]BLUE[/color]",
			),
			"Content"=> array(
				"SYSTEM"							=> "255,255,0",
				"SAY"								=> "255,255,255",
				"PARTY"								=> "170,170,255",
				"RAID"								=> "255,127,0",
				"GUILD"								=> "64,255,64",
				"OFFICER"							=> "64,192,64",
				"YELL"								=> "255,64,64",
				"WISPER"							=> "255,128,255",
				"WHISPER_FOREIGN"					=> "255,128,255",
				"WHISPER_INFORM"					=> "255,128,255",
				"EMOTE"								=> "255,128,64",
				"TEXT_EMOTE"						=> "255,128,64",
				"MONSTER_SAY"						=> "255,255,159",
				"MONSTER_PARTY"						=> "170,170,255",
				"MONSTER_YELL"						=> "255,64,64",
				"MONSTER_WHISPER"					=> "255,181,235",
				"MONSTER_EMOTE"						=> "255,128,64",
				"CHANNEL"							=> "255,192,192",
				"CHANNEL_JOIN"						=> "192,128,128",
				"CHANNEL_LEAVE"						=> "192,128,128",
				"CHANNEL_LIST"						=> "192,128,128",
				"CHANNEL_NOTICE"					=> "192,192,192",
				"CHANNEL_NOTICE_USER"				=> "192,192,192",
				"AFK"								=> "255,128,255",
				"DND"								=> "255,128,255",
				"IGNORED"							=> "255,0,0",
				"SKILL"								=> "85,85,255",
				"LOOT"								=> "0,170,0",
				"MONEY"								=> "255,255,0",
				"OPENING"							=> "128,128,255",
				"TRADESKILLS"						=> "255,255,255",
				"PET_INFO"							=> "128,128,255",
				"COMBAT_MISC_INFO"					=> "128,128,255",
				"COMBAT_XP_GAIN"					=> "111,111,255",
				"COMBAT_HONOR_GAIN"					=> "224,202,10",
				"COMBAT_FACTION_CHANGE"				=> "128,128,255",
				"BG_SYSTEM_NEUTRAL"					=> "255,120,10",
				"BG_SYSTEM_ALLIANCE"				=> "0,174,239",
				"BG_SYSTEM_HORDE"					=> "255,0,0",
				"RAID_LEADER"						=> "255,72,9",
				"RAID_WARNING"						=> "255,72,0",
				"RAID_BOSS_EMOTE"					=> "255,221,0",
				"RAID_BOSS_WHISPER"					=> "255,221,0",
				"FILTERED"							=> "255,0,0",
				"BATTLEGROUND"						=> "255,127,0",
				"BATTLEGROUND_LEADER"				=> "255,219,183",
				"RESTRICTED"						=> "255,0,0",
				"BATTLENET"							=> "255,255,255",
				"ACHIEVEMENT"						=> "255,255,0",
				"GUILD_ACHIEVEMENT"					=> "64,255,64",
				"ARENA_POINTS"						=> "255,255,255",
				"PARTY_LEADER"						=> "118,200,255",
				"TARGETICONS"						=> "255,255,0",
				"BN_WHISPER"						=> "0,255,246",
				"BN_WHISPER_INFORM"					=> "0,255,246",
				"BN_CONVERSATION"					=> "0,177,240",
				"BN_CONVERSATION_NOTICE"			=> "0,177,240",
				"BN_CONVERSATION_LIST"				=> "0,177,240",
				"BN_INLINE_TOAST_ALERT"				=> "130,197,255",
				"BN_INLINE_TOAST_BROADCAST"			=> "130,197,255",
				"BN_INLINE_TOAST_BROADCAST_INFO"	=> "130,197,255",
				"BN_INLINE_TOAST_CONVERSATION"		=> "130,197,255",
				"COMBAT_GUILD_XP_GAIN"				=> "111,111,255",
				"CHANNEL1"							=> "255,192,192",
				"CHANNEL2"							=> "255,192,192",
				"CHANNEL3"							=> "255,192,192",
				"CHANNEL4"							=> "255,192,192",
				"CHANNEL5"							=> "255,192,192",
				"CHANNEL6"							=> "255,192,192",
				"CHANNEL7"							=> "255,192,192",
				"CHANNEL8"							=> "255,192,192",
				"CHANNEL9"							=> "255,192,192",
				"CHANNEL10"							=> "255,192,192",
			),
		),

		"Text_capitalizer" => array(
			"Locale"=> array(
				"Title"=> "UpperCase Converter",
			),
			"Content"=> "Text will show up here:",
		),

		"Word_counter" => array(
			"Locale"=> array(
				"Title"=> "Word counter",
			),
			"Content"=> "Number of words:",
			"Extra"=> "Text inputted:",
		),

		"Mumble" => array(
			"Locale"=> array(
				"Title"=> "Mumble guide för guilden HeadZ",
				"Text"=> ".: [u][b]Mumble guide för WoW guildet HeadZ på Neptulon EU[/b][/u] :.",
			),
			"Content"=> array(
				"Gå in på: [url=http://sourceforge.net/projects/mumble/files/]Mumble[/url] och ladda ner den senaste versionen (just nu: 1.2.2).",

				"Öppna Mumble filen och installera den var du vill, du kan välja installations typen [color=orange]Client only[/color] i listan.

				Bild:
				[img]image/program/mumble/mumble_1.png[/img]",

				"När du väl installerat [url=http://sourceforge.net/projects/mumble/files/]Mumble[/url], så starta programmet och en ny ruta kommer upp.

				Bild:
				[img]image/program/mumble/mumble_2.png[/img]",

				"Tryck på [color=orange]Add New...[/color]-knappen nere till höger och skriv in dessa sakerna i rutorna:

				[color=orange]Raspen's Mumble Server[/color]
				[color=orange]mumble.raspen.se[/color]
				[color=orange]64738[/color]
				<[color=orange]Skriv ditt ingame nick[/color]>

				Bild:
				[img]image/program/mumble/mumble_3.png[/img]",

				"Så här ska det se ut. Välj [color=orange]Raspen's Mumble Server[/color] och tryck på [color=orange]Connect[/color].

				Bild:
				[img]image/program/mumble/mumble_4.png[/img]",

				"När du väl är inne på servern så tryck på [color=orange]Server[/color] uppe till vänster, sedan välj [color=orange]Access Tokens[/color].

				Bild:
				[img]image/program/mumble/mumble_5.png[/img]",

				"Du får upp en ny ruta och i den så ska du skriva in [color=orange]hejhejhej[/color]. Tryck sedan [color=orange]OK[/color] för att verkställa.

				Bild:
				[img]image/program/mumble/mumble_6.png[/img]",

				"Sedan går du in på [color=orange]Configure[/color] sedan [color=orange]Audio Wizard[/color] i menylistan på [url=http://sourceforge.net/projects/mumble/files/]Mumble[/url]. Du kan ändra olika inställningar där men sedan kommer du fram till en ruta [color=orange]Voice Activity Detection[/color] och där väljer du [color=orange]Push to Talk[/color] och väljer en knapp du använder för att kunna prata.

				Bild:
				[img]image/program/mumble/mumble_7.png[/img]",

				"Tryck på [color=orange]OK[/color] och allt ska vara klart!

				Har du problem/frågor så ställ dom till en officer eller veteran i guilden HeadZ!",
			),
		),

		"Day9" => array(
			"Locale"=> array(
				"Title"=> "Starcraft 2 - Day[9] Newbie Tuesday video list",
				"Text"=> ".: [u][b]Day[9] Newbie Tuesday video list[/b][/u] :.",
				"Information"=> "Copyright [url=http://day9.tv/]Day[9][/url] - List created by [url=http://saucy.se]Saucy[/url] (Problems/tips? E-mail me at: saucy [at] saucy [dot] se)",
				"Updates"=> "Page Updates",
				"Featured"=> "Saucy got featured, yay! (Starts @ 38:05)",
				"Episode"=> "Click to hide/show episode:",
			),
			"Episode" => array(
				"Featured"=> "rcbTdX2x-YU", // episode where Saucy got featured
				"Empty"=> 0,
				"Key"=> 0,
			),
			"Player" => array(
				"sWidth"				=> 360,
				"sHeight"				=> 202,
				"Width"					=> 560,
				"Height"				=> 315,
			),
			"Content"=> array(
				array(
					"Title" => "Starcraft 2 Day[9] Daily #184 - Newbie Tuesday: #1",
					"URL"	=> "epMo2fLDU1Q"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #189 - Newbie Tuesday: Losing to Early Pressure",
					"URL"	=> "8_CTnVpd2CA"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #194 - Newbie Tuesday: Drone Timing",
					"URL"	=> "-jxdX3514t0"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #198 - Newbie Tuesday: Banshee Harass",
					"URL"	=> "6Pm4GIQ5tqc"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #201 - Newbie Tuesday: Using Drops",
					"URL"	=> "LJ-IYrfWxH4"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #205 - Newbie Tuesday: Dealing with Colossi",
					"URL"	=> "HhTFFM7LNV8"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #210 - Newbie Tuesday: 2v2 Strategy",
					"URL"	=> "3vwmlpo5deQ"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #224 - Newbie Tuesday: Fighting an Early Expand",
					"URL"	=> "1K1WRSMFiXM"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #228 - Newbie Tuesday: Banelingz!",
					"URL"	=> "SyYFFTyJKPs"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #242 - Newbie Tuesday: Simple ZvZ Build",
					"URL"	=> "V4-aOzyXhXo"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #247 - Newbie Tuesday: Jinro's TvP Mech",
					"URL"	=> "X9QCROuJ64Y"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #252 - Secrets of Hotkeys, APM and Mouse Movement",
					"URL"	=> "RUohpQKVf_A"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #257 - Newbie Tuesday: Refining Mechanics",
					"URL"	=> "KKPDKtK4IMo"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #261 - Newbie Tuesday: Mechanics #2",
					"URL"	=> "F6nG0AaBl1A"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #269 - Newbie Tuesday: How to get into SC2! ",
					"URL"	=> "Hz_B9H8taG0"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #274 - Newbie Tuesday: Using Reapers",
					"URL"	=> "U9h1yvMXFKw"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #278 - Newbie Tuesday: Infestors!",
					"URL"	=> "XfC1YOH9tLM"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #285 - Newbie Tuesday: Stealing a Build",
					"URL"	=> "6ks4JVQarLk"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #289 - Newbie Tuesday: Refining a Stolen Build",
					"URL"	=> "n2tSq6E3KPs"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #294 - Newbie Tuesday: Zergless 2v2 Teams",
					"URL"	=> "vMbC7Gv5ihk"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #298 - Newbie Tuesday: Using Ghosts (audio out of sync)",
					"URL"	=> "r10PoGdoGNM"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #309 - Newbie Tuesday: The Right and Wrong Way to Learn (audio out of sync)",
					"URL"	=> "3FxGmBpoDX4"
				),
				array(
					"Title" => "Starcraft 2 Day[9] Daily #312 - Newbie Tuesday: How to Learn and Improve",
					"URL"	=> "i3WCoU_li_c"
				),
			),
			"Updates" => array(
				//"[title=Added: ]+-!?: .[/title]",
				"[title=Added: 17/01-2012]? Planned: Cookie to save the different hidden videos, need to learn how cookies work.[/title]",
				"[title=Added: 17/01-2012]! Changed: Reversed the list; now displays newest to oldest. Cleaned up the code.[/title]",
				"[title=Added: 27/06-2011]+ Added: #312 - How to Learn and Improve.[/title]",
				"[title=Added: 14/06-2011]+ Added: #311 - Funday Monday: No Keyboard, No Problem - [u]I, Saucy, got featured, yay![/u][/title]",
				"[title=Added: 10/06-2011]+ Added: #309 - The Right and Wrong Way to Learn.[/title]",
				"[title=Added: 02/06-2011]+ Added: #298 - Using Ghosts.[/title]",
				"[title=Added: 23/05-2011][s]? Planned - Chat: post date: X time ago[/s] - [u]Chat scrapped, to much work and who needs a chat HERE honestly?[/u][/title]",
				"[title=Added: 13/05-2011][s]? Planned - Chat: Captcha, pagination, ip banning, delay before you can post again.[/s][/title]",
				"[title=Added: 13/05-2011][s]? Planned - Chat, Page statistics.[/s][/title]",
				"[title=Added: 13/05-2011]+ Added: Page updates.[/title]",
				"[title=Added: 12/05-2011]+ Added: #252 - Secrets of Hotkeys, APM and Mouse Movement.[/title]",
				"[title=Added: 04/05-2011 - 11/05-2011]+ Added: Lots of episodes and styles to view the episodes.[/title]",
				"[title=Added: 04/05-2011]! Changed: Page created.[/title]",
			),
		),
	),
);

$copyright = array(
	"Enabled"					=> TRUE,
	"Content"					=> "COPYRIGHT &copy; ".$saucy["Default"]["CopyYear"] . (($saucy["Default"]["CopyYear"] != $saucy["Default"]["CurYear"]) ? '-' . $saucy["Default"]["CurYear"] : '')." : ALL RIGHTS RESERVED
		[urlcustom=,_self,information.php?page=cookies,link_click tooltip,".str_replace("'", "&#39;", $saucy["Default"]["Locale"]["CopyRight"])."]INFORMATION ABOUT COOKIES[/url] : [url=http://saucy.se/]SAUCY[/url] v".$saucy["Default"]["Version"]."
		[color=orange]HTML5[/color] : [color=orange]CSS3[/color] : [color=orange]PHP[/color] : [color=orange]MySQL[/color] : [color=orange]jQuery[/color]",
);
$mumble_amount					= count($saucy["Individual"]["Mumble"]["Content"]);
$saucy["Individual"]["Day9"]["Content"] = array_reverse($saucy["Individual"]["Day9"]["Content"]); // Newest first, oldest last
$day9_amount					= count($saucy["Individual"]["Day9"]["Content"]);
$day9_pageUpdates_amount		= count($saucy["Individual"]["Day9"]["Updates"]);
$day9_episodes					= "There are currently $day9_amount episodes I know of; where ".($day9_amount - $saucy["Individual"]["Day9"]["Episode"]["Empty"])." of them have been uploaded to Youtube.";

foreach ($saucy["Individual"]["Day9"]["Content"] as $day9_key => $day9_val) {

	if (is_array($day9_val)) {

		//loop through each values inside
		foreach ($day9_val as $day9_key1 => $day9_val1) {

			if (empty($day9_val1)) {

				$saucy["Individual"]["Day9"]["Episode"]["Empty"]++; //empty value-- increment the empty value count

			}

			$saucy["Individual"]["Day9"]["Episode"]["Key"]++; //increment the key count

		}

	} else {

		$saucy["Individual"]["Day9"]["Episode"]["Key"]++;

		if (empty($day9_val)) {

			$saucy["Individual"]["Day9"]["Episode"]["Empty"]++;

		}

	}

}





/*
	"Blog",
	"Reviews",
	"Tutorials",
	"Portfolio",
	"Poll",
	"List",
	"Code",
	"Config",
	"Map",
	"Model",
	"Video",
	"Wallpaper",
	"Question & Answer",
/*
*/

/*
$box = array(
	"Width" => 10,
	"Height" => 20,
);
$v = array(
	"Total" => ($box["Width"]+$box["Height"]),
);

echo $v["Total"];

$test = array(
	"Blog"=> array(
		"Dev"=> TRUE,
		"Nav"=> array(
			TRUE,
			"URL"=> "blog.php",
		),
	),
);

/*
foreach($test as $k => $v) {
	echo $k;
}
/*
if(!empty($test["Blog"]["Nav"])) {
	echo count($test);
}
/*
*/

/*
$banner = array(
	"Width"=> 88,
	"Height"=> 31,
	"Content"=> array(
		"headz"=> "http://twitter.com/Zaucy/",
		"headz2"=> "http://saucy.se/",
		"headz3"=> "http://saucy.se/",
	),
);
*/


// SOCIAL + SITE INFO + SEARCH
// MAX 13
$social_icon = array(
    "E-mail" => array(
		"Enabled"				=> TRUE,
		"Desc"					=> "E-mail me!",
		"URL"					=> "mailto:saucy@saucy.se"),
	"Steam" => array(
		"Enabled"				=> TRUE,
		"Desc"					=> "Gaming profile:",
		"URL"					=> "http://steamcommunity.com/id/saucyboy/"),
	"Facebook" => array(
		"Enabled"				=> FALSE,
		"Desc"					=> "Obligatory social link:",
		"URL"					=> "http://www.facebook.com/saucyboy/"),
	"Twitter" => array(
		"Enabled"				=> TRUE,
		"Desc"					=> "Shit no one care to reads:",
		"URL"					=> "http://twitter.com/Zaucy/"),
	"BattleNet" => array(
		"Enabled"				=> TRUE,
		"Desc"					=> "SC2 profile:",
		"URL"					=> "http://eu.battle.net/sc2/en/profile/865062/1/Saucy/"),
	"Battlefield" => array(
		"Enabled"				=> FALSE,
		"Desc"					=> "Fight me:",
		"URL"					=> "http://battlelog.battlefield.com/bf3/user/S4ucy/"),
	"Quake III Live" => array(
		"Enabled"				=> FALSE,
		"Desc"					=> "pwn me:",
		"URL"					=> "http://www.quakelive.com/#!profile/summary/S4ucy/"),
	"Minecraft" => array(
		"Enabled"				=> FALSE,
		"Desc"					=> "MC profile:",
		"URL"					=> "http://www.minecraftforum.net/user/221168-rarebear/"),
	"Youtube" => array(
		"Enabled"				=> TRUE,
		"Desc"					=> "Videos:",
		"URL"					=> "http://www.youtube.com/user/zaucyboy/"),
	"DeviantArt" => array(
		"Enabled"				=> TRUE,
		"Desc"					=> "Art:",
		"URL"					=> "http://ninjasaus.deviantart.com/"),
	"SoundCloud" => array(
		"Enabled"				=> FALSE,
		"Desc"					=> "Audios:",
		"URL"					=> "http://soundcloud.com/S4ucy/"),
	"LastFM" => array(
		"Enabled"				=> TRUE,
		"Desc"					=> "Music taste:",
		"URL"					=> "http://www.last.fm/user/skaUn1nG/"),
	"Pastebin" => array(
		"Enabled"				=> FALSE,
		"Desc"					=> "Copy & pasta codes:",
		"URL"					=> "http://pastebin.com/u/Saucy/"),
	"StackOverflow" => array(
		"Enabled"				=> FALSE,
		"Desc"					=> "My StackOverflow profile:",
		"URL"					=> "http://stackoverflow.com/users/1020364/saucy/"),
	"Skype" => array(
		"Enabled"				=> TRUE,
		"Desc"					=> "Just another chat tool:",
		"URL"					=> "skype:sgt.saucy?userinfo"),
	"Torhead" => array(
		"Enabled"				=> FALSE,
		"Desc"					=> "Profile & characters:",
		"URL"					=> "http://www.torhead.com/user/Saucy"),
	"Wowhead" => array(
		"Enabled"				=> FALSE,
		"Desc"					=> "Profile & characters:",
		"URL"					=> "http://www.wowhead.com/user=zAce"),
	"Spotify" => array(
		"Enabled"				=> FALSE,
		"Desc"					=> "",
		"URL"					=> "http://open.spotify.com/user/define%28s%C3%A5s%29%3b"),
);
$social_info = array(
	"Statistics" => array(
		"Enabled"				=> TRUE,
		"Desc"					=> "Website statistics",
		"URL"					=> "statistics"),
	"Sitemap" => array(
		"Enabled"				=> FALSE,
		"Desc"					=> "Website sitemap",
		"URL"					=> "link"), // use link to set the default viewing page
	//"Statistics"	=> "statistics.php",
	//"archive_Blog" => "archive_blog.php",
	//"archive_Poll" => "archive_poll.php",
	//"archive_List" => "archive_list.php",
);
$social_index					= 0;
$social							= 38;
$social_border					= 2;
$social_margin					= 12;
$social_amount 					= 0;
foreach($social_icon as $social_icon_key => $social_icon_value) {
	if(!empty($social_icon_value["Enabled"])) {
		$social_amount++;
	}
}
$social_width					= $social+$social_border+$social_margin; // 50 ~ 38+2+10=50 (width+border+margin)
$social_height					= $social+$social_border; // 40
$social_size					= $social_amount*$social_width; // number of social icons * social width(50)
$social_info_amount 			= 0;
foreach($social_info as $social_info_key => $social_info_value) {
	if(!empty($social_info_value["Enabled"])) {
		$social_info_amount++;
	}
}
$social_info_size				= $social_info_amount*$social_width; // number of social_info icons * social width(50)
$social_space					= ($saucy["Default"]["Width"]-($social_info_size+$saucy["Default"]["WidthPanel"])-$social_size+$social_margin);	// 290 ~ (980-(50+250)-400+10)
$social_hover					= "image/icon/social_hover.png";

// BOX
$box_pad						= 6;
$box_border						= 4;
$box_space						= 2;
$box_margin						= 1;
$box_width						= (($box_pad*2) - ($box_space*2))."px"; // 8
$box_height						= (($box_pad*2) - ($box_space*2))."px"; // 8
$box_small_width				= (($saucy["Default"]["WidthContent"]/2) - $box_border); // 226 - ((460/2) - 4)
$box_small_height				= 21;
$box_search_width				= 80;

// ADD COMMENT
$addcomment_margin				= 12;

// DEVELOPMENT
$dev_updated					= "18/07-2012";
$dev_twitter_date				= "2011-02-18 12:22:56";
$dev_texty						= "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."; // REMOVE
$dev_poll						= array(
	"Name of the poll option",
	"¯\_(ツ)_/¯",
	"Name of the poll option",
	"Name of the poll option",
	"åäö",
);
$dev_poll_title					= "[u]Poll[/u] title";
$dev_poll_amount				= count($dev_poll);
$dev_row_poll_comments_amount_index = 5;
$dev_text_width					= 400;
$dev_text = array(
	"Info"						=> "This is a preview of my future website, the design will most likely change. Consider this as a Alpha version of my website.
I am working on how to make the PHP/CSS code as good and compact as possible, and as easy for me to update all the things I have been working on.
Last updated: [color=orange] $dev_updated [/color]",

	"Done"						=> "search.php nästan klar
index: webmaster, list, categories, tools, banner, twitter?
index: latest (cut-off length)",

	"Todo"						=> "[u]LEGEND:[/u]
! = Important
@ = Dev/admin sida
+ = Lägg till
~ = Ta bort
^ = Fixa/Ändra
§ = Fundera på
½ = Viktig förändring
£ = Ändrade mig
| = Avklarad/genomstruken

Smilies funkar nu iaf :D

½ blog.php >> sector.php
½ info_blog.php >> article.php
½ search_category, search_tool >> search
½ cookies, about, statistics, contact, askmeanything, link, ip, text_capitalizer, word_counter, personality >> information
½ question & answers >> questions >> askmeanything

½ DB: question >> askmeanything
½ DB: comments >> comment
½ DB: downloads >> download
½ DB: links >> link
½ DB: reviews >> review
½ DB: tutorials >> tutorial

>> [url=http://net.tutsplus.com/tutorials/html-css-techniques/building-a-5-star-rating-system-with-jquery-ajax-and-php/]Building a 5 Star Rating System with jQuery, AJAX and PHP[/url]
>> [url=http://net.tutsplus.com/tutorials/php/build-your-own-captcha-and-contact-form/]Build your own captcha and contact form[/url]
>> [url=http://net.tutsplus.com/tutorials/html-css-techniques/build-a-neat-html5-powered-contact-form/]Build a neat HTML5 powered contact form[/url]
>> [url=http://net.tutsplus.com/tutorials/php/create-a-basic-shoutbox-with-php-and-sql/]Create a Basic Shoutbox with PHP and SQL[/url]
>> [url=http://net.tutsplus.com/tutorials/php/creating-a-web-poll-with-php/]Creating a web poll with PHP[/url]
>> [url=http://net.tutsplus.com/tutorials/php/how-to-create-an-advanced-twitter-widget/]How to create an advanced twitter widget[/url]
>> [url=http://net.tutsplus.com/tutorials/php/organize-your-next-php-project-the-right-way/]Organize your next PHP project the right way[/url]

>> [url=http://www.youtube.com/watch?v=L-FM97j7MyY&feature=BFa&list=PL081AC329706B2953&lf=plpp_play_all]HTML5 Tutorial - 25[/url]
>> [url=http://www.youtube.com/watch?v=mgwiCUpuCxA&feature=autoplay&list=PL46F0A159EC02DF82&lf=plpp_video&playnext=23]Beginner JavaScript Tutorial - 24[/url]
>> [url=http://www.youtube.com/playlist?list=PL32BC9C878BA72085&feature=plcp]MySQL Database Tutorial[/url]
>> [url=http://www.youtube.com/playlist?list=PLFF967D7CA020E636&feature=plcp]Unreal Development Kit UDK Tutorials Playlist[/url]
>> [url=http://www.youtube.com/playlist?list=PL442FA2C127377F07&feature=plcp]PHP Tutorials Playlist[/url]
>> [url=http://www.youtube.com/playlist?list=PLEA1FEF17E1E5C0DA&feature=plcp]Python Programming Tutorials[/url]
>> [url=http://www.youtube.com/playlist?list=PLA331A6709F40B79D&feature=plcp]Java Game Development Tutorials[/url]
>> [url=http://www.youtube.com/playlist?list=PLAE85DE8440AA6B83&feature=plcp]C++ Programming Tutorials Playlist[/url]
>> [url=http://www.youtube.com/playlist?list=PL6B08BAA57B5C7810&feature=plcp]jQuery Tutorials Playlist[/url]

^ Fixa formen på Add comment (article.php) och Contact (contact.php)
^ Flytta på Quote stället, från sid-titlen till annat
+ Lägg till List och Poll i Statistics ikonerna
+@ Varje article ska kunna ha Av/aktivera comments/add comments
^| Ändrade Enabled och Show i Panel-settings
§!| Byta namnet Questions >> Ask me anything? (borde vara lättare att förstå)
! Stäng av Archives panelen när allt är klart
½ Lade till font och line-height i body.php under BODY
^| Alla 10px mellanrum till \$box_pad*2 istället, fixa med storlekarna på WidthPanel & WidthContent
- Ta bort style='color:*' på titlar
+ Gör en lista där högst comments är överst
½ tog bort profile_img från The Webmaster : Me in a nutshell på index.php
½ lade till disabled='disabled' på index.php >> search
^ .: :. på Latest blog till article om man är där istället
+ Skapa checkbox lista med där användaren kan kryssa i vad den vill visa, t.ex. under Archive; alltid visa alla poster under alla år, spara i cookies?
^ search.php \$categories & \$tools ska dela på samma kod, får fixa med arraysen
^@ När jag lagt till t.ex. 5st blog/* inlägg, påminn mig att köra en backup på databasen
§! information.php?=about
§! search_category.php & search_tool.php tillsammans: search.php?
§! cookies.php, about.php, statistics.php, contact.php, question.php, links.php, ip.php, text_capitalizer.php tillsammans: ???
+§ Sedd från index.php, längst ner på blog.php kanske lägga till så att man laddar in flera blog inlägg om man trycker på den? (click here to load more blog entries)
+£ Induviduella ToggleSpeed på alla, alla ska köra default (slow), om \$saucy[\"Default\"][\"ToggleSpeed\"] är annorlunda än slow så ska det ändras till det
!| Chrome: ta bort search ikonen som dyker upp, förstör design layouten
! url (permalinks) mod_rewrite i .htaccess
^| List: fixa räknare så den börjar om från 1 >> den sista i listan, på index.php, blog.php, info_blog.php
^ DB: list >> list_parent, list_content >> list
^ search_tool lägg till så att det finns ett meddelande när det finns inga rows med den tool'en
^§ Under links, kanske kan toggle Science och sånt för att dölja
^| Tool & category till blog.php, info_blog.php, ändra från * till specfika rows
§ Skapa en funktion till 'Add comment'
^ archive_*.php fixa download tillsammans med resten = mindre kod använd
^ Något skumt med subMenu.php, antagligen som a.link_click i Qtip som spökar, fixa genom att ändra class namnet
^ Comment's timeago
+ Gör en 'site-map'
^ Auto thumbnail av portfolio bilderna på portfolio.php, slipper ladda så länge då
+! Sätt alla ip.php, text_capitalizer.php, text_wordcount.php ihopa till en och samma sida. Gäller mlp_songs med
^| line-height så att det ser likadant ut på FF som i Chrome (jämför boxar), 100% ger exakta mått, men vill ha större mellanrum. fixa submenu om jag ska ha kvar det på 150%
^ kanske fixa Comments locale till Comment om där bara finns 1 comment
^ Hover fastnar
½ .linkz (width: 210px; > auto)
^ verkigen fixa latest förkortningen på index, vill ha ... i en jämn linje dvs. texten ska använda hela boxen innan ... visas
^! DB: ändra VARCHAR från 100 till 255
^| göra om alla enskilda variablar till multidementional arrays > \$color_* till \$color[\"Namnet\"]
^| archive; title till höger, tid till vänster
!| ta bort eller ej: href='' under poll, categories, latest, archives behövs för att göra länkarna \"länkar\"
^ poll votes amount & comments & results länkarna
! Alla texter som ska bli hover; *MÅSTE HA BBKOD* funktionen på strängen
^| Namnen på alla mysql connections, t.ex: \"resultat_category_index\" >> \"tbl_category\"
+! Permanent link till t.ex: blog id's etc
^| Hover texten, fixa fade in/out
^| tillämpa hover texten på allt hover
^| locale_hover texten funkar ej
§ Sätt quotes ovanför Social istället för i titlen
^| Lägg till bbkod() framför alla \$locale_hover
+! I category_parent DB; autolägg till comments i MySQL DB när man länkar downloads med en category t.ex: // linked with code #11.
+| Ny SNYGG hover ruta över länkar (CSS & ändra width)
^! Alla * i SQL query till respektive saker som behövs
+ Last edited brevid tiden när man hover över Posted <X tid> ago
^| Flytta ut Add comment från divboxen när man trycker på # Comments, så att Add comment är fritt
^! get.php räknar downloads för filer, SPARA FILEN!
^| Colors variablerna till \$colors_#NAMNET
^| Allt från t.ex: blog_info till info_blog
+! Alla induviduella sidor med samma settings.php/body.css (t.ex: mumble.php, ip.php, text_capitalizer.php, text_wordcount.php)
§ Alla image länkar; image/LULZ > http://saucy.se/image/LULZ (använda \$default_url)
+@ Lista på alla filer som används (enklare att ladda upp), lägg som en list som TODO
^! Så att man kan gå bakåt till sidan där man var, Blog > Blog title SEDAN tillbaka = går tillbaka till annan sida (fixa med jQuery scriptet)
^ System där jag kan se vilka sektioner på sidan (Blog/Reviews/etc) har DB connection/submitRating igång
^ Ett enkelt system där jag kan kryssa i om jag vill visa Info/TODO/Done på sidan
§ Någon info box där jag ser vilka saker jag har igång. T.ex: header(Refresh) eller tagit bort någon Nav ikon: Facebook
§ MySQL koderna till MySQLi kod
+ .htaccess filen, Customize Error Messages
+! .htaccess filen, bara index.php ska vara accessable, blog.php, blog_info.php etc ska vara gömda
§ jQuery kunna stänga rutorna, t.ex. Search : Categories och Latest posts (panelerna)
^ jQuery disclaimer, om variablen d = 0 så ska disclaimer css klassen vara gömd när man är på websidan
^| Från ' till \\\" (quote) i PHP
^| Från \\\" till ' (singlequote) i HTML
+| Lägg till \"Friends\" lista med URL till mina vänners hemsidor
^ Clean up the code
§ List tooltip: lägg till bild brevid anime info text
^ Search (svårt & tidskrävande)
^ Admin sida (svårt & tidskrävande)
^ Antal downloads i downloads, portfolio
^| Category's sista \",\" sak
^| Rating saken, jag kan fortfarande vota även om jag har samma ip
^| \"Antal Views\"
^ Contact me formen
^ Comment formen
^ Replies till comments
+| portfolio tabell
+| tutorials tabell
+| downloads tabell
^| Poll ikonen till \"statistics\" sida
§@ När man lägger in nyhet, ha en funktion som fördröjer när nyheten ska visas? vid ett viss datum och tid. [url=http://www.php.net/manual/en/function.date.php#101470]URL[/URL]

index.php:
+§| Tutorial archive, eller lista alla tutorials under den senaste? (såfall + tutorial ikon längst upp)
^| Search : Categories, om man trycker på t.ex. CSS3 så kommer alla blog, reviews, list, poll, tutorial, portfolio, video, downloadable som har category = CSS3
^ poll, använd AJAX
+§ Twitter/facebook saken
^§| Senaste portfolio
^ Senaste video, HTML5, .ogg för FF, .mp4 för webkit
^§| Senaste tutorial
^§| Senaste downloadable

archive's.php:
^| poll, blog, reviews, list, tutorials

statistics.php
+ site timer since launch & site created
+ visitors
+ getIP();",
);

?>
