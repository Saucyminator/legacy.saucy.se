<?php

// Mysql config
$host     = 'localhost'; // Change this if needed
$database = 'database'; // Change this if needed
$username = 'username'; // Change this if needed
$password = 'password'; // Change this if needed

// Main connection
$conn = new PDO(
  "mysql:host=$host;dbname=$database",
  $username,
  $password,
  [
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::MYSQL_ATTR_FOUND_ROWS   => true,
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
  ]
);

define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']));
