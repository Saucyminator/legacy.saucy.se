<?php

/*	INCLUDED IN:
blog
review
tutorial
portfolio
list
poll
download
link (kanske inte behövs)
askmeanything (kanske inte behövs)
--sitemap  (kanske inte behövs)

.: FILES :.
sector
article
archive
information

.: SHOULD NOT BE IN :.
about
statistics
contact
cookies
*/

if(!isset($parent)) {

	$parent = str_replace(".php", "", strtolower(basename($_SERVER["PHP_SELF"])));
    
}

if($parent == "sector" OR $parent == "article" OR $parent == "archive" OR $parent == "information") {

	if($page == "blog" OR $page == "review" OR $page == "tutorial" OR $page == "portfolio" OR $page == "list" OR $page == "poll" OR $page == "download") {
	
		echo "<div class='box_small_wrapper'>
			<div class='box_small' style='float: left;'>
				<div class='text big'>
					<a id='sector.php?page=$page' class='link_click tooltipMenu' title='Latest ".ucfirst($page)." entries'>";
					
						if($parent == "sector" OR $parent == "article") {
						
							echo ".: Latest ".ucfirst($page)." :.";
							
						} else {
						
							echo "Latest ".ucfirst($page);
							
						}
						
					echo "</a>
				</div>
			</div>
			<div class='box_small' style='width: ".($box_small_width+1)."px; float: right;'>
				<div class='text big'>
					<a id='archive.php?page=$page' class='link_click tooltipMenu' title='".ucfirst($page)." Archive'>";
					
						if($parent == "archive") {
						
							echo ".: ".ucfirst($page)." Archive :.";
							
						} else {
						
							echo ucfirst($page)." Archive";
							
						}
						
					echo "</a>
				</div>
			</div>
		</div>";
		
	} elseif($page == "sitemap") { // tog bort askmeanything och link
	
		$sitemaplist_amount = (count($sitemaplist)/2);
		
		for($sitemaplist_index = 0; $sitemaplist_index < $sitemaplist_amount; $sitemaplist_index++) {
		
			echo "<div class='box_small_wrapper'>
				<div class='box_small' style='float: left;'>
					<div class='text big'>
						<a id='information.php?page=".$sitemaplist[$sitemaplist_index]."' class='link_click tooltipMenu' style='color: ".$saucy["Default"]["Color"]["Light"].";' title='".ucfirst($sitemaplist[$sitemaplist_index])."'>";
						
							if($page == "link") {
							
								echo ".: ".ucfirst($sitemaplist[$sitemaplist_index])."s :.";
								
							} else {
							
								echo ucfirst($sitemaplist[$sitemaplist_index])."s";
								
							}
							
						echo "</a>
					</div>
				</div>
				<div class='box_small' style='width: ".($box_small_width+1)."px; float: right;'>
					<div class='text big'>
						<a id='information.php?page=".$sitemaplist[$sitemaplist_index+$sitemaplist_amount]."' class='link_click tooltipMenu' style='color: ".$saucy["Default"]["Color"]["Light"].";' title='".ucfirst($sitemaplist[$sitemaplist_index+$sitemaplist_amount])."'>";
						
							if($page == "askmeanything") {
							
								echo ".: ".ucfirst($sitemaplist[$sitemaplist_index+$sitemaplist_amount])." :.";
								
							} else {
							
								echo ucfirst($sitemaplist[$sitemaplist_index+$sitemaplist_amount]);
								
							}
							
						echo "</a>
					</div>
				</div>
			</div>";
			
		}
		
	}
	
}

?>